# После клонирования репозитория

## Заменить в settings/common.py

### 1. Настройки sentry, чтобы ошибки падали для текущего проекта

```bash
SENTRY_DSN = 'https://c96b61ede5a34572a3eca4a9c0f8df8b@sentry.directline.company/...'
```
на
```bash
SENTRY_DSN = ''
```
При разворачивании проекта его заменят на необходимый


### 2. Ключи для капчи
Чтобы капча работала для создаваемого домена проекта

Ключи для капчи создаются по адресу https://www.google.com/recaptcha/admin/, настройки безопасности максимальные 

```bash
GOOGLE_RECAPTCHA_PUBLIC_KEY = '6LexNckcAAAAABYa2NyxTUWrVm_HegVgQVBnI2vn'
GOOGLE_RECAPTCHA_SECRET_KEY = '6LexNckcAAAAAKXbWFSoRKzXilU28m6tRvlNkA_w'
```

### 3. Для dev заменить почту админа, получающего сообщения об ошибках

```bash
ADMINS = (
    ('pix', 'x896321475@gmail.com'),
    ('kp', 'kpXXX@ya.ru'),
    ('i.sachev', 'i.sachev@directline.digital'),
    ('dev', '...'), # Здесь своя почта
)
```
# Стили для блоков
Стили/скрипты блоков подключаются автоматически, в pipeline.py необходимо называть из как 'block_..., 'block_..._critical'

Если необходимо добавить еще какой-то блок, то необходимо добавить, например в webpage

```bash
used_blocks.add_block(name='...')
```

Т.е. мы формируем список в used_blocks, который передается в темплейттег, и при формировании html по этому списку дописывается статика.
Аттач-блоки формируют название без подчеркиваний, поэтому блоки нужно называть также, чтобы они сами привязывались (см. attachable_blocks.py, строка 50)


# Тестирование покупки
## Данные PayPal:
Страница для авторизации тестового покупателя: https://sandbox.paypal.com
```bash
Покупатель:
Логин: sb-mdvvk13079298@personal.example.com
Пароль: zq&MAU2'
```
Карты генерируются здесь https://developer.paypal.com/developer/creditCardGenerator

## Данные Stripe:
Список карт для тестирования https://stripe.com/docs/testing
```bash
Карта для успешной покупки: 4242 4242 4242 4242
```


## Данные для проверки TaxJar:
Зарегистрировать свои, триал 30 дней
```bash
Логин: klinov.mikhail@inbox.ru
Пароль: DLP@$$w0rd
```


## Данные для проверки ShipStation
Зарегистрировать свои, если не получается - написать в техподдержку. Поддержка Chrome сомнительна
```bash
Логин: developer.klinov.mikhail@gmail.com
Пароль: DLP@$$w0rd
```
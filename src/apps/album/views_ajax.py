from django.views.generic.base import View
from ajax_views.decorators import ajax_view
from django.template.loader import render_to_string
from django.http import JsonResponse
from paginator.paginator import EmptyPage
from .views import get_paginator
from .models import AlbumConfig


@ajax_view('album.more')
class MoreView(View):
    coupons = None

    def get(self, request):
        config = AlbumConfig.get_solo()

        try:
            items = config.gallery.image_items
            paginator = get_paginator(request, items)

        except EmptyPage:
            return JsonResponse({}, status=400)

        return JsonResponse({
            'success_message': render_to_string('components/parts/gallery/_list.html', {
                'images': paginator.current_page
            }),
            'more_button': render_to_string('album/parts/_more.html', {
                'paginator': paginator
            }),
        }, status=200)

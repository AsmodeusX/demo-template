# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-10-20 09:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import gallery.fields
import gallery.models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gallery', '0001_initial'),
        ('attachable_blocks', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlbumBlock',
            fields=[
                ('attachableblock_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='attachable_blocks.AttachableBlock')),
                ('title', models.CharField(default='Gallery', max_length=128, verbose_name='title')),
            ],
            options={
                'verbose_name': 'Album block',
                'verbose_name_plural': 'Album blocks',
            },
            bases=('attachable_blocks.attachableblock',),
        ),
        migrations.CreateModel(
            name='AlbumConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.TextField(default='', max_length=128, verbose_name='header')),
                ('title', models.CharField(default='', max_length=128, verbose_name='title')),
                ('description', models.TextField(blank=True, max_length=512, verbose_name='description')),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('image', libs.stdimage.fields.StdImageField(aspects=('normal',), default='', min_dimensions=(480, 480), storage=libs.storages.media_storage.MediaStorage('webpage/images'), upload_to='', variations={'admin': {'size': (200, 200)}, 'desktop': {'create_webp': True, 'size': (1400, 260)}, 'mobile': {'create_webp': True, 'size': (375, 260)}, 'tablet1': {'create_webp': True, 'size': (1024, 260)}, 'tablet2': {'create_webp': True, 'size': (768, 260)}, 'wide': {'create_webp': True, 'size': (480, 480)}}, verbose_name='image')),
            ],
            options={
                'verbose_name': 'Settings',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'gallery',
                'verbose_name_plural': 'galleries',
                'abstract': False,
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='GalleryItem',
            fields=[
                ('galleryitembase_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='gallery.GalleryItemBase')),
                ('image', gallery.fields.GalleryImageField(storage=libs.storages.media_storage.MediaStorage(), upload_to=gallery.models.generate_filepath, verbose_name='image')),
                ('image_crop', models.CharField(blank=True, editable=False, max_length=32, verbose_name='stored_crop')),
                ('image_alt', models.CharField(blank=True, max_length=255, verbose_name='alt')),
            ],
            options={
                'verbose_name': 'image item',
                'verbose_name_plural': 'image items',
                'ordering': ('object_id', 'sort_order', 'created'),
                'abstract': False,
                'default_permissions': (),
            },
            bases=('gallery.galleryitembase',),
        ),
        migrations.AddField(
            model_name='albumconfig',
            name='gallery',
            field=gallery.fields.GalleryField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='album.Gallery', verbose_name='gallery'),
        ),
    ]

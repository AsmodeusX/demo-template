from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from attachable_blocks.models import AttachableBlock
from solo.models import SingletonModel
from webpage.models import StdPage, StdDisablePage
from gallery.models import GalleryBase, GalleryImageItem
from gallery.fields import GalleryField


class GalleryItem(GalleryImageItem):
    STORAGE_LOCATION = 'album/img'
    ADMIN_VARIATION = 'mobile'
    SHOW_VARIATION = 'desktop'
    ASPECTS = 'desktop'
    MIN_DIMENSIONS = (350, 250)
    VARIATIONS = {
        'desktop': {
            'size': (280, 240),
            'create_webp': True,
            # 'create_avif': True,
        },
        'tablet': {
            'size': (350, 250),
            'create_webp': True,
            # 'create_avif': True,
        },
        'mobile': {
            'size': (160, 130),
            'create_webp': True,
            # 'create_avif': True,
        }
    }


class Gallery(GalleryBase):
    IMAGE_MODEL = GalleryItem


class AlbumConfig(SingletonModel, StdPage, StdDisablePage):
    gallery = GalleryField(Gallery, verbose_name=_('gallery'), null=True, blank=True)

    class Meta:
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('album:index')

    def __str__(self):
        return self.header


class AlbumBlock(AttachableBlock):
    BLOCK_VIEW = 'album.views.album_block_render'
    title = models.CharField(_('title'), max_length=128, default='Gallery')

    class Meta:
        verbose_name = _('Album block')
        verbose_name_plural = _('Album blocks')

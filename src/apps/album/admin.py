from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from attachable_blocks.admin import AttachableBlockAdmin, AttachedBlocksTabularInline
from .models import AlbumBlock, AlbumConfig
from webpage.admin import SEOStdPageAdmin, StdDisablePageAdmin


class AlbumBlocksBottomInline(AttachedBlocksTabularInline):
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(AlbumConfig)
class AlbumConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'title',
            ),
        }),
    ) + StdDisablePageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'gallery',
            ),
        }),
    )
    inlines = (AlbumBlocksBottomInline, )
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )


@admin.register(AlbumBlock)
class AlbumAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )

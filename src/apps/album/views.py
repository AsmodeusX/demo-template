from django.shortcuts import loader, Http404
from paginator.paginator import Paginator, EmptyPage
from .models import AlbumConfig
from webpage.views import StdPageSingletonView

CNT_IMAGES = 8


class MorePaginator(Paginator):
    template = 'album/parts/_more.html'

def get_paginator(request, items):

    return MorePaginator(
        request,
        object_list=items,
        per_page=CNT_IMAGES,
        allow_empty_first_page=False,
    )


class IndexView(StdPageSingletonView):
    model = AlbumConfig
    template_name = 'album/index.html'

    def get_context_data(self, **kwargs):
        config = AlbumConfig.get_solo()

        try:
            items = config.gallery.image_items
            paginator = get_paginator(self.request, items)

        except EmptyPage:
            raise Http404

        context = super().get_context_data(**kwargs)
        context.update({
            'images': paginator.current_page,
            'paginator': paginator,
        })

        return context


# @cached('block.id')
def album_block_render(context, block, **kwargs):
    config = AlbumConfig.get_solo()
    gallery = config.gallery

    images = None

    if config.gallery:
        images = gallery.image_items[:CNT_IMAGES]

    if not images or not images.exists():
        return ''

    return loader.render_to_string('album/block.html', {
        'block': block,
        'images': images,
        'more': len(gallery.image_items) > CNT_IMAGES
    }, request=context.get('request'))

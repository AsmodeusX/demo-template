from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'album'
    verbose_name = _('Album')

    def ready(self):
        from . import views_ajax

/** @namespace window.ajax_views.album.more */


(function($) {
    var elem = '.page--album';

    $(document).on('click', elem + ' .btn--paginator', function() {
        var $btn = $(this);
        loadItems($btn);

        return false;
    });

    function loadItems($btn) {
        var page = $btn.data('page');

        return $.ajax({
            url: window.ajax_views.album.more,
            type: 'GET',
            data: {
                'page': page
            },
            dataType: 'json',
            success: function(response) {
                var $gallery = $(elem).find('.gallery');
                if (response.success_message)
                    $gallery.find('.grid').append(response.success_message);

                window.gallery_update($gallery);

                if (response.more_button) $btn.replaceWith(response.more_button);
                else $btn.remove();
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    }

})(jQuery);

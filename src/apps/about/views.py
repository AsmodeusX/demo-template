from django.shortcuts import loader
from .models import AboutConfig
from webpage.views import StdPageSingletonView
from libs.cache.cached import cached
from webpage.utils import UsedBlocks


class IndexView(StdPageSingletonView):
    model = AboutConfig
    template_name = 'about/index.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        return context


# @cached('block.id')
def about_block_render(context, block, **kwargs):

    return loader.render_to_string('about/block.html', {
        'block': block,
    }, request=context.get('request'))

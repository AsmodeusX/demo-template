from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from attachable_blocks.admin import AttachedBlocksTabularInline, AttachableBlockAdmin
from .models import AboutConfig, AboutBlock
from webpage.admin import SEOStdImagePageAdmin, TextPageAdmin, StdButtonPageAdmin, StdDisablePageAdmin


class AboutBlocksTopInline(AttachedBlocksTabularInline):
    set_name = 'top'
    verbose_name = 'Top block'
    verbose_name_plural = 'Top blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


class AboutBlocksBottomInline(AttachedBlocksTabularInline):
    verbose_name = 'Bottom block'
    verbose_name_plural = 'Bottom blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(AboutConfig)
class AboutConfigAdmin(SEOStdImagePageAdmin, SingletonModelAdmin):
    fieldsets = SEOStdImagePageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + StdDisablePageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    ) + TextPageAdmin.fieldsets
    inlines = (AboutBlocksTopInline, AboutBlocksBottomInline)
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )


@admin.register(AboutBlock)
class AboutBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'image', 'title', 'note',
            ),
        }),
    )


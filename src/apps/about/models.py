from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from webpage.models import TextPage, StdImagePage, StdButtonPage, StdDisablePage
from attachable_blocks.models import AttachableBlock
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from libs.description import description
from ckeditor.fields import CKEditorUploadField


class AboutConfig(SingletonModel, StdImagePage, StdButtonPage, StdDisablePage, TextPage):
    text = CKEditorUploadField(_('text'), blank=True)

    class Meta:
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('about:index')

    def __str__(self):
        return self.header


class AboutBlock(AttachableBlock):
    MAX_LETTERS = 300
    BLOCK_VIEW = 'about.views.about_block_render'

    image = StdImageField(_('image'),
                          default='',
                          storage=MediaStorage('about/images'),
                          min_dimensions=(480, 480),
                          admin_variation='admin',
                          crop_area=True,
                          aspects=('normal',),
                          variations=dict(
                              wide=dict(
                                  size=(480, 480),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              desktop=dict(
                                  size=(1400, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              tablet1=dict(
                                  size=(1024, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              tablet2=dict(
                                  size=(768, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              mobile=dict(
                                  size=(375, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              admin=dict(
                                  size=(200, 200),
                              ),
                          ),
                          )

    title = models.CharField(_('title'), max_length=128, )
    note = models.TextField(_('note'), blank=True)

    class Meta:
        verbose_name = _('About block')
        verbose_name_plural = _('About blocks')

    @property
    def short_note(self):
        return description(self.note, self.MAX_LETTERS - 50, self.MAX_LETTERS)

    def is_long_note(self):
        return len(self.note) > self.MAX_LETTERS


(function($) {

    $(document).ready(function() {
        var $block = $('.block--about');

        $block.find('.block__note').expander({
            speed: 800,
            after_expand: function(event, data) {
                data.button.text(gettext('Read Less'));
            },
            after_reduce: function(event, data) {
                data.button.text(gettext('Read More'));
            }
        });

    });

})(jQuery);
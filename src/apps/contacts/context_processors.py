from .models import ContactsConfig


def contacts(request):
    config = ContactsConfig.get_solo()

    return {
        'CONTACTS': config,
    }

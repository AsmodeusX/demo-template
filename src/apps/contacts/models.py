from django.db import models
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from attachable_blocks.models import AttachableBlock
from google_maps.fields import GoogleCoordsField
from webpage.models import StdPage, StdDisablePage
from libs.get_city import get_city


class ContactsConfig(SingletonModel, StdPage, StdDisablePage):
    btn_caption = models.CharField(_('caption'), max_length=128, default='Contact Us Today')

    # Contacts
    phone = models.CharField(_('phone'), max_length=255, blank=True)
    email = models.EmailField(_('email'), blank=True)

    # Address
    address = models.CharField(_('address'), max_length=255)
    city = models.CharField(_('city'), max_length=255)
    region = models.CharField(_('region'), max_length=64, blank=True)
    zip = models.CharField(_('zip'), max_length=32, blank=True)
    coords = GoogleCoordsField(_('coords'), blank=True)

    class Meta(StdPage.Meta):
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('contacts:index')

    def __str__(self):
        return self.header

    def str_address(self):
        address = '{address} {city}'.format(
            address=self.address, city=self.city
        )

        if self.region:
            address = '{address}, {region}'.format(
                address=address,
                region=self.region
            )

        if self.zip:
            address = '{address}, {zip}'.format(address=address, zip=self.zip)

        return address

    def str_region_address(self):
        if not (self.region and self.zip):
            return ''

        return '{region} {zip}'.format(
            region=self.region, zip=self.zip
        )

    def str_city_address(self):
        address = self.str_region_address()

        if self.address:
            address = '{address}, {region_address}'.format(
                address=self.address, region_address=address
            )

        return address

    def map_url(self):
        if not self.coords:
            return ''

        return 'https://maps.google.ru/maps?daddr={lat},{lng}&z=15'.format(lat=self.coords.lat, lng=self.coords.lng)

    def detail_city(self):
        return get_city(self.city, 'United States')


class SuccessConfig(StdPage, SingletonModel):
    header = models.TextField(_('header'), max_length=128, default='', )
    title = models.CharField(_('title'), max_length=128, default='', )
    description = models.TextField(_('description'), blank=True, max_length=512)

    description_status = models.TextField(_('description status'),
                                          max_length=128,
                                          default='Your application has been submitted!', )
    description_text = models.TextField(_('description text'),
                                        max_length=128,
                                        default='Our specialists will contact you as soon as possible. \n'
                                                'If you have an urgent question, you can call us', )
    updated = models.DateTimeField(_('change date'), auto_now=True, blank=True, )

    class Meta(StdPage.Meta):
        verbose_name = _('success page')

    def get_absolute_url(self):
        return resolve_url('contacts:success')

    def __str__(self):
        return self.header


class StopWord(models.Model):
    text = models.CharField(_('text'), max_length=255)

    class Meta:
        verbose_name = _('stop word')
        verbose_name_plural = _('stop words')

    def __str__(self):
        return self.text


class NotificationReceiver(models.Model):
    config = models.ForeignKey(ContactsConfig, related_name='receivers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('notification receiver')
        verbose_name_plural = _('notification receivers')

    def __str__(self):
        return self.email


class Message(models.Model):
    name = models.CharField(_('name'), max_length=128)
    phone = models.CharField(_('phone'), max_length=32, blank=True)
    email = models.EmailField(_('e-mail'), blank=True)
    message = models.TextField(_('message'), max_length=2048)

    is_spam = models.BooleanField(_('is spam'), default=False)
    date = models.DateTimeField(_('date sent'), default=now, editable=False)
    referer = models.TextField(_('from page'), blank=True, editable=False)

    class Meta:
        default_permissions = ('delete', )
        verbose_name = _('message')
        verbose_name_plural = _('messages')
        ordering = ('-date',)

    def __str__(self):
        return self.name


class ContactsBlock(AttachableBlock):
    BLOCK_VIEW = 'contacts.views.contacts_block_render'

    title = models.CharField(_('title'), max_length=128, blank=True)
    btn_caption = models.CharField(_('caption'), max_length=128, default='Contact Us Today')

    class Meta:
        default_permissions = ()
        verbose_name = _('Contacts block')
        verbose_name_plural = _('Contacts blocks')

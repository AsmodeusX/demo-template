from django.template import loader
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import FormMixin
from seo.seo import Seo
from menu.utils import activate_menu_nolink
from webpage.views import StdPageSingletonView
from libs.description import description
# from libs.cache.cached import cached
from .models import ContactsConfig, SuccessConfig
from .forms import ContactForm
# from webpage.utils import UsedBlocks


class IndexView(FormMixin, StdPageSingletonView):
    model = ContactsConfig
    form_class = ContactForm
    template_name = 'contacts/index.html'
    success_url = reverse_lazy('contacts:index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if 'form' not in kwargs:
            context['form'] = self.get_form()
        context['coords'] = self.object.coords
        context['is_contact_page'] = True
        return context


class SuccessView(StdPageSingletonView):
    template_name = 'contacts/success.html'

    def get(self, request, *args, **kwargs):
        config = SuccessConfig.get_solo()
        prefixes = ['mobile', 'header', 'footer']

        for prefix in prefixes:
            activate_menu_nolink(self.request, 'contacts_%s' % prefix)

        seo = Seo()
        seo.set({
            'title': config.title or _('Thank You!'),
            'og_title': config.title or _('Thank You!'),
            'description': description(config.description, 50, 160),
            'og_description': config.description,
        })
        seo.save(self.request)

        return self.render_to_response({
            'page': config,
            'is_success_page': True,
        })


# @cached('block.id')
def contacts_block_render(context, block, **kwargs):
    config = ContactsConfig.get_solo()
    form = ContactForm()

    return loader.render_to_string('contacts/block.html', {
        'form': form,
        'coords': config.coords,
        'map_url': config.map_url(),
        'block': block,
        'title': block.title,
        'btn_caption': block.btn_caption,
    }, request=context.get('request'))

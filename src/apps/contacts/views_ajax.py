from django.shortcuts import resolve_url
from django.http import JsonResponse
from django.utils.html import escape
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _
from ajax_views.decorators import ajax_view
from libs.email import send_template
from constant_contact.services import create_cc_contact
from constant_contact.models import ConstantContactConfig
from .models import NotificationReceiver, StopWord
from .forms import ContactForm
from components.utils.recaptcha.check_recaptcha import check_recaptcha


@ajax_view('contacts.message')
class Message(View):
    def post(self, request, *args, **kwargs):
        captcha_result = check_recaptcha(request)

        form = ContactForm(request.POST)

        if form.is_valid() and captcha_result:
            message = form.save(commit=False)
            referer = request.POST.get('referer')
            message.referer = escape(referer)

            is_spam = False
            stop_words = StopWord.objects.all().values_list('text', flat=True)

            for i in stop_words:
                stop_word = i.strip().lower()

                if stop_word in message.message.lower() or stop_word in message.name.lower():
                    is_spam = True
                    break

            if is_spam:
                message.is_spam = is_spam

            message.save()

            receivers = NotificationReceiver.objects.all().values_list('email', flat=True)

            if not is_spam:
                send_template(request, receivers,
                              subject=_('Message from {domain}'),
                              template='contacts/mails/message.html',
                              context={
                                  'message': message,
                              }
                              )

                cc_config = ConstantContactConfig.get_solo()

                if cc_config.use_module:
                    try:
                        create_cc_contact(request, message.email, message.name, cc_config.list_id)
                    except Exception as e:
                        pass

            return JsonResponse({
                'url': resolve_url('contacts:success')
            }, status=200)

        return JsonResponse({
            'errors': form.error_dict_full
        }, status=400)

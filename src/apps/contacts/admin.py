from django.conf import settings
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils import dateformat
from django.utils.timezone import localtime
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin

from attachable_blocks.admin import AttachableBlockAdmin, AttachedBlocksStackedInline
from libs.description import description
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from webpage.admin import SEOStdPageAdmin, StdDisablePageAdmin
from .models import (
    ContactsConfig, SuccessConfig,
    NotificationReceiver, ContactsBlock, Message, StopWord
)


class ContactsConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class NotificationReceiverAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = NotificationReceiver
    extra = 0
    suit_classes = 'suit-tab suit-tab-notify'


@admin.register(ContactsConfig)
class ContactsConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = (
                    (None, {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'header', 'title',
                        ),
                    }),
                ) + StdDisablePageAdmin.fieldsets + (
                    (_('Button'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'btn_caption',
                        ),
                    }),
                    (_('Contact'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'phone', 'email',
                        ),
                    }),
                    (_('Address'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'address', 'city', 'region', 'zip', 'coords',
                        ),
                    }),
                )
    inlines = (NotificationReceiverAdmin, ContactsConfigBlocksInline)
    suit_form_tabs = (
        ('general', _('General')),
        ('notify', _('Notifications')),
        ('blocks', _('Blocks')),
    )

    class Media:
        js = (
            'contacts/admin/js/coords.js',
        )


@admin.register(SuccessConfig)
class SuccessConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = (
                    (None, {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'header', 'title',
                        ),
                    }),
                    (_('Message'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': (
                            'description_status', 'description_text',
                        ),
                    })
                )
    suit_form_tabs = (
        ('general', _('General')),
    )


class SpamFilter(SimpleListFilter):
    title = _('Is Spam')
    parameter_name = 'is_spam'

    def value(self):
        value = super().value()
        if not value:
            value = '0'
        return value

    def lookups(self, request, model_admin):
        return (
            ('2', _('All')),
            ('1', _('Yes')),
            ('0', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == '0':
            return queryset.filter(is_spam=False)
        if self.value() == '1':
            return queryset.filter(is_spam=True)
        return queryset


@admin.register(Message)
class MessageAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'phone', 'email',
            ),
        }),
        (_('Text'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'message',
            ),
        }),
        (_('Info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'date_fmt', 'referer', 'is_spam',
            ),
        }),
    )
    readonly_fields = ('name', 'phone', 'email', 'message', 'date_fmt', 'is_spam', 'referer')
    list_display = ('name', 'message_fmt', 'date_fmt', 'is_spam',)
    list_filter = (SpamFilter,)
    suit_form_tabs = (
        ('general', _('General')),
    )

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def message_fmt(self, obj):
        return description(obj.message, 60, 80)
    message_fmt.short_description = _('Message')
    message_fmt.admin_order_field = 'message'

    def date_fmt(self, obj):
        return dateformat.format(localtime(obj.date), settings.DATETIME_FORMAT)
    date_fmt.short_description = _('Date')
    date_fmt.admin_order_field = 'date'


@admin.register(StopWord)
class StopWordAdmin(ModelAdminMixin, admin.ModelAdmin):
    """ Адрес """
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    list_display = ('text', )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(ContactsBlock)
class ContactBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
        (_('Button'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'btn_caption',
            ),
        }),
    )

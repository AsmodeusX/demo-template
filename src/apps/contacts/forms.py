from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
from libs.form_helper.forms import FormHelperMixin
from libs.phone import translate_phonewords, is_e164_format
from .models import Message
from components.utils.recaptcha.generate_id import get_id
import re


class ContactForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/unlabeled_field.html'

    message = forms.CharField(
        label=_('Message'),
        max_length=2048,
        widget=forms.Textarea(attrs={
            'placeholder': _('Message'),
            'rows': 5,
        }),
        error_messages={
            'required': _('Please enter your message'),
            'max_length': _('Message should not be longer than %(limit_value)d characters'),
            'min_length': _("The message is very short"),
            'incorrect': _('Your message contains illegal characters'),
        }
    )

    custom_id = None

    class Meta:
        model = Message
        fields = '__all__'
        exclude = ('is_spam', )

        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': _('Name'),
            }),
            'email': forms.TextInput(attrs={
                'placeholder': _('E-mail'),
            }),
            'phone': forms.TextInput(attrs={
                'placeholder': _('Phone'),
            }),
        }

        error_messages = {
            'name': {
                'required': _('Please enter your name'),
                'max_length': _('Name should not be longer than %(limit_value)d characters'),
                'min_length': _("The name is very short"),
                'incorrect': _('Your name contains illegal characters'),
            },
            'phone': {
                'required': _('Please enter your phone so we can contact you'),
                'max_length': _('Phone should not be longer than %(limit_value)d characters'),
                'min_length': _("The phone is very short"),
                'incorrect': _('Your phone contains illegal characters'),
                'invalid': _('Please enter correct phone'),
            },
            'email': {
                'required': _('Please enter your e-mail so we can contact you'),
                'max_length': _('Email should not be longer than %(limit_value)d characters'),
                'min_length': _("The email is very short"),
                'incorrect': _('Your email contains illegal characters'),
                'invalid': _('Please enter correct email'),
            },
            'message': {
                'required': _('Please enter your message'),
                'max_length': _('Message should not be longer than %(limit_value)d characters'),
                'min_length': _("The message is very short"),
                'incorrect': _('Your message contains illegal characters'),
            },
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.custom_id = get_id()

    def clean_name(self):
        if 'name' in self.cleaned_data:
            name = self.cleaned_data.get('name')

            if not name:
                self.add_field_error('name', 'required')
            else:
                r = re.findall(r'[а-яА-я]', name)

                if len(r) > 0:
                    self.add_field_error('name', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', name)

                    if len(r) < 3:
                        self.add_field_error('name', 'min_length')

            return name

    def clean_phone(self):
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')

            if phone:
                r = re.findall(r'[а-яА-я]', phone)

                if len(r) > 0:
                    self.add_field_error('phone', 'incorrect')
                else:
                    phone = translate_phonewords(phone)

                    if is_e164_format(phone):
                        self.cleaned_data['phone'] = phone
                    else:
                        self.add_field_error('phone', 'invalid')

            return phone

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if email:
                r = re.findall(r'[а-яА-я]', email)

                if len(r) > 0:
                    self.add_field_error('email', 'incorrect')
                else:
                    try:
                        EmailValidator()(email)
                        self.cleaned_data['email'] = email
                    except forms.ValidationError:
                        self.add_field_error('email', 'invalid')

            return email

    def clean_message(self):
        if 'message' in self.cleaned_data:
            message = self.cleaned_data.get('message')

            if not message:
                self.add_field_error('message', 'required')
            else:
                r = re.findall(r'[а-яА-я]', message)

                if len(r) > 0:
                    self.add_field_error('message', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', message)

                    if len(r) < 3:
                        self.add_field_error('message', 'min_length')

            return message

    def clean_zip(self):
        cnt_min = 5
        cnt_max = 6

        if 'zip' in self.cleaned_data:
            zip = self.cleaned_data.get('zip')

            if not zip:
                self.add_field_error('zip', 'required')
            else:

                if len(zip) < cnt_min:
                    self.add_field_error('zip', 'min_length')

                if len(zip) > cnt_max:
                    self.add_field_error('zip', 'max_length')

            return zip

    def clean(self):
        if 'phone' in self.cleaned_data and 'email' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')
            email = self.cleaned_data.get('email')

            if not phone and not email:
                self.add_field_error('phone', 'required')
                self.add_field_error('email', 'required')

        return super().clean()

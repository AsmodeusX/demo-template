/** @namespace window.ajax_views.contacts.message */


(function ($) {
    var recaptchaId;
    var labels_elem = 'form[id^=fm_contact_]';
    var classes = {
        progress: 'sending',
        invalid: 'invalid',
        message: 'error-message'
    }

    // Отправка запроса, обработка ответа
    window.fnContactSend = function(token) {
        if (window.$send_form.hasClass(classes.progress)) return false;

        var data = window.$send_form.serializeArray();

        data.push(
            {
                name: 'referer',
                value: window.location.href
            },
            {
                name: 'g_recaptcha_response',
                value: token
            }
        );

        $.ajax({
            url: window.ajax_views.contacts.message,
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                window.$send_form.find('.' + classes.invalid).removeClass(classes.invalid);
                window.$send_form.find('.' + classes.message).remove();
                window.$send_form.addClass(classes.progress);
                $.preloader();
            },
            success: function (response) {
                if (response.url) window.location.href = response.url;
            },
            error: $.parseError(function (response) {
                $.popup().hide();
                response ? renderErrors(response) : alert(window.DEFAULT_AJAX_ERROR);
            }),
            complete: function () {
                window.grecaptcha.reset(recaptchaId);
                window.$send_form.removeClass(classes.progress);
            }
        });
    };

    // Обработка состояний подписей
    $(document).ready(function () {
        setLabels();
    });

    function setLabels() {
        var $forms = $(labels_elem),
            $els = $forms.find('input, textarea');

        $els.each(function () {
            var $el = $(this),
                clActive = 'label-active';

            $el.on('focus change', function () {
                var $this = $(this),
                    $holders = $this.closest('.field').find('div.label-holder');
                $holders.addClass(clActive);
            });

            $el.on('focusout', function () {
                var val = $el.val();

                if (val.length === 0) {
                    $el.closest('.field').find('div.label-holder').removeClass(clActive);
                }
            });
        });
    }

    function renderErrors(response) {
        setTimeout(function () {
            $.each(response.errors, function () {
                var error = this;
                var $field = window.$send_form.find('div.' + error.fullname);
                    // $error_message = $('<span>')
                    //     .addClass(classes.message)
                    //     .html(error.errors[0]);
                $field.addClass(classes.invalid);
                // $field.find('div.control').append($error_message);
            })

        }, 300);
    }
})(jQuery);

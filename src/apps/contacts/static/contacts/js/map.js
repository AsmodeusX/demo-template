(function($) {
    var gmap,
        loaded = false,
        elem = '.google-map';

    var styleMap = [
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        }
    ];
    var list_markers = ['default'],
        markers = setListMarkers();

    $.visibilityInspector.inspect(elem, {
        top: 20,
        bottom: 20,
        afterCheck: function($elem, opts, state) {
            if (state && !loaded) {
                loaded = !loaded;
                setMap($elem);
            }
        }
    });

    function setListMarkers() {

        var markersPath = '/static/contacts/img/markers/',
            markersExt = '.png',
            markers = {};

        $.each(list_markers, function () {
            var marker = this;

            markers[marker] = markersPath + marker + markersExt;
        })

        return markers;
    }

    function setMap($map) {
        var coords = $map.data('coords'),
            list_coords = coords.split(', ');

        gmap = GMap($map, {
            center: GMapPoint(list_coords[0], list_coords[1]),
            zoom: 15,
            dblClickZoom: true,
            zoomControl: true,
            draggable: true,
            styles: styleMap
        }).on('ready', function () {
            GMapMarker({
                map: gmap,
                position: GMapPoint(list_coords[0], list_coords[1]),
                icon: markers.default
            }).on('mouseover', function () {
                var point = this;

                if (!(this.native.icon.url === markers.active)) {
                    point.icon({
                        url: markers.hover
                    });
                }
            }).on('mouseout', function () {
                var point = this;

                if (!(this.native.icon.url === markers.active)) {
                    point.icon({
                        url: markers.default
                    });
                }
            }).on('click', function () {
                var point = this;

                point.icon({
                    url: markers.active
                });
            })
        });
    }

})(jQuery);

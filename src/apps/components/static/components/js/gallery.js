(function($) {
    var $items, $gallery, $blocks, config = [], classNewItem = 'new-item';

    window.gallery_update = function($block) {
        addGalleryItems($block);
    }

    $(document).ready(function () {
        $blocks = $('.gallery');

        $blocks.each(function () {
            var $block = $(this);

           window.gallery_update($block);
        })
    });

    function addGalleryItems($block) {
        $items = $block.find('.preview.' + classNewItem);

        $items.each(function () {
            var $item = $(this),
                $preview = $item.find('img');
            config.push({
                src  : $preview.data('href'),
                opts : {
                    caption : $preview.data('caption'),
                    thumb   : $preview.data('thumb')
                }
            });
            $item.on('click', function () {
                var indexItem = $block.find('.preview').index($item);

                $gallery = $.fancybox.open(config, {
                    loop : true
                });

                $gallery.jumpTo(indexItem)
            })
        });

        $items.removeClass(classNewItem);
    }


})(jQuery);
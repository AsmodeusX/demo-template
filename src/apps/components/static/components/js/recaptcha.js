"use strict";

(function ($) {
    /*
    *
    * Вариаций даже одной формы может быть много на странице (блок, попап, скролл).
    * Также необходимо чтобы капча подгружалась одна и только после действия,
    * т.е. не влияла на скорость загрузки, и работала с различными формами
    *
    * Реализация:
    * У каждой формы должно генерироваться свое значение в поле custom_id
    * оно учавствует для идентификации формы и капчи
    *
    * В переменную forms записываются все типы форм (без префиксов, суффиксов),
    * чтобы они после прохождения проверки отправлялись одной функцией,
    * В самом приложении происходит только подготовка данных и отправка ajax
    * на момент подключения скрипта, на странице уже должна быть подключена функция обработки отправки
    *
    * */

    var form_mode,
        loaded = false;

    var forms = {
        contact: {
            name: 'contact',
            func: window.fnContactSend
        }
    }

    window.recaptchaCallback = function() {
        $.each(forms, function () {
            var form = this,
                form_id_start = 'fm_%form_'.replace('%form', form.name);
            var $forms = $('form[id^="' + form_id_start + '"]');

            $forms.each(function () {
                var $form = $(this),
                    form_unique_id = $form.attr('id').replace(form_id_start, ''),
                    recaptcha_name = 'recaptcha_%form_%suffix'
                        .replace('%form', form.name)
                        .replace('%suffix', form_unique_id),
                    recaptcha_id = '#%name'.replace('%name', recaptcha_name);

                var captchaOptions = {
                    sitekey: document.documentElement.getAttribute('data-google-recaptcha-key'),
                    size: 'invisible',
                    callback: form.func
                };
                if ($(recaptcha_id).length >= 0) {
                    window[recaptcha_name] = window.grecaptcha.render(recaptcha_name, captchaOptions);
                }
            });
        })
    };

    $.each(forms, function () {
        var form = this,
            form_id_start = 'fm_%form_'.replace('%form', form.name);

        var $forms = $('form[id^="' + form_id_start + '"]');

        $forms.each(function () {
            var $form = $(this),
                form_unique_id = $form.attr('id').replace(form_id_start, ''),
                recaptcha_name = 'recaptcha_%form_%suffix'
                    .replace('%form', form.name)
                    .replace('%suffix', form_unique_id);

            $(document).on('submit', $form, function (e) {
                form_mode = form;
                window.$send_form = $form;
                e.preventDefault();

                var token = window.grecaptcha.getResponse(window[recaptcha_name]);
                if (!token) {
                    window.grecaptcha.execute(window[recaptcha_name]);
                    return false;
                }
            });
        })

    })

    $(window).one('mousemove scroll keydown touchstart', function () {
        if (!loaded) {
            get_captcha();

            loaded = !loaded;
        }
    });

    function get_captcha() {
        $.getScript('https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit&hl=en');
    }
})(jQuery);

(function($) {

    var overflowPositions = {
        top: 'top',
        bottom: 'bottom'
    }
    var overflowActions = {
        show: 1,
        hide: 2
    }

    window.CheckMenu = checkScroll;

    var menuScrollableElement = '.scrollable';
    var $menuScrollableElement;

    var MobileBox = Class(EventedObject, function MobileBox(cls, superclass) {
        cls.init = function(options) {
            superclass.init.call(this);

            var that = this;

            this.opts = $.extend({
                mobileBoxSelector: '.mobile-box',
                buttonSelector: '.burger',
                openedClass: 'mobile-box-opened',
                rootEl: 'html',
                tabletWidth: 1024,
                fullHeight: false
            }, options);

            $(document)
                .off(this.opts.mobileBoxSelector)
                .on('click.' + this.opts.mobileBoxSelector, this.opts.buttonSelector, function() {
                    return that.isOpened() ? that.close() : that.open();
                });
        };

        cls.getMobileBox = function() {
            return $(this.opts.mobileBoxSelector).first();
        };

        cls.isOpened = function() {
            return $(this.opts.rootEl).hasClass(this.opts.openedClass);
        };

        cls._open = function() {
            return $(this.opts.rootEl).addClass(this.opts.openedClass);
        };

        cls._close = function() {
            return $(this.opts.rootEl).removeClass(this.opts.openedClass);
        };

        cls.update = function() {
            var $mobileBox = this.getMobileBox();
            if (!$mobileBox.length) return false;
        };

        cls.open = function() {
            var $mobileBox = this.getMobileBox();
            if (!$mobileBox.length) return false;

            if (this.trigger('before_open') === false) return false;

            this.update();
            this._open();
        };

        cls.close = function() {
            var $mobileBox = this.getMobileBox();

            if (!$mobileBox.length || this.trigger('before_close') === false) return false;

            this._close();
        };

        $(window).on('resize.mobile-box', $.rared(function() {
            window.mobile_box.close()
        }))
    });


    $(document).ready(function() {
        window.mobile_box = MobileBox()
            .on('resize', function(winWidth) {
                if (winWidth >= window.mobile_box.tabletWidth) this.close();
            });
    });


    $(window).on('resize.mobile-box', $.rared(function() {
        if (!window.mobile_box) return

        window.mobile_box.update();
        window.mobile_box.trigger('resize', $.winWidth());
    }, 100));


    $(document).ready(function () {
        $('.arrow--bottom, .arrow--top').hide();
        $menuScrollableElement = $(menuScrollableElement);
        $menuScrollableElement.on('scroll', function (e) {
            checkScroll();
        });
    })

    $(window).on('load resize', function () {
        setTimeout(checkScroll, 500); // because we're waiting a hide script
    })

    function checkScroll() {
        var action;
        var checkElement = $(menuScrollableElement).get(0);
        action = checkElement.scrollTop === 0 ? overflowActions.hide : overflowActions.show;
        changeArrowState(action, overflowPositions.top);
        action = checkElement.offsetHeight + checkElement.scrollTop < checkElement.scrollHeight ? overflowActions.show : overflowActions.hide;
        /* Debug */
        // console.log({
        //     offsetHeight: checkElement.offsetHeight,
        //     scrollTop: checkElement.scrollTop,
        //     sum: checkElement.offsetHeight + checkElement.scrollTop,
        //     checkElement: checkElement.scrollHeight,
        //     res: checkElement.offsetHeight + checkElement.scrollTop < checkElement.scrollHeight
        // });
        changeArrowState(action, overflowPositions.bottom);
    }

    function changeArrowState(action, position) {
        var className = '.arrow--' + position;
        if (action === overflowActions.hide) {
            $(className).hide();
        } else if (action === overflowActions.show) {
            $(className).show();
        }
    }

})(jQuery);

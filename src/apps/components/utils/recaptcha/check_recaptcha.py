import requests
from django.conf import settings
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def check_recaptcha(request):
    request.recaptcha_is_valid = None
    recaptcha_response = request.POST.get('g_recaptcha_response')
    data = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response,
    }
    try:
        r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data, verify=False)
        result = r.json()
        if result['success']:
            return True
        else:
            return False
    except Exception as e:
        return False

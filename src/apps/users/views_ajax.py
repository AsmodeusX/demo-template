from django.http import JsonResponse
from django.forms import model_to_dict
from django.http.response import Http404
from django.template import loader
from django.views.generic import View, FormView
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from ajax_views.decorators import ajax_view
from .forms import LoginForm, RegisterForm, PasswordResetForm, SetPasswordForm


def user_to_dict(user):
    """ Вывод информации о юзере в формате, пригодном для JSON """
    user_dict = model_to_dict(user, fields=(
        'id', 'email', 'username', 'first_name', 'last_name', 'is_staff', 'is_superuser'
    ))
    return user_dict


@ajax_view('users.signin')
class LoginView(FormView):
    form_class = LoginForm
    template_name = 'users/ajax_login.html'

    def form_valid(self, form):
        user = form.get_user()
        auth_login(self.request, user)
        return JsonResponse({
            'success': True,
            'user': user_to_dict(user),
        })

    def form_invalid(self, form):
        return JsonResponse({
            'success': False,
            'errors': form.error_dict_full,
        })


@ajax_view('users.signout')
class LogoutView(View):
    def post(self, request):
        auth_logout(request)
        return JsonResponse({

        })


@ajax_view('users.signup')
class RegisterView(FormView):
    form_class = RegisterForm
    template_name = 'users/ajax_register.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            raise Http404
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            raise Http404
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save()
        user = authenticate(
            username=user.username,
            password=form.cleaned_data.get('password1')
        )
        auth_login(self.request, user)
        return JsonResponse({
            'success': True,
            'user': user_to_dict(user),
        })

    def form_invalid(self, form):
        return JsonResponse({
            'success': False,
            'errors': form.error_dict_full,
        })


@ajax_view('users.reset')
class PasswordResetView(FormView):
    """ AJAX сброс пароля. Указание e-mail """
    email = ''
    form_class = PasswordResetForm
    template_name = 'users/ajax_reset.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            raise Http404
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.email = request.POST.get('email', '')
        request.session['reset_email'] = self.email
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': default_token_generator,
            'email_template_name': 'users/emails/reset_email.html',
            'subject_template_name': 'users/emails/reset_subject.html',
            'request': self.request,
            'html_email_template_name': 'users/emails/reset_email.html',
        }
        form.save(**opts)
        return JsonResponse({
            'success': True,
            'message': loader.render_to_string('users/ajax_reset_done.html', {
                'email': self.email,
            }, request=self.request),
        })

    def form_invalid(self, form):
        return JsonResponse({
            'success': False,
            'errors': form.error_dict_full,
        })


@ajax_view('users.reset_confirm')
class ResetConfirmView(FormView):
    """ AJAX сброс пароля. Указание нового пароля """
    form_class = SetPasswordForm
    template_name = 'users/ajax_reset_confirm.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise Http404
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise Http404
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.setdefault('user', self.request.user)
        return kwargs

    def form_valid(self, form):
        form.save()
        return JsonResponse({
            'success': True
        })

    def form_invalid(self, form):
        return JsonResponse({
            'success': False,
            'errors': form.error_dict_full,
        })

# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-10-20 13:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpageconfig',
            name='contact_button',
            field=models.BooleanField(default=True, verbose_name='show contact button'),
        ),
    ]

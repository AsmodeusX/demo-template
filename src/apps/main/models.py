from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from webpage.models import StdImagePage, StdButtonPage


class MainPageConfig(SingletonModel, StdImagePage, StdButtonPage):
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta(StdImagePage.Meta):
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('main:index')

    def __str__(self):
        return ugettext('Home page')

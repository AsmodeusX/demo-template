from seo.seo import Seo
from .models import MainPageConfig
from webpage.views import StdPageSingletonView


class IndexView(StdPageSingletonView):
    model = MainPageConfig
    context_object_name = 'page'
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        seo = Seo()
        seo.set_data(self.object)
        seo.save(self.request)
        # self.request.session.flush()

        context = super().get_context_data(**kwargs)
        context.update({
            'is_main_page': True,  # отменяет <noindex> шапки и подвала
        })
        return context

from django.utils.translation import ugettext_lazy as _


STATUS_DRAFT = 1
STATUS_PUBLIC = 2
STATUS_CHOICES = (
    (STATUS_DRAFT, _('Draft')),
    (STATUS_PUBLIC, _('Public')),
)
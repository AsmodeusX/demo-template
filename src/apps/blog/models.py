from django.db import models
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor.fields import CKEditorUploadField
from libs.autoslug import AutoSlugField
from libs.stdimage.fields import StdImageField
from attachable_blocks.models import AttachableBlock
from webpage.models import StdPage, StdImagePage, StdButtonPage, StdDisablePage, TextPage
from .conf import *


class BlogConfig(StdPage, StdButtonPage, StdDisablePage, SingletonModel):
    other_posts_title = models.CharField(_('title'), max_length=128, default='Other Blog Entries')
    posts_title = models.CharField(_('title'), max_length=128, default='Latest Blog Entries')

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('blog:index')

    def __str__(self):
        return self.header


class TagQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(
            posts__status=STATUS_PUBLIC,
            posts__date__lte=now()
        ).distinct()


class Tag(models.Model):
    title = models.CharField(_('title'), max_length=255)
    slug = AutoSlugField(_('slug'), populate_from='title', unique=True)
    sort_order = models.IntegerField(_('order'), default=0)

    objects = TagQuerySet.as_manager()

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
        ordering = ('sort_order',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return resolve_url('blog:tag', tag_slug=self.slug)


class BlogPostQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(
            status=STATUS_PUBLIC,
            date__lte=now()
        )


class PostQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(
            status=STATUS_PUBLIC,
            date__lte=now()
        )


class Post(StdImagePage, StdButtonPage, TextPage):
    preview = StdImageField(_('preview'),
                            default='',
                            upload_to='blog/preview',
                            min_dimensions=(480, 265),
                            admin_variation='admin',
                            crop_area=True,
                            aspects=('normal',),
                            variations=dict(
                                normal=dict(
                                    size=(380, 250),
                                    create_webp=True,
                                ),
                                tablet=dict(
                                    size=(480, 250),
                                    create_webp=True,
                                ),
                                mobile=dict(
                                    size=(460, 250),
                                    create_webp=True,
                                ),
                                admin=dict(
                                    size=(380, 250),
                                ),
                            ),
                            )
    alt_preview = models.CharField(_('alt for preview'), max_length=128, blank=True)
    note = models.CharField(_('note'), max_length=255, default='')
    slug = AutoSlugField(_('slug'), populate_from='header', unique=True)
    text = CKEditorUploadField(_('text'))
    status = models.IntegerField(_('status'), choices=STATUS_CHOICES, default=STATUS_DRAFT)
    tags = models.ManyToManyField(Tag, verbose_name=_('tags'), related_name='posts', blank=True)

    objects = PostQuerySet.as_manager()

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ('-date', '-id')

    def __str__(self):
        return self.header

    def get_absolute_url(self):
        return resolve_url('blog:detail', slug=self.slug)


class BlogBlock(AttachableBlock):
    BLOCK_VIEW = 'blog.views.blog_block_render'

    title = models.CharField(_('title'), max_length=128, default='Latest Blog Entries')

    class Meta:
        default_permissions = ()
        verbose_name = _('Blog block')
        verbose_name_plural = _('Blog blocks')

from django.http.response import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import loader
from seo.seo import Seo
from paginator.utils import get_paginator_meta
from paginator.paginator import Paginator, EmptyPage
from menu.utils import activate_menu_nolink
from libs.description import description
from .models import BlogConfig, Post, Tag
from libs.cache.cached import cached
from webpage.views import StdPageSingletonView, StdPageDetailView


CNT_POSTS = 6
CNT_BLOCK_POSTS = 3


class MorePaginator(Paginator):
    template = 'blog/parts/_btn.html'


class IndexView(StdPageSingletonView):
    model = BlogConfig
    context_object_name = 'page'
    template_name = 'blog/index.html'

    def get_object(self, queryset=None):
        return self.model.get_solo()

    def get_context_data(self, **kwargs):
        tag_slug = self.kwargs.get('tag_slug')

        if tag_slug:
            tag = get_object_or_404(Tag, slug=tag_slug)
            posts = Post.objects.visible().filter(tags=tag)
        else:
            tag = None
            posts = Post.objects.visible()

        try:
            paginator = Paginator(
                self.request,
                object_list=posts,
                per_page=CNT_POSTS,
                page_neighbors=1,
                side_neighbors=1,
                allow_empty_first_page=False,
            )
        except EmptyPage:
            raise Http404

        seo = Seo()
        seo.set(get_paginator_meta(paginator))
        seo.set_data(self.object, defaults={
            'title': self.object.header,
            'og_title': self.object.header,
        })
        # Unique title
        title_appends = ' | '.join(map(str, filter(bool, [
            'Page %d/%d' % (paginator.current_page_number, paginator.num_pages)
            if paginator.current_page_number >= 2 else '',
        ])))
        if title_appends:
            default_title = seo.title.popleft()
            seo.title = '%s | %s' % (default_title, title_appends)

            seo.description = ''
        seo.save(self.request)

        context = super().get_context_data(**kwargs)
        context.update({
            'posts': paginator,
            'tags': Tag.objects.visible(),
            'current_tag': tag,
            'is_blog_page': True,
        })
        return context


class BlogDetailView(StdPageDetailView):
    model = Post
    context_object_name = 'page'
    template_name = 'blog/detail.html'

    def get_context_data(self, **kwargs):
        config = BlogConfig.get_solo()

        menus = ['blog_mobile', 'blog_header', 'blog_footer']

        for menu in menus:
            activate_menu_nolink(self.request, menu)

        activate_menu_nolink(self.request, 'blog')

        seo = Seo()

        seo.set_data(self.object, defaults={
            'title': self.object.header,
            'description': description(self.object.description, 50, 160),
            'og_title': self.object.header,
            'og_image': self.object.preview.url if self.object.preview else '',
            'og_description': self.object.description,
        })
        seo.save(self.request)

        context = super().get_context_data(**kwargs)
        context.update({
            'config': config,
        })
        return context


@cached('block.id', time=5*60)
def blog_block_render(context, block, **kwargs):
    posts = Post.objects.visible()

    return loader.render_to_string('blog/block.html', {
        'title': block.title,
        'posts': posts[:CNT_BLOCK_POSTS],
        'block': block,
    }, request=context.get('request'))

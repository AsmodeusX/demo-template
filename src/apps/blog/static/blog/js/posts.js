(function($) {

    $(document).ready(function() {
        var $posts = $('.post');
        $posts.each(function () {
            var $post = $(this);
            var link = $post.find('.link-more a').attr('href');
            $post.on('click', function () {
                window.location.href = link;
            })
        })
    });

})(jQuery);
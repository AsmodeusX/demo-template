/** @namespace window.ajax_views.blog.more */


(function($) {

    $(document).on('click', '#more-posts .btn--paginator', function() {
        loadMore($('#current-page').val());
    });

    function loadMore(page) {

        return $.ajax({
            url: window.ajax_views.blog.more,
            type: 'GET',
            data: {
                'page': page
            },
            dataType: 'json',
            success: function(response) {
                if (response.success_message) {
                    $('.posts').append(response.success_message);
                }

                var $btn = $('#more-posts');

                if (response.more_button) {
                    $btn.html(response.more_button);
                } else {
                    $btn.remove();
                }
                var $posts = $('.post');
                $posts.each(function () {
                    var $post = $(this);
                    var link = $post.find('.link-more a').attr('href');
                    $post.on('click', function () {
                        window.location.href = link;
                    })
                })
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    }
})(jQuery);
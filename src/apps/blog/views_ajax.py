from django.views.generic.base import View
from django.template.loader import render_to_string
from django.http import JsonResponse
from ajax_views.decorators import ajax_view
from paginator.paginator import EmptyPage
from .models import Post
from .views import MorePaginator, CNT_POSTS


@ajax_view('blog.more')
class PostsView(View):

    def get(self, request):
        posts = Post.objects.visible()

        try:
            paginator = MorePaginator(
                request,
                object_list=posts,
                per_page=CNT_POSTS,
                allow_empty_first_page=False,
            )

        except EmptyPage:
            return JsonResponse({}, status=400)

        render_to_string('blog/parts/_more.html', {
            'posts': paginator
        })

        return JsonResponse({
            'success_message': render_to_string('blog/parts/_grid_list.html', {
                'posts': paginator.current_page
            }),
            'more_button': render_to_string('blog/parts/_more.html', {
                'posts': paginator
            }),
        })
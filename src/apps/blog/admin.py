from django import forms
from django.conf import settings
from django.contrib import admin
from django.utils import dateformat
from django.utils.timezone import localtime
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableModelAdmin
from libs.autocomplete.widgets import AutocompleteMultipleWidget
from social_networks.admin import AutoPostMixin
from attachable_blocks.admin import AttachedBlocksStackedInline, AttachableBlockAdmin
from .models import BlogConfig, Post, BlogBlock, Tag
from .conf import STATUS_PUBLIC, STATUS_DRAFT
from webpage.admin import SEOStdPageAdmin, SEOStdImagePageAdmin, TextPageAdmin, StdDisablePageAdmin, StdButtonPageAdmin


class BlogConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class PostBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(BlogConfig)
class BlogConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = StdDisablePageAdmin.fieldsets + \
                StdButtonPageAdmin.fieldsets + \
                SEOStdPageAdmin.fieldsets + (
                    (_('Block Posts'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': ('posts_title', ),
                    }),
                    (_('Block Other Posts'), {
                        'classes': ('suit-tab', 'suit-tab-general'),
                        'fields': ('other_posts_title', ),
                    }),
                )
    inlines = (BlogConfigBlocksInline, )
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )


class BlogPostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
        widgets = {
            'tags': AutocompleteMultipleWidget(),
        }


@admin.register(Tag)
class TagAdmin(SEOStdPageAdmin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'slug'),
        }),
    )
    sortable = 'sort_order'
    list_display = ('title',)
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Post)
class PostAdmin(SEOStdImagePageAdmin, AutoPostMixin, admin.ModelAdmin):
    fieldsets = SEOStdImagePageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + (
        (_('Content'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'preview', 'alt_preview', 'note', 'text',
            ),
        }),
        (_('Information'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'slug', 'status', 'tags',
            ),
        }),
    ) + TextPageAdmin.fieldsets
    list_filter = ('status', 'tags')
    date_hierarchy = 'date'
    form = BlogPostForm
    search_fields = ('header',)
    list_display = ('view', '__str__', 'date_fmt', 'status')
    list_display_links = ('__str__',)
    actions = ('make_public_action', 'make_draft_action')
    inlines = (PostBlocksInline, )
    prepopulated_fields = {
        'slug': ('header', )
    }

    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )

    def tags_list(self, obj):
        return ' / '.join((str(item.title) for item in obj.tags.all()))
    tags_list.short_description = _('tags')

    def date_fmt(self, obj):
        return dateformat.format(localtime(obj.date), settings.DATETIME_FORMAT)
    date_fmt.short_description = _('Publication date')
    date_fmt.admin_order_field = 'date'

    def get_autopost_text(self, obj):
        return obj.description

    def make_public_action(self, request, queryset):
        queryset.update(status=STATUS_PUBLIC)
    make_public_action.short_description = _('Make public')

    def make_draft_action(self, request, queryset):
        queryset.update(status=STATUS_DRAFT)
    make_draft_action.short_description = _('Make draft')


@admin.register(BlogBlock)
class BlogBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )

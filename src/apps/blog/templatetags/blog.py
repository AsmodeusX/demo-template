from django.template import loader
from libs import jinja2
from ..models import BlogConfig, Post
from ..views import CNT_BLOCK_POSTS
from webpage.utils import UsedBlocks


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'posts', }
    takes_context = True

    def _posts(self, context, post=None, classes='', template='blog/block.html'):
        posts = Post.objects.visible()
        config = BlogConfig.get_solo()

        title = config.posts_title

        if post:
            posts = posts.exclude(id=post)
            title = config.other_posts_title

        UsedBlocks.add_block(name='blog')

        return loader.render_to_string(template, {
            'classes': classes,
            'posts': posts[:CNT_BLOCK_POSTS],
            'title': title,
        }, request=context.get('request'))

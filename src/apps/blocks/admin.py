from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    AdvantagesBlock, Advantage,
)
from suit.admin import SortableStackedInline
from project.admin.base import ModelAdminInlineMixin
from attachable_blocks.admin import AttachableBlockAdmin


# Advantages
class AdvantagesInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Advantage
    min_num = 0
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(AdvantagesBlock)
class AdvantagesBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (

            ),
        }),
    )
    inlines = (AdvantagesInline, )

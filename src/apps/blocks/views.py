from django.shortcuts import loader
from libs.cache.cached import cached
from webpage.utils import UsedBlocks


@cached('block.id')
def advantages_block_render(context, block, **kwargs):

    return loader.render_to_string('blocks/advantages.html', {
        'block': block,
    }, request=context.get('request'))

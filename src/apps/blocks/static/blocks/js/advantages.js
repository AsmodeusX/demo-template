(function($) {
    var documentStyle = document.documentElement.style;
    var advantages_count = {
        desktop: Number(documentStyle.getPropertyValue('--advantages-count')) || 4,
        tablet: Number(documentStyle.getPropertyValue('--advantages-count-tablet')) || 2,
        mobile: Number(documentStyle.getPropertyValue('--advantages-count-mobile')) || 1
    }

    var sizes = {
        tablet: 1024,
        mobile: 768
    }

    var slider_config = {
        pagination: true,
        navigation: true
    }

    var slider_params = {
        spaceBetween: 20,
        threshold: 10,
        watchSlidesVisibility: true,
        watchOverflow: true
    };

    var elem = '.block--advantages';

    $(window).on('load', function() {
        var $blocks = $(elem);
        $blocks.each(function () {
            var $block = $(this);
            setSwiper($block);
        });
    });

    function setSwiper($block) {
        var w = $.winWidth();
        var $advantages = $block.find('.advantages');

        slider_params['slidesPerView'] = w >= sizes.tablet ? advantages_count.desktop : w >= sizes.mobile ? advantages_count.tablet : advantages_count.mobile;

        if (slider_config.pagination) {
            slider_params["pagination"] = {
                el: $block.find('.swiper-pagination').get(0),
                type: 'progressbar'
            };
        }

        if (slider_config.navigation) {
            slider_params["navigation"] = {
                nextEl: $block.find('div.swiper-button-next').get(0),
                prevEl: $block.find('div.swiper-button-prev').get(0)
            };
        }

        new Swiper($advantages.find('.swiper-container').get(0), slider_params);
    }

})(jQuery);
from django.db import models
from attachable_blocks.models import AttachableBlock
from django.utils.translation import ugettext_lazy as _


class AdvantagesBlock(AttachableBlock):
    BLOCK_VIEW = 'blocks.views.advantages_block_render'

    class Meta:
        verbose_name = _('Advantages block')
        verbose_name_plural = _('Advantages blocks')


class Advantage(models.Model):
    block = models.ForeignKey(AdvantagesBlock, verbose_name=_('block'), related_name='advantages')
    title = models.CharField(_('title'), max_length=128)

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('Advantage')
        verbose_name_plural = _('Advantages')
        ordering = ('sort_order', )

    def __str__(self):
        return self.title

from django.template import loader
from libs import jinja2
from ..models import ShopConfig, ShopProduct
from .. import conf
from webpage.utils import UsedBlocks


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'other_products', }
    takes_context = True

    def _other_products(self, context, product_id=None, classes='', template='shop/blocks/products.html'):
        products = ShopProduct.objects.visible()
        config = ShopConfig.get_solo()

        UsedBlocks.add_block(name='shopproducts')

        title = config.other_title
        cnt = conf.BLOCK_PRODUCTS_COUNT

        if product_id:
            cnt = 0
            category = products.get(id=product_id).category
            products = products.exclude(id=product_id).filter(category_id=category.id)

        if not cnt == 0:
            products = products[:cnt]

        return loader.render_to_string(template, {
            'classes': classes,
            'products': products,
            'title': title,
        }, request=context.get('request'))

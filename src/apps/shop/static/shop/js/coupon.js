/** @namespace window.ajax_views.shop.coupon.check */


(function ($) {
    var classes = {
        progress: 'sending',
        invalid: 'invalid',
        message: 'error-message'
    };
    var $form;
    var coupon_selector = 'id_coupon';

    $(document).ready(function () {
        $form = $('#formCoupon');
        var $coupon = $('#' + coupon_selector);

        $form.on('submit', function (e) {
            e.preventDefault();

            setCoupon($coupon);
        })

        $('.apply-coupon').on('click', function (e) {
            e.preventDefault();

            $form.trigger('submit');
        })
    });

    function setCoupon($coupon) {
        if (!($coupon.length === 0)) {
            var coupon = $coupon.val();

            // if (coupon !== '') {
            var data = [];
            data.push({
                name: 'coupon',
                value: coupon
            });

            $.ajax({
                url: window.ajax_views.shop.coupon.check,
                type: 'GET',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $form.find('.' + classes.invalid).removeClass(classes.invalid);
                    $form.find('.' + classes.message).remove();
                    $form.addClass(classes.progress);
                },
                success: function (response) {
                    $('.order-information').replaceWith(response.html);
                    window.$setOrder('.checkout');
                },
                error: $.parseError(function (response) {
                    response ? renderErrors(response) : alert(window.DEFAULT_AJAX_ERROR);
                }),
                complete: function () {
                    $form.removeClass(classes.progress);
                }
            })
            // }
        }
    }

    function renderErrors(response) {
        setTimeout(function () {
            $.each(response.errors, function () {
                var error = this;
                var $field = $form.find('div.' + error.fullname);
                $field.addClass(classes.invalid);
            })

        }, 300);
    }
})(jQuery);
(function ($) {
    var r = /^\$?[0-9]+(\.[0-9][0-9])?$/;

    var old_range_start = 0,
        old_range_end = 0

    $(document).ready(function () {
        old_range_start = $('#price_range_from').attr('min');
        old_range_end = $('#price_range_to').attr('max');
        // console.log(old_range_start, old_range_end);
        window.setPriceUi({
            elem: 'priceRangeSlider',
            from: '#price_range_from',
            to: '#price_range_to'
        });
    })

    function setValues(field, view) {
        var val = field.value;

        // r.test(val) ? old_range_end = val : this.value = old_range_end;

        switch (view) {
            case 'start':
                r.test(val) ? old_range_start = val : field.value = old_range_start;
                break;
            case 'end':
                r.test(val) ? old_range_end = val : field.value = old_range_end;
                break;
        }
    }

    window.setPriceUi = function (params) {
        _setUi(params);
    }
    function _setUi(params) {
        var priceRangeSlider = document.getElementById(params.elem);

        var $priceRangeFrom = $(params.from),
            $priceRangeTo = $(params.to);

        var minPrice = parseFloat($priceRangeFrom.attr('min')),
            maxPrice = parseFloat($priceRangeTo.attr('max'));

        old_range_start = minPrice;
        old_range_end = maxPrice;

        window.sl = noUiSlider.create(priceRangeSlider, {
            start: [Math.round(minPrice), Math.round(maxPrice)],
            step: 1,
            connect: true,
            range: {
                'min': [Math.round(minPrice)],
                'max': [Math.round(maxPrice)]
            }
        }).on('update', function (values) {
            var value_from = values[0],
                value_to = values[1];

            $priceRangeFrom.val(value_from);
            $priceRangeTo.val(value_to);

            window.updateParams('price', [values[0], values[1]]);
        });

        priceRangeSlider.noUiSlider.set([old_range_start, old_range_end]);

        $priceRangeFrom.on('change', function () {
            setValues(this, 'start');
            priceRangeSlider.noUiSlider.set([old_range_start, old_range_end]);
        });

        $priceRangeTo.on('change', function () {
            setValues(this, 'end');
            priceRangeSlider.noUiSlider.set([old_range_start, old_range_end]);
        });
    }
})(jQuery);
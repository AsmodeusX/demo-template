/** @namespace window.ajax_views.shop.payment.card */
/** @namespace window.ajax_views.shop.payment.change */
/** @namespace window.ajax_views.shop.payment.paypal */


(function ($) {
    var amount;

    $(document).ready(function () {
        var $cardNumber = $('[name="card_number"]'),
            $dateExpired = $('[name="date_expired"]'),
            $cvv = $('[name="cvv"]');

        var $tabs = $('.tab'),
            $pages = $('.tab-page');

        $cardNumber.mask("9999 9999 9999 9999", {
            placeholder: "###",
            autoclear: false
        });
        $dateExpired.mask("99/99", {
            placeholder: "MM/YY",
            autoclear: false
        });
        $cvv.mask("999", {
            placeholder: "XXX",
            autoclear: false
        });

        $tabs.on('click', function () {
            var $tab = $(this);
            var tab = $tab.find('.btn').data('payment-with');
            $pages.hide();

            var form_tab = tab;

            if (form_tab !== 'paypal') {
                form_tab = 'card';
            }

            $('.tab-page[data-tab="' + form_tab + '"]').show();
            $tabs.removeClass('active');
            $tab.addClass('active');
            change_payment(tab);
        });

        $tabs.eq(0).trigger('click');

        $(document).on('click', '#saveOrder', function (e) {
            e.preventDefault();
            var $form = $('#formCardData');
            var data = $form.serializeArray();
            var payment_system = $('.tabs .tab.active .btn').data('payment-with');

            data.push({
                name: 'type_payment',
                value: payment_system
            });

            $.ajax({
                url: window.ajax_views.shop.payment.card,
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    $form.find('.errorMessage').remove();
                },
                data: data,
                success: function (response) {
                    cleanCart();

                    if (response.redirect_url) {
                        window.location.href = response.redirect_url;
                    }
                },
                error: $.parseError(function (response) {
                    renderErrors($form, response);
                })
            });
        });

        loadPaypal();
    });

    // Осталось придумать как передавать цену на кнопку пейпала

    window.paypal_button = function () {
        return new Promise(function(resolve, reject) {
            amount = $('#payAmount').data('amount');

            paypal.Buttons({

                // Sets up the transaction when a payment button is clicked
                createOrder: function (data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: amount // Can reference variables or functions. Example: `value: document.getElementById('...').value`
                            }
                        }]
                    });
                },

                // Finalize the transaction after payer approval
                onApprove: function (data, actions) {
                    return actions.order.capture().then(function (orderData) {
                        $.ajax({
                            url: window.ajax_views.shop.payment.paypal,
                            type: 'post',
                            dataType: 'json',
                            data: orderData,
                            success: function (response) {
                                cleanCart();

                                if (response.redirect_url) {
                                    window.location.href = response.redirect_url;
                                }
                            }
                        });
                    });
                }
            }).render('#paypal-button-container');
        });
    }

    $(document).on('focus', 'input', function (e) {
        if ($(e.currentTarget).val() === '') {
            e.preventDefault;
            setTimeout(function () {
                $(e.currentTarget).caret(0, 0)
            }, 100)
        }
    })

    function loadPaypal() {
        var js, fjs = document.getElementsByTagName("script")[0];
        js = document.createElement("script");
        js.id = "paypal-script";
        js.src = "https://www.paypal.com/sdk/js?client-id=" + window.paypal_client_id + "&currency=USD";
        js.onload=window.paypal_button
        fjs.parentNode.insertBefore(js, fjs);
    }

    function change_payment(payment) {
        $.ajax({
            url: window.ajax_views.shop.payment.change,
            type: 'GET',
            data: [
                {
                    'name': 'payment',
                    'value': payment
                }
            ],
            dataType: 'json',
            success: function(response) {
                $('.order-information').replaceWith(response.html);
                window.$setOrder('.payment');
                amount = $('#payAmount').data('amount');
                var is_paypal = payment === 'paypal';
                $('#saveOrder').prop('disabled', is_paypal);
            }
        });
    }

    function renderErrors($form, response) {
        if (response.error || response.errors) {
            var $errorMessage = $('<div />').addClass('errorMessage');
            if (response.error) {
                $errorMessage.html(response.error);
            } else if (response.errors) {
                $errorMessage.html(response.errors[0].errors[0]);
            }
            $form.append($errorMessage);
        }
    }

    function cleanCart() {
        localStorage.setItem(window.cartStorageKey, null);
    }
})(jQuery);
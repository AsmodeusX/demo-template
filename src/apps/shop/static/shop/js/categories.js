(function ($) {
    var used_categories = [];
    var classActive = 'active';

    $(document).ready(function () {

        var $categories = $('.category');

        $categories.on('click', function () {
            var $category = $(this);
            var category = $category.find('button').data('category');

            if ($category.hasClass(classActive)) {
                var category_index = used_categories.indexOf(category);

                used_categories.splice(category_index, 1);
                $category.removeClass(classActive);
            } else {
                used_categories.push(category);
                $category.addClass(classActive);
            }

            window.updateParams('categories', used_categories);
            window.sendFilters();
        });
    });


})(jQuery);
(function ($) {
    var used_filters = [];
    var maxFiltersInIcon = 9;

    $(document).on('click', '.filtersClear', function () {
        $('.formFilters').each(function () {
            var $formFilters = $(this);
            $formFilters.get(0).reset();
            window.updateParams('filters', []);
            document.getElementById('priceRangeSlider').noUiSlider.set([$('#price_range_from').attr('min'), $('#price_range_to').attr('max')]);
            window.sendFilters();
        });
    })


    $(document).on('click', '.filter input[type="checkbox"]', function () {
        var $filters = $('.filter input[type="checkbox"]');
        used_filters = [];

        $filters.each(function () {
            var $filter = $(this);
            if ($filter.prop('checked'))
                used_filters.push($filter.val());
        });

        window.updateParams('filters', used_filters);
    });

    window.updateCountFilters = function () {
        var $filtersIcon = $('.panel-filters__icon'),
            countFilters = window.queryParameters['filters'].length,
            val = countFilters === 0 ? '' : countFilters > maxFiltersInIcon ? maxFiltersInIcon + '+' : countFilters.toString();
        $filtersIcon.attr('data-count', val);
    }

})(jQuery);
(function ($) {
    window.$setOrder = function (parent) {
        var $orderInfo = $('#orderInfo'),
            $header = $('header'),
            $parent = $(parent);

        if ($orderInfo.length > 0) {
            $orderInfo.sticky({
                blockSelector: $parent,
                topOffset: $header.innerHeight(),
                minEnabledWidth: 1024
            });
        }
    }
})(jQuery);
/** @namespace window.ajax_views.shop.city.filter */
/** @namespace window.ajax_views.shop.delivery.change */
/** @namespace window.ajax_views.shop.deliveries.update */
/** @namespace window.ajax_views.shop.order_info */
/** @namespace window.ajax_views.shop.order.save */
/** @namespace window.ajax_views.shop.set_address */


(function ($) {
    var city_id, country_id, zip_code;
    var animateTime = 1; // Seconds

    var classes = {
        progress: 'sending',
        invalid: 'invalid',
        message: 'error-message'
    }
    var classCity = '.field-city';
    var $checkoutForm;

    $(document).ready(function () {
        var $country = $('#id_country');
        country_id = $country.val();
        city_id = $('#id_city').val();

        $('[name="zip_code"]').on('input', function (e) {
            e.preventDefault();
            var v = $(e.currentTarget).val();
            if (v.length >= 5) {
                zip_code = v;
                setAddress(country_id, city_id);
                updateDeliveryMethods();
                updateOrderInfo();
            }
        });

        window.$setOrder('.checkout');
        var $city = $(classCity);

        $city.find('select').selectmenu({
            appendTo: $city.find('.control'),
            create: function (event, ui) {
                city_id = event.target.value;
                setAddress(country_id, city_id);
            },
            change: function (_, ui) {
                city_id = ui.item.value;
                setAddress(country_id, city_id);
                updateDeliveryMethods();
                updateOrderInfo();
            }
        })

        $country.selectmenu({
            appendTo: $country.closest('.control'),
            create: function( event, ui ) {
                country_id = event.target.value;
                $.ajax({
                    url: window.ajax_views.shop.city.filter,
                    method: 'GET',
                    data: {
                        country: country_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        $city.find('select').selectmenu("destroy");
                        $city.replaceWith(response.html);
                        $city = $(classCity);
                        $city.find('select').selectmenu({
                            appendTo: $city.find('.control'),
                            create: function (event, ui) {
                                city_id = event.target.value;
                                setAddress(country_id, city_id);
                            },
                            change: function (_, ui) {
                                city_id = ui.item.value;
                                setAddress(country_id, city_id);
                                updateDeliveryMethods();
                                updateOrderInfo();
                            }
                        });
                        setAddress(country_id, city_id);
                        updateDeliveryMethods();
                    }
                })

                updateOrderInfo();
            },
            change: function(event, ui) {
//                     if ($(event.target).attr('id') === 'id_country') {
                country_id = ui.item.value;
                $.ajax({
                    url: window.ajax_views.shop.city.filter,
                    method: 'GET',
                    data: {
                        country: country_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        var $city = $(classCity);
                        $city.find('select').selectmenu("destroy");
                        $city.replaceWith(response.html);
                        $city = $(classCity);

                        $city.find('select').selectmenu({
                            appendTo: $city.find('.control'),
                            create: function (event, ui) {
                                city_id = event.target.value;
                                setAddress(country_id, city_id);
                            },
                            change: function (_, ui) {
                                city_id = ui.item.value;
                                setAddress(country_id, city_id);
                                updateDeliveryMethods();
                                updateOrderInfo();
                            }
                        });
                        updateOrderInfo();
                    }
                })

                setAddress(country_id, city_id);
                updateDeliveryMethods();
                updateOrderInfo();
//                     } else if ($(event.target).attr('id') === 'id_city') {
//                         setAddress($('#id_country').val(), ui.item.value);
//                         updateDeliveryMethods();
//                         updateOrderInfo();
//                     })
            }
        });
    });
//
    $(document)
        .on('click', '#saveOrder', saveOrder)
        .on('click', 'li.delivery input', function (e) {
            $checkoutForm = $('#formOrder');
            var data = $checkoutForm.serializeArray();
            var $delivery = $(e.target);

            data.push({
                name: 'delivery',
                value: $delivery.val()
            });

            data.push({
                name: 'city',
                value: city_id
            });

            data.push({
                name: 'country',
                value: country_id
            });

            data.push({
                name: 'zip_code',
                value: zip_code
            });

            $.ajax({
                url: window.ajax_views.shop.delivery.change,
                type: 'GET',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $checkoutForm.addClass(classes.progress);
                    $.preloader();
                },
                success: function(response) {
                    if (response.html) {
                        $('.order-information').replaceWith(response.html);
                        window.$setOrder('.checkout');

                        updateOrderInfo();
                    }
                },
                error: $.parseError(function(response) {
                    response ? renderErrors(response) : alert(window.DEFAULT_AJAX_ERROR);
                }),
                complete: function () {
                    $checkoutForm.removeClass(classes.progress);
                    if ($.popup() !== null) {
                        $.popup().hide();
                    }
                }
            });
        });

    function saveOrder () {
        $checkoutForm = $('#formOrder');
        if ($checkoutForm.hasClass(classes.progress)) return false;
        var data = $checkoutForm.serializeArray();

        $.ajax({
            url: window.ajax_views.shop.order.save,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $checkoutForm.find('.' + classes.invalid).removeClass(classes.invalid);
                $checkoutForm.find('.' + classes.message).remove();
                $checkoutForm.addClass(classes.progress);
                $.preloader();
            },
            success: function(response) {
                if (response.redirect_url) {
                    window.location.href = response.redirect_url;
                }
            },
            error: $.parseError(function(response) {
                $.popup().hide();
                response ? renderErrors(response) : alert(window.DEFAULT_AJAX_ERROR);
            }),
            complete: function () {
                $checkoutForm.removeClass(classes.progress);
            }
        });
    }

    function renderErrors(response) {
        setTimeout(function () {
            $.each(response.errors, function (index) {
                var error = this;
                var $field = $checkoutForm.find('div.' + error.fullname),
                    $error_message = $('<span>')
                        .addClass(classes.message)
                        .html(error.errors[0]);
                $field.addClass(classes.invalid);
                $field.find('div.control').append($error_message);
                if (index === 0) $.scrollTo($field.offset().top - $('header').innerHeight(), animateTime * 1000);
            })

        }, 300);
    }

    function updateDeliveryMethods () {
        var data = [];

        data.push({
            name: 'country',
            value: country_id
        })

        data.push({
            name: 'city',
            value: city_id
        })

        data.push({
            name: 'zip_code',
            value: zip_code
        })

        $.ajax({
            url: window.ajax_views.shop.deliveries.update,
            type: 'GET',
            data: data,
            dataType: 'json',
            success: function(response) {
                $('.form__section--delivery').replaceWith(response.html);
                setDeliveries();
            },
            error: $.parseError(function(response) {
                response ? renderErrors(response) : alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    }

    function updateOrderInfo() {
        var $orderInfo = $('.order-information');

        var data = [
            {
                name: 'use_coupon',
                value: false
            },
            {
                name: 'is_save',
                value: true
            }
        ]

        if ($orderInfo.length) {
            return $.ajax({
                url: window.ajax_views.shop.order_info,
                type: 'GET',
                dataType: 'json',
                data: data,
                success: function(response) {
                    $('.order-information').replaceWith(response.html);
                    window.$setOrder('.checkout');
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        }
    }

    function setAddress() {
        var data = [];

        data.push({
            name: 'city',
            value: city_id
        });

        data.push({
            name: 'country',
            value: country_id
        });

        data.push({
            name: 'zip_code',
            value: zip_code
        });

        return $.ajax({
            url: window.ajax_views.shop.set_address,
            type: 'GET',
            dataType: 'json',
            data: data,
            success: function (response) {
                updateOrderInfo();
                // $('.order-information').replaceWith(response.html);
                // window.$setOrder('.checkout');
            },
            error: $.parseError(function () {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });

    }


    function setDeliveries() {
        $('li.delivery:first-of-type').trigger('click');
    }

})(jQuery);
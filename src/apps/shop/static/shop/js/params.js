/** @namespace window.ajax_views.shop.filter */


(function ($) {
    window.queryParameters = {};

    window.sendFilters = function (page) {
        var isReplace = page === undefined;

        window.queryParameters['page'] = !isReplace ? page : 1;
        $.ajax({
            url: window.ajax_views.shop.filter,
            type: 'get',
            data: window.queryParameters,
            dataType: 'json',
            success: function (response) {
                var $btnPaginator = $('.shop .btn--paginator')
                if ($btnPaginator.length > 0) $btnPaginator.parent().remove();

                if (response) {
                    var $container = $('.products');
                    if (!$container.length) $container = $('.empty-page');

                    isReplace ? $container.html(response.html) : $container.append(response.html);
                }

                var $btnWrapper = $('<div class="btn-wrapper">');

                $('.shop .container-products').append(
                    $btnWrapper.append(
                        response.more_button,
                    ),
                );
            },
            error: $.parseError(function (response) {
                alert("Can't change view params");
            })
        });
    }
    window.updateParams = function(view, params) {
        window.queryParameters[view] = params;
    }
})(jQuery);
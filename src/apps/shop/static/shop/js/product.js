/** @namespace window.ajax_views.shop.product.get_modification */


(function($) {
    var elem = '.page--product',
        clActiveTab = 'swiper-slide-active';
    var $countNumber;
    var $buyProduct;

    var
        slider_config = {
            pagination: true,
            navigation: true
        },
        slider_params = {
            autoHeight: true,
            slidesPerView: $.winWidth() >= 768 ? 1 : 'auto',
            spaceBetween: 20,
            watchSlidesVisibility:true,
            watchOverflow: true,
            preloadImages: false,
            lazy: true
        };

    $(document).ready(function () {
        var product_id = $('#product').data('product');
        window.MAX_COUNT = parseInt($('#stock_count').html());
        $countNumber = $('.count__number');
        $buyProduct = $('#buyProduct');

        setCartBtn(product_id, '');
        checkBuyAllow(product_id, '');

        var $page = $(elem).eq(0),
            $formProductModification = $('#formProductModification'),
            $modification = $formProductModification.find('select[name^="modification"]');

        $modification.each(function () {
            $modification.selectmenu({
                appendTo: $modification.closest('.control'),
                change: function( event, ui ) {
                    var modification = ui.item.value;
                    getProductModification(modification);
                }
            });
        });

        setGallerySwiper($page);
        setTabs();
    });

    function setGallerySwiper($page) {
        var $gallery = $page.find('.product__gallery');

        if (slider_config.pagination) {
            slider_params["pagination"] = {
                el: $gallery.find('.swiper-pagination').get(0),
                type: 'bullets',
                clickable: true,
                dynamicBullets: true
            };
        }

        if (slider_config.navigation) {
            slider_params["navigation"] = {
                nextEl: $gallery.find('div.swiper-button-next').get(0),
                prevEl: $gallery.find('div.swiper-button-prev').get(0)
            };
        }

        new Swiper($gallery.find('.swiper-container').get(0), slider_params);
    }

    function setTabs() {
        var $tabsNames = $('.tabs__names'),
            $tabsPages = $('.tabs__pages');

        var tabs_names = new Swiper($tabsNames.get(0), {
            spaceBetween: 44,
            autoHeight: true,
            // freeMode: true,
            slidesPerView: 'auto',
            slidesPerGroup: 1,
            allowTouchMove: true,
            slideToClickedSlide: true,

        }).on('transitionEnd', function (x) {
                var index = x.isEnd ? x.activeIndex : x.clickedIndex;
                index = !!x.clickedIndex ? x.clickedIndex : x.activeIndex;
                changeSlide($tabsNames, tabs_names, index, tabs_pages)
            }
        );
        tabs_names.slides.each(function(_, index){
            var $slide = $(this);

            $slide.on("click", function() {
                changeSlide($tabsNames, tabs_names, index, tabs_pages)
            });
        });

        var tabs_pages = new Swiper($tabsPages.get(0), {
            spaceBetween: 0,
            autoHeight: false
        }).on('slideChange', function (event) {
            tabs_pages.updateAutoHeight();
            tabs_pages.updateSize();
            changeSlide($tabsNames, tabs_names, event.activeIndex, tabs_pages);
        });

        $(tabs_names.slides).eq(0).trigger('click');
    }

    function changeSlide($tabsNames, tabs_names, index, tabs_pages) {
        var $tabsNamesSlides = $(tabs_names.slides),
            $tabsPagesSlides = $(tabs_pages.slides);

        tabs_pages.slideTo(index, 500, false);
        tabs_names.slideTo(index, 300, false);

        $tabsNamesSlides.removeClass(clActiveTab);
        $tabsNamesSlides.eq(index).addClass(clActiveTab);
        $tabsPagesSlides.removeClass(clActiveTab);
        $tabsPagesSlides.eq(index).addClass(clActiveTab);

        setTimeout(function () {
            var $tabs_name = $tabsNamesSlides.eq(index);
            document.documentElement.style.setProperty('--tab-width', Math.round($tabs_name.innerWidth()).toString() + 'px');
            document.documentElement.style.setProperty('--tab-left', Math.round($tabs_name.offset().left - $tabsNames.offset().left).toString() + 'px');
        }, 300)

    }

    function getProductModification(modification) {
        $.ajax({
            url: window.ajax_views.shop.product.get_modification,
            type: 'get',
            dataType: 'json',
            data: [
                {
                    name: 'product',
                    value: $('#product').data('product')
                },
                {
                    name: 'modification',
                    value: modification
                }
            ],
            success: function (response) {
                var $product = $('#product');
                var product_id = $product.data('product');

                window.MAX_COUNT = parseInt(response.max_count);

                $product.find('.product__sku').html(response.sku);
                $product.find('.count__stock').html(response.max_count + ' pcs in stock');
                $product.find('.product__price').replaceWith(response.price_html);
                $buyProduct = $('#buyProduct');

                $buyProduct.length ? $buyProduct.replaceWith(response.buy_product) : $('#productInfo').append(response.buy_product);

                setCartBtn(product_id, modification);
                checkBuyAllow(product_id, modification);
            }
        });
    }

    function checkBuyAllow(productId, modification) {
        var currentProductCount = window.getCountByProductModification(productId, modification || '');
        if (currentProductCount >= window.MAX_COUNT) $('#buyProduct').remove();
    }

    function setCartBtn(product_id, modification_id) {
        var in_cart = window.checkProductInCart(product_id, modification_id);
        if (in_cart) {
            var currentProductCount = window.getCountByProductModification(product_id, modification_id || '');

            $('#product .btn-wrapper .btn').addClass('cart--visible');
            $('#btnCountCart').html(currentProductCount);
        }
        window.$updateCount();
    }

})(jQuery);
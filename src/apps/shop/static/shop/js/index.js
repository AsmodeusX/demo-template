/** @namespace window.ajax_views.shop.popups.filters */
/** @namespace window.ajax_views.shop.popups.sort */

(function($) {

    var mobileWidth = 768;
    var animationTime = 0.5; //Seconds

    $(document).ready(function() {
        var $filters = $('.filter'),
            $btnPopupSort = $('#showPopupSort'),
            $btnPopupFilters = $('#showPopupFilters');
        var elem = '.shop';

        $filters.addClass('visible');

        setFilters();

        $btnPopupSort.on('click', function () {
            var $sort = $.winWidth() < mobileWidth ? $('.panel-sort__criteria') : $('.sort-criterion.active');

            $.ajax({
                url: window.ajax_views.shop.popups.sort,
                type: 'GET',
                data: {
                    'sort': $sort.data('sort')
                },
                dataType: 'json',
                success: function(response) {
                    $.popup({
                        classes: 'sort-popup',
                        content: response.html
                    }).show()
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        });

        $btnPopupFilters.on('click', function () {
            $.ajax({
                url: window.ajax_views.shop.popups.filters,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    $.popup({
                        classes: 'filters-popup',
                        content: response.html

                    }).on('after_show', function () {
                        setFilters();
                        window.setPriceUi({
                            elem: 'popuppriceRangeSlider',
                            from: '#popup-price_range_from',
                            to: '#popup-price_range_to'
                        })
                    }).show();
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        });

        $(document).on('click', elem + ' .btn--paginator', function() {
            var $btn = $(this);
            // loadItems($btn);
            var page = $btn.data('page');
            window.sendFilters(page);

            return false;
        });

        $(document).on('click', '.showResults',  function () {
            window.sendFilters();
        })
    });

    function toggleForm($filterForm, $filter, time) {
        $filterForm.slideToggle(time);
        $filter.toggleClass('visible');
    }

    function setFilters() {
        $('.filter').each(function (index) {
            var $filter = $(this),
                $filterName = $filter.find('.filter__name'),
                $filterForm = $filter.find('.filter__form');

            $filterName.on('click', function () {
                toggleForm($filterForm, $filter, animationTime * 1000);
            });

            if (!(index === 0)) toggleForm($filterForm, $filter, 0);
        });

        var filters = window.queryParameters['filters'];

        $.each(filters, function (index) {
            var $characteristics = $('[name="popup-characteristic"]');
            $characteristics.each(function () {
                var $characteristic = $(this);
                if ($characteristic.val() === filters[index]) {
                    $characteristic.prop('checked', true);
                }
            })
        })
    }

})(jQuery);
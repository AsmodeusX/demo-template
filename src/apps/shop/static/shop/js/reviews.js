/** @namespace window.ajax_views.shop.more_reviews */


(function($) {
    var elem = '.block--reviews';

    $(document).on('click', elem + ' .btn--paginator', function() {
        var $btn = $(this);
        loadItems($btn);

        return false;
    });

    function loadItems($btn) {
        var page = $btn.data('page'),
            arr = location.pathname.split('/'); //Делаем массив, разбив путь по элементам.

        return $.ajax({
            url: window.ajax_views.shop.more_reviews,
            type: 'GET',
            data: {
                'page': page,
                'product': arr[arr.length - 2] //Предпоследний элемент, т. к. пустая строка после / тоже учитывается. -2 т.к. нумерация с 0
            },
            dataType: 'json',
            success: function(response) {
                var $elem = $(elem).find('.reviews'),
                    $wrapper = $btn.closest('.btn-wrapper');

                $elem.append(response.html);

                if (response.more_button)
                    $wrapper.replaceWith(response.more_button);
                else
                    $wrapper.remove();
            },
            error: $.parseError(function() {
                alert(window.DEFAULT_AJAX_ERROR);
            })
        });
    }

})(jQuery);

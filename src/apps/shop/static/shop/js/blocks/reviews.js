(function($) {
    var documentStyle = document.documentElement.style;
    var reviews_count = {
        desktop: Number(documentStyle.getPropertyValue('--reviews-count')) || 3,
        tablet: Number(documentStyle.getPropertyValue('--reviews-count-tablet')) || 2,
        mobile: Number(documentStyle.getPropertyValue('--reviews-count-mobile')) || 1
    }

    var sizes = {
        tablet: 1024,
        mobile: 768
    }

    var slider_config = {
        pagination: true,
        navigation: true
    }

    var slider_params = {
        spaceBetween: 20,
        threshold: 10,
        watchSlidesVisibility: true,
        watchOverflow: true
    };

    var elem = '.block--reviews';

    $(window).on('load', function() {
        var $blocks = $(elem);

        $blocks.each(function () {
            var $block = $(this);
            setSwiper($block);
        });
    });

    function setSwiper($block) {
        var w = $.winWidth();
        var $reviews = $block.find('.reviews');

        slider_params['slidesPerView'] = w >= sizes.tablet ? reviews_count.desktop : w >= sizes.mobile ? reviews_count.tablet : reviews_count.mobile;

        if (slider_config.pagination) {
            slider_params["pagination"] = {
                el: $block.find('.swiper-pagination').get(0),
                type: 'progressbar'
            };
        }

        if (slider_config.navigation) {
            slider_params["navigation"] = {
                nextEl: $block.find('div.swiper-button-next').get(0),
                prevEl: $block.find('div.swiper-button-prev').get(0)
            };
        }
        new Swiper($reviews.find('.swiper-container').get(0), slider_params);
    }

})(jQuery);
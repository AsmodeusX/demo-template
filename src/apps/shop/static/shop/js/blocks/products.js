(function($) {
    var documentStyle = document.documentElement.style;
    var elem = '.block--products';

    var products_count = {
        desktop: Number(documentStyle.getPropertyValue('--products-count')) || 4,
        tablet: Number(documentStyle.getPropertyValue('--products-count-tablet')) || 2,
        mobile: Number(documentStyle.getPropertyValue('--products-count-mobile')) || 1
    }

    var sizes = {
        tablet: 1024,
        mobile: 768
    }

    var slider_config = {
        pagination: true,
        navigation: true
    }

    var slider_params = {
        spaceBetween: 20,
        threshold: 10,
        watchSlidesVisibility: true,
        watchOverflow: true
    };

    $(window).on('load', function() {
        var $blocks = $(elem);

        $blocks.each(function () {
            var $block = $(this);
            setSwiper($block);
        });
    });

    function setSwiper($block) {
        var w = $.winWidth();
        var $products = $block.find('.products');

        slider_params['slidesPerView'] = w >= sizes.tablet ? products_count.desktop : w >= sizes.mobile ? products_count.tablet : products_count.mobile;

        if (slider_config.pagination) {
            slider_params["pagination"] = {
                el: $block.find('.swiper-pagination').get(0),
                type: 'progressbar'
            };
        }

        if (slider_config.navigation) {
            slider_params["navigation"] = {
                nextEl: $block.find('div.swiper-button-next').get(0),
                prevEl: $block.find('div.swiper-button-prev').get(0)
            };
        }

        new Swiper($products.find('.swiper-container').get(0), slider_params);
    }

})(jQuery);
/** @namespace window.ajax_views.shop.cart.add */
/** @namespace window.ajax_views.shop.cart.delete */
/** @namespace window.ajax_views.shop.cart.update */
/** @namespace window.ajax_views.shop.order_info */


(function($) {
    var $countNumber,
        $product,
        productId,
        MIN_COUNT = 1;

    $(document)
        .ready(function () {
            $countNumber = $('.count__number');
            $product = $('#product');

            $('.product').each(function () {
                var $this = $(this);
                var product_id = $this.data('product'),
                    modification_id = $this.data('modification'),
                    currentProductCurrent = window.getCountByProductModification(product_id, modification_id || '');

                window.MAX_COUNT = parseInt($this.data('max-count'));

                if (currentProductCurrent === window.MAX_COUNT) {
                    // $this.find('.btn-wrapper').remove();
                }
            });

            if ($('#content .cart').length > 0) {
                window.$setOrder('.cart');
            }

            if ($product.length) {
                window.MAX_COUNT = parseInt($product.data('max-count'));
                productId = $product.data('product');
                checkBuyAllow(productId);
            }
        })

        .on('change', '.row .count__number', function () {
            var $product = $(this).closest('.row');
            productId = parseInt($product.data('product'));
            window.MAX_COUNT = parseInt($product.data('max-count'));

            var $cur = $product.find('.count__number'),
                modification = parseInt($product.data('modification')) || '',
                count = isNumeric($cur.val()) ? parseInt($cur.val()) : 0;

            count = count > window.MAX_COUNT ? window.MAX_COUNT : count < MIN_COUNT ? 0 : count;
            if ($cur) $cur.val(count);

            if (count === 0) count = 1;

            count = count > window.MAX_COUNT ? window.MAX_COUNT : count < MIN_COUNT ? 0 : count;
            updateProduct(productId, modification, count);
        })
        .on('click', '.count__action--minus', function () {
            var $product = $(this).closest('.row');
            if (!($product).length) $product = $('#product');
            productId = parseInt($product.data('product'));

            window.MAX_COUNT = parseInt($product.data('max-count'));

            var $cur = $product.find('.count__number');
            var modification = parseInt($product.data('modification')) || '';
            var val = isNumeric($cur.val()) ? parseInt($cur.val()) : 0;
            var count = val - 1;

            if (count === 0 && $product.length) count = 1;
            if ($product.hasClass('row')) {
                count = count > window.MAX_COUNT ? window.MAX_COUNT : count < MIN_COUNT ? 1 : count;
                $cur.val(count);
                updateProduct(productId, modification, count);
            } else {
                setCountValue($cur, count, productId, modification);
            }
        })
        .on('click', '.count__action--plus', function () {
            var $product = $(this).closest('.row');
            if (!($product).length) $product = $('#product');
            productId = parseInt($product.data('product'));

            window.MAX_COUNT = parseInt($product.data('max-count'));
            var $cur = $product.find('.count__number');

            var modification = parseInt($product.data('modification')) || '';
            var val = isNumeric($cur.val()) ? parseInt($cur.val()) : 0;
            var count = val + 1;
            if ($product.hasClass('row')) {
                count = count > window.MAX_COUNT ? window.MAX_COUNT : count < MIN_COUNT ? 1 : count;
                $cur.val(count);
                updateProduct(productId, modification, count);
            } else {
                setCountValue($cur, count, productId, modification);
            }
        })
        .on('change', '#product .count__number', function (e) {
            var $cur = $(e.target);


            $product = $('#product');
            productId = parseInt($product.data('product'));

            var productModification = parseInt($product.data('modification')) || '';

            var $modification = $('#modification');
            if ($modification.length) {
                productModification = parseInt($modification.val()) || '';
            }
            var count = isNumeric($cur.val()) ? parseInt($cur.val()) : 0;
            setCountValue($cur, count, productId, productModification);
        })
        .on('click', '.add-product', function(e) {
            e.preventDefault();

            if ($(e.target).hasClass('btn__cart')) {
                window.location.href = $(e.target).attr('href');
                return false;
            }

            var $btn = $(this);

            var productId = parseInt($btn.data('id'));
            var count = 1;
            var $count;
            var $product = $('#product');
            if ($product.length) {
                $count = $product.find('.count__number');
                count = parseInt($count.val());
                window.MAX_COUNT = parseInt($product.data('max-count'));
                var productModification = $('#modification').val();
            } else if ($btn.closest('.page').length) {
                var $countNumber = $btn.closest('.page').find('.count__number');
                count = parseInt($countNumber.val());
                $countNumber.val(1);
            } else if ($btn.closest('.product-wrapper').length) {
                $product = $btn.closest('.product-wrapper').find('.product');
                count = 1;
                window.MAX_COUNT = parseInt($product.data('max-count'));
            } else if ($btn.closest('.count').length) {
                $count = $btn.closest('.count').find('.count__number');
                count = parseInt($count.val());
            }
            count = setCountValue(undefined, count, productId, productModification);

            if (!isNaN(productId)) {
                addToCart(productId, productModification, count);
            }
            $btn.addClass('cart--visible');

            return false;
        })
        .on('click', '.update-product', function(e) {

        })
        .on('click', '.delete-product', function(e) {
            e.preventDefault();
            var productId = parseInt($(this).data('id'));
            var $row = $(this).closest('.row');
            var productModification = $row.data('modification');

            if (!isNaN(productId)) {
                deleteProduct(productId, productModification);
            }
            return false;
        });

    function isNumeric(value) {
        return /^-{0,1}\d+$/.test(value);
    }

    function addToCart(productId, productModification, count) {
        count = typeof count === 'undefined' ? 1 : parseInt(count) || 0;
        return $.ajax({
            url: window.ajax_views.shop.cart.add,
            method: 'POST',
            data: {
                product: productId,
                count: count,
                modification: productModification || ''
            },
            dataType: 'json',
            success: function(response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();
                count = setCountValue(null, 1, productId, productModification);
                var n = count < parseInt($('#stock_count').html()) ? 1 : 0;
                $('.count__number').val(n);

                checkBuyAllow(productId, productModification || '');
            },
            error: $.parseError(function(response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();

                if (response.msg) {
                    alert(response.msg)
                }
            })
        })
    }

    function updateProduct(productId, productModification, count) {
        count = typeof count === 'undefined' ? 1 : parseInt(count) || 0;

        return $.ajax({
            url: window.ajax_views.shop.cart.update,
            method: 'POST',
            data: {
                product: productId,
                count: count,
                modification: productModification || ''
            },
            dataType: 'json',
            success: function(response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();
                updateOrderInfo();
                checkBuyAllow(productId, productModification);
            },
            error: $.parseError(function(response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();
                updateOrderInfo();
                checkBuyAllow(productId, productModification);

                if (response.msg) {
                    alert(response.msg)
                }
            })
        })
    }

    function deleteProduct(productId, productModification) {

        return $.ajax({
            url: window.ajax_views.shop.cart.delete,
            method: 'POST',
            data: {
                product: productId,
                modification: productModification || ''
            },
            dataType: 'json',
            success: function(response) {
                $('.delete-product[data-id="' + productId + '"]').closest('.row[data-modification="' + productModification + '"]').remove();
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();
                updateOrderInfo();

                if (response.is_empty) window.location.reload()
            },
            error: $.parseError(function(response) {
                if (response.msg) {
                    alert(response.msg)
                }
            })
        })
    }

    function setCountValue($cur, count, product_id, modification) {
        var currentCountProducts = window.getCountByProductModification(product_id, modification || '');
        count = (count + currentCountProducts) >= window.MAX_COUNT ? window.MAX_COUNT - currentCountProducts : count < MIN_COUNT ? 0 : count;

        if ($cur) $cur.val(count);
        return count
    }

    function updateOrderInfo() {
        var $orderInfo = $('.order-information');
        if ($orderInfo.length) {
            return $.ajax({
                url: window.ajax_views.shop.order_info,
                type: 'GET',
                dataType: 'json',
                data: [
                    {
                        name: 'use_coupon',
                        value: true
                    }
                ],
                success: function(response) {
                    $orderInfo.replaceWith(response.html);
                    window.$setOrder('.cart');
                },
                error: $.parseError(function() {
                    alert(window.DEFAULT_AJAX_ERROR);
                })
            });
        }
    }

    function checkBuyAllow(productId, modification) {
        if (!(modification)) {
            modification = '';
        }

        var currentProductCurrent = window.getCountByProductModification(productId, modification || '');
        var is_max = currentProductCurrent === window.MAX_COUNT
        if (is_max) $('#buyProduct').remove();

        return is_max
    }

})(jQuery);

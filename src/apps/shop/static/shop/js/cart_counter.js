/** @namespace window.ajax_views.cart.clear */


(function ($) {
    window.cartStorageKey = 'cart'; //Название переменной в localStorage
    window.maxCountCounter = 9; // Это в кругляшке максимальное число, после которого будет добавляться "+"

    window.$updateCount = function () {
        checkCart();
    }

    $(document).ready(function () {
        window.$updateCount();
    });

    $(document).on("localDataStorage", function () {
        // console.log('******************');
        window.$updateCount();
        // console.log('******************');
    });

    $(window).on("storage", function () {
        // console.log('###################');
        window.$updateCount();
        // console.log('###################');
    });

    // setInterval(checkCookies, 1000);

    window.getProductsCount = _getCount();
    window.getCountByProductModification = function (product_id, modification) {
        return _getCountByProductModification(product_id, modification);
    }

    window.checkProductInCart = function (product_id, modification) {
        var count = _getCountByProductModification(product_id, modification);
        return count > 0;
    }

    // function checkCookies() {
    //     if (!($.cookie('sessionid') === oldSessionId)) {
    //         clearCart();
    //         oldSessionId = $.cookie('sessionid');
    //     }
    // }

    function clearCart() {
        $.ajax({
            url: window.ajax_views.shop.cart.clear,
            type: 'GET',
            dataType: 'json',
            data: [{
                name: 'page',
                value: window.location.pathname
            }],
            success: function (response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
                window.$updateCount();

                if (response.is_reload) {
                    window.location.reload()
                }
            }
        });
        // return false;
    }

    // window.clearCart = clearCart();


    function _getCount() {
        var storage = localStorage.getItem(window.cartStorageKey);
        var count = 0;
        if (storage === null) clearCart();
        if (!(storage === 'undefined' || storage === null || storage === 'null')) {
            var products = JSON.parse(localStorage.getItem(window.cartStorageKey));
            Object.entries(products).forEach(function (entry) {
                var modifications = entry[1];
                Object.entries(modifications).forEach(function (entry) {
                    count += entry[1];
                })
            })
        }

        return count;
    }
    function _getCountByProductModification(product_id, modification_id) {
        var storage = localStorage.getItem(window.cartStorageKey);
        var d = {};

        if (!(storage === 'undefined' || storage === null || storage === 'null')) {
            var products = JSON.parse(localStorage.getItem(window.cartStorageKey));
            Object.entries(products).forEach(function (entry) {

                var productId = entry[0];
                var modifications = entry[1];
                Object.entries(modifications).forEach(function (entry) {
                    var modificationId = entry[0];
                    if (!(d[productId])) d[productId] = {};
                    d[productId][modificationId] = entry[1];
                })
            })
        }

        var n = 0;
        if (product_id in d) {
            n = d[product_id][modification_id] ? d[product_id][modification_id] : 0
        }

        return n;
    }

    function checkCart() {
        var storage = localStorage.getItem(window.cartStorageKey),
            cart_correct = !(storage === 'undefined' || storage === null || storage === 'null');

        if (!cart_correct) {
            get_session_backend();
        }

        verify_session_backend();
    }

    function get_session_backend() {
        return $.ajax({
            url: window.ajax_views.shop.get_session_backend,
            method: 'GET',
            dataType: 'json',
            success: function(response) {
                localStorage.setItem(window.cartStorageKey, response.cart);
            },
            error: $.parseError(function(response) {
                if (response.msg) {
                    alert(response.msg)
                }
            })
        })
    }

    function verify_session_backend() {
        return $.ajax({
            url: window.ajax_views.shop.verify_session_backend,
            method: 'GET',
            dataType: 'json',
            data: {
                'data': localStorage.getItem(window.cartStorageKey)
            },
            success: function(response) {
                if (!response.result) {
                    localStorage.setItem(window.cartStorageKey, response.cart);
                }
            },
            error: $.parseError(function(response) {
                if (response.msg) {
                    alert(response.msg)
                }
            }),
            complete: function () {
                setCount();
            }
        })
    }

    function setCount() {
        var count = _getCount();
        var $counterElems = $('.cart__icon');
        var $counterBtnElems = $('#btnCountCart');

        if (count === 0) {
            $counterElems.each(function () {
                var $counterElem = $(this);
                $counterElem.removeAttr('data-count');
            })
        } else {
            var c = count > 999 ? '999+' : count;
            $counterBtnElems.html(c);
            $counterElems.attr('data-count', '');
        }
    }
})(jQuery);
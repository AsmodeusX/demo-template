(function ($) {
    var used_sort = '',
        classActive = 'active',
        classCriterion = '.sort-criterion',
        sortAttr = 'sort';

    $(document).on('click', classCriterion,  function () {

        var $criterion = $(this),
            $criteria = $('.panel-sort__criteria');

        if (!$criterion.hasClass(classActive)) {
            used_sort = $criterion.data(sortAttr);
            var $relatedCriteria = $(classCriterion + "[data-sort=" + used_sort + "]");

            $(classCriterion).removeClass(classActive);
            $relatedCriteria.addClass(classActive);
            $criteria.text($criterion.text());
            $criteria.data(sortAttr, $criterion.data(sortAttr));
        } else {
            used_sort = '';
            $criteria.html('Sort criterion').data(sortAttr, '');
            $criterion.removeClass(classActive);
        }

        window.updateParams('sort', used_sort);
        window.sendFilters();
    })

})(jQuery);
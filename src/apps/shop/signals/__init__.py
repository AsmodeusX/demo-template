from django.dispatch import Signal


shop_order_shipped = Signal(providing_args=[
    'request', 'order'
])
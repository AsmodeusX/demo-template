from . import conf
from shipstation.models import ShipStationConfig, Weight
from libs.valute_field.valute import Valute
from libs.get_city import get_city
from .models import ShopProduct, Coupon, ShopProductModification, ShopOrder
from decimal import Decimal
from taxjar_payments.utils import get_order_tax
import json

class Cart:
    __slots__ = ('request', '_data', 'coupon', 'delivery', 'address', 'uuid')

    def __init__(self, request):
        self.request = request
        session_data = self.request.session.get(conf.SESSION_CART_NAME) or {}
        self.coupon = self.request.session.get(conf.SESSION_COUPON_NAME) or None
        self.delivery = json.loads(self.request.session.get(conf.SESSION_DELIVERY_NAME)) if isinstance(self.request.session.get(conf.SESSION_DELIVERY_NAME), str) else None
        self.uuid = self.request.session.get(conf.SESSION_UUID_NAME) or None
        self.address = self.request.session.get(conf.SESSION_ADDRESS_NAME) or None
        self._data = self._clean(session_data)

    def __bool__(self):
        return bool(self._data)

    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, item):
        return self._data.get(item)

    def __setitem__(self, key, value):
        characteristics = list(value.keys())[0]
        count = list(value.values())[0]
        value = count

        if value <= 0:
            return

        self._data[key] = {}
        self._data[key][str(characteristics)] = value

    def __delitem__(self, item):
        del self._data[item]

    def __repr__(self):
        return repr(self._data)

    @staticmethod
    def _clean(data):
        """
        Фильтрация пустых и некорректных данных в словаре.

        :param data: dict
        :return: dict
        """
        result = {}
        for product_id, params in data.items():
            try:
                int(product_id)
                isinstance(params, dict)

            except (TypeError, ValueError):
                pass
            else:
                params = dict(params)

                result[product_id] = {}
                for characteristics, count in params.items():
                    if count > 0:
                        result[product_id][characteristics] = count

        return result

    @property
    def data(self):
        return self._data

    @property
    def get_products(self):
        obj = self.request.session.get(conf.SESSION_CART_NAME)
        products = []

        if not obj:
            return products

        for product_id, modifications in dict(obj).items():
            try:
                product = ShopProduct.objects.get(id=product_id)
            except ShopProduct.DoesNotExist:
                return products
            for modification_id, count in modifications.items():

                if modification_id == '' or modification_id == 'None' or modification_id == 'NaN':
                    modification = product
                    t = modification.title
                else:
                    modification = ShopProductModification.objects.get(id=int(modification_id))
                    t = f"{modification.product.title} ({modification.__str__()})"
            #
                products.append({
                    'product': modification,
                    'product_title': t,
                    'price': modification.price,  # Здесь заменится на расчитанное значение с учетом распродажи
                    'count': count,
                    'modification': modification_id,
                    'weight': modification.weight,
                    'max_count': modification.stock
                })

        return products

    @property
    def get_total_weight(self):
        products = self.get_products
        config = ShipStationConfig.get_solo()
        units = config.weight_unit
        weight = 0

        for product in products:
            if product.get('weight'):
                if units == Weight.WEIGHT_CHOICES[0][0]:
                    weight += Weight.to_pounds(product.get('weight')) * product.get('count')
                elif units == Weight.WEIGHT_CHOICES[1][0]:
                    weight += Weight.to_ounces(product.get('weight')) * product.get('count')
                elif units == Weight.WEIGHT_CHOICES[2][0]:
                    weight += Weight.to_grams(product.get('weight')) * product.get('count')
        return round(weight, 2), units

    def set_coupon(self, coupon_coupon=None):
        coupon = None

        try:
            coupon = Coupon.objects.get(coupon=coupon_coupon)
        except (Coupon.DoesNotExist, AttributeError):
            pass

        if coupon and coupon.is_active:
            self.coupon = coupon
            self.request.session[conf.SESSION_COUPON_NAME] = coupon.id

    def set_delivery(self, delivery=None):
        if delivery is None or not isinstance(delivery, dict):
            return None

        self.delivery = delivery

        self.request.session[conf.SESSION_DELIVERY_NAME] = json.dumps(delivery)

    def set_order(self, order_uuid=None):
        self.request.session[conf.SESSION_UUID_NAME] = str(order_uuid)
        self.uuid = order_uuid

        order = ShopOrder.objects.get(uuid=order_uuid)
        self.set_address(order.city.__str__(), order.city.country.__str__(), order.zip_code)

    def set_address(self, city, country, zip_code):
        self.address = get_city(city.__str__(), country.__str__())
        try:
            self.address['zip'] = zip_code
        except TypeError:
            pass
        self.request.session[conf.SESSION_ADDRESS_NAME] = self.address
    @property
    def get_order(self):
        return self.uuid

    @staticmethod
    def calc_fees(use_fees, total):
        fees = 0

        if use_fees == conf.PAYMENT_METHODS.get('stripe'):
            fees = total * Decimal(0.029) + Decimal(0.3)
        if use_fees == conf.PAYMENT_METHODS.get('authorize'):
            fees = total * Decimal(0.029) + Decimal(0.3)
        elif use_fees == conf.PAYMENT_METHODS.get('paypal'):
            fees = total * Decimal(0.0349) + Decimal(0.49)
        total_full = total + fees

        return total_full, fees

    def calculate_order_tax(self, order_cost, shipping_cost):
        city = get_city(self.address.get('city'), self.address.get('country', 'US'))
        return Valute(get_order_tax(
            shipping_cost=shipping_cost.as_string(),
            order_cost=order_cost.as_string(),
            to_addr={
                'city': self.address.get('city'),
                'state': city.get('state_code'),
                'zip': self.address.get('zip'),
                'country': city.get('country_code'),
            },
        ))

    def get_order_info(self, use_fees=list(conf.PAYMENT_METHODS.values())[0]):
        coupon = None

        if self.coupon:
            if type(self.coupon) == int:
                coupon = Coupon.objects.get(id=self.coupon)
            else:
                coupon = self.coupon

        products = self.get_products

        count = 0
        price = Valute(0)
        coupon_price = Valute(0)

        for product in products:
            cur_product = product.get('product')
            cur_price = cur_product.cur_price
            product_count = product.get('count')
            count += product_count
            price += cur_price * product_count

        taxjar_tax = Valute(0)

        if self.address:
            taxjar_tax = self.calculate_order_tax(price, Valute(self.delivery.get('shipping')) if isinstance(self.delivery, dict) else Valute(0))

        if coupon:
            if coupon.discount_type == conf.DISCOUNT_TYPE_PERCENT:
                coupon_price = price * coupon.discount_value / 100
            elif coupon.discount_type == conf.DISCOUNT_TYPE_AMOUNT:
                coupon_price = coupon.discount_value

        subtotal = price - coupon_price
        total = Valute(0)

        if subtotal < 0:
            subtotal = Valute(0)

        else:
            total = subtotal + taxjar_tax

        if self.delivery and isinstance(self.delivery, dict):
            total += Valute(float(self.delivery.get('shipping')))

        fees = Valute(0)
        total_full = Valute(0)

        if not subtotal == 0:
            try:
                total_full, fees = self.calc_fees(use_fees=use_fees, total=total)
            except:
                total = Decimal(total)

                total_full, fees = self.calc_fees(use_fees=use_fees, total=total)
        shipping = Valute(self.delivery.get('shipping')) if isinstance(self.delivery, dict) else Valute(0)
        order = {
            'count': count,
            'price': Valute(price) if isinstance(price, Decimal) else price,
            'coupon': coupon.discount if coupon and price > 0 else '',
            'subtotal': Valute(subtotal) if isinstance(subtotal, Decimal) else subtotal,
            'shipping': shipping,
            'tax': taxjar_tax,
            'fees': Valute(fees) if isinstance(fees, Decimal) else fees,
            'total': Valute(total) if isinstance(total, Decimal) else total,
            'total_full': Valute(total_full) if isinstance(total_full, Decimal) else total_full,
            'amount': price + shipping,
        }

        return order

    def save(self):
        """
        Сохранение корзины в сессию.
        """

        session_data = self._clean(self._data)
        self.request.session[conf.SESSION_CART_NAME] = session_data

    def clear(self):
        """
        Очистка корзины.
        """
        self._data = {}
        self.request.session[conf.SESSION_CART_NAME] = None

        self.clear_order()

    def clear_order(self):
        self.delivery = None
        self.request.session[conf.SESSION_UUID_NAME] = None
        self.request.session[conf.SESSION_COUPON_NAME] = None
        self.request.session[conf.SESSION_DELIVERY_NAME] = None
        self.request.session[conf.SESSION_ADDRESS_NAME] = None

import uuid
import datetime

from django.db import models
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.db.models.functions import Coalesce
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from solo.models import SingletonModel
from libs.autoslug import AutoSlugField
from libs.valute_field.fields import ValuteField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from libs.description import description
from libs.get_city import get_city
from gallery.fields import GalleryField
from gallery.models import GalleryBase, GalleryImageItem
from ckeditor.fields import CKEditorField
from webpage.models import TextPage, StdPage, StdDisablePage, StdButtonPage
from users.models import CustomUser
from attachable_blocks.models import AttachableBlock
from shipstation.models import Weight, Dimension
from shipstation.states_titlecase import STATES_TITLECASE

from . import conf


class PercentField(models.FloatField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.FloatField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(PercentField, self).formfield(**defaults)


class ShopConfig(SingletonModel, TextPage, StdPage, StdDisablePage, StdButtonPage):
    # tax = PercentField(_('tax'), min_value=0, max_value=100, default=10, help_text=_('tax for state'))

    address = models.CharField(_('address'), max_length=255, default='')
    city = models.CharField(_('city'), max_length=255, default='')
    region = models.CharField(_('region'), max_length=64, default='')
    country = models.CharField(_('country'), max_length=64, default='United States')
    zip = models.CharField(_('zip'), max_length=32, default='')

    # About section
    about_visible = models.BooleanField(_('show about section'), default=True)
    about_title = models.CharField(_('title'), max_length=60, default='About our products', blank=True)
    about_description = models.TextField(_('description'), blank=True)

    use_paypal = models.BooleanField(_('use paypal'), default=True)
    use_stripe = models.BooleanField(_('use stripe'), default=True)
    use_authorize = models.BooleanField(_('use authorize'), default=True)

    # Other products block
    other_title = models.CharField(_('title'), max_length=60, default='', )

    # Reviews block
    reviews_title = models.CharField(_('title'), max_length=60, default='', )
    reviews_description = models.TextField(_('description'), blank=True)

    # Cart page
    cart_header = models.TextField(_('header'), max_length=128, default='', )
    cart_title = models.CharField(_('title'), max_length=60, default='', )
    cart_description = models.TextField(_('description'), blank=True)

    # Checkout page
    checkout_header = models.TextField(_('header'), max_length=128, default='', )
    checkout_title = models.CharField(_('title'), max_length=60, default='', )
    checkout_description = models.TextField(_('description'), blank=True)

    # Payment page
    payment_header = models.TextField(_('header'), max_length=128, default='', )
    payment_title = models.CharField(_('title'), max_length=60, default='', )
    payment_description = models.TextField(_('description'), blank=True)

    # Success page
    success_header = models.TextField(_('header'), max_length=128, default='', )
    success_title = models.CharField(_('title'), max_length=60, default='', )
    success_description = models.TextField(_('description'), blank=True)

    # Empty page
    empty_title = models.CharField(_('title'), max_length=60, default='', )
    empty_description = models.TextField(_('description'), blank=True)

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('shop:index')

    def __str__(self):
        return self.header


class Tax(models.Model):
    state = models.CharField(
        _('state'), max_length=255,
        choices=((key, key) for key in STATES_TITLECASE.keys()),
    )
    city = models.CharField(
        _('city'), max_length=255,
        help_text=_('Leave this blank, for all cities'), blank=True,
    )
    rate = models.DecimalField(
        _('tax rate %'), max_digits=8, decimal_places=2,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    class Meta:
        verbose_name = _('tax')
        verbose_name_plural = _('taxes')
        ordering = ('state', 'city', )
        unique_together = ('state', 'city')

    def __str__(self):
        return f'Tax for {self.city}, {self.state}'



class ShopCategory(models.Model):
    title = models.CharField(_('title'), max_length=128)
    slug = AutoSlugField(_('slug'), populate_from='title', unique=True)
    is_visible = models.BooleanField(_('visible'), default=True, db_index=True)
    created = models.DateTimeField(_('create date'), default=now, editable=False)
    updated = models.DateTimeField(_('change date'), auto_now=True)
    sort_order = models.IntegerField(_('order'), default=0)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.title

    @staticmethod
    def visible():
        return ShopCategory.objects.filter(is_visible=True, products__is_visible__isnull=False)

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': obj.title,
        }


class Characteristic(models.Model):
    title = models.CharField(_('title'), max_length=64)
    is_selectable = models.BooleanField(_('is selectable'), default=False)
    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('characteristic')
        verbose_name_plural = _('characteristics')
        ordering = ('sort_order', )

    def __str__(self):
            return self.title


class CharacteristicValue(models.Model):
    title = models.CharField(_('title'), max_length=64, default='')
    characteristic = models.ForeignKey(Characteristic, verbose_name=_('characteristic'), related_name='characteristic_values')

    class Meta:
        verbose_name = _('characteristic value')
        verbose_name_plural = _('characteristic values')
        ordering = ('characteristic__title', 'title', )

    @cached_property
    def fullname(self):
        return "%s: %s" % (self.characteristic, self.title)

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': f"{obj.characteristic}: {obj.title}"
        }

    def __str__(self):
        return f"{self.characteristic.title}: {self.title}"


class Modification(models.Model):

    class Meta:
        verbose_name = _('modification')
        verbose_name_plural = _('modifications')

    def __str__(self):
        characteristics = self.characteristics.all()
        arr = []
        for characteristic in characteristics:
            arr.append(characteristic.__str__())
        return ", ".join(arr)



class ModificationCharacteristic(models.Model):
    modification = models.ForeignKey(Modification, verbose_name='modification', related_name='characteristics')
    characteristic = models.ForeignKey(CharacteristicValue, verbose_name='characteristic', )
    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('characteristic')
        verbose_name_plural = _('characteristics')
        ordering = ('sort_order', )

    def __str__(self):
        return self.characteristic.__str__()


class ShopProductQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(
            is_visible=True, category__is_visible=True,
        )

    def by_category(self, category):
        if isinstance(category, (str, int)):
            category = ShopCategory.objects.get(pk=category)

        return self.filter(
            category__in=category
        )


class ShopProductGalleryImageItem(GalleryImageItem):
    STORAGE_LOCATION = 'shop/product/gallery'
    MIN_DIMENSIONS = (100, 60)
    ADMIN_CLIENT_RESIZE = True

    SHOW_VARIATION = 'normal'
    ASPECTS = 'normal'
    ADMIN_VARIATION = 'admin'
    VARIATIONS = dict(
        normal=dict(
            size=(380, 480),
            crop=False,
        ),
        tablet=dict(
            crop=False,
            size=(300, 300),
        ),
        mobile=dict(
            crop=False,
            size=(245, 245),
        ),
        admin=dict(
            crop=False,
            size=(245, 245),
        ),
        admin_micro=dict(
            crop=False,
            size=(40, 40),
        )
    )


class ShopProductGallery(GalleryBase):
    IMAGE_MODEL = ShopProductGalleryImageItem


class ShopProduct(models.Model):
    # поля для ускорения SQL-запроса
    _photo = None
    _photo_description = None

    preview = StdImageField(_('image'),
                            default='',
                            storage=MediaStorage('shop/product/previews/'),
                            min_dimensions=(350, 350),
                            admin_variation='admin',
                            crop_area=True,
                            aspects=('wide',),
                            variations=dict(
                                default=dict(
                                    size=(380, 450),
                                    crop=True,
                                    create_webp=True,
                                    quality=100,
                                    # create_avif=True,
                                ),
                                normal=dict(
                                    size=(280, 330),
                                    crop=True,
                                    create_webp=True,
                                    # create_avif=True,
                                ),
                                small=dict(
                                    size=(160, 185),
                                    crop=True,
                                    create_webp=True,
                                    # create_avif=True,
                                ),
                                admin=dict(
                                    crop=True,
                                    size=(160, 185),
                                ),
                            ),
                            )
    category = models.ForeignKey(ShopCategory,
                                 verbose_name=_('category'),
                                 related_name='products'
                                 )
    characteristics = models.ManyToManyField(CharacteristicValue, blank=True, related_name='product_characteristics')
    sku = models.CharField(_('sku'), max_length=128, default='')
    header = models.CharField(_('header'), max_length=128, default='')
    title = models.CharField(_('title'), max_length=64)
    slug = AutoSlugField(_('slug'), populate_from='title', unique=True)
    gallery = GalleryField(ShopProductGallery, verbose_name=_('gallery'), blank=True, null=True)

    weight = models.ForeignKey(
        Weight, on_delete=models.CASCADE, null=True, default=''
    )
    dimension = models.ForeignKey(
        Dimension, on_delete=models.SET_NULL, null=True, default='',
    )

    # Information
    info_overview = CKEditorField(_('overview'), blank=True)
    info_specifications = CKEditorField(_('specifications'), blank=True)
    info_shipping = CKEditorField(_('shipping'), blank=True)
    info_return = CKEditorField(_('return'), blank=True)

    price = ValuteField(_('price'), decimal_places=2, max_digits=7,)
    is_visible = models.BooleanField(_('visible'), default=True,)
    view_count = models.PositiveSmallIntegerField(_('view count'), default=0)

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)
    created = models.DateTimeField(_('create date'), default=now, editable=False)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    objects = ShopProductQuerySet.as_manager()

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')
        index_together = (('category', 'is_visible'),)
        ordering = ('sort_order',)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return resolve_url('shop:detail',
                           category_slug=self.category.slug,
                           slug=self.slug)

    @cached_property
    def photo(self):
        if self._photo:
            return self._photo

        if self.gallery:
            item = self.gallery.image_items.first()
            return item.image if item else None
        return None

    @cached_property
    def photo_description(self):
        default_description = self.title
        if self._photo_description is not None:
            return self._photo_description or default_description

        if self.gallery:
            item = self.gallery.image_items.first()
            return item.description if item else default_description
        return default_description

    def calculate_price(self, sale):
        price = self.price

        if sale.discount_type == conf.DISCOUNT_TYPE_PERCENT:
            price = self.price - self.price * sale.discount_value / 100

        elif sale.discount_type == conf.DISCOUNT_TYPE_AMOUNT and sale.discount_value < self.price:
            price = self.price - sale.discount_value

        return price

    @cached_property
    def get_sale(self):
        current_date = datetime.date.today()
        sales = self.product_sales.filter(date_start__lte=current_date, date_end__gte=current_date)

        if not sales.exists():
            for characteristic in self.characteristics.all():
                sales = characteristic.characteristic_sales.filter(date_start__lte=current_date, date_end__gte=current_date)

                if sales.exists():
                    break

        if not sales.exists():
            sales = self.category.category_sales.filter(date_start__lte=current_date, date_end__gte=current_date)

        return sales.last()

    def sale(self):
        sale = self.get_sale

        return sale

    @cached_property
    def sale_price(self):
        sale = self.sale()

        if sale:
            return self.calculate_price(sale)

        return None

    @cached_property
    def is_sale(self):
        return True if self.get_sale else False

    @cached_property
    def str_sku(self):
        if self.sku:
            return f"Product SKU: {self.sku}"

        return None

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': obj.title
        }

    @property
    def stock(self):
        count = 0

        order_products = self.order_products.only('count')
        supply_products = self.supply_products.only('count')

        for val in supply_products: count += val.count
        for val in order_products: count -= val.count

        if count < 0: count = 0

        return count

    @property
    def is_sold(self):
        return self.stock == 0

    @property
    def is_modifications_sold(self):
        modifications = self.modifications.all()

        for modification in modifications:
            if not modification.is_sold:
                return False

        return True

    @property
    def cur_price(self):
        return self.sale_price if self.get_sale else self.price


class ShopProductModification(models.Model):
    product = models.ForeignKey(ShopProduct, verbose_name=_('product'), related_name='modifications')
    price = ValuteField(_('price'), decimal_places=2, max_digits=7,)
    sku = models.CharField(_('sku'), max_length=128, default='')
    modification = models.ForeignKey(Modification, verbose_name=_('modification'), null=True)
    title = models.CharField('title', blank=True, max_length=255)

    weight = models.ForeignKey(
        Weight, on_delete=models.CASCADE, null=True, default=''
    )
    dimension = models.ForeignKey(
        Dimension, on_delete=models.SET_NULL, null=True, default='',
    )
    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('product modification')
        verbose_name_plural = _('product modifications')
        ordering = ('sort_order', )
        unique_together = ('product', 'modification', )

    def __str__(self):
        characteristics = self.modification.characteristics.all()
        arr = []

        for characteristic in characteristics:
            arr.append(characteristic.__str__())
        return ", ".join(arr)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.title = self.__str__()
        super(ShopProductModification, self).save()

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': obj.__str__()
        }

    @staticmethod
    def autocomplete__product_item(obj):
        return {
            'id': obj.pk,
            'text': f"{obj.product.__str__()} ({obj.__str__()})"
        }

    def stock(self):
        count = 0
        order_products = self.order_products.only('count')
        supply_products = self.supply_products.only('count')

        for val in supply_products:
            count += val.count

        for val in order_products:
            count -= val.count
        if count < 0: count = 0
        return count

    @property
    def is_sold(self):
        return not self.stock() > 0

    @cached_property
    def str_sku(self):
        if self.sku:
            return f"Product SKU: {self.sku}"

        return None

    def calculate_price(self, sale):
        if sale.discount_type == conf.DISCOUNT_TYPE_PERCENT:
            price = self.price - self.price * sale.discount_value / 100

        elif sale.discount_type == conf.DISCOUNT_TYPE_AMOUNT and sale.discount_value < self.price:
            price = self.price - sale.discount_value
        else:
            price = self.price

        return price

    @cached_property
    def get_sale(self):
        current_date = datetime.date.today()
        sales = self.modification_sales.filter(date_start__lte=current_date, date_end__gte=current_date)

        return sales.last()

    @cached_property
    def sale(self):
        sale = self.get_sale()

        return sale

    @cached_property
    def sale_price(self):
        sale = self.sale()

        if sale:
            return self.calculate_price(sale)

        return None

    @cached_property
    def is_sale(self):
        return True if self.get_sale() else False

    @cached_property
    def cur_price(self):
        return self.sale_price if self.get_sale else self.price


class StarField(models.PositiveSmallIntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.PositiveSmallIntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(StarField, self).formfield(**defaults)


class ReviewQuerySet(models.QuerySet):
    def visible(self):
        return self.filter(visible=True)


class Review(models.Model):
    product = models.ForeignKey(ShopProduct, verbose_name=_('product'), related_name='reviews', on_delete=models.CASCADE)
    author = models.CharField(_('author'), max_length=50)
    text = models.TextField(_('text'), blank=True)
    rating = StarField(_('stars count'), min_value=1, max_value=5)

    visible = models.BooleanField(_('visible'), default=True)
    sort_order = models.PositiveIntegerField(_('order'), default=0)
    objects = ReviewQuerySet.as_manager()

    class Meta:
        verbose_name = _('review')
        verbose_name_plural = _('reviews')
        ordering = ('sort_order', )

    def __str__(self):
        return self.author

    @cached_property
    def short_text(self):
        return description(self.text, 400, 450)

    @cached_property
    def is_long_text(self):
        return len(self.text) > 450


class Sale(models.Model):
    DATE_START = datetime.datetime.now().date() + datetime.timedelta(days=3)
    DATE_END = datetime.datetime.now().date() + datetime.timedelta(days=11)

    title = models.CharField(_('title'), max_length=64)

    categories = models.ManyToManyField(ShopCategory,
                                        verbose_name=_('categories'),
                                        related_name='category_sales',
                                        blank=True,
                                        )
    products = models.ManyToManyField(ShopProduct,
                                      verbose_name=_('products'),
                                      related_name='product_sales',
                                      blank=True,
                                      )
    modifications = models.ManyToManyField(ShopProductModification,
                                      verbose_name=_('modifications'),
                                      related_name='modification_sales',
                                      blank=True,
                                      )
    characteristics = models.ManyToManyField(CharacteristicValue,
                                      verbose_name=_('characteristics'),
                                      related_name='characteristic_sales',
                                      blank=True,
                                      )

    date_start = models.DateField(_('date start sale'), default=DATE_START)
    date_end = models.DateField(_('date end sale'), default=DATE_END)
    discount_value = models.PositiveSmallIntegerField(_('discount value'), default=10)
    discount_type = models.PositiveSmallIntegerField(_('discount type'), choices=conf.DISCOUNT_TYPES, default=conf.DISCOUNT_TYPE_PERCENT)

    class Meta:
        verbose_name = _('sale')
        verbose_name_plural = _('sales')

    def __str__(self):
        return self.title

    @cached_property
    def discount(self):
        if self.discount_type == conf.DISCOUNT_TYPE_PERCENT:
            discount_char = '%'
        elif self.discount_type == conf.DISCOUNT_TYPE_AMOUNT:
            discount_char = '$'
        else:
            return self.discount_value

        return '{value}{char}'.format(value=self.discount_value, char=discount_char)


class Coupon(models.Model):
    DATE_START = datetime.datetime.now().date() + datetime.timedelta(days=3)
    DATE_END = datetime.datetime.now().date() + datetime.timedelta(days=11)

    title = models.CharField(_('title'), max_length=64)

    date_start = models.DateField(_('date start sale'), default=DATE_START)
    date_end = models.DateField(_('date end sale'), default=DATE_END)
    discount_value = models.PositiveSmallIntegerField(_('discount value'), default=10)
    discount_type = models.PositiveSmallIntegerField(_('discount type'), choices=conf.DISCOUNT_TYPES, default=conf.DISCOUNT_TYPE_PERCENT)
    limit_count = models.PositiveSmallIntegerField(_('limit for count of use'), default=0, help_text=_('Set 0 for unlimited'))
    used_count = models.PositiveSmallIntegerField(_('count of use code'), default=0, )
    coupon = models.CharField(_('coupon'), max_length=32, unique=True)

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')

    def __str__(self):
        return self.title

    @cached_property
    def discount(self):
        if self.discount_type == conf.DISCOUNT_TYPE_PERCENT:
            discount_char = '%'
        elif self.discount_type == conf.DISCOUNT_TYPE_AMOUNT:
            discount_char = '$'
        else:
            return self.discount_value

        return '{value}{char}'.format(value=self.discount_value, char=discount_char)

    @cached_property
    def remaining_quantity(self):
        if self.limit_count == 0:
            return _('Unlimited')

        return self.limit_count - self.used_count

    @cached_property
    def is_active(self):
        if isinstance(self.remaining_quantity, int) and self.remaining_quantity == 0:
            return False
        return self.date_start <= datetime.date.today() <= self.date_end


class Country(models.Model):
    name = models.CharField(_('name'), max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')
        ordering = ('name', )


class City(models.Model):
    country = models.ForeignKey(Country, verbose_name=_('country'), null=True, blank=True, default='')
    name = models.CharField(_('name'), max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')
        ordering = ('name', )


class Subscriber(models.Model):
    email = models.EmailField(_('email'), default='')
    active = models.BooleanField(_('active'), default=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('subscriber')
        verbose_name_plural = _('subscribers')


class ShopOrder(models.Model):
    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, unique=True, editable=False)
    session = models.CharField(_('session'), max_length=64, editable=False)

    city = models.ForeignKey(City, verbose_name=_('city'), max_length=64, blank=True, null=True)

    first_name = models.CharField(_('first name'), max_length=64, default='')
    last_name = models.CharField(_('last name'), max_length=64, default='')
    email = models.EmailField(_('e-mail'), default='')
    phone = models.CharField(_('phone'), max_length=32, default='')

    address_line_1 = models.CharField(_('street address'), max_length=64, default='')
    address_line_2 = models.CharField(_('apt., suite, building'), max_length=64, blank=True)
    zip_code = models.CharField(_('ZIP code'), max_length=6, blank=True)

    # Отменен плательщиком
    is_cancelled = models.BooleanField(_('cancelled'), default=False)
    cancel_date = models.DateTimeField(_('cancel date'),
                                       null=True,
                                       editable=False,
                                       )

    # Заказ еще не просматривался
    is_new = models.BooleanField(_('is new'), default=True)

    # Оплачен
    is_paid = models.BooleanField(_('paid'), default=False)
    pay_date = models.DateTimeField(_('pay date'),
                                    null=True,
                                    editable=False,
                                    )

    # Передан в доставку
    is_shipstationed = models.BooleanField(
        _('transferred to Ship Station'), default=False,
    )
    transfer_date = models.DateTimeField(_('transfer date'), null=True)

    shipstation_order_id = models.CharField(_('orderId'), max_length=32, blank=True)
    shipstation_order_key = models.CharField(_('orderKey'), max_length=32, blank=True)
    shipstation_order_link = models.URLField(_('link'), max_length=128, blank=True)

    # Доставлен покупателю
    is_shipped = models.BooleanField(_('shipped to customer'), default=False)
    ship_date = models.DateTimeField(_('ship date'), null=True)

    products_cost = ValuteField(_('products cost'), editable=False) # Здесь цена с учетом купона(если есть)
    subtotal = ValuteField(_('subtotal'), editable=False) # Здесь цена полученная при калькуляции
    tax = ValuteField(_('tax'), editable=False, default=0)
    fees = ValuteField(_('fees'), editable=False, default=0)
    link = models.URLField(_('link'), blank=True, default='')
    tmp_id = models.CharField(_('order id'), max_length=64, blank=True)

    career = models.CharField(_('delivery career'), default='', max_length=128)
    shipping = ValuteField(_('shipping'), editable=False, default=0)

    created = models.DateTimeField(_('create date'), default=now, editable=False)

    class Meta:
        verbose_name = _('order')
        verbose_name_plural = _('orders')
        ordering = ('-created', )

    def __str__(self):
        return _('Order %s') % self.uuid

    def clean(self):
        errors = {}

        if errors:
            raise ValidationError(errors)

    @cached_property
    def calculate_products_cost(self):
        """ Рассчет стоимости товаров """
        return self.records.aggregate(
            cost=Coalesce(models.Sum(
                models.F('order_price') * models.F('count'), output_field=ValuteField()
            ), 0)
        )['cost']

    @cached_property
    def total_cost(self):
        """
            Метод получения полной стоимости заказа,
            которая может включать доставку / налог / ...
        """

        return self.subtotal + self.tax + self.shipping + self.fees

    @cached_property
    def full_address(self):
        address = self.address_line_1

        if self.address_line_2:
            address = "{address_line_1}, {address_line_2}".format(
                address_line_1=self.address_line_1,
                address_line_2=self.address_line_2,
            )

        return address

    @cached_property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    @cached_property
    def address_object(self):
        return get_city(self.city.__str__(), self.city.country.__str__())


class OrderRecord(models.Model):
    order = models.ForeignKey(ShopOrder, verbose_name=_('order'), related_name='records')
    product = models.ForeignKey(ShopProduct, verbose_name=_('product'), null=True, on_delete=models.SET_NULL, related_name='order_products')
    modification = models.ForeignKey(ShopProductModification, verbose_name=_('modification'), null=True, on_delete=models.SET_NULL, related_name='order_products')
    order_price = ValuteField(_('price per item'))
    count = models.PositiveIntegerField(_('quantity'))

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')

    @cached_property
    def total_cost(self):
        return self.order_price * self.count

    @cached_property
    def item(self):
        return self.modification if self.modification else self.product

    def __str__(self):
        product_name = _('Unknown')

        if self.product:
            product_name = self.product.title
        elif self.modification:
            product_name = self.modification.__str__()

        return '%s (x%s)' % (product_name, self.count)

    @cached_property
    def title(self):
        return f"{self.product.title} ({self.modification.__str__()})" if self.modification else self.product.title

    def title_count(self):
        return f"{self.product.title} ({self.modification.__str__()}) x{self.count}" if self.modification else self.product.title

    def str_modifications(self):
        return self.modification.modification.__str__() if self.modification else ''


class Supply(models.Model):
    date = models.DateTimeField(_('date'), default=now, editable=False)
    manager = models.ForeignKey(CustomUser, verbose_name=_('manager'), null=True, default='')

    class Meta:
        verbose_name = 'Supply'
        verbose_name_plural = 'Supplies'

    def __str__(self):
        return f"Supply #{self.id}"


class SupplyProduct(models.Model):
    supply = models.ForeignKey(Supply, verbose_name=_('supply'), )
    product = models.ForeignKey(ShopProduct, verbose_name=_('product'), related_name='supply_products', blank=True, null=True)
    modification = models.ForeignKey(ShopProductModification, verbose_name=_('modification'), related_name='supply_products', blank=True, null=True)
    count = models.PositiveIntegerField(_('count'), default=0)

    def save(self, *args, **kwargs):
        if self.count <= 0:
            raise ValidationError('Enter correct count')

        super().save(*args, **kwargs)

    def __str__(self):
        return self.product.__str__() if self.product else self.modification.__str__()


class OrderRecordCharacteristic(models.Model):
    record = models.ForeignKey(OrderRecord, verbose_name=_('order record'), related_name='characteristics')
    characteristic = models.ForeignKey(CharacteristicValue, verbose_name=_('characteristic'))

    class Meta:
        verbose_name = _('record characteristic')
        verbose_name_plural = _('record characteristics')

    def __str__(self):
        return self.characteristic.title


class NotificationReceiver(models.Model):
    config = models.ForeignKey(ShopConfig, related_name='receivers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('notification receiver')
        verbose_name_plural = _('notification receivers')

    def __str__(self):
        return self.email


class ShopProductsBlock(AttachableBlock):
    BLOCK_VIEW = 'shop.views.shop_products_block_render'

    title = models.CharField(_('title'), max_length=128, default='Why Our Company Is Your Best Choice')

    class Meta:
        default_permissions = ()
        verbose_name = _('Shop products block')
        verbose_name_plural = _('Shop products blocks')


class ShopReviewsBlock(AttachableBlock):
    BLOCK_VIEW = 'shop.views.shop_reviews_block_render'

    title = models.CharField(_('title'), max_length=128, default='Why Our Company Is Your Best Choice')
    description = models.TextField(_('description'), blank=True)

    class Meta:
        verbose_name = _('Shop reviews block')
        verbose_name_plural = _('Shop reviews blocks')
from django.db.models import Avg
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET
from django.core.urlresolvers import reverse

from authorize_payments.views import AuthorizeAPI
from stripe_payments.views import StripeAPI
from taxjar_payments.views import TaxJarAPI
from shipstation.views import send_to_shipstation, get_services_from_ShipStation
from paginator.paginator import Paginator, EmptyPage
from ajax_views.decorators import ajax_view
from libs.email import send_template
from libs.valute_field.fields import Valute
from libs.cache.cached import cached
from . import conf
from datetime import date

from .cart import Cart
from .models import (ShopConfig, ShopProduct, OrderRecord, Subscriber, City,
                     Characteristic, CharacteristicValue, ShopOrder,
                     NotificationReceiver, Coupon, ShopProductModification)
from .views import CNT_PRODUCTS, get_filters_forms, get_reviews_paginator, get_products_paginator, get_form_cities
from .forms import CheckoutForm, SubscribeForm, CityForm, CouponForm, CardForm
from contacts.models import ContactsConfig
import json


@ajax_view('shop.more_products')
@require_GET
def more_products(request, *args, **kwargs):
    paginator = get_products_paginator(request)

    return JsonResponse({
        'html': render_to_string('shop/parts/_list_products.html', {
            'products': paginator.current_page
        }),
        'more_button': render_to_string('shop/parts/_more_products.html', {
            'products': paginator,
        })
    })


@ajax_view('shop.city.filter')
@require_GET
def city_filter(request, *args, **kwargs):
    try:
        country_id = int(request.GET.get('country'))
    except ValueError:
        return return_error_json()

    city_form = get_form_cities(country_id)['form']
    html = render_to_string('shop/parts/_city_select.html', context={
        'city_form': city_form
    }, request=request)

    return JsonResponse({
        'html': html
    })


@ajax_view('shop.order_info')
@require_GET
def order_info(request, *args, **kwargs):
    cart = Cart(request)
    coupon_form = CouponForm()

    use_coupon = request.GET.get('use_coupon')
    is_save = request.GET.get('is_save')
    use_coupon = True if use_coupon == 'true' else False
    is_save = True if is_save == 'true' else False
    order_info = cart.get_order_info()

    return JsonResponse({
        'html': render_to_string('shop/parts/_order.html', {
            'order': order_info,
            'use_coupon': use_coupon,
            'save_order': is_save,
            'btn_caption': f"Pay {order_info.get('total_full')}" if is_save else _('Proceed to Checkout'),
            'btn_url': reverse('shop:checkout'),
            'coupon_form': coupon_form,
        }, request),
    })


@ajax_view('shop.more_reviews')
@require_GET
def more_reviews(request, *args, **kwargs):
    product_slug = request.GET.get('product')
    try:
        product = ShopProduct.objects.get(slug=product_slug)
    except ShopProduct.DoesNotExist:
        return return_error_json()

    paginator = get_reviews_paginator(request, product)

    return JsonResponse({
        'html': render_to_string('shop/parts/_reviews.html', {
            'reviews_paginator': paginator['paginator'],
        }),
        'more_button': render_to_string('shop/parts/_more_reviews.html', {
            'btn_caption': paginator['btn_caption'],
            'reviews_paginator': paginator['paginator'],
        })
    })


@ajax_view('shop.filter')
@require_GET
def shop_filter(request, *args, **kwargs):
    sort = request.GET.get('sort')
    filters = request.GET.getlist('filters[]')
    price = request.GET.getlist('price[]')
    categories = request.GET.getlist('categories[]')

    products = ShopProduct.objects.visible()

    if sort:
        products = products.annotate(average_rating=Avg('view_count')).order_by('-average_rating') if sort == 'rating' else products.order_by(sort)
    if price:
        price_from = Valute(price[0])
        price_to = Valute(price[1])

        sale_products = []

        for product in products:
            if price_from <= product.cur_price <= price_to:
                sale_products.append(product.id)

        products = products.filter(id__in=sale_products)

        if filters:
            groups = Characteristic.objects.filter(characteristic_values__in=filters).distinct()
            lp = []
            for group in groups:
                for product in products:
                    q_c = product.characteristics.all()
                    for c in q_c:
                        if c in group.characteristic_values.filter(id__in=filters):
                            lp.append(product.id)

                products = products.filter(id__in=lp)
        if categories:

            lp = []
            for product in products:

                for selected_category in categories:
                    if selected_category == product.category.slug:
                        lp.append(product.id)

            products = products.filter(id__in=lp)

    try:
        paginator = Paginator(
            request,
            object_list=products,
            per_page=CNT_PRODUCTS,
            allow_empty_first_page=False,
        )
    except EmptyPage:
        return JsonResponse({
            'html': render_to_string('shop/parts/_empty_page.html'),
        })
    return JsonResponse({
        'html': render_to_string('shop/parts/_products.html', {
            'products': paginator.current_page
        }),
        'more_button': render_to_string('shop/parts/_more_products.html', {
            'products': paginator,
        })
    })


@ajax_view('shop.popups.sort')
@require_GET
def popup_sort(request, *args, **kwargs):
    sort = request.GET.get('sort')

    return JsonResponse({
        'html': render_to_string('shop/popups/sort.html', {
            'current_sort': sort,
            'active': False
        })
    })


@ajax_view('shop.popups.filters')
@require_GET
def popup_filters(request, *args, **kwargs):
    filters = get_filters_forms(prefix='popup')

    return JsonResponse({
        'html': render_to_string('shop/popups/filters.html', {
            'characteristics': filters.get('filters'),
            'price_range': filters.get('price_range'),
        }, request=request)
    })


@csrf_exempt
@require_POST
@ajax_view('shop.cart.add')
def add_product(request, *args, **kwargs):
    """ Добавление продукта в корзину """

    '''
    В результате должен получиться объект со следующей структурой:
    {
        id продукта 1: {
            '': количество, # Нет модификации
            id модификации: количество # Есть модификация
        },
        id продукта 2: {
            id модификации: количество # Есть модификация
        }
    }
    
    Пример структуры json:
    {
        3: {
            '': 6,
            6: 2
        },
        8: {
            1: 4
        }
    }
    '''
    product_id = request.POST.get('product')
    count = int(request.POST.get('count'))
    modification = str(request.POST.get('modification'))
    cart = Cart(request)

    # Если есть запись по товару с id
    if cart[product_id]:
        # Если товар имеет заданные характеристики
        if modification:
            # И такие же есть в сессии
            if cart[product_id].get(modification):
                # Берем значение из сессии
                old_count = cart[product_id].get(modification)
                # Складываем имеющееся количество с заданным
                cart[product_id][modification] = old_count + count
            # Если товар есть, но с другими характеристиками
            else:
                # То добавляем продукту ключ, со значением в виде количества товара
                cart[product_id][modification] = count
            res = check_count(product_id, cart[product_id][modification], modification)
            cart[product_id][modification] = res.get('count')
        # Если товар не имеет характеристик, даже по умолчанию
        else:
            old_count = cart[product_id].get('') or 0
            # То записываем количество для ключа - списка с пустой строкой
            res = check_count(product_id, old_count + count, modification)
            cart[product_id][''] = res.get('count')

    # Если товара нет вообще в сессии
    else:
        res = check_count(product_id, count, modification)
        # Если имеются характеристики
        if modification:
            # То создаем запись, со значением - объектом, который имеет ключ - характеристики, и значение - количество
            cart[product_id] = {
                modification: res.get('count')
            }
        # Если характеристик нет
        else:
            # То создаем запись, со значением - объектом, который имеет ключ - список с пустой строкой, и значение - количество
            cart[product_id] = {
                '': res.get('count')
            }

    # Сохраняем
    cart.save()

    if res.get('is_error'):
        return JsonResponse({
            'cart': json.dumps(cart.data),
            'msg': res.get('msg')
        }, status=400)

    return JsonResponse({
        'cart': json.dumps(cart.data)
    })


@csrf_exempt
@require_POST
@ajax_view('shop.cart.update')
def update_product(request, *args, **kwargs):
    try:
        product_id = int(request.POST.get('product'))
        count = int(request.POST.get('count'))
        modification = str(request.POST.get('modification'))
    except ValueError as e:
        return return_error_json(e)

    cart = Cart(request)

    res = check_count(product_id, count, modification)
    cart[str(product_id)][modification] = res.get('count')
    cart.save()

    if res.get('is_error'):
        return JsonResponse({
            'cart': json.dumps(cart.data),
            'message': res.get('msg')
        }, status=400)

    return JsonResponse({
        'success': True,
        'cart': json.dumps(cart.data)
    })


@csrf_exempt
@require_POST
@ajax_view('shop.cart.delete')
def delete_product(request, *args, **kwargs):
    """ Удаление продукта из корзины """
    try:
        product_id = request.POST.get('product')
        modification = str(request.POST.get('modification'))

    except (TypeError, ValueError) as e:
        return JsonResponse({
            'success': False,
        })

    cart = Cart(request)

    if product_id in cart and modification in cart[product_id]:
        del cart[product_id][modification]
        cart.save()

    if len(cart.get_products) == 0:
        cart.clear()

        return JsonResponse({
            'success': True,
            'is_empty': True,
            'cart': json.dumps(cart.data)
        })

    return JsonResponse({
        'success': True,
        'cart': json.dumps(cart.data)
    })


@csrf_exempt
@require_GET
@ajax_view('shop.product.get_modification')
def get_product_modification(request, *args, **kwargs):
    try:
        modification_id = request.GET.get('modification')
        if modification_id == '':
            product_id = int(request.GET.get('product'))
            modification = ShopProduct.objects.get(id=product_id)
        else:
            modification = ShopProductModification.objects.get(id=int(modification_id))



        return JsonResponse({
            'max_count': modification.stock(),
            'price': str(modification.price),
            'sku': modification.str_sku,
            'buy_product': render_to_string('shop/parts/buy.html', {
                'product': modification,
            }),
            'price_html': render_to_string('shop/parts/_price.html', {
                'product': modification,
            })
        })

    except (TypeError, ValueError) as e:
        return JsonResponse({
            'success': False,
        })


@csrf_exempt
@require_GET
@ajax_view('shop.set_address')
def set_address(request, *args, **kwargs):
    city_id = request.GET.get('city')
    zip_code = request.GET.get('zip_code')
    try:
        city = City.objects.get(id=int(city_id))
    except City.DoesNotExist:
        return return_error_json(_(f"Can't find city by id {city_id}"))
    except ValueError:
        return _(f"Can't convert {city_id} to number")

    cart = Cart(request)

    cart.set_address(city.__str__(), city.country.__str__(), zip_code)

    return JsonResponse({})


@require_POST
@ajax_view('shop.order.save')
def save_order(request, *args, **kwargs):
    """ Сохранение данных о заказе со страницы checkout """
    data = request.POST

    checkout_form = CheckoutForm(data)
    city_form = CityForm(data)
    subscribe_form = SubscribeForm(data)

    cart = Cart(request)
    order_info = cart.get_order_info()

    if checkout_form.is_valid():
        products = cart.get_products

        checkout_form.products_cost = order_info.get('price')

        if city_form.is_valid():
            checkout_form.city = city_form.cleaned_data['city']
            checkout_form.zip_code = city_form.cleaned_data['zip_code']
        else:
            return JsonResponse({
                'errors': city_form.error_dict_full
            }, status=400)

        order = checkout_form.save(commit=False)

        order.tax = order_info.get('tax')
        order.fees = order_info.get('fees')
        order.career = cart.delivery.get('title')
        order.shipping = cart.delivery.get('shipping')
        order.subtotal = order_info.get('subtotal')
        order.zip_code = city_form.cleaned_data.get('zip_code')
        order.save()

        cart.set_order(order_uuid=order.uuid)

        for product in products:

            current_product = product['product']

            if isinstance(current_product, ShopProductModification):
                record = OrderRecord.objects.create(
                    order=order,
                    count=product['count'],
                    product=current_product.product,
                    modification=current_product,
                    order_price=current_product.sale_price if current_product.is_sale else current_product.price,
                )
            else:
                record = OrderRecord.objects.create(
                    order=order,
                    count=product['count'],
                    product=current_product,
                    order_price=current_product.sale_price if current_product.is_sale else current_product.price,
                )
            record.save()


        if subscribe_form.is_valid():
            if subscribe_form.cleaned_data['active']:
                try:
                    subscriber = Subscriber.objects.get(email=checkout_form.cleaned_data['email'])
                    subscriber.active = True
                    subscriber.save()
                except Subscriber.DoesNotExist:
                    Subscriber.objects.create(
                        email=checkout_form.cleaned_data['email'],
                        active=True
                    )
        else:
            return JsonResponse({
                'errors': subscribe_form.error_dict_full
            }, status=400)
    else:
        return JsonResponse({
            'errors': checkout_form.error_dict_full
        }, status=400)

    return JsonResponse({
        'redirect_url': reverse('shop:payment')
    })


@require_GET
@ajax_view('shop.coupon.check')
def check_coupon(request, *args, **kwargs):
    """ Проверка валидности купона """
    data = request.GET

    coupon_form = CouponForm(data)

    if coupon_form.is_valid():
        coupon = coupon_form.cleaned_data['coupon']

        cart = Cart(request)
        cart.set_coupon(coupon_coupon=coupon)

        try:
            coupon = Coupon.objects.get(coupon=coupon)
        except Coupon.DoesNotExist:
            return return_error_json(_('Can\'t check coupon'))

        coupon.used_count += 1
        coupon.save()

        return JsonResponse({
            'html': render_to_string('shop/parts/_order.html', {
                'btn_caption': _('Proceed to Checkout'),
                'btn_url': reverse('shop:checkout'),
                'order': cart.get_order_info()
            }),
        })
    else:
        return JsonResponse({
            'errors': coupon_form.error_dict_full
        }, status=400)


@require_GET
@ajax_view('shop.delivery.change')
def change_delivery(request, *args, **kwargs):
    """ Изменяем используемый метод доставки """
    data = request.GET
    use_coupon = data.get('use_coupon') or False

    cart = Cart(request)

    address = {
        'city': request.GET.get('city'),
        'zip_code': request.GET.get('zip_code'),
        'country': request.GET.get('country'),
    }

    city = City.objects.get(id=address.get('city'))
    country = city.country

    careers = get_services_from_ShipStation(
        cart,
        city=city.__str__(),
        postal_code=address['zip_code'],
        country=country.__str__(),
    )

    for value, title, price, old_price in careers:
        if value == request.GET.get('delivery'):
            cart.set_delivery(delivery={
                'id': value,
                'title': title,
                'shipping': str(price.as_decimal()),
            })
            break

    cart.set_address(city=city.__str__(), country=country.__str__(), zip_code=address['zip_code'])

    return JsonResponse({
        'html': render_to_string('shop/parts/_order.html', {
            'btn_caption': f"Pay {cart.get_order_info().get('total_full')}",
            'use_coupon:': use_coupon,
            'save_order': True,
            'id': 'saveOrder',
            'order': cart.get_order_info()
        }),
    })


@require_POST
@ajax_view('shop.payment.card')
def payment_card(request, *args, **kwargs):
    data = request.POST

    type_payment = data.get('type_payment') or None
    form = CardForm(data)

    if form.is_valid():
        card_number = data.get('card_number') or None
        date_expired = data.get('date_expired') or None
        cvv = data.get('cvv') or None
        name = data.get('name') or None
        cart = Cart(request)
        order_info = cart.get_order_info()
        order_uuid = cart.get_order

        try:
            order = ShopOrder.objects.get(uuid=order_uuid)
        except ShopOrder.DoesNotExist:
            return return_error_json(_("Sorry, we can't found your order"))

        card = {
            'card_number': card_number,
            'date_expired': date_expired,
            'cvv': cvv,
            'name': name,
        }
        config = ShopConfig.get_solo()

        if type_payment == 'stripe':
            if not config.use_stripe:
                return payment_system_disabled()

            payment = payment_stripe(request, order_info, card, order)

            send_to_shipstation(order=order)

            return payment

        if type_payment == 'authorize':

            if not config.use_authorize:
                return payment_system_disabled()

            payment = payment_authorize(request, order_info, card, order)

            send_to_shipstation(order=order)

            return payment

        return return_error_json(_("Unknown payment system"))
    else:
        return JsonResponse({
            'errors': form.error_dict_full
        }, status=400)


@csrf_exempt
@require_POST
@ajax_view('shop.payment.paypal')
def payment_paypal(request, *args, **kwargs):
    cart = Cart(request)
    order_uuid = cart.get_order

    data = request.POST

    try:
        order = ShopOrder.objects.get(uuid=order_uuid)
    except ShopOrder.DoesNotExist:
        return return_error_json(_("Order doesn't exist"))
    except ShopOrder.MultipleObjectsReturned:
        return return_error_json(_("Too much orders returned"))

    order.tmp_id = data.get('id')
    order.link = data.get('links[0][href]')
    order.is_paid = True
    order.pay_date = date.today()
    order.save()

    send_to_shipstation(order=order)

    send_taxjar(request, order)
    send_notifications(request, order)
    cart.clear()

    return JsonResponse({
        'redirect_url': reverse('shop:payment_success')
    })


@require_GET
@ajax_view('shop.payment.change')
def payment_change(request, *args, **kwargs):
    data = request.GET
    payment = data.get('payment')
    cart = Cart(request)
    order = cart.get_order_info(use_fees=payment)

    return JsonResponse({
        'html': render_to_string('shop/parts/_order.html', {
            'btn_caption': f"Pay {order.get('total_full')}",
            'use_coupon:': False,
            'save_order': True,
            'id': 'saveOrder',
            'order': order
        }),
    })


@require_GET
@ajax_view('shop.cart.clear')
def cart_clear(request, *args, **kwargs):
    Cart(request).clear()

    page = request.GET.get('page')
    data = {}
    cart_url = reverse('shop:cart')

    data['is_reload'] = page == cart_url


    return JsonResponse(data)


@ajax_view('shop.deliveries.update')
def get_shipping_methods(request):
    template = 'shop/parts/_deliveries.html'

    try:
        cart = Cart(request)

        try:
            city = City.objects.get(id=request.GET.get('city'))
        except City.DoesNotExist:
            return JsonResponse({
                'html': render_to_string(
                    template, {
                        'error': "Can't find city"
                    }, request=request
                ),
            })

        shipping_methods = get_services_from_ShipStation(
            cart,
            city=city.__str__(),
            postal_code=request.GET.get('zip_code'),
            country=city.country.__str__(),
        )

    except Exception as _:
        return JsonResponse({
            'html': render_to_string(
                template, {
                    'error': "Can't find delivery methods"
                },
                request=request
            ),
        })

    return JsonResponse({
        'html': render_to_string(
            template, {
                'deliveries': shipping_methods
            },
            request=request
        ),
    })


@cached('product_id', time=10*60)
def find_characteristics(product_id):
    characteristics = []
    # Получили продукт
    product = ShopProduct.objects.get(id=product_id)

    # Получили характеристики, которые могут ему принадлежать
    product_characteristics_values = product.characteristics.filter(characteristic__is_selectable=True)
    list_characteristics = Characteristic.objects.filter(
        characteristic_values__in=product_characteristics_values, is_selectable=True).distinct()
    for c in list_characteristics:
        if len(product.characteristics.filter(id__in=c.characteristic_values.all())) > 1:
            i = CharacteristicValue.objects.filter(characteristic_id=c.id).first()
            characteristics.append(i.id)

    return characteristics


@cached('product_id', 'modification_id', 'count', time=10*60)
def check_count(product_id, count, modification_id=''):
    if modification_id == '':
        try:
            modification = ShopProduct.objects.get(id=product_id)
        except ShopProduct.DoesNotExist:
            return return_error_json(_('Product not found'))
    else:
        try:
            modification = ShopProductModification.objects.get(id=modification_id)
        except ShopProductModification.DoesNotExist:
            return return_error_json(_('Modification not found'))

    is_error = count > modification.stock

    if is_error:
        count = modification.stock
    return {
        'count': count,
        'msg': _('Sorry we don\'t have that many product. We\'re add maximum count of product.'),
        'is_error': is_error,
    }


@cached('msg', time=10*60)
def return_error_json(msg=None):
    data = {}

    if msg is not None:
        data['error'] = msg if isinstance(msg, str) else str(msg)

    return JsonResponse(data, status=400)


def send_notifications(request, order):
    receivers = [order.email, ]

    send_template(request, receivers,
                  subject=_('Message from {domain}'),
                  template='shop/mails/receipt.html',
                  fail_silently=True,
                  context={
                      'order': order,
                  })

    receivers = NotificationReceiver.objects.values_list('email', flat=True)
    send_template(request, receivers,
                  subject=_('Message from {domain}'),
                  template='shop/mails/new_order.html',
                  fail_silently=True,
                  context={
                      'order': order,
                  }),


def send_taxjar(request, order):
    cart = Cart(request)
    order_info = cart.get_order_info()

    today = date.today()

    contacts_config = ContactsConfig.get_solo()
    to_address = cart.address
    from_address = contacts_config.detail_city()

    line_items = []

    for product in cart.get_products:
        line_items.append({
            "quantity": product.get('count'),
            "product_identifier": product.get('product').sku,
            "description": product.get('product').title,
            "unit_price": float(product.get('product').cur_price.as_decimal()),
            "sales_tax": float(order_info.get('tax').as_decimal())
        })

    data = {
        'transaction_id': f'Order_{order.id}',
        'transaction_date': f'{today.year:02d}/{today.month:02d}/{today.day:02d}',
        'from_country': from_address['country_code'],
        'from_zip': contacts_config.zip,
        'from_state': from_address['state_code'],
        'from_city': from_address['city'],
        'from_street': contacts_config.address,
        'to_country': to_address['country_code'],
        'to_zip': to_address['zip'],
        'to_state': to_address['state_code'],
        'to_city': to_address['city'],
        'to_street': order.full_address,
        'amount': order_info.get('amount').as_string(),
        'shipping': order_info.get('shipping').as_string(),
        'sales_tax': order_info.get('tax').as_string(),
        "line_items": line_items
    }

    result = TaxJarAPI().send(
        data=data,
    )

    return result


def payment_stripe(request, order_info, card, order):
    result = StripeAPI().payment(
        card=card,
        amount=order_info.get('total_full'),
        description=order.__str__()
    )

    if result.get("status") == "succeeded":
        receipt_url = result.get("receipt_url")

        order.is_paid = True
        order.link = receipt_url
        order.save()

        send_taxjar(request, order)
        send_notifications(request, order)

        Cart(request).clear()

        return JsonResponse({
            'redirect_url': reverse('shop:payment_success')
        })
    else:
        order.is_cancelled = True
        order.save()

        e = _("Can't sent payment")
        if result['error']:
            e = result['error']

        return return_error_json(e)


def payment_authorize(request, order_info, card, order):
    result = AuthorizeAPI().payment(
        request=request,
        card=card,
        amount=order_info.get('total_full'),
    )

    e = None

    if result is None:
        e = _("Can't send payment")
    elif result.get('error'):
        e = result['error']

    if e:
        return return_error_json(e)

    order.is_paid = True
    order.save()

    send_taxjar(request, order)
    send_notifications(request, order)

    Cart(request).clear()

    return JsonResponse({
        'redirect_url': reverse('shop:payment_success')
    })


def payment_system_disabled():
    return return_error_json(_("This payment system is disabled"))


@ajax_view('shop.get_session_backend')
@require_GET
def get_session_backend(request):
    try:
        cart = Cart(request)
        print(cart)
    except TypeError:
        cart = ""
    return JsonResponse({
        'cart': cart
    })


@ajax_view('shop.verify_session_backend')
@require_GET
def verify_session_backend(request):
    backend_data = Cart(request).data
    frontend_data = request.GET.get('data')

    return JsonResponse({
        'result': frontend_data == str(backend_data),
        'cart': json.dumps(backend_data)
    })

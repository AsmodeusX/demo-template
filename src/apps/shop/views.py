from django.http.response import Http404
from django.shortcuts import redirect, reverse, loader
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache, cache_control
from paginator.paginator import Paginator, EmptyPage
from django.utils.functional import cached_property
from seo.seo import Seo
from webpage.views import StdPageSingletonView, StdPageDetailView
from .models import ShopConfig, ShopCategory, ShopProduct, City, Country, Review
from .cart import Cart
from .forms import (
    CharacteristicForm, RangeValueForm, SelectableModificationForm, CityForm,
    ClientForm, CheckoutForm, SubscribeForm, CardForm, CouponForm
)

decorators = [never_cache,]

CNT_PRODUCTS = 9
CNT_REVIEWS = 5
CNT_BLOCK_PRODUCTS = 8
CNT_BLOCK_REVIEWS = 5


class MoreReviewsPaginator(Paginator):
    template = 'shop/parts/_more_reviews.html'


class MoreProductsPaginator(Paginator):
    template = 'shop/parts/_more_products.html'

def get_form_cities(country_id):
    cities = City.objects.filter(country_id=country_id).values_list('id', 'name')
    form = CityForm()
    form.fields['city'].widget.choices = cities

    return {
        'form': form
    }


def add_view(product):
    product.view_count += 1
    product.save()

    return product.view_count


def get_reviews_paginator(request, product):
    items = product.reviews.visible()
    paginator = MoreReviewsPaginator(
        request,
        object_list=items,
        per_page=CNT_REVIEWS,
        allow_empty_first_page=True,
    )
    count_next_reviews = 0

    if paginator.next_page_number:
        count_next_reviews = len(paginator.page(paginator.next_page_number))

    return {
        'paginator': paginator,
        'btn_caption': f"Show +{count_next_reviews} reviews"
    }


def get_products_paginator(request):
    items = ShopProduct.objects.visible()

    try:
        paginator = MoreProductsPaginator(
            request,
            object_list=items,
            per_page=CNT_PRODUCTS,
            allow_empty_first_page=False,
        )
    except EmptyPage:
        return None

    return paginator


def get_filters_forms(prefix=None):
    used_characteristics = []
    characteristics_list = []
    characteristics_forms = []

    products_visible = ShopProduct.objects.visible()

    for product in products_visible:
        for characteristic in product.characteristics.all():
            used_characteristics.append(characteristic.characteristic)

    used_characteristics = set(list(used_characteristics))

    price_sorted_products = products_visible.order_by('price')
    first_price_sorted_product = price_sorted_products.first()
    last_price_sorted_product = price_sorted_products.last()

    range_form_first = RangeValueForm()
    range_form_last = RangeValueForm()

    first_price_sorted_product_price = first_price_sorted_product.cur_price
    last_price_sorted_product_price = last_price_sorted_product.cur_price

    range_form_first.fields['value'].initial = str(first_price_sorted_product_price).replace("$", "")
    range_form_last.fields['value'].initial = str(last_price_sorted_product_price).replace("$", "")

    range_form_first.fields['value'].widget.attrs['id'] = 'price_range_from'
    range_form_last.fields['value'].widget.attrs['id'] = 'price_range_to'

    if prefix:
        range_form_first.fields['value'].widget.attrs['id'] = f"{prefix}-{range_form_first.fields['value'].widget.attrs['id']}"
        range_form_last.fields['value'].widget.attrs['id'] = f"{prefix}-{range_form_last.fields['value'].widget.attrs['id']}"

    range_form_first.fields['value'].widget.attrs['class'] = range_form_last.fields['value'].widget.attrs['class'] = 'price-range-field'

    range_form_first.fields['value'].widget.attrs['min'] = first_price_sorted_product_price.as_decimal()
    range_form_first.fields['value'].widget.attrs['max'] = last_price_sorted_product_price.as_decimal()

    range_form_last.fields['value'].widget.attrs['min'] = first_price_sorted_product_price.as_decimal()
    range_form_last.fields['value'].widget.attrs['max'] = last_price_sorted_product_price.as_decimal()

    price_range_slider_id = "priceRangeSlider"

    if prefix:
        price_range_slider_id = f"{prefix}{price_range_slider_id}"

    price_range = {
        'price_range_slider_id': price_range_slider_id,
        'price_min': range_form_first,
        'price_max': range_form_last,
    }

    for characteristic in used_characteristics:
        if characteristic.characteristic_values:
            characteristics_list.append(characteristic)

    for characteristic in characteristics_list:
        characteristic_form = CharacteristicForm(prefix=prefix)
        characteristic_values = characteristic.characteristic_values.values_list('id', 'title')

        characteristic_form.fields['characteristic'].label = characteristic.title.replace(' ', '-')
        if prefix:
            characteristic_form.fields['characteristic'].label = f"{prefix}-{characteristic_form.fields['characteristic'].label}"
        characteristic_form.fields['characteristic'].choices = characteristic_values

        characteristics_forms.append({
            'title': characteristic.title,
            'form': characteristic_form
        })
    return {
        'price_range': price_range,
        'filters': characteristics_forms
    }


class IndexView(StdPageSingletonView):
    model = ShopConfig
    context_object_name = 'page'
    template_name = 'shop/index.html'

    def get_object(self, queryset=None):
        return self.model.get_solo()

    @cached_property
    def get_categories_list(self):
        categories = ShopCategory.visible().distinct()
        categories_list = []

        for category in categories:
            if category.products.filter(is_visible=True).count() > 0:
                categories_list.append(category)

        return categories_list

    def get_context_data(self, **kwargs):

        paginator = get_products_paginator(self.request)

        if not paginator:
            raise Http404

        filters = get_filters_forms()

        seo = Seo()
        seo.set_data(self.object, defaults={
            'title': self.object.header,
            'og_title': self.object.header,
        })
        seo.save(self.request)

        context = super().get_context_data(**kwargs)
        context.update({
            'is_shop_page': True,
            'categories': self.get_categories_list,
            'products': paginator,
            'characteristics': filters.get('filters'),
            'price_range': filters.get('price_range'),
        })
        return context


class ProductView(StdPageDetailView):
    model = ShopProduct
    template_name = 'shop/product.html'

    def get_context_data(self, **kwargs):
        config = ShopConfig.get_solo()

        try:
            category_slug = self.object.category.slug
            category = ShopCategory.objects.get(slug=category_slug, is_visible=True)
        except (ShopCategory.DoesNotExist, ShopCategory.MultipleObjectsReturned) as e:
            raise Http404

        try:
            slug = self.object.slug
            product = ShopProduct.objects.get(slug=slug, is_visible=True)

        except (ShopProduct.DoesNotExist, ShopProduct.MultipleObjectsReturned):
            raise Http404

        modification_form = None

        if product.modifications.exists():
            modifications = [
                ('', _('Default')),
            ]

            modifications_list = list(set(product.modifications.values_list('id', 'title')))
            modifications.extend(modifications_list)

            modification_form = SelectableModificationForm()
            modification_form.fields['modification'].choices = modifications
            modification_form.fields['modification'].widget.attrs['id'] = 'modification'
            modification_form.fields['modification'].widget.attrs['class'] = 'modification'
            modification_form.fields['modification'] = modification_form.fields['modification']
        # del modification_form.fields['modification']
        paginator = get_reviews_paginator(self.request, product)

        add_view(self.object)

        context = super().get_context_data(**kwargs)
        context.update({
            'is_product_page': True,
            'config': config,
            'category': category,
            'product': product,
            'reviews_paginator': paginator['paginator'],
            'btn_caption': paginator['btn_caption'],
            'modification_form': modification_form,
        })
        return context


@method_decorator(decorators, name='dispatch')
class CartView(TemplateView):
    template_name = 'shop/cart.html'

    def get_handler(self, request):
        handler = super().get_handler(request)

        if handler:
            handler = cache_control(private=True, max_age=0)(handler)

        return handler

    def get(self, *args, **kwargs):
        config = ShopConfig.get_solo()

        title = config.cart_title
        header = config.cart_header
        description = config.cart_description

        cart = Cart(self.request)

        products = cart.get_products
        coupon_form = CouponForm()

        page = {
            'title': title,
            'header': header,
            'description': description,
        }

        # SEO
        seo = Seo()
        seo.set_data(config, defaults={
            'title': title,
            'og_title': title,
        })

        seo.save(self.request)
        data = {}

        if not cart.get_products:
            self.template_name = 'shop/empty.html'
            data['is_empty_shop_page'] = not cart.get_products,
            data['cart'] = not cart,
            return self.render_to_response(data)

        else:
            data = {
                'page': page,
                'products': products,
                'order': cart.get_order_info(),
                'btn_caption': _('Proceed to Checkout'),
                'btn_url': reverse('shop:checkout'),
                'coupon_form': coupon_form,
            }
            self.request.breadcrumbs.add(config.title, config.get_absolute_url())
            self.request.breadcrumbs.add(config.cart_title, reverse('shop:cart'))

            return self.render_to_response(data)


class CheckoutView(TemplateView):
    template_name = 'shop/checkout.html'

    def get(self, *args, **kwargs):
        config = ShopConfig.get_solo()

        title = config.checkout_title
        header = config.checkout_header
        description = config.checkout_description

        countries = Country.objects.only('id')

        if countries.exists():
            country_id = Country.objects.only('id').first().id
            city_form = get_form_cities(country_id).get('form')
        else:
            city_form = CityForm()

        checkout_form = CheckoutForm()
        # delivery_form = DeliveryMethodForm()
        subscribe_form = SubscribeForm()
        client_form = ClientForm()

        cart = Cart(self.request)
        products = cart.get_products

        page = {
            'title': title,
            'header': header,
            'description': description,
        }
        order_info = cart.get_order_info()

        data = {
            'page': page,
            'products': products,
            'order': order_info,
            'btn_caption': f"Pay {cart.get_order_info().get('total_full')}",
            'checkout_form': checkout_form,
            'city_form': city_form,
            # 'delivery_form': delivery_form,
            'subscribe_form': subscribe_form,
            'client_form': client_form,
        }

        self.request.breadcrumbs.add(config.title, config.get_absolute_url())
        self.request.breadcrumbs.add(config.checkout_title, reverse('shop:checkout'))

        # SEO
        seo = Seo()
        seo.set_data(config, defaults={
            'title': title,
            'og_title': title,
        })

        seo.save(self.request)

        return self.render_to_response(data)


class PaymentView(TemplateView):
    template_name = 'shop/payment.html'

    def get(self, *args, **kwargs):
        config = ShopConfig.get_solo()

        title = config.payment_title
        header = config.payment_header
        description = config.payment_description

        card_form = CardForm()

        cart = Cart(self.request)

        # print(cart.get_order)
        if not cart.get_order:
            return redirect('shop:cart')

        page = {
            'title': title,
            'header': header,
            'description': description,
        }

        data = {
            'page': page,
            'use_paypal': config.use_paypal,
            'use_authorize': config.use_authorize,
            'use_stripe': config.use_stripe,
            'card_form': card_form,
            'order': cart.get_order_info,
            'btn_caption': f"Pay {cart.get_order_info().get('total_full')}",
            'btn_url': reverse('shop:payment'),
        }

        self.request.breadcrumbs.add(config.title, config.get_absolute_url())
        self.request.breadcrumbs.add(config.payment_title, reverse('shop:payment'))

        # SEO
        seo = Seo()
        seo.set_data(config, defaults={
            'title': title,
            'og_title': title,
        })

        seo.save(self.request)

        return self.render_to_response(data)


class PaymentSuccessView(TemplateView):
    template_name = 'shop/success.html'

    def get(self, *args, **kwargs):
        config = ShopConfig.get_solo()

        title = config.success_title
        header = config.success_header
        description = config.success_description

        page = {
            'title': title,
            'header': header,
            'description': description,
        }

        data = {
            'page': page,
            'payment_success_page': True,
        }

        # SEO
        seo = Seo()
        seo.set_data(config, defaults={
            'title': title,
            'og_title': title,
        })

        seo.save(self.request)

        return self.render_to_response(data)


# @cached('block.id', time=5*60)
def shop_products_block_render(context, block, **kwargs):
    products = ShopProduct.objects.visible()

    return loader.render_to_string('shop/blocks/products.html', {
        'title': block.title,
        'products': products[:CNT_BLOCK_PRODUCTS],
    }, request=context.get('request'))


# @cached('block.id', time=5*60)
def shop_reviews_block_render(context, block, **kwargs):
    reviews = Review.objects.visible()

    return loader.render_to_string('shop/blocks/reviews.html', {
        'title': block.title,
        'reviews': reviews[:CNT_BLOCK_REVIEWS],
    }, request=context.get('request'))


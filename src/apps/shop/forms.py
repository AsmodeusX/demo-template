import datetime

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
from libs.form_helper.forms import FormHelperMixin
from .models import ShopOrder, Subscriber, City, Country, Coupon
from libs.phone import translate_phonewords, is_e164_format
from libs.get_city import get_city
from pyzipcode import ZipCodeDatabase
import re


class CharacteristicForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/multiple_choice_field.html'
    csrf_token = False
    characteristic = forms.MultipleChoiceField(
        choices=[],
        # queryset=CharacteristicValue.objects.all(),
        widget=forms.CheckboxSelectMultiple(),
        required=False,
    )


class SelectableCharacteristicForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'
    csrf_token = False
    characteristic = forms.ChoiceField(
        choices=[],
        widget=forms.Select()
    )

    def __init__(self, *args, **kwargs):
        super(SelectableCharacteristicForm, self).__init__(*args, **kwargs)


class SelectableModificationForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'
    csrf_token = False
    modification = forms.ChoiceField(
        choices=[],
        widget=forms.Select()
    )

    def __init__(self, *args, **kwargs):
        super(SelectableModificationForm, self).__init__(*args, **kwargs)


class RangeValueForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'
    csrf_token = False
    value = forms.CharField(
        required=False,
        widget=forms.TextInput()
    )


class CheckoutForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/field.html'

    csrf_token = False

    first_name = forms.CharField(
        label=_('First Name'),
        max_length=2048,
        widget=forms.TextInput(attrs={
            'placeholder': _('James'),
            'class': 'input-half'
        }),
        error_messages={
            'required': _('Please enter your first name'),
            'max_length': _('First name should not be longer than %(limit_value)d characters'),
            'min_length': _("The first name is very short"),
            'incorrect': _('Your first name contains illegal characters'),
        },
    )

    last_name = forms.CharField(
        label=_('Last Name'),
        max_length=2048,
        widget=forms.TextInput(attrs={
            'placeholder': _('Sunderland'),
            'class': 'input-half'
        }),
        error_messages={
            'required': _('Please enter your last name'),
            'max_length': _('Last name should not be longer than %(limit_value)d characters'),
            'min_length': _("The last name is very short"),
            'incorrect': _('Your last name contains illegal characters'),
        },
    )

    country = forms.ChoiceField(
        widget=forms.Select(),
    )

    address_line_1 = forms.CharField(
        label=_('Street Address'),
        max_length=50,
        error_messages={
            'required': _('Please enter your address'),
            'max_length': _('Address should not be longer than %(limit_value)d characters'),
            'min_length': _("The address is very short"),
            'incorrect': _('Your address contains illegal characters'),
        }
    )

    address_line_2 = forms.CharField(
        label=_('Apt., Suite, Building (optional)'),
        max_length=50,
        required=False,
        error_messages={
            # 'required': _('Please enter your address'),
            'max_length': _('Address should not be longer than %(limit_value)d characters'),
            'min_length': _("The address is very short"),
            'incorrect': _('Your address contains illegal characters'),
        }
    )

    zip_code = forms.CharField(
        label=_('ZIP Code'),
        max_length=6,
        error_messages={
            'required': _('Please enter your ZIP code'),
            'max_length': _('ZIP code should not be longer than %(limit_value)d characters'),
            'min_length': _("The ZIP code is very short"),
            'incorrect': _('Your ZIP code contains illegal characters'),
        },
        widget=forms.TextInput()
    )

    class Meta:
        model = ShopOrder
        fields = '__all__'
        exclude = ('is_cancelled', 'is_paid', 'transfer_date', 'ship_date', 'career', 'shipping', )

    def __init__(self, *args, **kwargs):
        super(CheckoutForm, self).__init__(*args, **kwargs)
        self.fields['country'].choices = list(Country.objects.values_list('id', 'name'))

    def clean_first_name(self):

        if 'first_name' in self.cleaned_data:
            first_name = self.cleaned_data.get('first_name')

            if not first_name:
                self.add_field_error('first_name', 'required')
            else:
                r = re.findall(r'[а-яА-я]', first_name)

                if len(r) > 0:
                    self.add_field_error('first_name', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', first_name)

                    if len(r) < 3:
                        self.add_field_error('first_name', 'min_length')

            return first_name


    def clean_last_name(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')
            else:
                r = re.findall(r'[а-яА-я]', last_name)

                if len(r) > 0:
                    self.add_field_error('last_name', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', last_name)

                    if len(r) < 3:
                        self.add_field_error('last_name', 'min_length')

            return last_name


    def clean_address_line_1(self):
        if 'address_line_1' in self.cleaned_data:
            address_line_1 = self.cleaned_data.get('address_line_1')

            if not address_line_1:
                self.add_field_error('address_line_1', 'required')
            else:
                r = re.findall(r'[а-яА-я]', address_line_1)

                if len(r) > 0:
                    self.add_field_error('address_line_1', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', address_line_1)

                    if len(r) < 3:
                        self.add_field_error('address_line_1', 'min_length')

            return address_line_1


    def clean_address_line_2(self):
        if 'address_line_2' in self.cleaned_data:
            address_line_2 = self.cleaned_data.get('address_line_2')

            if address_line_2:
                r = re.findall(r'[а-яА-я]', address_line_2)

                if len(r) > 0:
                    self.add_field_error('address_line_2', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', address_line_2)

                    if len(r) < 3:
                        self.add_field_error('address_line_2', 'min_length')

            return address_line_2


    def clean_zip_code(self):
        cnt_min = 5
        cnt_max = 6

        if 'zip' in self.cleaned_data:
            zip = self.cleaned_data.get('zip')

            if not zip:
                self.add_field_error('zip', 'required')
            else:

                if len(zip) < cnt_min:
                    self.add_field_error('zip', 'min_length')

                if len(zip) > cnt_max:
                    self.add_field_error('zip', 'max_length')

            return zip


class CityForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/field.html'

    csrf_token = False

    city = forms.CharField(
        label=_('City'),
        # choices=[],
        widget=forms.Select(
            choices=[]
        ),
        error_messages={
            'required': _('Please select your city'),
            'not_found': _('Can\'t find city'),
        },
    )

    zip_code = forms.CharField(
        widget=forms.HiddenInput(),
        error_messages={
            'required': _('Please enter your ZIP code'),
            'incorrect': _('ZIP incorrect'),
            'state_not_match': _('ZIP not match for selected state'),
        },
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def clean(self):
        if 'zip_code' in self.cleaned_data and 'city' in self.cleaned_data:
            zip_code = self.cleaned_data.get('zip_code')
            city = self.cleaned_data.get('city')

            if not zip_code:
                self.add_field_error('zip_code', 'required')
                return self.cleaned_data

            elif not city:
                self.add_field_error('city', 'required')
                return self.cleaned_data

            try:
                city_obj = City.objects.get(id=city)
            except City.DoesNotExist:
                self.add_field_error('city', 'not_found')
                return self.cleaned_data
            name_city = city_obj.__str__()
            name_country = city_obj.country.__str__()

            data_city = get_city(name_city, name_country)

            try:
                zcdb = ZipCodeDatabase()
                zcbd_zip = zcdb[zip_code]

                if not zcbd_zip.state == data_city['state_code']:
                    self.add_field_error('zip_code', 'state_not_match')
            except Exception as _:
                self.add_field_error('zip_code', 'incorrect')
                return self.cleaned_data


        return self.cleaned_data


class ClientForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/field.html'
    csrf_token = False

    email = forms.EmailField(
        label=_('E-mail Address'),
        max_length=128,
        error_messages={
            'required': _('Please enter your e-mail'),
            'max_length': _('E-mail should not be longer than %(limit_value)d characters'),
            'min_length': _("The e-mail is very short"),
            'incorrect': _('Your e-mail contains illegal characters'),
        },
        widget=forms.TextInput(
            attrs={
                'placeholder': 'James@gmail.com'
            }
        )
    )

    phone = forms.CharField(
        label=_('Phone Number'),
        max_length=32,
        error_messages={
            'required': _('Please enter your phone'),
            'max_length': _('Phone should not be longer than %(limit_value)d characters'),
            'min_length': _("The phone is very short"),
            'incorrect': _('Your phone contains illegal characters'),
        },
        widget=forms.TextInput(
            attrs={
                'placeholder': '5127911104'
            }
        )
    )

    def clean_phone(self):
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')

            if phone:
                r = re.findall(r'[а-яА-я]', phone)

                if len(r) > 0:
                    self.add_field_error('phone', 'incorrect')
                else:
                    phone = translate_phonewords(phone)

                    if is_e164_format(phone):
                        self.cleaned_data['phone'] = phone
                    else:
                        self.add_field_error('phone', 'invalid')

            return phone

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if email:
                r = re.findall(r'[а-яА-я]', email)

                if len(r) > 0:
                    self.add_field_error('email', 'incorrect')
                else:
                    try:
                        EmailValidator()(email)
                        self.cleaned_data['email'] = email
                    except forms.ValidationError:
                        self.add_field_error('email', 'invalid')

            return email


class SubscribeForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/boolean_field.html'
    csrf_token = False
    active = forms.BooleanField(
        required=False,
        label=_('Keep me up to date on news and exclusive offers'),
    )
    class Meta:
        model = Subscriber
        fields = ('active', )


class CardForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/field.html'
    csrf_token = False
    card_number = forms.CharField(
        label=_('Card Number'),
        max_length=19,
        widget=forms.TextInput(
            attrs={

            },
        ),
        error_messages={
            'required': _('Please, enter card number'),
            'invalid': _('Card number is invalid'),
        }
    )
    date_expired = forms.CharField(
        label=_('Expired'),
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'placeholder': _('MM/YY')
            }
        ),
        error_messages={
            'required': _('Please, enter date expired'),
            'invalid': _('Date expired is invalid'),
        }
    )
    cvv = forms.CharField(
        label=_('CVV code'),
        max_length=3,
        widget=forms.TextInput(
            attrs={
                'type': 'password'
            }
        ),
        error_messages={
            'required': _('Please, enter CVV'),
            'invalid': _('CVV is invalid'),
        }
    )
    name = forms.CharField(
        label=_('Holder Name'),
        max_length=32,
        widget=forms.TextInput(
            attrs={

            }
        ),
        error_messages={
            'required': _('Please, enter holder name'),
            'invalid': _('Holder name is invalid'),
        }
    )

    def clean_card_number(self):
        if 'card_number' in self.cleaned_data:
            card_number = self.cleaned_data.get('card_number')

            if not card_number:
                self.add_field_error('email', 'required')

            return card_number

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if email:
                r = re.findall(r'[а-яА-я]', email)

                if len(r) > 0:
                    self.add_field_error('email', 'incorrect')
                else:
                    try:
                        EmailValidator()(email)
                        self.cleaned_data['email'] = email
                    except forms.ValidationError:
                        self.add_field_error('email', 'invalid')

            return email


class CouponForm(FormHelperMixin, forms.Form):
    default_field_template = 'form_helper/unlabeled_field.html'
    csrf_token = False
    coupon = forms.CharField(
        max_length=32,
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Add Coupon Code')
            }
        ),
        error_messages=({
            'invalid': _('Coupon is invalid'),
            'expired': _('Coupon is expired'),
            'limit': _('Coupon\'s limit is finished'),
        })
    )

    def clean_coupon(self):
        if 'coupon' in self.cleaned_data:
            coupon = self.cleaned_data.get('coupon')

            if coupon:
                try:
                    cur_coupon = Coupon.objects.get(coupon=coupon)

                    if not (cur_coupon.date_start <= datetime.date.today() <= cur_coupon.date_end):
                        self.add_field_error('coupon', 'expired')

                    if cur_coupon.limit_count <= cur_coupon.used_count:
                        self.add_field_error('coupon', 'limit')

                except Coupon.DoesNotExist:
                    self.add_field_error('coupon', 'invalid')

            return coupon

from django import forms
from django.contrib import admin
from django.utils.timezone import now
from django.contrib.admin.utils import unquote
from django.utils.translation import ugettext_lazy as _
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from project.admin.base import SortableModelAdmin
from suit.admin import SortableStackedInline, SortableTabularInline
from libs.admin_utils import get_change_url
from libs.autocomplete.widgets import AutocompleteWidget, AutocompleteMultipleWidget
from attachable_blocks.admin import AttachedBlocksStackedInline, AttachedBlocksTabularInline, AttachableBlockAdmin
from .models import (Characteristic, CharacteristicValue, ShopConfig, Review, Subscriber, Supply, City,
                     SupplyProduct, Modification, ModificationCharacteristic, ShopProductModification, Coupon, Sale, Tax,
                     Country, ShopCategory, ShopProduct, ShopOrder, NotificationReceiver, ShopProductsBlock, ShopReviewsBlock)
from webpage.admin import StdPageAdmin, StdDisablePageAdmin, StdButtonPageAdmin
from shipstation.models import ShipStationOrderDetails


class ShopConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class ShopCategoryBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class NotificationReceiverAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = NotificationReceiver
    extra = 0
    suit_classes = 'suit-tab suit-tab-notify'


class ShopProductModificationAdmin(ModelAdminInlineMixin, SortableTabularInline):
    model = ShopProductModification
    extra = 0
    exclude = ('title', )
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-modifications'


class ShipStationOrderDetailsInline(ModelAdminInlineMixin, admin.StackedInline):
    suit_classes = 'suit-tab suit-tab-shipping'
    model = ShipStationOrderDetails
    extra = 0
    can_delete = False
    verbose_name_plural = ''
    readonly_fields = (
        'shipstation_order_number', 'shipstation_order_id',
        'shipstation_tracking_number', 'shipping_cost', 'carrier_code',
        'service_code', 'sub_service_code',
    )

    def has_add_permission(self, request):
        return False


class SupplyProductForm(forms.ModelForm):
    class Meta:
        model = SupplyProduct
        fields = ('product', 'modification', 'count')

        widgets = {
            'product': AutocompleteWidget(
                format_item=ShopProduct.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
            'modification': AutocompleteWidget(
                format_item=ShopProductModification.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
        }


class SupplyProductInlineAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = SupplyProduct
    extra = 0
    min_num = 1
    form = SupplyProductForm
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(ShopConfig)
class ShopConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = StdPageAdmin.fieldsets + StdDisablePageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + (
        (_('Address'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'country', 'region', 'city', 'address', 'zip',
            ),
        }),
        (_('About section'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'about_visible', 'about_title', 'about_description',
            ),
        }),
        (_('Payment Services (need complete API settings in menu items)'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'use_authorize', 'use_paypal', 'use_stripe',
            ),
        }),
        (_('Reviews'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'reviews_title', 'reviews_description',
            ),
        }),
        (_('Other products'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'other_title',
            ),
        }),
        (_('Cart'), {
            'classes': ('suit-tab', 'suit-tab-cart'),
            'fields': (
                'cart_header', 'cart_title', 'cart_description',
            ),
        }),
        (_('Checkout'), {
            'classes': ('suit-tab', 'suit-tab-checkout'),
            'fields': (
                'checkout_header', 'checkout_title', 'checkout_description',
            ),
        }),
        (_('Payment'), {
            'classes': ('suit-tab', 'suit-tab-payment'),
            'fields': (
                'payment_header', 'payment_title', 'payment_description',
            ),
        }),
        (_('Success'), {
            'classes': ('suit-tab', 'suit-tab-success'),
            'fields': (
                'success_header', 'success_title', 'success_description',
            ),
        }),
        (_('Empty'), {
            'classes': ('suit-tab', 'suit-tab-empty'),
            'fields': (
                'empty_title', 'empty_description',
            ),
        }),
    )
    inlines = (ShopConfigBlocksInline, NotificationReceiverAdmin)
    suit_form_tabs = (
        ('general', _('General')),
        ('cart', _('Cart')),
        ('checkout', _('Checkout')),
        ('payment', _('Payment')),
        ('success', _('Success page')),
        ('empty', _('Empty page')),
        ('notify', _('Notifications')),
        ('blocks', _('Blocks')),
    )


@admin.register(Tax)
class TaxAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'state', 'city', 'rate',
            ),
        }),
    )
    list_display = ('__str__', 'rate', )
    list_editable = ('rate', )
    list_filter = ('state', )
    search_fields = ('state', 'city', )
    suit_form_tabs = (
        ('general', _('General')),
    )


class CharacteristicValueAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = CharacteristicValue
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(Characteristic)
class CharacteristicAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
    inlines = (CharacteristicValueAdmin, )
    sortable = 'sort_order'
    list_display = ('__str__', 'sort_order', )
    suit_form_tabs = (
        ('general', _('General')),
        ('notify', _('Notifications')),
        ('blocks', _('Blocks')),
    )


@admin.register(ShopCategory)
class ShopCategoryAdmin(SeoModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (

            ),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'slug', 'is_visible'
            ),
        }),
    )
    # form = ShopCategoryForm
    inlines = (ShopCategoryBlocksInline, )
    actions = ('action_hide', 'action_show')
    list_display = ('view', 'title', 'is_visible', )
    prepopulated_fields = {
        'slug': ('title',)
    }
    sortable = 'sort_order'
    suit_form_tabs = (
        ('general', _('General')),
    )

    def action_hide(self, request, queryset):
        queryset.update(is_visible=False)
    action_hide.short_description = _('Hide selected %(verbose_name_plural)s')

    def action_show(self, request, queryset):
        queryset.update(is_visible=True)

    action_show.short_description = _('Show selected %(verbose_name_plural)s')


class ShopProductForm(forms.ModelForm):
    class Meta:
        model = ShopProduct
        fields = ('category', 'characteristics')
        widgets = {
            'category': AutocompleteWidget(
                format_item=ShopCategory.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
            'characteristics': AutocompleteMultipleWidget(
                format_item=CharacteristicValue.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            )
        }


class ProductBlocksBottomInline(AttachedBlocksTabularInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class ReviewAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = Review
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-reviews'


class ModificationCharacteristicAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = ModificationCharacteristic
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(Modification)
class ModificationAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                # 'category', 'title', 'header', 'price', 'sku','preview',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )
    inlines = (ModificationCharacteristicAdmin, )


@admin.register(ShopProduct)
class ShopProductAdmin(SeoModelAdminMixin, SortableModelAdmin):

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'category', 'title', 'header', 'price', 'sku','preview',
            ),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            # 'fields': ('weight', ),
            'fields': ('weight', 'dimension'),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'gallery', 'characteristics',
            ),
        }),
        (_('information'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'info_overview', 'info_specifications', 'info_shipping', 'info_return',
            ),
        }),
        (_('Service information'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'slug', 'is_visible',
            ),
        }),
    )
    form = ShopProductForm

    sortable = 'sort_order'
    # ordering = ('sort_order', )
    actions = ('action_hide', 'action_show')
    search_fields = ('title', 'category__title')
    list_display = (
        'view', '__str__', 'category_link',
        'price_alternative', 'sale_price', 'sale', 'stock', 'is_sold', 'is_visible',
    )
    # list_select_related = ('category', )
    list_display_links = ('__str__', )
    # actions_on_top = ()
    inlines = (ProductBlocksBottomInline, ReviewAdmin, ShopProductModificationAdmin, )
    prepopulated_fields = {
        'slug': ('title', ),
    }
    suit_form_tabs = (
        ('general', _('General')),
        ('modifications', _('Modifications')),
        ('reviews', _('Reviews')),
        ('blocks', _('Blocks')),
    )

    def suit_cell_attributes(self, obj, column):
        """ Классы для ячеек списка """
        default = super().suit_cell_attributes(obj, column)
        if column == 'micropreview':
            default.setdefault('class', '')
            default['class'] += ' mini-column'
        return default



    def micropreview(self, obj):
        if not obj.photo:
            return '-//-'
        return f'<img' \
               f'src="{obj.photo.admin_micro.url}"' \
               f'width="{obj.photo.admin_micro.target_width}"' \
               f'height="{obj.photo.admin_micro.target_height}">'
    micropreview.short_description = _('Preview')
    micropreview.allow_tags = True

    # def visible_custom(self, obj):
    #     from django.conf import settings
    #     icon = 'yes' if obj.is_visible else 'no'
    #     return f'<img src="{settings.STATIC_URL}admin/img/icon-{icon}.svg">'
    # visible_custom.short_description = _('Visible')
    # visible_custom.allow_tags = True

    def category_link(self, obj):
        if obj.category is not None:
            meta = getattr(self.model.category.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.category.pk),
                obj.category
            )
    category_link.short_description = _('Categories')
    category_link.allow_tags = True

    def price_alternative(self, obj):
        return f'<nobr>{obj.price.alternative}</nobr>'
    price_alternative.allow_tags = True
    # price_alternative.admin_order_field = 'price'
    price_alternative.short_description = _('Price')

    def action_hide(self, request, queryset):
        queryset.update(is_visible=False)
        queryset.values_list(
            'category_id', flat=True
        )
    action_hide.short_description = _('Hide selected %(verbose_name_plural)s')

    def action_show(self, request, queryset):
        queryset.update(is_visible=True)
        queryset.values_list(
            'category_id', flat=True
        )
    action_show.short_description = _('Show selected %(verbose_name_plural)s')


class ShopOrderForm(forms.ModelForm):
    DATED_FLAGS = (
        ('is_cancelled', 'cancel_date'),
        ('is_paid', 'pay_date'),
    )

    class Meta:
        model = ShopOrder
        fields = '__all__'

    def _set_date(self, value, flagname, datename):
        if value and not getattr(self.instance, flagname):
            setattr(self.instance, datename, now())
        elif not value:
            setattr(self.instance, datename, None)

    def clean(self):
        data = super().clean()
        for flagname, datename in self.DATED_FLAGS:
            value = data.get(flagname)
            if value is not None:
                self._set_date(value, flagname, datename)
        return data


@admin.register(ShopOrder)
class ShopOrderAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'uuid', 'created', 'link',
            ),
        }),
        (_('Client'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'first_name', 'last_name',
            ),
        }),
        (_('Contacts'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'email', 'phone',
            ),
        }),
        (_('Delivery'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'city', 'career'
            ),
        }),
        (_('Cancelled'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_cancelled', 'cancel_date',
            ),
        }),
        (_('Paid'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_paid', 'pay_date',
            ),
        }),
    )
    form = ShopOrderForm
    date_hierarchy = 'created'
    readonly_fields = (
        'products_cost', 'fmt_total_cost', 'created', 'first_name', 'pay_date',
        'last_name', 'email', 'phone', 'uuid', 'city','link', 'cancel_date', 'career',
    )
    list_display = (
        '__str__', 'fmt_total_cost', 'is_cancelled', 'is_paid', 'is_new', 'pay_date', 'created',
    )
    actions = ('action_set_cancelled', 'action_set_paid')
    list_filter = ('is_new', 'is_cancelled', 'is_paid')
    search_fields = ('uuid', 'is_new')
    inlines = (ShipStationOrderDetailsInline, )
    suit_form_tabs = (
        ('general', _('General')),
        ('products', _('Products')),
        ('shipping', _('Shipping')),
        ('status', _('Status')),
    )
    suit_form_includes = (
        ('shop/admin/products.html', 'top', 'products'),
    )

    def suit_row_attributes(self, obj, request):
        if obj.is_cancelled:
            return {'class': 'error'}
        if obj.is_paid:
            return {'class': 'success'}

    def fmt_total_cost(self, obj):
        return obj.total_cost
    fmt_total_cost.short_description = _('Total cost')
    fmt_total_cost.admin_order_field = 'products_cost'

    def has_add_permission(self, request):
        return False

    def action_set_paid(self, request, queryset):
        queryset.update(is_paid=True)
    action_set_paid.short_description = _('Set %(verbose_name_plural)s paid')

    def action_set_cancelled(self, request, queryset):
        queryset.update(is_cancelled=True)
    action_set_cancelled.short_description = _('Set %(verbose_name_plural)s cancelled')

    def change_view(self, request, object_id, *args, **kwargs):
        if object_id is None:
            entity = None
        else:
            entity = self.get_object(request, unquote(object_id))

            if entity.is_new:
                entity.is_new = False

            entity.save()

        extra_context = kwargs.pop('extra_context', None) or {}
        kwargs['extra_context'] = dict(extra_context, **{
            'entity': entity,
        })
        return super().change_view(request, object_id, *args, **kwargs)


class SaleForm(forms.ModelForm):
    class Meta:
        model = Sale
        fields = ('categories', 'products', 'characteristics', 'modifications')
        widgets = {
            'categories': AutocompleteMultipleWidget(
                format_item=ShopCategory.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
            'products': AutocompleteMultipleWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
            'characteristics': AutocompleteMultipleWidget(
                format_item=CharacteristicValue.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
            'modifications': AutocompleteMultipleWidget(
                format_item=ShopProductModification.autocomplete__product_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
        }


@admin.register(Sale)
class SaleAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'categories', 'products', 'characteristics', 'modifications',
            ),
        }),
        (_('Period'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'date_start', 'date_end',
            ),
        }),
        (_('Discount'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'discount_value', 'discount_type'
            ),
        }),
    )
    form = SaleForm
    list_display = (
        '__str__', 'discount', 'date_start', 'date_end'
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Coupon)
class CouponAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
        (_('Period'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'date_start', 'date_end',
            ),
        }),
        (_('Discount'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'discount_value', 'discount_type',
            ),
        }),
        (_('Coupon'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'limit_count', 'used_count', 'coupon',
            ),
        }),
    )
    readonly_fields = ('used_count', )
    list_display = (
        '__str__', 'discount', 'date_start', 'date_end', 'coupon', 'remaining_quantity', 'is_active',
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Subscriber)
class SubscriberAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'email', 'active',
            ),
        }),
    )
    list_display = (
        'email', 'active',
    )
    list_editable = (
        'active',
    )
    search_fields = ('email', )
    list_filter = ('active', )
    actions = ('action_hide', 'action_show', )
    suit_form_tabs = (
        ('general', _('General')),
    )

    def has_add_permission(self, request):
        return False

    def action_hide(self, request, queryset):
        queryset.update(active=False)
    action_hide.short_description = _('Hide selected %(verbose_name_plural)s')

    def action_show(self, request, queryset):
        queryset.update(active=True)

    action_show.short_description = _('Show selected %(verbose_name_plural)s')


class CityAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = City
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(Country)
class CountryAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name',
            ),
        }),
    )
    inlines = (CityAdmin, )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Supply)
class SupplyAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'manager', 'date',
            ),
        }),
    )
    readonly_fields = ('date', )
    inlines = (SupplyProductInlineAdmin, )
    suit_form_tabs = (
        ('general', _('General')),
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(ShopProductsBlock)
class BlogBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )


@admin.register(ShopReviewsBlock)
class BlogBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'description',
            ),
        }),
    )
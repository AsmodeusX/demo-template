from django.utils.translation import ugettext_lazy as _


# Имя ключа сессии, где хранится корзина
SESSION_CART_NAME = 'cart'
SESSION_COUPON_NAME = 'coupon'
SESSION_DELIVERY_NAME = 'delivery'
SESSION_UUID_NAME = 'order'
SESSION_ADDRESS_NAME = 'address'

# Максимальное кол-во каждой единицы товара в заказе
MAX_PRODUCT_COUNT = 99

BLOCK_PRODUCTS_COUNT = 4

DISCOUNT_TYPE_AMOUNT = 1
DISCOUNT_TYPE_PERCENT = 2

DISCOUNT_TYPES = (
    (DISCOUNT_TYPE_AMOUNT, _('Amount')),
    (DISCOUNT_TYPE_PERCENT, _('Percent'))
)

PAYMENT_METHODS = {
    'authorize': 'authorize',
    'paypal': 'paypal',
    'stripe': 'stripe',
}

# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-10-28 06:07
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0090_auto_20221026_0146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coupon',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 11, 8), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 10, 31), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 11, 8), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 10, 31), verbose_name='date start sale'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-04-14 06:12
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0077_auto_20220411_0530'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopproduct',
            name='dimension',
        ),
        migrations.RemoveField(
            model_name='shopproduct',
            name='weight',
        ),
        migrations.RemoveField(
            model_name='shopproductmodification',
            name='dimension',
        ),
        migrations.RemoveField(
            model_name='shopproductmodification',
            name='weight',
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 25), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 4, 17), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 25), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 4, 17), verbose_name='date start sale'),
        ),
    ]

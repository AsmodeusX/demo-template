# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-12-23 06:42
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_auto_20211222_0836'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopproduct',
            name='sku',
            field=models.CharField(default='', max_length=128, verbose_name='sku'),
        ),
        migrations.AddField(
            model_name='shopproduct',
            name='sort_order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='sort order'),
        ),
        migrations.AlterField(
            model_name='shopproduct',
            name='price',
            field=libs.valute_field.fields.ValuteField(max_digits=7, verbose_name='price'),
        ),
    ]

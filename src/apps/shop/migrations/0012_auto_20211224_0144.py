# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-12-24 06:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20211224_0102'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='characteristicvalue',
            options={'ordering': ('value',), 'verbose_name': 'characteristic value', 'verbose_name_plural': 'characteristic values'},
        ),
        migrations.AddField(
            model_name='characteristicvalue',
            name='title',
            field=models.CharField(default='xxx', max_length=64, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='characteristicvalue',
            name='characteristic',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.Characteristic', verbose_name='characteristic'),
        ),
    ]

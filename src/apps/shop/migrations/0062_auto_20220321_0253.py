# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-03-21 06:53
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0061_auto_20220318_0246'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='review',
            options={'ordering': ('sort_order',), 'verbose_name': 'review', 'verbose_name_plural': 'reviews'},
        ),
        migrations.AddField(
            model_name='shopconfig',
            name='about_description',
            field=models.TextField(blank=True, verbose_name='description'),
        ),
        migrations.AddField(
            model_name='shopconfig',
            name='about_title',
            field=models.CharField(default='About our products', max_length=60, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 1), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 24), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 1), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 24), verbose_name='date start sale'),
        ),
    ]

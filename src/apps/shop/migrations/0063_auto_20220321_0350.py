# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-03-21 07:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0062_auto_20220321_0253'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopconfig',
            name='about_visible',
            field=models.BooleanField(default=True, verbose_name='show about section'),
        ),
        migrations.AlterField(
            model_name='shopconfig',
            name='about_title',
            field=models.CharField(blank=True, default='About our products', max_length=60, verbose_name='title'),
        ),
    ]

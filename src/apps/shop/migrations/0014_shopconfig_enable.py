# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-12-24 07:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0013_auto_20211224_0153'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopconfig',
            name='enable',
            field=models.BooleanField(default=True, verbose_name='enable module'),
        ),
    ]

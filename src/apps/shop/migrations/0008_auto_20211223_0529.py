# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-12-23 10:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0007_auto_20211223_0454'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=64, verbose_name='title')),
                ('date_start', models.DateField(verbose_name='date start sale')),
                ('date_end', models.DateField(default=datetime.date(2022, 1, 2), verbose_name='date end sale')),
                ('discount_value', models.PositiveSmallIntegerField(default=10, verbose_name='discount value')),
                ('discount_type', models.PositiveSmallIntegerField(choices=[(1, 'Amount'), (2, 'Percent')], default=2, verbose_name='discount type')),
                ('categories', models.ManyToManyField(blank=True, related_name='categories', to='shop.ShopCategory', verbose_name='categories')),
            ],
            options={
                'verbose_name': 'product',
                'verbose_name_plural': 'products',
            },
        ),
        migrations.AlterField(
            model_name='shopproduct',
            name='preview',
            field=libs.stdimage.fields.StdImageField(aspects=('wide',), default='', min_dimensions=(350, 350), storage=libs.storages.media_storage.MediaStorage('webpage/images'), upload_to='', variations={'admin': {'size': (200, 200)}, 'normal': {'create_webp': True, 'size': (280, 330)}, 'small': {'create_webp': True, 'size': (160, 185)}}, verbose_name='image'),
        ),
        migrations.AddField(
            model_name='sale',
            name='products',
            field=models.ManyToManyField(blank=True, related_name='products', to='shop.ShopProduct', verbose_name='products'),
        ),
    ]

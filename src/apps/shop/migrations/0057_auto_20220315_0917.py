# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-03-15 13:17
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0056_auto_20220304_0502'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopconfig',
            name='use_authorize',
            field=models.BooleanField(default=True, verbose_name='use authorize'),
        ),
        migrations.AddField(
            model_name='shopconfig',
            name='use_paypal',
            field=models.BooleanField(default=True, verbose_name='use paypal'),
        ),
        migrations.AddField(
            model_name='shopconfig',
            name='use_stripe',
            field=models.BooleanField(default=True, verbose_name='use stripe'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 3, 26), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 18), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 3, 26), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 18), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='shopproduct',
            name='characteristics',
            field=models.ManyToManyField(blank=True, related_name='product_characteristics', to='shop.CharacteristicValue'),
        ),
    ]

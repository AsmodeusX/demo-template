# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-04-05 10:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0071_auto_20220405_0529'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopproductmodification',
            name='title',
            field=models.CharField(blank=True, max_length=255, verbose_name='title'),
        ),
        migrations.AddField(
            model_name='supplyproduct',
            name='modification',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='supply_products', to='shop.ShopProductModification', verbose_name='modification'),
        ),
        migrations.AlterField(
            model_name='supplyproduct',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='supply_products', to='shop.ShopProduct', verbose_name='product'),
        ),
        migrations.AlterUniqueTogether(
            name='shopproductmodification',
            unique_together=set([('product', 'modification')]),
        ),
    ]

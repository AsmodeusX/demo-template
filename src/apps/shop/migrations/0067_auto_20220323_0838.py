# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2022-03-23 12:38
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0066_auto_20220322_0545'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopproduct',
            name='pcs',
        ),
        migrations.AddField(
            model_name='shopproduct',
            name='header',
            field=models.CharField(default='', max_length=128, verbose_name='header'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 3), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 26), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_end',
            field=models.DateField(default=datetime.date(2022, 4, 3), verbose_name='date end sale'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_start',
            field=models.DateField(default=datetime.date(2022, 3, 26), verbose_name='date start sale'),
        ),
        migrations.AlterField(
            model_name='shopproduct',
            name='title',
            field=models.CharField(max_length=64, verbose_name='title'),
        ),
    ]

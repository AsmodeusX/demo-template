from django.conf.urls import url
from libs.autoslug import ALIAS_REGEXP
from . import views


app_name = 'shop'
urlpatterns = [
    url(
        r'^$', views.IndexView.as_view(),
        name='index'
    ),
    url(
        r'^cart/$', views.CartView.as_view(),
        name='cart'
    ),
    url(
        r'^checkout/$', views.CheckoutView.as_view(),
        name='checkout'
    ),
    url(
        r'^payment/$', views.PaymentView.as_view(),
        name='payment'
    ),
    url(
        r'^payment/success/$', views.PaymentSuccessView.as_view(),
        name='payment_success'
    ),
    url(
        r'^(?P<category_slug>{0})/(?P<slug>{0})/$'.format(ALIAS_REGEXP),
        views.ProductView.as_view(),
        name='detail'
    ),
]

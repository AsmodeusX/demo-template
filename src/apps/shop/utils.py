def list_to_str(obj):
    arr = []

    for elem in obj:
        arr.append(str(elem))

    return ", ".join(arr)

def str_to_list(obj):
    l = []
    arr = obj.split(', ')

    for elem in arr:
        l.append(elem)

    return l

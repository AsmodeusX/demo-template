from .models import FAQConfig
from webpage.views import StdPageSingletonView


class IndexView(StdPageSingletonView):
    model = FAQConfig
    template_name = 'faq/index.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context.update({
            'is_faq_page': True,
        })
        return context

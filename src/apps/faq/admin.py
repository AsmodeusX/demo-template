from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from attachable_blocks.admin import AttachedBlocksStackedInline
from project.admin.base import ModelAdminInlineMixin
from suit.admin import SortableStackedInline
from .models import FAQConfig, FAQQuestion
from webpage.admin import SEOStdPageAdmin, TextPageAdmin, StdDisablePageAdmin, StdButtonPageAdmin


class FAQConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class FAQQuestionInline(ModelAdminInlineMixin, SortableStackedInline):
    model = FAQQuestion
    extra = 0
    # max_num = 10
    suit_classes = 'suit-tab suit-tab-faq'
    sortable = 'sort_order'


@admin.register(FAQConfig)
class FAQConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = SEOStdPageAdmin.fieldsets + StdDisablePageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (

            ),
        }),
    )
    inlines = (FAQQuestionInline, FAQConfigBlocksInline, )
    suit_form_tabs = (
        ('general', _('General')),
        ('faq', _('FAQ')),
        ('blocks', _('Blocks')),
    )

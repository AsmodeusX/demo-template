from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor.fields import CKEditorUploadField, CKEditorField
from webpage.models import StdPage, TextPage, StdDisablePage, StdButtonPage


class FAQConfig(StdPage, StdDisablePage, StdButtonPage, SingletonModel):

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('faq:index')

    def __str__(self):
        return self.header


class FAQQuestion(models.Model):
    config = models.ForeignKey(FAQConfig, related_name='questions')
    question = models.CharField(_('question'), max_length=255)
    answer = CKEditorField(_('answer'), )

    sort_order = models.IntegerField(_('order'), default=0)

    class Meta:
        ordering = ('sort_order',)
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')

    def __str__(self):
        return self.question

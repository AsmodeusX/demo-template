(function($) {
    var clActive = 'active';

    $(document).ready(function() {
        var $questions_wrapper = $('.questions'),
            $questions = $questions_wrapper.find('.question');

            $questions.find('.question__answer').slideUp(0);

            $questions.on('click', function(e) {
                if (
                    e.target.tagName !== 'A' &&
                    e.target.tagName !== 'IFRAME'  &&
                    e.target.tagName !== 'VIDEO') {
                    var $question = $(this);
                    if ($question.hasClass(clActive)) {
                        $questions.removeClass(clActive).find('.question__answer').slideUp('slow');
                    } else {
                        $questions.removeClass(clActive).find('.question__answer').slideUp('slow');
                        $question.addClass(clActive).find('.question__answer').slideDown('slow');
                    }
                }
            })
    });

})(jQuery);
from django.template import loader
from .models import Service
from menu.utils import activate_menu_nolink
from webpage.views import StdPageDetailView


def block_format(context, title, services=None):

    return loader.render_to_string('services/block.html', {
        'title': title,
        'services': services if services else Service.objects.root_block_services(),
    }, request=context.get('request'))


class ServiceView(StdPageDetailView):
    model = Service
    template_name = 'services/detail.html'

    def get_context_data(self, **kwargs):
        ancestors = self.object.get_ancestors()
        children = self.object.get_children()

        prefixes = ['mobile', 'header', 'footer']

        if ancestors or children:
            activate_menu_nolink(self.request, 'services')

        for parent in self.object.get_ancestors():
            for prefix in prefixes:
                activate_menu_nolink(self.request, 'service_{prefix}_{service_id}'.format(
                    prefix=prefix,
                    service_id=parent.id
                ))

                activate_menu_nolink(self.request, 'service_{prefix}_{service_id}_link'.format(
                    prefix=prefix,
                    service_id=parent.id
                ))


        context = super().get_context_data(**kwargs)
        context.update({
            'is_service_page': True
        })
        return context


# @cached('block.id')
def services_block_render(context, block, **kwargs):
    return block_format(context, block.title)


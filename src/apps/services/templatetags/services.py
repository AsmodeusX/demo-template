from django.template import loader
from libs import jinja2
from ..models import Service
from django.core.exceptions import ObjectDoesNotExist

BLOCK_TEMPLATE = 'services/block.html'


def return_no_services(template, context):
    return loader.render_to_string(template, {
        'services': [],
    }, request=context.get('request'))


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'other_services', 'sub_services', }
    takes_context = True

    def _other_services(self, context, service=None, classes='', template=None):
        if not service:
            return ''

        try:
            service = Service.objects.get(id=service)
        except ObjectDoesNotExist:
            return ''

        services = service.get_siblings().filter(visible=True)

        return loader.render_to_string(template or BLOCK_TEMPLATE, {
            'title': service.title_other_services or None,
            'classes': classes,
            'services': services,
        }, request=context.get('request'))

    def _sub_services(self, context, service=None, classes='', template=None):
        if not service:
            return ''

        try:
            service = Service.objects.get(id=service)
        except ObjectDoesNotExist:
            return ''

        services = service.get_children().filter(visible=True)

        return loader.render_to_string(template or BLOCK_TEMPLATE, {
            'title': service.title_sub_services or None,
            'classes': classes,
            'services': services,
        }, request=context.get('request'))

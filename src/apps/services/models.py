from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.functional import cached_property
from solo.models import SingletonModel
from ckeditor.fields import CKEditorUploadField
from attachable_blocks.models import AttachableBlock
from libs.autoslug import AutoSlugField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from libs.mptt import *
from webpage.models import StdImagePage, TextPage, StdButtonPage, StdDisablePage


class ServicesConfig(SingletonModel, StdDisablePage):

    class Meta:
        verbose_name = _('Settings')

    def __str__(self):
        return ugettext('Settings')


class ServiceTreeManager(MPTTQuerySetManager):

    def visible(self):
        if not ServicesConfig.get_solo().enable:
            return None

        return self.filter(
            visible=True,
        )

    def root_services(self):
        return self.root_nodes().filter(visible=True)

    def root_block_services(self):
        return self.root_services().filter(visible_block=True)

    def get_hierarchy(self, active_service=None, exclude_service=None, **kwargs):
        """
            Возврат всего дерева сервисов одним SQL-запросом.
        """
        if active_service is not None:
            active_nodes = active_service.get_ancestors(include_self=True).values_list('pk', flat=True)
        else:
            active_nodes = []

        services = self.filter(visible=True, **kwargs).select_related('parent')
        if exclude_service:
            services = services.exclude(pk=exclude_service.pk)

        hierarchy = []
        hierarchy_map = {}
        for node in services:
            # Активный ли сервис
            if node.pk in active_nodes:
                node.active = True

            node.childs = []

            if node.parent_id is None:
                hierarchy_map[node.pk] = node
                hierarchy.append(node)
            elif node.parent_id in hierarchy_map:
                hierarchy_map[node.pk] = node
                parent_node = hierarchy_map[node.parent_id]
                parent_node.childs.append(node)
            else:
                pass

        return hierarchy


class Service(MPTTModel, StdImagePage, StdButtonPage, TextPage):
    preview = StdImageField(_('preview'),
                            default='',
                            storage=MediaStorage('services/preview'),
                            min_dimensions=(355, 355),
                            admin_variation='admin',
                            crop_area=True,
                            aspects=('normal',),
                            variations=dict(
                                normal=dict(
                                    size=(355, 355),
                                    create_webp=True,
                                    # create_avif=True,
                                ),
                                tablet=dict(
                                    size=(350, 250),
                                    create_webp=True,
                                    # create_avif=True,
                                ),
                                mobile=dict(
                                    size=(160, 160),
                                    create_webp=True,
                                    # create_avif=True,
                                ),
                                admin=dict(
                                    size=(100, 100),
                                ),
                            ),
                            )
    alt_preview = models.CharField(_('alt for preview'), max_length=128, blank=True)
    text = CKEditorUploadField(_('text'), blank=True)
    note = models.TextField(_('note'), blank=True)
    title_other_services = models.CharField(_('title other services block'), default='Others Our Unrivaled Services', max_length=128)
    title_sub_services = models.CharField(_('title sub services block'), default='Related services', max_length=128)
    parent = TreeForeignKey('self',
                            blank=True,
                            null=True,
                            limit_choices_to={
                                'parent': None
                            },

                            verbose_name=_('parent service'),
                            related_name='children',

                            )

    visible = models.BooleanField(_('visible'), default=True)
    visible_block = models.BooleanField(_('show in services block'), default=True)
    slug = AutoSlugField(_('slug'), populate_from='header', unique=True)
    sort_order = models.PositiveIntegerField(_('order'), default=0)

    objects = ServiceTreeManager()

    class Meta:
        verbose_name = _('service')
        verbose_name_plural = _('services')

    class MPTTMeta:
        order_insertion_by = ('sort_order',)

    def get_absolute_url(self):
        if self.parent:
            return resolve_url('services:service', parent_slug=self.parent.slug, slug=self.slug)
        else:
            return resolve_url('root_router', slug=self.slug)

    @cached_property
    def subservices(self):
        """ Видимые дочерние подсервисы """
        return self.get_children().filter(visible=True)

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': '–' * obj.level + ' ' + obj.title,
        }

    @staticmethod
    def module_enabled():
        return ServicesConfig.get_solo().enable

    def __str__(self):
        return self.header


class ServicesBlock(AttachableBlock):
    BLOCK_VIEW = 'services.views.services_block_render'

    title = models.CharField(_('title'), max_length=128, )

    class Meta:
        default_permissions = ()
        verbose_name = _('Services block')
        verbose_name_plural = _('Services blocks')

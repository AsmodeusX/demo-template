from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from libs.autocomplete.widgets import AutocompleteWidget
from attachable_blocks.admin import AttachableBlockAdmin, AttachedBlocksTabularInline
from .models import ServicesConfig, Service, ServicesBlock
from webpage.admin import SEOStdImagePageAdmin, TextPageAdmin, StdDisablePageAdmin, StdButtonPageAdmin
from solo.admin import SingletonModelAdmin
from project.admin.base import ModelAdminMixin
from libs.mptt import *


class ServiceBlocksTopInline(AttachedBlocksTabularInline):
    set_name = 'top'
    verbose_name = 'Top block'
    verbose_name_plural = 'Top blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


class ServiceBlocksMiddleInline(AttachedBlocksTabularInline):
    set_name = 'middle'
    verbose_name = 'Middle block'
    verbose_name_plural = 'Middle blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


class ServiceBlocksBottomInline(AttachedBlocksTabularInline):
    verbose_name = 'Bottom block'
    verbose_name_plural = 'Bottom blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(ServicesConfig)
class ServicesConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = StdDisablePageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (

            ),
        }),
    )

    suit_form_tabs = (
        ('general', _('General')),
    )


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = '__all__'
        widgets = {
            'parent': AutocompleteWidget(
                expressions='header__icontains',
                format_item=Service.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        """ Exclude self """
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['parent'].queryset = self.fields['parent'].queryset.exclude(pk=self.instance.pk)


@admin.register(Service)
class ServiceAdmin(SEOStdImagePageAdmin, SortableMPTTModelAdmin):
    fieldsets = SEOStdImagePageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'parent', 'slug', 'preview', 'alt_preview', 'note', 'visible',
            ),
        }),
        (_('Content'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
        (_('Blocks'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title_other_services', 'title_sub_services', 'visible_block',
            ),
        }),
    ) + TextPageAdmin.fieldsets
    actions = ('make_visible_action', 'make_invisible_action')
    sortable = 'sort_order'
    form = ServiceForm
    list_display = ('title', 'visible', 'visible_block', )
    mptt_level_indent = 20
    prepopulated_fields = {
        'slug': ('header', ),
    }
    inlines = (ServiceBlocksTopInline, ServiceBlocksMiddleInline, ServiceBlocksBottomInline, )
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )

    def make_visible_action(self, request, queryset):
        queryset.update(visible=True)
    make_visible_action.short_description = _('Make visible')

    def make_invisible_action(self, request, queryset):
        queryset.update(visible=False)
    make_invisible_action.short_description = _('Make invisible')


@admin.register(ServicesBlock)
class ServicesBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
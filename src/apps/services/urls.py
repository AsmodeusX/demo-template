from django.conf.urls import url
from libs.autoslug import ALIAS_REGEXP
from . import views


app_name = 'services'
urlpatterns = [
    url(r'^(?P<parent_slug>{0})/(?P<slug>{0})/$'.format(ALIAS_REGEXP), views.ServiceView.as_view(), name='service'),
]

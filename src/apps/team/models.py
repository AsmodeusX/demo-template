from django.db import models
from attachable_blocks.models import AttachableBlock
from django.utils.translation import ugettext_lazy as _
from social_networks.conf import SOCIAL_LINK_CHOICES
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage


class TeamBlock(AttachableBlock):
    BLOCK_VIEW = 'team.views.team_block_render'
    title = models.CharField(_('title'), max_length=128, default='Meet Our Team')

    class Meta:
        verbose_name = _('Team block')
        verbose_name_plural = _('Team blocks')


class Position(models.Model):
    title = models.CharField(_('title'), max_length=128, blank=True)

    class Meta:
        verbose_name = _('Position')
        verbose_name_plural = _('Positions')
        ordering = ('title', )

    def __str__(self):
        return self.title


class Worker(models.Model):
    name = models.CharField(_('title'), max_length=128)
    position = models.ForeignKey(Position, verbose_name=_('position'))
    description = models.TextField(_('description'), blank=True, max_length=100)

    photo = StdImageField(_('photo'),
                          default='',
                          storage=MediaStorage('team/photos'),
                          min_dimensions=(255, 255),
                          admin_variation='admin',
                          crop_area=True,
                          aspects=('desktop',),
                          variations=dict(
                              desktop=dict(
                                  size=(255, 255),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              mobile=dict(
                                  size=(165, 165),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              admin=dict(
                                  size=(100, 100),
                              ),
                          ),
                          )

    order = models.IntegerField(_('order'), default=0)

    class Meta:
        verbose_name = _('Worker')
        verbose_name_plural = _('Workers')
        ordering = ('order', 'id')

    def __str__(self):
        return self.name


class LinkWorker(models.Model):
    worker = models.ForeignKey(Worker, verbose_name=_('worker'), related_name='social_links', on_delete=models.CASCADE)
    provider = models.CharField(_('provider'), choices=SOCIAL_LINK_CHOICES, max_length=16)
    url = models.URLField(_('URL'))
    order = models.IntegerField(_('order'), default=0)

    class Meta:
        ordering = ('order', )
        verbose_name = _('Link')
        verbose_name_plural = _('Links')

    def __str__(self):
        return self.get_provider_display()
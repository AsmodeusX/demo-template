# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-11-17 13:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0003_auto_20211112_0647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkworker',
            name='provider',
            field=models.CharField(choices=[('pinterest', 'Pinterest'), ('vimeo', 'Vimeo'), ('google', 'Google'), ('linkedin', 'LinkedIn'), ('twitter', 'Twitter'), ('youtube', 'YouTube'), ('instagram', 'Instagram'), ('yelp', 'Yelp'), ('facebook', 'Facebook'), ('teleprice', 'TelePrice')], max_length=16, verbose_name='provider'),
        ),
    ]

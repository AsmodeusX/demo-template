# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-11-12 11:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0002_auto_20211026_0742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='worker',
            name='description',
            field=models.TextField(blank=True, max_length=100, verbose_name='description'),
        ),
    ]

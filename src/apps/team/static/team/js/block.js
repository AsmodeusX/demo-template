(function($) {
    var team_count = Number(document.documentElement.style.getPropertyValue('--team-count')) || 4,
        team_count_tablet = Number(document.documentElement.style.getPropertyValue('--team-count-tablet')) || 2,
        team_count_mobile = Number(document.documentElement.style.getPropertyValue('--team-count-mobile')) || 1;

    var tablet_size = 1024,
        mobile_size = 768;

    var slider_config = {
        pagination: true,
        navigation: true
    }

    var slider_params = {
        spaceBetween: 20,
        threshold: 10,
        watchSlidesVisibility: true,
        watchOverflow: true
    };

    $(window).on('load', function() {
        var $blocks = $('.block--team');

        $blocks.each(function () {
            var $block = $(this);
            setSwiper($block);
        });
    });

    function setSwiper($block) {
        var w = $.winWidth();
        var $workers = $block.find('.workers');

        slider_params['slidesPerView'] = w >= tablet_size ? team_count : w >= mobile_size ? team_count_tablet : team_count_mobile;

        if (slider_config.pagination) {
            slider_params["pagination"] = {
                el: $block.find('.swiper-pagination').get(0),
                type: 'progressbar'
            };
        }

        if (slider_config.navigation) {
            slider_params["navigation"] = {
                nextEl: $block.find('div.swiper-button-next').get(0),
                prevEl: $block.find('div.swiper-button-prev').get(0)
            };
        }

        new Swiper($workers.find('.swiper-container').get(0), slider_params);
    }

})(jQuery);
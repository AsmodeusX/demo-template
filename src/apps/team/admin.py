from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from attachable_blocks.admin import AttachableBlockAdmin
from .models import TeamBlock, Position, Worker, LinkWorker
from suit.admin import SortableModelAdmin, SortableStackedInline
from project.admin.base import ModelAdminMixin, ModelAdminInlineMixin


class LinkInline(ModelAdminInlineMixin, SortableStackedInline):
    model = LinkWorker
    min_num = 0
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(Position)
class PositionAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets =  (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Worker)
class WorkerAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets =  (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'position', 'description', 'photo',
            ),
        }),
    )

    list_display = ('name', 'position')
    inlines = (LinkInline, )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(TeamBlock)
class TeamBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )


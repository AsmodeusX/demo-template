from django.shortcuts import loader
from .models import Worker
from libs.cache.cached import cached
from webpage.utils import UsedBlocks


# @cached('block.id')
def team_block_render(context, block, **kwargs):
    workers = Worker.objects.all()

    if not workers.exists():
        return ''

    return loader.render_to_string('team/block.html', {
        'block': block,
        'workers': workers,
    }, request=context.get('request'))

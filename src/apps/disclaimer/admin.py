from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from attachable_blocks.admin import AttachedBlocksTabularInline
from .models import DisclaimerConfig
from webpage.admin import SEOStdPageAdmin, StdButtonPageAdmin, StdDisablePageAdmin


class DisclaimerBlocksTopInline(AttachedBlocksTabularInline):
    set_name = 'top'
    verbose_name = 'Top block'
    verbose_name_plural = 'Top blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


class DisclaimerBlocksBottomInline(AttachedBlocksTabularInline):
    verbose_name = 'Bottom block'
    verbose_name_plural = 'Bottom blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(DisclaimerConfig)
class DisclaimerConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = SEOStdPageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + StdDisablePageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    inlines = (DisclaimerBlocksTopInline, DisclaimerBlocksBottomInline)
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )




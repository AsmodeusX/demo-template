from .models import DisclaimerConfig
from webpage.views import StdPageSingletonView


class IndexView(StdPageSingletonView):
    model = DisclaimerConfig
    template_name = 'disclaimer/index.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context.update({
            'is_disclaimer_page': True,
        })
        return context

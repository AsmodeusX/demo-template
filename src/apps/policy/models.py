from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from webpage.models import TextPage, StdPage, StdButtonPage, StdDisablePage
from ckeditor.fields import CKEditorUploadField


class PolicyConfig(SingletonModel, StdPage, StdButtonPage, StdDisablePage):
    text = CKEditorUploadField(_('text'), blank=True)

    class Meta:
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('policy:index')

    def __str__(self):
        return self.header

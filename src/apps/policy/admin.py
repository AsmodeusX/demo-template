from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from attachable_blocks.admin import AttachedBlocksTabularInline
from .models import PolicyConfig
from webpage.admin import SEOStdPageAdmin, StdButtonPageAdmin, StdDisablePageAdmin


class PolicyBlocksTopInline(AttachedBlocksTabularInline):
    set_name = 'top'
    verbose_name = 'Top block'
    verbose_name_plural = 'Top blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


class PolicyBlocksBottomInline(AttachedBlocksTabularInline):
    verbose_name = 'Bottom block'
    verbose_name_plural = 'Bottom blocks'
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(PolicyConfig)
class PolicyConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = SEOStdPageAdmin.fieldsets + StdButtonPageAdmin.fieldsets + StdDisablePageAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    inlines = (PolicyBlocksTopInline, PolicyBlocksBottomInline)
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )




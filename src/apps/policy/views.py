from .models import PolicyConfig
from webpage.views import StdPageSingletonView


class IndexView(StdPageSingletonView):
    model = PolicyConfig
    template_name = 'policy/index.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context.update({
            'is_policy_page': True,
        })
        return context

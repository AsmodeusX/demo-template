from django.conf import settings
from urllib import parse
from bs4 import BeautifulSoup as Soup
from io import BytesIO
from PIL import Image
import re
import os
import requests


re_nbsp = re.compile('(?<![\w’\'"])([\w’]{1,3})\s+')


class UsedBlocks:
    BLOCKS = []

    @classmethod
    def add_block(cls, name):
        cls.BLOCKS.append(name)
        return cls.BLOCKS

    @classmethod
    def list_blocks(cls):
        return cls.BLOCKS

    @classmethod
    def clean_blocks(cls):
        return cls.BLOCKS.clear()


def _get_yt_key(src):
    key = None

    parse_patterns = (
        re.compile(r'(?:https?:)?//(?:www\.)?youtube\.com/watch\?v=([-\w]{11,})'),
        re.compile(r'(?:https?:)?//(?:www\.)?youtube\.com/(?:v|embed)/([-\w]{11,})'),
        re.compile(r'(?:https?:)?//youtu\.be/([-\w]{11,})'),
    )

    for pattern in parse_patterns:
        match = pattern.match(src)

        if match:
            key = match.group(1)

    return key


def _image_lazyload_swiper(tag):
    tag['data-src'] = tag['src']

    if tag.get('srcset'):
        tag['data-srcset'] = tag['srcset']
        del tag['srcset']

    if tag.get('sizes'):
        tag['data-sizes'] = tag['sizes']
        del tag['sizes']

    if not tag.get('alt'):
        tag['alt'] = ''

    tag['src'] = "/static/img/blank.gif"
    tag['class'] = 'swiper-lazy'
    tag['loading'] = 'lazy'
    tag['decoding'] = 'async'


def _image_lazyload(tag):

    if tag.get('srcset'):
        tag['data-srcset'] = tag['srcset']
        del tag['srcset']

    if tag.get('sizes'):
        tag['data-sizes'] = tag['sizes']
        del tag['sizes']

    if tag.get('alt'):
        tag['alt'] = ''

    tag['loading'] = 'lazy'
    tag['decoding'] = 'async'

    return tag


def _set_swiper_lazy(blocks):
    for block in blocks:
        for tag in block.findAll('img'):
            _image_lazyload_swiper(tag)

            if tag.string:
                tag.string = parse.unquote(tag.string)


def _set_lazy(soup, blocks):
    for block in blocks:
        for tag in block.findAll('img'):
            # noscript_tag = soup.new_tag('noscript')

            new_tag = soup.new_tag('img',
                                   src=tag.get('src'),
                                   sizes=tag.get('sizes'),
                                   alt=tag.get('alt'),
                                   title=tag.get('title'),
                                   srcset=tag.get('srcset'),
                                   width=tag.get('width'),
                                   height=tag.get('height'),
                                   )

            if tag.get('id'):
                new_tag['id'] = tag.get('id')

            if not tag.get('alt'):
                tag['alt'] = ''

            if tag.get('data-description'):
                new_tag['data-description'] = tag.get('data-description'),
            new_tag['class'] = tag.get('class')
            tag = _image_lazyload(tag)
            # noscript_tag.append(new_tag)
            # tag.insert_after(noscript_tag)

            if tag.string:
                tag.string = parse.unquote(tag.string)

    return soup.body.decode_contents()


def _set_youtube(soup, tag):
    noscript_tag = soup.new_tag('noscript')
    src = tag.get('src')
    img_dir = f'{settings.MEDIA_ROOT}/yt_previews'

    if not os.path.isdir(img_dir):
        os.mkdir(img_dir)

    key = _get_yt_key(src)
    sources = []

    if key:
        variations = ['maxresdefault', 'sddefault', 'mqdefault', 'hqdefault', 'default']

        for variation in variations:
            src_preview = f'https://img.youtube.com/vi/{key}/{variation}.jpg'

            try:
                r = requests.get(src_preview, timeout=1)

                if r.status_code != 404:
                    source_image = Image.open(BytesIO(r.content))
                    source_format = source_image.format

                    source_info = source_image.info
                    dest_info = source_info.copy()
                    if source_format in ('PNG', 'JPG'):
                        dest_info['optimize'] = True

                    img_path = f'{img_dir}/{key}.{variation}.{source_format}'

                    with open(img_path, 'wb') as f:
                        source_image.save(f, source_format, **dest_info)

                    img_path_webp = img_path + '.webp'

                    with open(img_path_webp, 'wb') as f:
                        source_image.save(f, format='WEBP', **dest_info)

                    source_tag = soup.new_tag('source')
                    source_tag['type'] = 'image/webp'
                    source_tag['srcset'] = f"/media/{img_path_webp.split('/media/', 1)[1].lstrip()}"
                    sources.append(source_tag)

                    source_tag = soup.new_tag('source')
                    source_tag['type'] = 'image/jpeg'
                    source_tag['srcset'] = f"/media/{img_path.split('/media/', 1)[1].lstrip()}"
                    sources.append(source_tag)

            except requests.exceptions.ConnectionError:
                return soup.body.decode_contents()
            except:
                return soup.body.decode_contents()

    img_tag = soup.new_tag('img')
    img_tag['src'] = f'/{settings.MEDIA_URL}/yt_previews/{key}.default.jpg'
    img_tag['class'] = tag.get('class')
    img_tag['loading'] = 'lazy'
    img_tag['decoding'] = 'async'
    img_tag['width'] = '120'
    img_tag['height'] = '90'
    img_tag['alt'] = ''

    picture_tag = soup.new_tag('picture')
    for source in sources:
        picture_tag.append(source)
    picture_tag.extend([img_tag])

    tag.insert_before(picture_tag)

    # tag.insert_after(img_tag)
    tag.wrap(noscript_tag)

    # print(tag)

    return soup.body.decode_contents()


def _set_vimeo(soup, tag):
    parse_patterns = (
        re.compile(r'(?:https?:)?//(?:www\.)?player\.vimeo\.com/video/(\d+)'),
    )
    noscript_tag = soup.new_tag('noscript')
    key = None
    src = tag.get('src')
    src_preview = '/static/img/blank.gif'

    for pattern in parse_patterns:
        match = pattern.match(src)

        if match:
            key = match.group(1)
    if key:
        video_info_url = 'https://vimeo.com/api/v2/video/{}.json'.format(key)
        try:
            response = requests.get(video_info_url).json()[0]
            variations = ['large', 'medium', 'small']

            for variation in variations:
                src_preview = response['thumbnail_{}'.format(variation)]
                try:
                    r = requests.get(src_preview, timeout=1)

                    if r.status_code != 404:
                        break
                except requests.exceptions.ConnectionError:
                    return soup.body.decode_contents()
                except:
                    return
        except requests.exceptions.ConnectionError:
            return soup.body.decode_contents()
        except:
            return soup.body.decode_contents()

    img_tag = soup.new_tag('img', src='/static/img/blank.gif')
    img_tag['src'] = src_preview
    img_tag['class'] = tag.get('class')
    img_tag['loading'] = 'lazy'
    img_tag['decoding'] = 'async'
    img_tag['data-key'] = key
    img_tag['alt'] = ''

    tag.insert_after(img_tag)
    tag.wrap(noscript_tag)

    return soup.body.decode_contents()


def lazy_images(html):
    """
        Добавляет lazyload к картинкам и ставит ютуб-заглушку
    """

    soup = Soup(html, 'html5lib')

    single_photos = soup.find_all('p', class_='single-image')
    multi_photos = soup.find_all('p', class_='multi-image')
    _set_lazy(soup, single_photos)
    _set_swiper_lazy(multi_photos)

    views_protocol = ['//', 'http://', 'https://']

    iframes = soup.find_all("iframe")

    for tag in iframes:
        src = tag.get('src')
        address_youtube = 'youtube.com'
        address_vimeo = 'player.vimeo.com'
        addresses_youtube = [address_youtube, 'www.{}'.format(address_youtube)]
        addresses_vimeo = [address_vimeo, 'www.{}'.format(address_vimeo)]
        match_found = False

        for view_protocol in views_protocol:

            for address in addresses_youtube:
                if src.startswith('%s%s' % (view_protocol, address)):
                    _set_youtube(soup, tag)
                    match_found = True
                    parent_tag = tag.parent.parent
                    parent_tag['class'] = " ".join(parent_tag['class']) + " lazy"
                    break

            if match_found:
                break

            for address in addresses_vimeo:
                if src.startswith('%s%s' % (view_protocol, address)):
                    _set_vimeo(soup, tag)
                    match_found = True
                    parent_tag = tag.parent.parent
                    parent_tag['class'] = " ".join(parent_tag['class']) + " lazy"
                    break

            if match_found:
                break

            tag['loading'] = 'lazy'

    return soup.body.decode_contents()


def _set_picture(html, image):
    soup = Soup(html, 'html5lib')
    image.wrap(soup.new_tag('picture'))
    str_srcset = image.get('srcset')
    list_srcset = str_srcset.split(', ')

    widths = []
    media_sizes = []

    for srcset in list_srcset:
        srcset_parts = srcset.split(' ')
        srcset_width = int(srcset_parts[1].replace('w', ''))
        widths.append(srcset_width)
    widths.sort(reverse=True)

    for i, width in enumerate(widths):
        if i != len(widths) - 1 and i != len(
                widths) - 2:  # Unique media for mobile formats, 2 because using jpg and webp
            media_sizes.append(
                f'(min-width: {width}px)'
            )
        else:
            media_sizes.append(
                f'(max-width: {width - 1}px)'
            )

    for i, srcset in enumerate(list_srcset):
        srcset_parts = srcset.split(' ')
        srcset_url = srcset_parts[0]
        srcset_width = srcset_parts[1]
        source = soup.new_tag('source')
        source['srcset'] = srcset_url
        # source['sizes'] = '100vw'
        source['media'] = media_sizes[i]
        source['type'] = f"image/{srcset_url.split('?')[0].split('.')[-1]}"
        image.parent.append(source)
    del image['srcset']
    del image['sizes']
    picture = image.parent
    picture.append(image.extract())


def picture_images(html):
    """
        Оборачивает image в picture, меняет srcset на sources
    """

    soup = Soup(html, 'html5lib')
    images = soup.select("p.page-images > img")
    videos = soup.select("p.page-videos > img")

    for image in images:
        _set_picture(html, image)

    for image in videos:
        _set_picture(html, image)

    return soup.body.decode_contents()


def https_links(html):
    """
        Заменяет http://youtube.com... на https://youtube.com
        Заменяет http://player.vimeo.com... на https://player.vimeo.com
    """

    soup = Soup(html, 'html5lib')

    iframes = soup.find_all("iframe")

    for tag in iframes:
        src = tag.get('src')
        addresses = ['youtube.com', 'player.vimeo.com']

        for address in addresses:
            if src.startswith('http://%s' % address) or src.startswith('http://www.%s' % address):
                tag['src'] = src.replace('http', 'https')
    return soup.body.decode_contents()


def _format_anchor(s):
    if not (s.startswith('http://') or
            s.startswith('https://') or
            s.startswith('ftp://') or
            s.startswith('mailto:') or
            s.startswith('tel:') or
            s.startswith('/')):

        separator = '-'

        s = list(re.sub(r'[^A-z0-9]', separator, s))

        if s[0] == separator:
            s[0] = '#'

        s = ''.join(s)

    return s.lower()


def format_anchors(html):
    soup = Soup(html, 'html5lib')

    tags = soup.findAll("a")

    for tag in tags:
        tag_href = tag.get('href')
        tag_id = tag.get('id')

        if tag_href:
            tag['href'] = _format_anchor(tag_href)

        if tag_id:
            tag['id'] = _format_anchor(tag_id)

        if tag.get('name'):
            if not tag_id:
                tag['id'] = _format_anchor(tag['name'])
            del (tag['name'])

    return soup.body.decode_contents()


def remove_old_attributes(html):
    soup = Soup(html, 'html5lib')

    elements = soup.findAll("iframe")
    tags = ['allowtransparency', 'frameborder', 'scrolling', ]

    for element in elements:

        for tag in tags:
            if element.get(tag) == '' or element.get(tag):
                del (element[tag])

    return soup.body.decode_contents()


def format_faq(html):
    soup = Soup(html, 'html5lib')

    tables = soup.findAll("table", class_='questions')

    for table in tables:
        questions_tag = soup.new_tag('ul')
        questions_tag['class'] = 'questions'

        rows = table.tbody.findAll('tr')

        for row in rows:
            obj = row.findAll('div')

            if len(obj) == 0:
                obj = row.findAll('td')

            question = obj[0].get_text()
            answer = obj[1].get_text()

            question_tag = soup.new_tag('li')
            question_tag['class'] = 'question'

            question_question_tag = soup.new_tag('span')
            question_question_tag['class'] = 'question__question title title--h5'
            question_question_tag.string = question

            question_answer_tag = soup.new_tag('span')
            question_answer_tag['class'] = 'question__answer'
            question_answer_inner_tag = soup.new_tag('span')
            question_answer_inner_tag['class'] = 'text-styles'
            question_answer_inner_tag.string = answer

            question_answer_tag.append(question_answer_inner_tag)
            question_tag.append(question_question_tag)
            question_tag.append(question_answer_tag)

            questions_tag.append(question_tag)

        table.insert_after(questions_tag)
        table.extract()

    return soup.body.decode_contents()
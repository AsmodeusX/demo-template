(function($) {
    var loaded = false;

    $(window).on('mousemove keydown scroll click touchstart', function (e) {
        if (!loaded) {
            set_scripts();

            loaded = !loaded;
        }
    });

    function set_scripts() {
        generateFb();
        generateTw();
        generateGl();
    }

    function generateFb() {
        var js, fjs = document.getElementsByTagName('script')[0];
        if (document.getElementById('facebook-jssdk')) return;
        js = document.createElement('script');
        js.id = 'facebook-jssdk';
        js.async = true;
        js.defer = true;
        js.crossorigin = "anonymous";
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0"
        // js.nonce = "MxHXU6Hr";
        fjs.parentNode.insertBefore(js, fjs);
    }

    function generateTw() {
        var js, fjs = document.getElementsByTagName("script")[0],
            t = window.twttr || {};
        if (document.getElementById("twitter-wjs")) return t;
        js = document.createElement("script");
        js.id = "twitter-wjs";
        js.async = true;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
            t._e.push(f);
        };

        return t;
    }

    function generateGl() {
        var js, gjs = document.getElementsByTagName("script")[0];
        if (document.getElementById("google-sdk")) return;
        js = document.createElement("script");
        js.id = "google-sdk";
        js.async = true;
        js.src = "https://apis.google.com/js/platform.js";
        gjs.parentNode.insertBefore(js, gjs);
    }

})(jQuery);
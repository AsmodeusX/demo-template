from django.utils.translation import ugettext_lazy as _
from seo.admin import SeoModelAdminMixin
from project.admin.base import ModelAdminMixin


class StdFields(ModelAdminMixin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'title', 'description',
            ),
        }),
    )


class StdImageFields(StdFields):
    fieldsets = StdFields.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'image',
            ),
        }),
    )


class StdPageAdmin(ModelAdminMixin):
    fieldsets = StdFields.fieldsets


class SEOStdPageAdmin(SeoModelAdminMixin):
    fieldsets = StdFields.fieldsets


class StdImagePageAdmin(ModelAdminMixin):
    fieldsets = StdImageFields.fieldsets


class SEOStdImagePageAdmin(SeoModelAdminMixin):
    fieldsets = StdImageFields.fieldsets


class TextPageAdmin(ModelAdminMixin):
    fieldsets = (
        (_('Text page'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'share_block', 'text_info_block', 'date',  'author',
            ),
        }),
    )


class StdButtonPageAdmin(ModelAdminMixin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'contact_button', 'contact_button_caption', 'contact_button_url',
            ),
        }),
    )


class StdDisablePageAdmin(ModelAdminMixin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'enable',
            ),
        }),
    )

from django.http import Http404
from django.utils.timezone import now
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.detail import BaseDetailView
from blog.models import BlogConfig, Post
from services.models import Service
from shop.models import ShopProduct, ShopConfig
from webpage.utils import UsedBlocks
from libs.description import description
from seo.seo import Seo
from attachable_blocks.models import AttachableReference


class StdPageDetailView(TemplateResponseMixin, BaseDetailView):
    context_object_name = 'page'
    template_name = 'webpage/page.html'
    seo_title_default = None
    context_update = None
    used_blocks = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.used_blocks = UsedBlocks()
        self.used_blocks.clean_blocks()

        for reference in AttachableReference.get_for(self.object):
            name_block = str(reference.block_ct.model).replace('block', '')

            self.used_blocks.add_block(name_block)

        if isinstance(self.object, Service):
            self.used_blocks.add_block('services')

        if isinstance(self.object, Post):
            self.used_blocks.add_block('blog')

        if isinstance(self.object, ShopProduct):
            self.used_blocks.add_block('shop_products')
            self.used_blocks.add_block('shopproducts')
        self.get_seo()
        
        self.breadcrumbs()
        
        self.page_visibility_access()
        
        self._context_update(context)
        
        return context
    
    def _context_update(self, context):
        _dict = {}
        
        if self.context_update:
            for key, value in self.context_update.items():
                _dict.update({
                    key: value
                })
        _dict.update({
            'used_blocks': self.used_blocks.list_blocks()
        })
        
        return context.update(_dict)
    
    def get_seo(self):
        seo = Seo()
        seo_title_default = self.request.get_host()

        if self.seo_title_default:
            seo_title_default = self.seo_title_default
        
        elif getattr(self.get_object(), 'title', False):
            seo_title_default = self.object.title

        preview = False

        if hasattr(self.object, 'preview'):
            preview = self.object.preview.url if self.object.preview else False

        if hasattr(self.object, 'background'):
            preview = self.object.background.url if self.object.background else False



        seo.set_data(self.object, defaults={
            'title': '%s' % seo_title_default,
            'description': description(getattr(self.object, 'description', ''), 50, 160),
            'og_title': seo_title_default,
            'og_image': preview,
            'og_description': getattr(self.object, 'description', ''),
        })
        return seo.save(self.request)
    
    def page_visibility_access(self):
        time = now()
        
        # common pages
        if not getattr(self.get_object(), 'active', True):
            raise Http404

        try:
            if not self.get_object().module_enabled():
                raise Http404
        except AttributeError:
            pass

        # blog pages
        if hasattr(self.get_object(), 'date'):
            time = getattr(self.get_object(), 'date', now())
        
        if getattr(self.get_object(), 'status', 2) == 1 or (time and time >= now()):
            raise Http404
    
    def breadcrumbs(self):
        if hasattr(self.get_object(), 'title'):
            
            if hasattr(self.object, 'parent_singleton'):
                self.request.breadcrumbs.add(self.object.title)

            if isinstance(self.object, Post):
                blog_config = BlogConfig.get_solo()
                self.request.breadcrumbs.add(blog_config.title, blog_config.get_absolute_url())

            if isinstance(self.object, ShopProduct):
                shop_config = ShopConfig.get_solo()
                self.request.breadcrumbs.add(shop_config.title, shop_config.get_absolute_url())
            
            if hasattr(self.object, 'get_ancestors'):
                for ancestor in self.object.get_ancestors():
                    self.request.breadcrumbs.add(ancestor.title, ancestor.get_absolute_url())
            
            self.request.breadcrumbs.add(self.object.title)


class StdPageSingletonView(StdPageDetailView):
    def get_object(self, queryset=None):
        return self.model.get_solo()


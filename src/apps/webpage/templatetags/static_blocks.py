from django.template import loader
from libs import jinja2
from webpage.utils import UsedBlocks


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'static_blocks', }
    takes_context = True

    def _static_blocks(self, context, view_static=None, used_blocks=None,  template='webpage/static_blocks.html'):
        static = ''
        suffix = ''

        if view_static == 'critical_styles':
            suffix = '_critical'

        for used_block in used_blocks:
            static += loader.render_to_string(template, {
                'view_static': view_static,
                'static_file': f'block_{used_block}{suffix}',
            }, request=context.get('request'))
        # print(static)
        return static
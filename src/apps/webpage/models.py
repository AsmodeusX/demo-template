from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from ckeditor.fields import CKEditorUploadField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from users.models import CustomUser
from .utils import picture_images, remove_old_attributes, lazy_images, format_anchors, https_links


class StdPage(models.Model):
    header = models.TextField(_('header'), max_length=128, default='', )
    title = models.CharField(_('title'), max_length=60, default='', )
    description = models.TextField(_('description'), blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True, blank=True,)

    class Meta:
        abstract = True
        default_permissions = ('change', )

    @staticmethod
    def is_image_page():
        return True


class StdButtonPage(models.Model):
    contact_button_url = models.CharField(_('url button'), max_length=128, default='/contact/')
    contact_button_caption = models.CharField(_('caption button'), max_length=64, default='Contact Us Today')
    contact_button = models.BooleanField(_('show button'), default=True)

    class Meta:
        abstract = True

    def contact_button_is_external(self):
        _prefixes = ['http', 'www.']

        for prefix in _prefixes:
            if self.contact_button_url.startswith(prefix):
                return True

        return False


class StdDisablePage(models.Model):
    enable = models.BooleanField(_('enable module'), default=True)

    class Meta:
        abstract = True


class StdImagePage(StdPage):
    image = StdImageField(_('image'),
                          blank=True,
                          storage=MediaStorage('webpage/images'),
                          min_dimensions=(350, 350),
                          admin_variation='admin',
                          crop_area=True,
                          aspects=('wide',),
                          variations=dict(
                              wide=dict(
                                  size=(480, 480),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              desktop=dict(
                                  size=(1400, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              tablet1=dict(
                                  size=(1024, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              tablet2=dict(
                                  size=(768, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              mobile=dict(
                                  size=(375, 260),
                                  create_webp=True,
                                  # create_avif=True,
                              ),
                              admin=dict(
                                  size=(200, 200),
                              ),
                          ),
                          )

    class Meta:
        abstract = True

    @staticmethod
    def is_image_page():
        return True


class TextPage(models.Model):
    optimized_text = CKEditorUploadField(_('text'), blank=True)
    author = models.ForeignKey(CustomUser, null=True, blank=True)
    share_block = models.BooleanField(_('share block'), default=True)
    text_info_block = models.BooleanField(_('date and author'), default=False)
    date = models.DateTimeField(_('publication date'), default=now, blank=True, null=True)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if hasattr(self, 'text'):
            self.optimized_text = picture_images(
                remove_old_attributes(
                    format_anchors(
                        lazy_images(
                            https_links(
                                self.text
                            )
                        )
                    )
                )
            )

        return super(TextPage, self).save()

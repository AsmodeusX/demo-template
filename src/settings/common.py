import os
import re
import sys
from django.utils.translation import ugettext_lazy as _
from .pipeline import PIPELINE

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))
sys.path.insert(0, os.path.join(BASE_DIR, 'apps_common'))

SECRET_KEY = 'tn_jq5aa$6z4p7zxuc1gjczkw--vcyhu6r9t+6h58k2=pz(a(s'

DEBUG = False

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('ru', _('Russian')),
    ('en', _('English')),
)

TIME_ZONE = 'America/New_York'
USE_I18N = True
USE_L10N = True
USE_TZ = True

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'paypal',
    'constant_contact',

    'ajax_views',
    'admin_honeypot',
    'django_jinja',
    'mptt',
    'pipeline',
    'solo',
    'statici18n',
    'suit_ckeditor',

    'components',
    'webpage',

    # Apps
    'about',
    'album',
    'blocks',
    'blog',
    'contacts',
    'disclaimer',
    'faq',
    'main',
    'policy',
    'services',
    # 'shipstation_payments',
    'authorize_payments',
    'stripe_payments',
    'taxjar_payments',
    'shipstation',
    'shop',
    'team',
    'users',

    # Apps common
    'admin_ctr',
    'admin_log',
    'attachable_blocks',
    'backups',
    'breadcrumbs',
    'ckeditor',
    'footer',
    'gallery',
    'google_maps',
    'header',
    'menu',
    'paginator',
    'seo',
    'social_networks',

    # Libs
    'libs.autocomplete',
    'libs.file_field',
    'libs.form_helper',
    'libs.jinja2',
    'libs.management',
    'libs.pipeline',
    'libs.stdimage',
    'libs.templatetags',
    'libs.variation_field',
    'libs.sentry',
)

# Имя проекта
PROJECT_NAME = "Ecommerce Minimalism Template Project"

# Suit
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': PROJECT_NAME,

    # search
    'SEARCH_URL': '',

    # menu
    'MENU': (
        {
            'app': 'main',
            'icon': 'icon-file',
            'models': (
                'MainPageConfig',
            )
        },
        {
            'app': 'about',
            'icon': 'icon-file',
            'models': (
                'AboutConfig',
            )
        },
        {
            'app': 'team',
            'icon': 'icon-file',
            'models': (
                'Position',
                'Worker',
            )
        },
        {
            'app': 'album',
            'icon': 'icon-file',
            'models': (
                'AlbumConfig',
            )
        },
        {
            'app': 'blog',
            'icon': 'icon-file',
            'models': (
                'Tag',
                'Post',
                'BlogConfig',
            )
        },
        {
            'app': 'disclaimer',
            'icon': 'icon-file',
            'models': (
                'DisclaimerConfig',
            )
        },
        {
            'app': 'faq',
            'icon': 'icon-file',
            'models': (
                'FAQQuestion',
                'FAQConfig',
            )
        },
        {
            'app': 'policy',
            'icon': 'icon-file',
            'models': (
                'PolicyConfig',
            )
        },
        {
            'app': 'services',
            'icon': 'icon-file',
            'models': (
                'Service',
                'ServicesConfig',
            )
        },
        {
            'app': 'shop',
            'icon': 'icon-file',
            'models': (
                'Sale',
                'Client',
                # 'City',
                'Country',
                'Subscriber',
                'Coupon',
                'Supply',
                'ShopOrder',
                'ShopProduct',
                'ShopCategory',
                'Modification',
                'Characteristic',
                # 'Tax',
                'ShopConfig',
            )
        },
        '-',
        {
            'app': 'authorize_payments',
            'icon': 'icon-shopping-cart',
            'models': (
                'AuthorizeConfig',
            )
        },
        {
            'app': 'stripe_payments',
            'icon': 'icon-shopping-cart',
            'models': (
                'StripeConfig',
            )
        },
        {
            'app': 'taxjar_payments',
            'icon': 'icon-shopping-cart',
            'models': (
                'TaxJarConfig',
            )
        },
        {
            'app': 'shipstation',
            'icon': 'icon-lock',
            'models': (
                'ShipStationConfig',
                'Log',
            )
        },
        {
            'app': 'paypal',
            'icon': 'icon-shopping-cart',
        },
        {
            'app': 'constant_contact',
            'icon': 'icon-shopping-cart',
            'models': (
                'ConstantContactConfig',
                'Log',
            )
        },
        '-',
        {
            'app': 'contacts',
            'icon': 'icon-file',
            'models': (
                'Message',
                'Address',
                'StopWord',
                'SuccessConfig',
                'ContactsConfig',
            )
        },
        '-',
        {
            'app': 'social_networks',
            'icon': 'icon-lock',
            'models': (
                # 'FeedPost',
                'SocialLink',
                'SocialConfig',
            )
        },
        '-',
        {
            'icon': 'icon-lock',
            'label': 'Authentication and Authorization',
            'permissions': 'users.change_customuser',
            'models': (
                'auth.Group',
                'users.CustomUser',
            )
        },
        {
            'app': 'backups',
            'icon': 'icon-hdd',
            'permissions': 'users.admin_menu',
        },
        {
            'app': 'django_cron',
            'icon': 'icon-hdd',
            'permissions': 'users.admin_menu',
        },
        {
            'app': 'admin',
            'icon': 'icon-list-alt',
            'label': _('History'),
            'permissions': 'users.admin_menu',
        },
        {
            'app': 'sites',
            'permissions': 'users.admin_menu',
        },
        {
            'app': 'seo',
            'icon': 'icon-tasks',
            'permissions': 'users.admin_menu',
            'models': (
                'SeoConfig',
                'Redirect',
                'Counter',
                'Robots',
            ),
        },
    ),
}


# Pipeline
SASS_INCLUDE_DIR = BASE_DIR + '/static/scss/'
PIPELINE['SASS_BINARY'] = '/usr/bin/env sassc --load-path ' + SASS_INCLUDE_DIR

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.gzip.GZipMiddleware',

    'libs.cache.middleware.SCCMiddleware',
    'libs.middleware.utm.UTMMiddleware',
    'breadcrumbs.middleware.BreadcrumbsMiddleware',
    'menu.middleware.MenuMiddleware',
    'seo.middleware.RedirectMiddleware',
)

ALLOWED_HOSTS = ()

ROOT_URLCONF = 'urls.common'

WSGI_APPLICATION = 'project.wsgi.application'


# Sites and users
SITE_ID = 1
ANONYMOUS_USER_ID = -1
AUTH_USER_MODEL = 'users.CustomUser'
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = 'main:index'
LOGIN_REDIRECT_URL = 'main:index'
RESET_PASSWORD_REDIRECT_URL = 'main:index'
LOGOUT_URL = 'main:index'

# Email
# EMAIL_HOST = 'smtp.yandex.ru'
# EMAIL_PORT = 587
EMAIL_HOST = 'ingressdev.directline.company'
EMAIL_PORT = 31587
EMAIL_HOST_USER = 'noreply@directline.company'
EMAIL_HOST_PASSWORD = '4yZD9DBD7c8P8a9F'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = SERVER_EMAIL = 'noreply@directline.company'
EMAIL_SUBJECT_PREFIX = '[%s] ' % (SUIT_CONFIG['ADMIN_NAME'], )

# ==================================================================
# ==================== APPS SETTINGS ===============================
# ==================================================================

# Honeypot
ADMIN_HONEYPOT_EMAIL_ADMINS = False

# Admin Dump
BACKUP_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'backup'))

# Директория для robots.txt и других открытых файлов
PUBLIC_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'public'))

# Django solo caching
SOLO_CACHE = 'default'
SOLO_CACHE_TIMEOUT = 10 * 60

# Smart Cache-Control
SCC_IGNORE_URLS = [
    r'/admin/',
    r'/dladmin/',
]

# ==================================================================
# ==================== END APPS SETTINGS ===========================
# ==================================================================

# Домен для куки сессий (".example.com")
SESSION_COOKIE_DOMAIN = None
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_CACHE_ALIAS = 'default'
SESSION_COOKIE_AGE = 30 * 24 * 3600

# Домен для куки CSRF (".example.com")
CSRF_COOKIE_DOMAIN = None

# Список скомпилированных регулярных выражений
# запретных юзер-агентов
DISALLOWED_USER_AGENTS = ()

# Получатели писем о ошибках при DEBUG = False
ADMINS = (
    ('pix', 'x896321475@gmail.com'),
    ('kp', 'kpXXX@ya.ru'),
    ('i.sachev', 'i.sachev@directline.digital'),
    ('dev', 'klinov.mikhail@inbox.ru'),
)

# Получатели писем о битых ссылках при DEBUG=False
# Требуется подключить django.middleware.common.BrokenLinkEmailsMiddleware
MANAGERS = (
    ('pix', 'x896321475@gmail.com'),
)

# Список скомпилированных регулярных выражений адресов страниц,
# сообщения о 404 на которых не должны отправляться на почту (MANAGERS)
IGNORABLE_404_URLS = (
    re.compile(r'^/apple-touch-icon.*\.png$'),
    re.compile(r'^/favicon\.ico$'),
    re.compile(r'^/robots\.txt$'),
)

# DB
DATABASES = {}

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/0',
        'VERSION': 1,
        'KEY_PREFIX': 'django',
        'KEY_FUNCTION': 'project.utils.cache_key_func',
        'REVERSE_KEY_FUNCTION': 'project.utils.reverse_key_func',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        },
    }
}

# Templates
TEMPLATES = [
    {
        'BACKEND': 'libs.jinja2.Jinja2Backend',
        'DIRS': (
            os.path.join(BASE_DIR, 'templates'),
        ),
        'APP_DIRS': True,
        'OPTIONS': {
            'match_regex': r'.*\.(html|xml)$',
            'exclude_regex': r'.*(admin|admin_honeypot|suit|registration)/.*',
            'app_dirname': 'templates',
            'undefined': 'libs.jinja2.UndefinedSilently',
            'extensions': [
                'jinja2.ext.i18n',
                'jinja2.ext.loopcontrols',
                'django_jinja.builtins.extensions.CsrfExtension',
                'django_jinja.builtins.extensions.StaticFilesExtension',
                'django_jinja.builtins.extensions.TimezoneExtension',
                'django_jinja.builtins.extensions.DjangoFiltersExtension',
                'django_jinja.builtins.extensions.DjangoExtraFiltersExtension',
            ],
            'context_processors': (
                'django.template.context_processors.i18n',
                'django.contrib.auth.context_processors.auth',
                'social_networks.context_processors.google_apikey',
                'libs.context_processors.domain',
                'libs.context_processors.google_recaptcha_public_key',
                'libs.context_processors.company',
                'contacts.context_processors.contacts',
                'paypal.context_processors.config_paypal',
                'libs.sentry.context_processors.sentry',
            ),
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (
            os.path.join(BASE_DIR, 'templates'),
        ),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': (
                'django.contrib.messages.context_processors.messages',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                'social_networks.context_processors.google_apikey',
                'libs.sentry.context_processors.sentry',
            ),
        }
    },
]

# Locale
LOCALE_PATHS = (
    'locale',
)

# Datetime formats
FORMAT_MODULE_PATH = [
    'project.formats',
]

# Media
MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'media'))
MEDIA_URL = '/media/'
FILE_UPLOAD_PERMISSIONS = 0o644

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'static'))
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Создать проект на https://sentry.directline.company/ и взять Client Keys (DSN)
SENTRY_DSN = 'https://c96b61ede5a34572a3eca4a9c0f8df8b@sentry.directline.company/35'

# Ключи капчи
GOOGLE_RECAPTCHA_PUBLIC_KEY = '6LexNckcAAAAABYa2NyxTUWrVm_HegVgQVBnI2vn'
GOOGLE_RECAPTCHA_SECRET_KEY = '6LexNckcAAAAAKXbWFSoRKzXilU28m6tRvlNkA_w'

# PayPal
PAYPAL_CURRENCY = 'USD'
PAYPAL_SUCCESS_URL = 'paypal:result'
PAYPAL_CANCEL_URL = 'shop:payment'
PAYPAL_MODE = 'live'

#Geonames.org
GEONAMES_USER = 'astaroth'
SESSION_COOKIE_HTTPONLY = False

# Constant contact API
CC_REDIRECT_URI = 'https://demo-minimalism-ecommerce.directline.company/constant-contact/callback/'
CC_AUTH_URL = 'https://authz.constantcontact.com/oauth2/default/v1/authorize'
# CC_SUB_LIST_ID = '6ca807e4-539e-11ed-a227-fa163ed70180'

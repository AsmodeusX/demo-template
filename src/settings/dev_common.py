from settings.common import *

DOMAIN = '.local.com'
SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN
ALLOWED_HOSTS = (
    DOMAIN,
    'localhost',
    '127.0.0.1',
)
MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_PANELS = (
    # 'debug_toolbar.panels.history.HistoryPanel',
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'pympler.panels.MemoryPanel',
)

DEBUG = True
INSTALLED_APPS +=(
    'debug_toolbar',
    'pympler',
)

INTERNAL_IPS = (
    '127.0.0.1'
)

ROOT_URLCONF = 'urls.dev_common'

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'project',
        'USER': 'project',
        'PASSWORD': 'password',
        'HOST': 'localhost',
    }
})

# Нарезка в два потока (на продакшене для этого памяти не хватит)
VARIATION_THREADS = 2

# Отключение компрессии SASS (иначе теряется наглядность кода)
PIPELINE['SASS_ARGUMENTS'] = '-t nested'

STATICFILES_FINDERS += (
    'libs.pipeline.debug_finder.PipelineFinder',
)

# =========
#  Logging
# =========
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s',
        },
        'server': {
            '()': 'libs.logging.formatters.SQLFormatter',
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(message)s',
        },
        'file': {
            '()': 'libs.logging.formatters.UTCFormatter',
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(levelname)-8s %(name)s\n'
                      '%(message)s'
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
        'server': {
            'class': 'logging.StreamHandler',
            'formatter': 'server',
        },
        'debug_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'logs', 'debug.log')),
            'maxBytes': 1024*1024,
            'backupCount': 5,
            'formatter': 'file',
        },
        'errors_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'logs', 'errors.log')),
            'maxBytes': 1024*1024,
            'backupCount': 2,
            'level': 'ERROR',
            'formatter': 'file',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
        },
        'django.db': {              # лог SQL-запросов
            'handlers': ['null'],
            'propagate': False,
            'level': 'DEBUG',
        },
        'django.server': {
            'handlers': ['server'],
            'propagate': False,
        },
        'debug': {
            'handlers': ['console', 'debug_log'],
            'propagate': False,
            'level': 'DEBUG',
        },
        '': {
            'handlers': ['console', 'errors_log'],
            'level': 'INFO',
        }
    }
}

PAYPAL_MODE = 'sandbox'


# Constant contact API
CC_REDIRECT_URI = 'localhost:8001/constant-contact/callback/'
CC_AUTH_URL = 'https://authz.constantcontact.com/oauth2/default/v1/authorize'
# CC_SUB_LIST_ID = '6ca807e4-539e-11ed-a227-fa163ed70180'

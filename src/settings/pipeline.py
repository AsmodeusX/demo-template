from ._pipeline import PIPELINE, Slider, GoogleMap, ContactForm


CORE_CSS = (
    'scss/layout.scss',
    'blog/scss/parts/posts.scss',

    # Components
    'components/scss/items.scss',
    'components/scss/slider.scss',
    'components/scss/gallery.scss',

    'shop/scss/parts/_products.scss',
    'shop/scss/parts/_reviews.scss',

    'faq/scss/block.scss',
    'css/jquery.fancybox.min.css',
    'scss/preloader.scss',
    'scss/popups/popups.scss',
    'scss/popups/preloader.scss',
    'social_networks/scss/social_links.scss',
    'footer/scss/footer.scss',
    'components/scss/recaptcha.scss',
)

CRITICAL_CSS = (
    'fonts/Montserrat/700/normal/font.scss',
    'fonts/Montserrat/800/normal/font.scss',
    'scss/critical.scss',
    'scss/grid.scss',
    'header/scss/header.scss',
    'breadcrumbs/scss/breadcrumbs.scss',
    'webpage/scss/header.scss',
    'menu/scss/menu.scss',
    'fonts/Lato/400/normal/font.scss',
    'components/scss/logo.scss',
    'components/scss/phone.scss',
    'components/scss/button.scss',
    'components/scss/burger.scss',
    'components/scss/mobile_box.scss',
)


PIPELINE['STYLESHEETS'].update({
    'critical': {
        'source_filenames': CRITICAL_CSS + Slider.css,
        'output_filename': 'css_build/critical.css',
    },
    'forms': {
        'source_filenames': (
            'scss/forms.scss',
        ),
        'output_filename': 'css_build/forms.css',
    },
    'contacts_block': {
        'source_filenames': ContactForm.css + GoogleMap.css,
        'output_filename': 'css_build/contacts_block.css',
    },

    'core': {
        'source_filenames': CORE_CSS,
        'output_filename': 'css_build/head_core.css',
    },

    'fonts': {
        'source_filenames': (
            'fonts/Lato/700/normal/font.scss',
            'fonts/Montserrat/400/normal/font.scss',
            'fonts/Montserrat/600/normal/font.scss',
            'fonts/Montserrat/700/normal/font.scss',
        ),
        'output_filename': 'css_build/fonts.css',
    },
    'error': {
        'source_filenames': (
            'scss/error_page.scss',
        ),
        'output_filename': 'css_build/error.css',
    },
    'error_critical': {
        'source_filenames': (
            'scss/error_page_critical.scss',
        ),
        'output_filename': 'css_build/error_critical.css',
    },
    'main_critical': {
        'source_filenames': (
            'main/scss/critical.scss',
        ),
        'output_filename': 'css_build/main_critical.css',
    },
    'main': {
        'source_filenames': (
            'main/scss/index.scss',
        ),
        'output_filename': 'css_build/main.css',
    },
    'about': {
        'source_filenames': (
            'about/scss/index.scss',
        ),
        'output_filename': 'css_build/about.css',
    },
    'about_critical': {
        'source_filenames': (
            'about/scss/critical.scss',
        ),
        'output_filename': 'css_build/about_critical.css',
    },
    'album': {
        'source_filenames': (
            'album/scss/index.scss',
        ),
        'output_filename': 'css_build/album.css',
    },
    'blog': {
        'source_filenames': (
            'blog/scss/index.scss',
        ),
        'output_filename': 'css_build/blog.css',
    },
    'blog_post': {
        'source_filenames': (
            'blog/scss/detail.scss',
        ),
        'output_filename': 'css_build/blog_post.css',
    },
    'album_critical': {
        'source_filenames': (
            'album/scss/critical.scss',
        ),
        'output_filename': 'css_build/album_critical.css',
    },
    'blog_critical': {
        'source_filenames': (
            'blog/scss/critical.scss',
        ),
        'output_filename': 'css_build/blog_critical.css',
    },
    'blog_post_critical': {
        'source_filenames': (
            'blog/scss/detail_critical.scss',
        ),
        'output_filename': 'css_build/blog_post_critical.css',
    },
    'faq': {
        'source_filenames': (
            'faq/scss/index.scss',
        ),
        'output_filename': 'css_build/faq.css',
    },
    'faq_critical': {
        'source_filenames': (
            'faq/scss/critical.scss',
        ),
        'output_filename': 'css_build/faq_critical.css',
    },
    'service': {
        'source_filenames': (
            'services/scss/detail.scss',
        ),
        'output_filename': 'css_build/service.css',
    },
    'service_critical': {
        'source_filenames': (
            'services/scss/detail_critical.scss',
        ),
        'output_filename': 'css_build/service_critical.css',
    },
    'contacts': {
        'source_filenames': (
            'contacts/scss/index.scss',
        ),
        'output_filename': 'css_build/contacts.css',
    },
    'contacts_critical': {
        'source_filenames': ContactForm.css + GoogleMap.css + (
            'contacts/scss/block.scss',
            'contacts/scss/critical.scss',
        ),
        'output_filename': 'css_build/contacts_critical.css',
    },
    'success': {
        'source_filenames': (
            'contacts/scss/success.scss',
        ),
        'output_filename': 'css_build/success.css',
    },
    'success_critical': {
        'source_filenames': (
            'contacts/scss/success_critical.scss',
        ),
        'output_filename': 'css_build/success_critical.css',
    },
    'disclaimer': {
        'source_filenames': (
            'disclaimer/scss/index.scss',
        ),
        'output_filename': 'css_build/disclaimer.css',
    },
    'disclaimer_critical': {
        'source_filenames': (
            'disclaimer/scss/critical.scss',
        ),
        'output_filename': 'css_build/disclaimer_critical.css',
    },
    'policy': {
        'source_filenames': (
            'policy/scss/index.scss',
        ),
        'output_filename': 'css_build/policy.css',
    },
    'policy_critical': {
        'source_filenames': (
            'policy/scss/critical.scss',
        ),
        'output_filename': 'css_build/policy_critical.css',
    },
    'shop_critical': {
        'source_filenames': (
            'css/nouislider.min.css',
            'shop/scss/critical.scss',
        ),
        'output_filename': 'css_build/shop_critical.css',
    },
    'shop': {
        'source_filenames': (
            'shop/scss/index.scss',
        ),
        'output_filename': 'css_build/shop.css',
    },
    'product_critical': {
        'source_filenames': (
            'shop/scss/product_critical.scss',
            'scss/jquery-ui.scss',
        ),
        'output_filename': 'css_build/product_critical.css',
    },
    'product': {
        'source_filenames': (
            'shop/scss/product.scss',
        ),
        'output_filename': 'css_build/product.css',
    },
    'shop_cart': {
        'source_filenames': (
            'shop/scss/cart.scss',
        ),
        'output_filename': 'css_build/cart.css',
    },
    'shop_cart_critical': {
        'source_filenames': (
            'shop/scss/cart_critical.scss',
        ),
        'output_filename': 'css_build/cart_critical.css',
    },
    'shop_empty': {
        'source_filenames': (
            'shop/scss/empty.scss',
        ),
        'output_filename': 'css_build/shop_empty.css',
    },
    'shop_empty_critical': {
        'source_filenames': (
            'shop/scss/empty_critical.scss',
        ),
        'output_filename': 'css_build/shop_empty_critical.css',
    },
    'shop_checkout': {
        'source_filenames': (
            'shop/scss/checkout.scss',
        ),
        'output_filename': 'css_build/checkout.css',
    },
    'shop_checkout_critical': {
        'source_filenames': (
            'scss/jquery-ui.scss',
            'shop/scss/checkout_critical.scss',
        ),
        'output_filename': 'css_build/checkout_critical.css',
    },
    'shop_payment': {
        'source_filenames': (
            'shop/scss/payment.scss',
        ),
        'output_filename': 'css_build/payment.css',
    },
    'shop_payment_critical': {
        'source_filenames': (
            'shop/scss/payment_critical.scss',
        ),
        'output_filename': 'css_build/payment_critical.css',
    },
    'shop_payment_success': {
        'source_filenames': (
            'shop/scss/success.scss',
        ),
        'output_filename': 'css_build/success.css',
    },
    'shop_payment_success_critical': {
        'source_filenames': (
            'shop/scss/payment_success_critical.scss',
        ),
        'output_filename': 'css_build/payment_success_critical.css',
    },
    'text_page': {
        'source_filenames': (
            'scss/text_styles.scss',
            'webpage/scss/textpage_info.scss',
        ),
        'output_filename': 'css_build/text_page.css',
    },
    'block_about': {
        'source_filenames': (
            'about/scss/block.scss',
        ),
        'output_filename': 'css_build/block_about.css',
    },
    'block_album': {
        'source_filenames': (
            'album/scss/block.scss',
        ),
        'output_filename': 'css_build/block_album.css',
    },
    'block_advantages': {
        'source_filenames': (
            'blocks/scss/advantages.scss',
        ),
        'output_filename': 'css_build/block_advantages.css',
    },
    'block_blog': {
        'source_filenames': (
            'blog/scss/block.scss',
        ),
        'output_filename': 'css_build/block_blog.css',
    },
    'block_contacts': {
        'source_filenames': (
            'contacts/scss/block.scss',
        ),
        'output_filename': 'css_build/block_contacts.css',
    },
    'block_services': {
        'source_filenames': (
            'services/scss/block.scss',
        ),
        'output_filename': 'css_build/block_services.css',
    },
    'block_shopproducts': {
        'source_filenames': (
            'shop/scss/blocks/products.scss',
        ),
        'output_filename': 'css_build/block_shop_products.css',
    },
    'block_shopreviews': {
        'source_filenames': (
            'shop/scss/blocks/reviews.scss',
        ),
        'output_filename': 'css_build/block_shop_reviews.css',
    },
    'block_team': {
        'source_filenames': (
            'team/scss/block.scss',
        ),
        'output_filename': 'css_build/block_team.css',
    },
})

PIPELINE['JAVASCRIPT'].update({
    'core': {
        'source_filenames': (
                                'polyfills/modernizr.js',
                                'js/jquery-3.3.1.min.js',
                                'js/jquery-migrate-3.3.2.js',
                                'js/jquery-ui.min.js',
                                'common/js/js.cookie.min.js',
                                'js/jquery.cookie.js',
                                'common/js/jquery.utils.js',
                                'webpage/js/header.js',
                                'sentry/js/sentry.bundle.tracing.min.js',
                                'sentry/js/sentry.js',
                            ) + Slider.js + (
                                'js/popups/jquery.popups.js',
                                'js/popups/preloader.js',
                                'js/jquery.inspectors.js',
                                'js/jquery.scrollTo.js',

                                'components/js/mobile_box.js',
                                'components/js/gallery.js',

                                'js/expander.js',
                                'js/jquery.fancybox.min.js',
                                'blog/js/posts.js',
                                'faq/js/block.js',
                            ) + ContactForm.js +  (

                                'attachable_blocks/js/async_blocks.js',
                                'menu/js/menu.js',

                                'components/js/recaptcha.js',
                            ) + GoogleMap.js + (
                                'shop/js/cart_counter.js',
                            ),
        'output_filename': 'js_build/core.js',
    },
    'main': {
        'source_filenames': (
            'main/js/index.js',
        ),
        'output_filename': 'js_build/main.js',
    },
    'about': {
        'source_filenames': (
            'about/js/index.js',
        ),
        'output_filename': 'js_build/about.js',
    },
    'album': {
        'source_filenames': (
            'album/js/index.js',
        ),
        'output_filename': 'js_build/album.js',
    },
    'blog': {
        'source_filenames': (
            'blog/js/index.js',
        ),
        'output_filename': 'js_build/blog.js',
    },
    'faq': {
        'source_filenames': (
            'faq/js/index.js',
        ),
        'output_filename': 'js_build/faq.js',
    },
    'service': {
        'source_filenames': (
            'services/js/detail.js',
        ),
        'output_filename': 'js_build/service.js',
    },
    'shop': {
        'source_filenames': (
            'js/nouislider.min.js',
            'shop/js/params.js',
            'shop/js/index.js',
            'shop/js/categories.js',
            'shop/js/filters.js',
            'shop/js/sort.js',
            'shop/js/price.js',
            'js/jquery.session.js',
            'shop/js/cart.js',
        ),
        'output_filename': 'js_build/shop.js',
    },
    'product': {
        'source_filenames': (
            'shop/js/order.js',
            'shop/js/cart.js',
            'shop/js/product.js',
            'shop/js/reviews.js',
        ),
        'output_filename': 'js_build/product.js',
    },
    'shop_cart': {
        'source_filenames': (
            'js/sticky.js',
            'shop/js/order.js',
            'shop/js/cart.js',
            'shop/js/reviews.js',
            'shop/js/coupon.js',
        ),
        'output_filename': 'js_build/cart.js',
    },
    'shop_checkout': {
        'source_filenames': (
            'shop/js/checkout.js',
            'js/sticky.js',
            'shop/js/order.js',
        ),
        'output_filename': 'js_build/checkout.js',
    },
    'shop_payment': {
        'source_filenames': (
            'js/jquery.maskedinput.js',
            'js/sticky.js',
            'shop/js/order.js',
            'shop/js/payment.js',
        ),
        'output_filename': 'js_build/payment.js',
    },
    'policy': {
        'source_filenames': (
            'policy/js/index.js',
        ),
        'output_filename': 'js_build/policy.js',
    },
    'disclaimer': {
        'source_filenames': (
            'disclaimer/js/index.js',
        ),
        'output_filename': 'js_build/disclaimer.js',
    },
    'contacts': {
        'source_filenames': (
            'contacts/js/block.js',
            'contacts/js/index.js',
        ),
        'output_filename': 'js_build/contacts.js',
    },
    'text_page': {
        'source_filenames': (
            'js/jquery.fitvids.js',
            'js/jquery.youtube.js',
            'js/vimeo.js',
            'js/text_styles.js',
            'webpage/js/share.js',
        ),
        'output_filename': 'js_build/text_page.js',
    },
    'block_about': {
        'source_filenames': (
            'about/js/block.js',
        ),
        'output_filename': 'js_build/block_about.js',
    },
    'block_album': {
        'source_filenames': (
            'album/js/block.js',
        ),
        'output_filename': 'js_build/block_album.js',
    },
    'block_advantages': {
        'source_filenames': (
            'blocks/js/advantages.js',
        ),
        'output_filename': 'js_build/block_advantages.js',
    },
    'block_blog': {
        'source_filenames': (
            'blog/js/block.js',
        ),
        'output_filename': 'js_build/block_blog.js',
    },
    'block_contacts': {
        'source_filenames': (
            'contacts/js/block.js',
        ),
        'output_filename': 'js_build/block_contacts.js',
    },
    'block_services': {
        'source_filenames': (
            'services/js/block.js',
        ),
        'output_filename': 'js_build/block_services.js',
    },
    # 'block_shop': {
    #     'source_filenames': (
    #         'shop/js/block.js',
    #     ),
    #     'output_filename': 'js_build/block_shop.js',
    # },

    'block_shopproducts': {
        'source_filenames': (
            'shop/js/blocks/products.js',
        ),
        'output_filename': 'js_build/block_shop_products.js',
    },
    'block_shopreviews': {
        'source_filenames': (
            'shop/js/blocks/reviews.js',
        ),
        'output_filename': 'js_build/block_shop_reviews.js',
    },
    'block_team': {
        'source_filenames': (
            'team/js/block.js',
        ),
        'output_filename': 'js_build/block_team.js',
    },
})

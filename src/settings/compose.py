from settings.dev_common import *

DOMAIN = '127.0.0.1'

SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN

ALLOWED_HOSTS = (
    DOMAIN,
    'localhost',
    '127.0.0.1',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

PIPELINE['PIPELINE_ENABLED'] = False

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS'],
        'HOST': os.environ['DB_HOST'],
        'CONN_MAX_AGE': 60,
    }
})

CACHES['default']['LOCATION'] = 'redis://redis-master.default.svc:6379/0'

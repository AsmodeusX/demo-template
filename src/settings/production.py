from settings.common import *

DOMAIN = '.directlinedev.com'
VZ_DIRECTORY = '/app'

SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN

ALLOWED_HOSTS = (
    DOMAIN,
)

ROOT_URLCONF = 'urls.production'

SENTRY_ENVIRONMENT = 'production'

# настройки статики
STATIC_ROOT = os.path.join(VZ_DIRECTORY, 'static')
MEDIA_ROOT = os.path.join(VZ_DIRECTORY, 'media')
BACKUP_ROOT = os.path.join(VZ_DIRECTORY, 'backup')
PUBLIC_DIR = os.path.join(VZ_DIRECTORY, 'public')

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS'],
        'HOST': os.environ['DB_HOST'],
        'CONN_MAX_AGE': 60,
    }
})

CACHES['default']['LOCATION'] = 'redis://redis-master.default.svc:6379/0'

# =========
#  Logging
# =========
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'file': {
            '()': 'libs.logging.formatters.UTCFormatter',
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(levelname)-8s %(name)s\n'
                      '%(message)s'
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'debug_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(os.path.join(VZ_DIRECTORY, 'logs'), 'debug.log')),
            'maxBytes': 1024*1024,
            'backupCount': 5,
            'formatter': 'file',
        },
        'errors_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(os.path.join(VZ_DIRECTORY, 'logs'), 'errors.log')),
            'maxBytes': 1024*1024,
            'backupCount': 5,
            'level': 'WARNING',
            'formatter': 'file',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
        },
        'django.server': {
            'handlers': ['null'],
            'propagate': False,
        },
        'debug': {
            'handlers': ['debug_log'],
            'level': 'DEBUG',
            'propagate': False,
        },
        '': {
            'handlers': ['errors_log', 'mail_admins'],
        }
    }
}

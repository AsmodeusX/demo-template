import re
import string

E164_TEST_REGEX = re.compile(
    r'^(\+?\d{1,3}-?|\d{1,4}-?)?\s*'  # optional country code with -, whitespace
    r'\(?([1-9]\d{2})\)?'  # extract first three digits (0 first digit is international)
    r'[-.\s]*'  # strip -, ., and whitespace
    r'(\d{3})'
    r'[-.\s]*'  # strip -, ., and whitespace
    r'(\d{2})'  # next 2 digits
    r'[-.\s]*'  # strip -, ., and whitespace
    r'(\d{2})$'  # last four digits
)


def translate_phonewords(value):
    return value.lower().translate(str.maketrans(string.ascii_lowercase, '22233344455566677778889999'))


def is_e164_format(value):
    return bool(re.match(E164_TEST_REGEX, value))


def is_e164_usa_format(value):
    return is_e164_format(value) and value.startswith('+1')

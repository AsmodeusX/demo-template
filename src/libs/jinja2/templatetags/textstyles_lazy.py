from django_jinja.library import filter
from urllib import parse
from bs4 import BeautifulSoup as Soup
from django.template.defaultfilters import safe
from libs.templatetags.templatetags.text import typograf, unescape_html
from libs import jinja2
import re
import requests

re_nbsp = re.compile('(?<![\w’\'"])([\w’]{1,3})\s+')


def _image_lazyload_swiper(tag):
    tag['data-src'] = tag['src']

    if tag.get('srcset'):
        tag['data-srcset'] = tag['srcset']
        del tag['srcset']

    if tag.get('sizes'):
        tag['data-sizes'] = tag['sizes']
        del tag['sizes']

    if not tag.get('alt'):
        tag['alt'] = ''

    tag['src'] = "/static/img/blank.gif"
    tag['class'] = 'swiper-lazy'
    tag['loading'] = 'lazy'
    tag['decoding'] = 'async'


def _image_lazyload(tag):

    if tag.get('srcset'):
        tag['data-srcset'] = tag['srcset']
        del tag['srcset']

    if tag.get('sizes'):
        tag['data-sizes'] = tag['sizes']
        del tag['sizes']

    if tag.get('alt'):
        tag['alt'] = ''

    tag['loading'] = 'lazy'
    tag['decoding'] = 'async'

    return tag


def _set_swiper_lazy(blocks):
    for block in blocks:
        for tag in block.findAll('img'):
            _image_lazyload_swiper(tag)

            if tag.string:
                tag.string = parse.unquote(tag.string)


def _set_lazy(soup, blocks):
    for block in blocks:
        for tag in block.findAll('img'):
            # noscript_tag = soup.new_tag('noscript')

            new_tag = soup.new_tag('img',
                                   src=tag.get('src'),
                                   sizes=tag.get('sizes'),
                                   alt=tag.get('alt'),
                                   title=tag.get('title'),
                                   srcset=tag.get('srcset'),
                                   width=tag.get('width'),
                                   height=tag.get('height'),
                                   )

            if tag.get('id'):
                new_tag['id'] = tag.get('id')

            if not tag.get('alt'):
                tag['alt'] = ''

            if tag.get('data-description'):
                new_tag['data-description'] = tag.get('data-description'),
            new_tag['class'] = tag.get('class')
            tag = _image_lazyload(tag)
            # noscript_tag.append(new_tag)
            # tag.insert_after(noscript_tag)

            if tag.string:
                tag.string = parse.unquote(tag.string)

    return soup.body.decode_contents()

def set_youtube(soup, tag):
    parse_patterns = (
        re.compile(r'(?:https?:)?//(?:www\.)?youtube\.com/watch\?v=([-\w]{11,})'),
        re.compile(r'(?:https?:)?//(?:www\.)?youtube\.com/(?:v|embed)/([-\w]{11,})'),
        re.compile(r'(?:https?:)?//youtu\.be/([-\w]{11,})'),
    )
    noscript_tag = soup.new_tag('noscript')
    key = None
    src = tag.get('src')
    src_preview = '/static/img/blank.gif'

    for pattern in parse_patterns:
        match = pattern.match(src)

        if match:
            key = match.group(1)
    if key:
        variations = ['maxresdefault', 'sddefault', 'mqdefault', 'hqdefault', 'default']

        for variation in variations:
            src_preview = 'https://img.youtube.com/vi/{}/{}.jpg'.format(key, variation)
            try:
                r = requests.get(src_preview, timeout=1)

                if r.status_code != 404:
                    break
            except requests.exceptions.ConnectionError:
                return soup.body.decode_contents()
            except:
                return soup.body.decode_contents()

    img_tag = soup.new_tag('img', src='/static/img/blank.gif')
    img_tag['src'] = src_preview
    img_tag['class'] = tag.get('class')
    img_tag['loading'] = 'lazy'
    img_tag['decoding'] = 'async'
    img_tag['alt'] = ''

    tag.insert_after(img_tag)
    tag.wrap(noscript_tag)

    return soup.body.decode_contents()

def set_vimeo(soup, tag):
    parse_patterns = (
        re.compile(r'(?:https?:)?//(?:www\.)?player\.vimeo\.com/video/(\d+)'),
    )
    noscript_tag = soup.new_tag('noscript')
    key = None
    src = tag.get('src')
    src_preview = '/static/img/blank.gif'

    for pattern in parse_patterns:
        match = pattern.match(src)

        if match:
            key = match.group(1)
    if key:
        video_info_url = 'https://vimeo.com/api/v2/video/{}.json'.format(key)
        try:
            response = requests.get(video_info_url).json()[0]
            variations = ['large', 'medium', 'small']

            for variation in variations:
                src_preview = response['thumbnail_{}'.format(variation)]
                try:
                    r = requests.get(src_preview, timeout=1)

                    if r.status_code != 404:
                        break
                except requests.exceptions.ConnectionError:
                    return soup.body.decode_contents()
                except:
                    return
        except requests.exceptions.ConnectionError:
            return soup.body.decode_contents()
        except:
            return soup.body.decode_contents()

    img_tag = soup.new_tag('img', src='/static/img/blank.gif')
    img_tag['src'] = src_preview
    img_tag['class'] = tag.get('class')
    img_tag['loading'] = 'lazy'
    img_tag['decoding'] = 'async'
    img_tag['data-key'] = key
    img_tag['alt'] = ''

    tag.insert_after(img_tag)
    tag.wrap(noscript_tag)

    return soup.body.decode_contents()


def lazy_images(html):
    """
        Добавляет lazyload к картинкам и ставит ютуб-заглушку
    """

    soup = Soup(html, 'html5lib')

    single_photos = soup.find_all('p', class_='single-image')
    multi_photos = soup.find_all('p', class_='multi-image')
    _set_lazy(soup, single_photos)
    _set_swiper_lazy(multi_photos)

    views_protocol = ['//', 'http://', 'https://']

    iframes = soup.find_all("iframe")

    for tag in iframes:
        src = tag.get('src')
        address_youtube = 'youtube.com'
        address_vimeo = 'player.vimeo.com'
        addresses_youtube = [address_youtube, 'www.{}'.format(address_youtube)]
        addresses_vimeo = [address_vimeo, 'www.{}'.format(address_vimeo)]
        match_found = False

        for view_protocol in views_protocol:

            for address in addresses_youtube:
                if src.startswith('%s%s' % (view_protocol, address)):
                    set_youtube(soup, tag)
                    match_found = True
                    break

            if match_found:
                break

            for address in addresses_vimeo:
                if src.startswith('%s%s' % (view_protocol, address)):
                    set_vimeo(soup, tag)
                    match_found = True
                    break

            if match_found:
                break

            tag['loading'] = 'lazy'

    return soup.body.decode_contents()


def _format_anchor(s):
    if not (s.startswith('http://') or
            s.startswith('https://') or
            s.startswith('ftp://') or
            s.startswith('mailto:') or
            s.startswith('tel:') or
            s.startswith('/')):

        separator = '-'

        s = list(re.sub(r'[^A-z0-9]', separator, s))

        if s[0] == separator:
            s[0] = '#'

        s = ''.join(s)

    return s.lower()


def format_anchors(html):
    soup = Soup(html, 'html5lib')

    tags = soup.findAll("a")

    for tag in tags:
        tag_href = tag.get('href')
        tag_id = tag.get('id')

        if tag_href:
            tag['href'] = _format_anchor(tag_href)

        if tag_id:
            tag['id'] = _format_anchor(tag_id)

        if tag.get('name'):
            if not tag_id:
                tag['id'] = _format_anchor(tag['name'])
            del (tag['name'])

    return soup.body.decode_contents()


def remove_old_attributes(html):
    soup = Soup(html, 'html5lib')

    elements = soup.findAll("iframe")
    tags = ['allowtransparency', 'frameborder', 'scrolling', ]

    for element in elements:

        for tag in tags:
            if element.get(tag) == '' or element.get(tag):
                del (element[tag])

    return soup.body.decode_contents()


@jinja2.evalcontextfilter
@filter(name='textstyles_lazy')
def do_textstyles_lazy(eval_ctx, html):

    content = remove_old_attributes(format_anchors(lazy_images(typograf(eval_ctx, html))))
    return safe(unescape_html(content))

from django_jinja.library import filter
from urllib import parse
from bs4 import BeautifulSoup as Soup
from django.template.defaultfilters import safe
from libs.templatetags.templatetags.text import typograf, unescape_html
from libs import jinja2
import re
import requests

def format_faq(html):
    soup = Soup(html, 'html5lib')

    tables = soup.findAll("table", class_='questions')

    for table in tables:
        questions_tag = soup.new_tag('ul')
        questions_tag['class'] = 'questions'

        rows = table.tbody.findAll('tr')

        for row in rows:
            obj = row.findAll('div')

            if len(obj) == 0:
                obj = row.findAll('td')

            question = obj[0].get_text()
            answer = obj[1].get_text()

            question_tag = soup.new_tag('li')
            question_tag['class'] = 'question'

            question_question_tag = soup.new_tag('span')
            question_question_tag['class'] = 'question__question title title--h5'
            question_question_tag.string = question

            question_answer_tag = soup.new_tag('span')
            question_answer_tag['class'] = 'question__answer'
            question_answer_inner_tag = soup.new_tag('span')
            question_answer_inner_tag['class'] = 'text-styles'
            question_answer_inner_tag.string = answer

            question_answer_tag.append(question_answer_inner_tag)
            question_tag.append(question_question_tag)
            question_tag.append(question_answer_tag)

            questions_tag.append(question_tag)

        table.insert_after(questions_tag)
        table.extract()

    return soup.body.decode_contents()


@jinja2.evalcontextfilter
@filter(name='reformat')
def do_reformat(eval_ctx, html):

    content = format_faq(typograf(eval_ctx, html))
    return safe(unescape_html(content))

def get_type_card(num):
    num = str(num)
    t = ''

    if num.startswith('4'):
        t = 'visa'
    elif num.startswith('5') or num.startswith('2'):
        t = 'mastercard'
    elif num.startswith('6'):
        t = 'discover'
    elif num.startswith('3'):
        t = 'amex'
    return t
(function($) {

    var sentry_environment = document.documentElement.getAttribute('data-sentry-environment');
    var sentry_dsn = document.documentElement.getAttribute('data-sentry-dsn');

    if (sentry_dsn && sentry_environment) {
        Sentry.setTag('issue', 'front-end');
        Sentry.init({
            dsn: sentry_dsn,
            tunnel: '/sentry/tunnel/',
            integrations: [new Sentry.Integrations.BrowserTracing()],

            tracesSampleRate: 1.0,
            environment: sentry_environment,
        });
    }

})(jQuery);

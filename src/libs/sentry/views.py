import requests
from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def tunnel(request):
    sentry_dsn = getattr(settings, 'SENTRY_DSN', '')
    sentry_environment = getattr(settings, 'SENTRY_ENVIRONMENT', '')
    if sentry_dsn and sentry_environment:
        data = request.body.decode('utf-8')

        project_id = sentry_dsn.split('/')[-1]
        sentry_key = sentry_dsn.split('/')[2].split('@')[0]
        url = 'https://sentry.directline.company/api/{}/envelope/?sentry_key={}&sentry_version=7'.format(project_id, sentry_key)
        headers = {'Content-type': 'application/x-sentry-envelope', }

        r = requests.post(url=url, headers=headers, data=data)

        return JsonResponse(r.json())

    return JsonResponse({})

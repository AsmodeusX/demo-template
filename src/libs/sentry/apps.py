from django.conf import settings
from django.apps import AppConfig
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration


class Config(AppConfig):
    name = 'libs.sentry'

    def ready(self):
        if getattr(settings, 'SENTRY_DSN', '') and getattr(settings, 'SENTRY_ENVIRONMENT', ''):
            sentry_sdk.set_tag('issue', 'back-end')
            sentry_sdk.init(
                dsn=settings.SENTRY_DSN,
                integrations=[DjangoIntegration()],
                traces_sample_rate=1.0,
                send_default_pii=True,
                environment=settings.SENTRY_ENVIRONMENT,
            )
from django.apps import apps
from django.conf import settings
from django.core.management import BaseCommand
from django.db import models


class Command(BaseCommand):
    def handle(self, *args, **options):
        for app in settings.INSTALLED_APPS:
            models_name = app + ".models"

            if models_name.startswith("django."):
                continue

            try:
                models_module = __import__(models_name, fromlist=["models"])
                attributes = dir(models_module)

                for attr in attributes:
                    try:
                        attrib = models_module.__getattribute__(attr)

                        if issubclass(attrib, models.Model) and attrib.__module__ == models_name:
                            self.stdout.write(f'==> ReSave model {models_name}.{attr}...')

                            try:
                                apps.get_model(models_name, attr).save()

                            except LookupError:
                                self.stderr.write(f'Model {models_name}.{attr} not found')

                    except TypeError as e:
                        self.stderr.write(str(e))

            except ImportError as e:
                self.stderr.write(str(e))

        self.stdout.write('All models ReSaved')

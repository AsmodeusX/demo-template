from django.conf import settings
import requests
from libs.cache.cached import cached


@cached('city', 'country', time=7 * 24 * 60 * 60) # Неделя кэша - вряд ли данные о городе будут меняться очень часто.
def get_city(city, country):
    data = {
        'q': f"{city} {country}", # В данном случае ищется по городу и стране, но критерии могут быть разные
        'username': settings.GEONAMES_USER, # По сути база свободная, нужно только зарегаться
        'maxRows': 1 # Нужно чтобы отбросить, при наличии, объекты со схожим названием. Например При запросе Пермь - оставить только город, убрать Пермский край, Пермскую область и т.д.
    }
    url = f"http://api.geonames.org/searchJSON"

    try:
        r = requests.get(url, params=data)
    except requests.exceptions.ConnectionError:
        return None

    data = r.json()

    try:
        state = data['geonames'][0]['adminName1']
        state_code = data['geonames'][0]['adminCode1']
        country_code = data['geonames'][0]['countryCode']

        city_info = {
            'city': city,
            'state': state,
            'state_code': state_code,
            'country': country,
            'country_code': country_code,
        }
    except Exception as e:
        return None
    return city_info


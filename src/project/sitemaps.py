from django.contrib.sitemaps import GenericSitemap
from about.models import AboutConfig
from album.models import AlbumConfig
from contacts.models import ContactsConfig
from disclaimer.models import DisclaimerConfig
from main.models import MainPageConfig
from blog.models import BlogConfig, Post
from policy.models import PolicyConfig
from faq.models import FAQConfig
from services.models import Service


main_page = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}
policy_page = {
    'queryset': PolicyConfig.objects.filter(enable=True),
    'date_field': 'updated',
}
disclaimer_page = {
    'queryset': DisclaimerConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

about_page = {
    'queryset': AboutConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

album_page = {
    'queryset': AlbumConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

blog_page = {
    'queryset': BlogConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

post_page = {
    'queryset': Post.objects.visible(),
    'date_field': 'updated',
}

contacts_page = {
    'queryset': ContactsConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

faq_page = {
    'queryset': FAQConfig.objects.filter(enable=True),
    'date_field': 'updated',
}

service_page = {
    'queryset': Service.objects.filter(visible=True),
    'date_field': 'updated',
}


site_sitemaps = {
    'main': GenericSitemap(main_page, changefreq='daily', priority=1),
    'policy': GenericSitemap(policy_page, changefreq='weekly', priority=0.5),
    'disclaimer': GenericSitemap(disclaimer_page, changefreq='weekly', priority=0.5),
    'about': GenericSitemap(about_page, changefreq='weekly', priority=0.5),
    'album': GenericSitemap(album_page, changefreq='weekly', priority=0.5),
    'contacts': GenericSitemap(contacts_page, changefreq='weekly', priority=0.5),
    'faq': GenericSitemap(faq_page, changefreq='weekly', priority=0.5),
    'service': GenericSitemap(service_page, changefreq='weekly', priority=0.5),
    'blog': GenericSitemap(blog_page, changefreq='weekly', priority=0.5),
    'post': GenericSitemap(post_page, changefreq='weekly', priority=0.5)
}

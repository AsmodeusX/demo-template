(function($) {
    'use strict';
    /*
        Требует:
            jquery.fitvids.js, swiper.min.js
    */


    /*
        Вырезает дочерние текстовые DOM-узлы из элемента.
     */
    var cut_description = function(element) {
        var i = 0;
        var child;
        var description = '';
        var childs = element.childNodes;
        while (child = childs[i]) {
            if (child.nodeType === 3) {
                description += child.data;
                element.removeChild(child);
            } else if (child.tagName === 'BR') {
                if (description) {
                    // пропускаем первые BR
                    description += child.outerHTML;
                }
                element.removeChild(child);
            } else if (child.tagName === 'SPAN') {
                var text = cut_description(child);
                if (text) {
                    description += text;
                }
                element.removeChild(child);
            } else {
                i++
            }
        }

        return $.trim(description).replace(/[\n\r]+/g, '<br>');
    };

    /*
        Получение текста из base64 в data-аттрибуте
     */
    var decode_description = function($element) {
        var description = $element.data('description') || '';
        if (!description) {
            return '';
        }

        description = decodeURIComponent(atob(description));
        return $.trim(description).replace(/[\n\r]+/g, '<br>');
    };


    /*
        Обработка текстовых блоков
     */
    window.prepareTextStyles = function ($popupWrap) {
        var $el = $popupWrap ? $popupWrap : $('.text-styles');

        prepareTextBlocks($el);
    }

    function prepareTextBlocks($blocks) {
        $blocks.each(function() {
            var $text_block = $(this);
            // var $imgs = $text_block.find('.lazyload');
            //
            // $imgs.loadImage();
            // оборачивание таблиц
            $text_block.find('table').each(function() {
                $(this).wrap(
                    $('<div>').addClass('page-table')
                )
            });

            // описание к одиночной картинке, добавленной через перетаскивание
            $text_block.find('.simple-photo').each(function() {
                var $image = $(this),
                    $block = $image.parent(),
                    description = cut_description($block.get(0)) || decode_description($image);
                if (!description) return;

                $block.append(
                    $('<span>').addClass('object-description').html(description)
                );
            });

            // описание к одиночной картинке, добавленной через загрузчик
            $text_block.find('.single-image').each(function() {
                var $block = $(this),
                    $image = $block.find('img').first(),
                    description = cut_description($block.get(0)) || decode_description($image);
                if (!description) return;

                $block.replaceWith(
                    $('<figure>').addClass($block.attr('class')).append(
                        $image,
                        $('<figcaption>').addClass('object-description').html(description)
                    )
                );
            });

            $text_block.find('.page-video').each(function() {
                var $this = $(this);
                var $wrapper, $previewImg, $player, $playBtn;
                var key;

                if ($this.hasClass('youtube')) {
                    $wrapper = $('<div/>').addClass('wrapper');
                    $player = $('<div/>').addClass('player');
                    $playBtn = $('<div/>').addClass('play');
                    $previewImg = $this.find('picture').eq(0);

                    var url = $this.data('url');
                    if (key = /v=([-\w]+)/i.exec(url)) {
                        key = key[1];
                    } else if (key = /embed\/([-\w]+)/i.exec(url)) {
                        key = key[1];
                    } else {
                        $this.fitVids();
                        return
                    }

                    $this.prepend(
                        $wrapper.append(
                            $previewImg,
                            $player,
                            $playBtn
                        )
                    );

                    $this.on('click', function() {
                        $wrapper.addClass('loading');
                        YouTube($player, {
                            video: key
                        }).on('ready', function() {
                            this.$iframe.addClass('visible');
                            this.play();
                            $wrapper.removeClass('loading');
                        });
                    });
                } else if ($this.hasClass('vimeo')) {
                    $this.on('click', function() {
                        var options = {
                            id: key,
                            width: 640,
                            loop: false,
                            autoplay: true
                        };

                        new Vimeo.Player($player, options);
                    });
                } else {
                    $this.fitVids();
                }

                var description = cut_description(this);
                if (!description) return;

                $(this).append(
                    $('<span>').addClass('object-description').html(description)
                )
            });

            $text_block.find('a').each(function () {
                var $link = $(this);
                var href = $link.attr('href');
                var pattern = /^((http|https|ftp):\/\/)/;

                if(pattern.test(href)) {
                    $link.attr('rel', 'nofollow').attr('target', '_blank')
                }
            })

            // Слайдеры с описанием

            $text_block.find('p.page-images.multi-image').each(function() {
                var $block = $(this),
                    $slider = $('<div>').addClass('slider'),
                    $image_controls = $('<div>').addClass('image-controls'),
                    $swiper_container = $('<div>').addClass('swiper-container'),
                    $controls_container = $('<div>').addClass('controls-container'),
                    $swiper_wrapper = $('<div>').addClass('swiper-wrapper'),
                    $swiper_ui = $('<div>').addClass('swiper-ui'),
                    $swiper_pagination = $('<div>').addClass('swiper-pagination'),
                    $swiper_navigation = $('<div>').addClass('swiper-navigation').addClass('swiper-navigation--color-light'),
                    $swiper_button_prev = $('<div>').addClass('swiper-button').addClass('swiper-button-prev'),
                    $swiper_button_next = $('<div>').addClass('swiper-button').addClass('swiper-button-next'),
                    $slide_description = $('<div>').addClass('item-description'),
                    $swiper_slides = $block.find('img').map(function() {
                        var $image = $(this),
                            $image_wrapper = $('<div>').addClass('image-wrapper'),
                            // $preload_wrapper = $('<div>').addClass('swiper-lazy-preloader'),
                            $slide = $('<div>').addClass('swiper-slide');
                        $slide.append(
                            $image_wrapper.prepend(
                                $image,
                                // $preload_wrapper
                            ),
                        );
                        return $slide.get(0);
                    });

                $slider.append(
                    $image_controls.append(
                        $swiper_container.append(
                            $swiper_wrapper.append(
                                $swiper_slides
                            )
                        ),
                        $controls_container.append(
                            $swiper_navigation.append(
                                $swiper_button_prev,
                                $swiper_button_next,
                            )
                        )
                    ),
                    $slide_description,
                    $swiper_ui.append(
                        $swiper_pagination
                    ),
                )

                $block.empty().append(
                    $slider
                );
                var $desc = $block.find('.item-description');
                var swiper = new Swiper($block.find('.swiper-container').get(0), {
                    init: false,
                    autoHeight: true,
                    slidesPerView: 1,
                    spaceBetween: 20,
                    watchSlidesVisibility:true,
                    loop: true,
                    watchOverflow: true,
                    preloadImages: false,
                    lazy: true,
                    loopedSlides: 3,
                    loopFix: true,
                    // lazy: {
                    //     loadPrevNext: $.winWidth() < 768
                    // },
                    // zoom: {
                    //     maxRatio: 1.2,
                    // },
                    pagination: {
                        el: $block.find('div.swiper-pagination'),
                        clickable: true,
                        type: 'bullets',
                        dynamicBullets: true,
                        dynamicMainBullets: 2
                    },
                    navigation: {
                        nextEl: $block.find('.swiper-button-next').get(0),
                        prevEl: $block.find('.swiper-button-prev').get(0)
                    }
                }).on('init', function() {
                    var $img = $block.find('.swiper-slide[data-swiper-slide-index="' + swiper.realIndex + '"] img');
                    $desc.html(decode_description($img))
                })
                    .on('slideChange', function() {
                        var $img = $block.find('.swiper-slide[data-swiper-slide-index="' + swiper.realIndex + '"] img');
                        var desc = decode_description($img);
                        if (desc.length > 0){
                            $desc.html(desc);
                            $desc.slideDown(300)
                        } else {
                            $desc.slideUp(300);
                            setTimeout(function () {
                                $desc.html(desc);
                            }, 300)
                        }
                    });
                swiper.once('transitionEnd', function () {
                    swiper.loopFix();
                });
                swiper.init();
            });
        });
    }


    /*
        Обработка текстовых блоков
     */

    $(document).ready(function() {
        prepareTextBlocks($('.text-styles'));
    });

})(jQuery);

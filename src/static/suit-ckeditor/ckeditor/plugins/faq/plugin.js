(function() {

    CKEDITOR.plugins.add("faq", {
        requires: 'widget,dialog',
        icons: 'faq',
        // lang: 'en,ru',
        init: function (editor) {
            editor.addContentsCss(this.path + 'styles/widget.css');

            // ======================================
            //      Dialogs
            // ======================================
            CKEDITOR.dialog.add("faq_dialog", this.path + "dialogs/faq.js");

            // ======================================
            //      Commands
            // ======================================
            editor.addCommand("init_dialog", new CKEDITOR.dialogCommand("faq_dialog"));

            // ======================================
            //      Buttons
            // ======================================
            editor.ui.addButton("Faq", {
                label: 'Add FAQ',
                command: "init_dialog",
                toolbar: 'insert'
            });
        }
    })

})();

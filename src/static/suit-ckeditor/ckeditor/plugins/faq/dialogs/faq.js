(function() {

    CKEDITOR.dialog.add("faq_dialog", function (editor) {
        return {
            title: 'Add FAQ',
            minWidth: 400,
            minHeight: 100,
            contents: [{
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [{
                    id: 'count_questions',
                    type: 'text',
                    label: 'Count questions'
                }]
            }],

            onOk: function() {
                var element = editor.getSelection().getStartElement();
                var container = element.hasClass('faq') ? element : element.getParent();

                var dialogElement = this.getContentElement('tab-basic', 'count_questions').getElement();
                var input = dialogElement.getElementsByTag('input').getItem(0);

                var cnt = CKEDITOR.tools.trim(input.getValue());
                if (cnt) {
                    var s = '';
                    for (var i = 0; i < cnt; i++) {
                        s += '<tr>' +
                                '<td>Text Question</td>' +
                                '<td>Text Answer</td>' +
                            '</tr>'
                    }
                    element.appendHtml(
                        '<div class="faq">' +
                        '<table class="questions">' +
                        '<thead>' +
                        '<tr>' +
                        '<th contenteditable="false">Questions</th>' +
                        '<th contenteditable="false">Answers</th>' +
                        '</th>' +
                        '</thead>' +
                        '<tbody>' + s + '</tbody>' +
                        '</table>' +
                        '</div>',
                    );
                }
            }
        }
    })

})();

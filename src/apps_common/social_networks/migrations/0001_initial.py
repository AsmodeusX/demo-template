# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-10-20 09:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('network', models.CharField(choices=[('facebook', 'Facebook'), ('twitter', 'Twitter'), ('google', 'Google Plus'), ('linkedin', 'Linked In')], default='facebook', max_length=32, verbose_name='social network')),
                ('text', models.TextField(verbose_name='text')),
                ('url', models.URLField(verbose_name='URL')),
                ('scheduled', models.BooleanField(default=True, verbose_name='sheduled to share')),
                ('object_id', models.PositiveIntegerField(blank=True, editable=False, null=True)),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='created on')),
                ('posted', models.DateTimeField(editable=False, null=True, verbose_name='posted on')),
                ('content_type', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'feed post',
                'verbose_name_plural': 'feeds',
                'ordering': ('-scheduled', '-created'),
            },
        ),
        migrations.CreateModel(
            name='SocialConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('google_apikey', models.CharField(blank=True, default='AIzaSyDAx6URKWh7OAwJqKoiIkBq47mpZ0BGgR0', max_length=48, verbose_name='API Key')),
                ('twitter_client_id', models.CharField(blank=True, max_length=48, verbose_name='API Key')),
                ('twitter_client_secret', models.CharField(blank=True, max_length=64, verbose_name='API Secret')),
                ('twitter_access_token', models.CharField(blank=True, max_length=64, verbose_name='Access Token')),
                ('twitter_access_token_secret', models.CharField(blank=True, max_length=64, verbose_name='Access Token Secret')),
                ('facebook_client_id', models.CharField(blank=True, max_length=48, verbose_name='App ID')),
                ('facebook_client_secret', models.CharField(blank=True, max_length=64, verbose_name='App Secret')),
                ('facebook_access_token', models.TextField(blank=True, verbose_name='Access Token')),
                ('linkedin_client_id', models.CharField(blank=True, max_length=48, verbose_name='API Key')),
                ('linkedin_client_secret', models.CharField(blank=True, max_length=48, verbose_name='API Secret')),
                ('linkedin_access_token', models.TextField(blank=True, verbose_name='Access Token')),
                ('instagram_client_id', models.CharField(blank=True, max_length=48, verbose_name='Client ID')),
                ('instagram_client_secret', models.CharField(blank=True, max_length=48, verbose_name='Client Secret')),
                ('instagram_access_token', models.CharField(blank=True, max_length=64, verbose_name='Access Token')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'Settings',
                'default_permissions': ('change',),
            },
        ),
        migrations.CreateModel(
            name='SocialLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('provider', models.CharField(choices=[('pinterest', 'Pinterest'), ('vimeo', 'Vimeo'), ('google', 'Google'), ('linkedin', 'LinkedIn'), ('twitter', 'Twitter'), ('youtube', 'YouTube'), ('instagram', 'Instagram'), ('yelp', 'Yelp'), ('facebook', 'Facebook'), ('teleprice', 'TelePrice')], max_length=16, unique=True, verbose_name='provider')),
                ('url', models.URLField(verbose_name='URL')),
                ('order', models.IntegerField(default=0, verbose_name='order')),
            ],
            options={
                'verbose_name': 'Link',
                'verbose_name_plural': 'Links',
                'ordering': ('order', 'id'),
            },
        ),
        migrations.CreateModel(
            name='SocialLinks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('social_google', models.URLField(blank=True, max_length=255, verbose_name='google plus')),
                ('social_twitter', models.URLField(blank=True, max_length=255, verbose_name='twitter')),
                ('social_facebook', models.URLField(blank=True, max_length=255, verbose_name='facebook')),
                ('social_instagram', models.URLField(blank=True, max_length=255, verbose_name='instagram')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'Links',
                'default_permissions': ('change',),
            },
        ),
        migrations.AlterIndexTogether(
            name='feedpost',
            index_together=set([('network', 'content_type', 'object_id')]),
        ),
    ]

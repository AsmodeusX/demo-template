from django.db import models
from main.models import MainPageConfig


class Breadcrumbs:
    def __init__(self):
        main_config = MainPageConfig.get_solo()
        self.crumbs = [{
            'title': main_config.title,
            'url': main_config.get_absolute_url(),
        }]

    def __bool__(self):
        return bool(self.crumbs)

    def __iter__(self):
        return iter(self.crumbs)

    def add(self, title, url='', **kwargs):
        if isinstance(title, models.Model):
            self.crumbs.append(dict(
                title=str(title),
                url=title.get_absolute_url(),
                **kwargs
            ))
        else:
            self.crumbs.append(dict(
                title=title,
                url=url,
                **kwargs
            ))

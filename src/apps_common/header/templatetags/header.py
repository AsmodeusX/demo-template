from django.template import loader
from libs import jinja2


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'header'}
    takes_context = True

    def _header(self, context, item_id_mod=None, colored=False, template='header/header.html'):
        return loader.render_to_string(template, {
            'is_main_page': context.get('is_main_page'),
            'item_id_mod': item_id_mod,
            'colored': colored,
        }, request=context.get('request'))

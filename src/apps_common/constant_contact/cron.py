import logging

from django_cron import CronJobBase, Schedule

from constant_contact.services import refresh_tokens
from settings.dev import CC_REDIRECT_URI

log = logging.getLogger(__name__)


class RefreshTokensJob(CronJobBase):
    RUN_EVERY_MINS = 100
    RETRY_AFTER_FAILURE_MINS = 10
    
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'constant_contact.refresh_tokens_job'
    
    def do(self):
        try:
            # config = ConstantContactConfig.get_solo()
            refresh_tokens(CC_REDIRECT_URI)
            
            # if config.refresh_token_expires_in is not None and (datetime.datetime.now() - config.refresh_token_expires_in.replace(tzinfo=None)).days <= 10:
            #     refresh_tokens(CC_REDIRECT_URI)
        
        except Exception as ex:
            log.error(ex)

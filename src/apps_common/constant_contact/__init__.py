"""
    Интеграция Constant Contact (https://constantcontact.com/)


    !!!КОСЯКИ АПИ:
        1. Аккаунты блокируют очень быстро, даже с американским VPN и телефоном - нужно звонить подтверждать аккаунт.
        2. Callback-ссылка криво работает с указанием порта при получении токена (после двоеточия режет адрес),
            так что при локальной отладке нужно брать ссылку из дебаг-консоли и дописывать вручную

    Установка:
    1 Настройка на стороне поставщика API:
        1.1 Зарегистрироваться по ссылке https://developer.constantcontact.com/
        1.2 Создать приложение на странице https://app.constantcontact.com/pages/dma/portal/ путем нажатия кнопки "New Application".
            В результате мы должны получить CLIENT_ID и CLIENT_SECRET.
            P.S. В поле Redirect Url нужно указать url, по которому будет приниматься ответ о успехе/неудаче авторизации.
                Он должен быть:
                https://<domain.com>/constant-contact/callback/, где <domain> - реальный домен разрабатываемого сайта, либо:
                localhost(:8001)/constant-contact/callback/

    2 Файл src/settings/common.py
        2.1 Проставить в блоке настроек "Constant contact API" собственные данные.
            P.S. Если необходимо иметь разные учетные данные на разных стадиях разработки
            - переопределить их в соответствующих файлах настроек.

        2.2 Прописать "constant_contact" в списке INSTALLED_APPS
        2.3 Прописать в список "SUIT_CONFIG":
            {
                'app': 'constant_contact',
                'icon': 'icon-shopping-cart',
                'models': (
                    'ConstantContactConfig',
                    'Log',
                )
            },

    3 Файл src/urls/common.py
        3.1 Добавить в список "urlpatterns":
            url(r'^constant-contact/', include('constant_contact.urls')),

    4 Миграции
        4.1 Выполнить комманду:
            python3 manage.py migrate constant_contact

"""

default_app_config = 'constant_contact.apps.Config'

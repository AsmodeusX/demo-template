from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _, ugettext
from ipware.ip import get_ip

from solo.models import SingletonModel


class ConstantContactConfig(SingletonModel):
    use_module = models.BooleanField(_('use module'), default=True)

    """
        refresh_token - токен обновления access_token,
        access_token - токен доступа
        
        state_token - скрытый токен для проверки состояния
        realm_id - для отправки запросов

    """

    client_id = models.CharField(_('Client ID'), max_length=255, blank=True, null=True)
    client_secret = models.CharField(_('Client Secret'), max_length=255, blank=True, null=True, )

    refresh_token = models.CharField(_('Refresh token'), max_length=255, blank=True, null=True)
    refresh_token_expires_in = models.DateTimeField(_('Refresh token expires in'), blank=True, null=True,
                                                    editable=False)
    
    access_token = models.TextField(_('Access token'), max_length=1024, blank=True, null=True)
    access_token_expires_in = models.DateTimeField(_('Access token expires in'), blank=True, null=True, editable=False)
    
    # дополнительные обслуживающие атрибуты
    state_token = models.CharField(_('Refresh token'), max_length=255, blank=True, null=True, editable=False)
    # id_token = models.CharField(_('Id token'), max_length=255, blank=True, null=True, editable=False)
    realm_id = models.CharField(_('Realm Id'), max_length=255, blank=True, null=True, editable=False)
    list_id = models.CharField(_('list id'), max_length=255, blank=True, null=True,
                               help_text=_("You can get id from https://app.constantcontact.com/pages/contacts/ui#lists"
                                           " or generate new list by \"Create new list\" button"))
    
    updated = models.DateTimeField(_('Tokens updated'), default=now, blank=True, null=True, editable=False)
    
    @staticmethod
    def _get_hidden_text(string, visible_characters=12):
        if not string:
            return '-'
        elif len(string) > visible_characters:
            half = int(visible_characters / 2)
            return string[:half] + " ... <hidden for security reasons> ... " + string[-half:]
        else:
            return "hidden for security reasons"
    
    # def is_active(self):
    #     return self.refresh_token_expires_in is not None and self.refresh_token_expires_in > now()
    
    def is_active(self):
        return self.access_token_expires_in is not None and self.access_token_expires_in > now()
    
    is_active.boolean = True
    
    @property
    def refresh_token_repr(self):
        return self._get_hidden_text(self.refresh_token)
    
    @property
    def access_token_repr(self):
        return self._get_hidden_text(self.access_token)
    
    @property
    def realm_id_repr(self):
        return self._get_hidden_text(self.realm_id, visible_characters=8)
    
    def __init__(self, *args, **kwargs):
        super(ConstantContactConfig, self).__init__(*args, **kwargs)
        self._previous_refresh_token = self.refresh_token
        self._previous_access_token = self.access_token
    
    class Meta:
        default_permissions = ('change',)
        verbose_name = _('API Settings')
    
    def save(self, *args, **kwargs):
        if self._previous_refresh_token != self.refresh_token or self._previous_access_token != self.access_token:
            self.updated = now()
        super(ConstantContactConfig, self).save(*args, **kwargs)
    
    @staticmethod
    def get_absolute_url():
        return None
    
    def __str__(self):
        return ugettext('API Settings')


class Log(models.Model):
    STATUS_CREATED = 1
    STATUS_UPDATED = 2
    STATUS_ERROR = 3
    STATUS_EXCEPTION = 4
    STATUSES = (
        (STATUS_CREATED, _('Created')),
        (STATUS_UPDATED, _('Updated')),
        (STATUS_ERROR, _('Error')),
        (STATUS_EXCEPTION, _('Exception')),
    )
    
    # qb_id = models.CharField(_('qb item id'), max_length=255, blank=True, null=True)
    status = models.PositiveSmallIntegerField(_('status'), choices=STATUSES)
    msg_body = models.TextField(_('message'), null=True, blank=True)
    # request_get = models.TextField(_('GET'))
    request_post = models.TextField(_('payload'), null=True, blank=True)
    request_ip = models.GenericIPAddressField(_('IP'), null=True, blank=True)
    created = models.DateTimeField(_('create date'), default=now, editable=False)
    
    class Meta:
        default_permissions = ('delete',)
        verbose_name = _('log message')
        verbose_name_plural = _('log messages')
        ordering = ('-created',)
    
    def __str__(self):
        status = dict(self.STATUSES).get(self.status)
        return f'[{status}] {self.msg_body}'
    
    @classmethod
    def create(cls, request=None, body=None, status=None):
        return cls.objects.create(
            status=status,
            msg_body=body,
            request_post=request,
            request_ip=get_ip(request) if request else None,
        )
    
    @classmethod
    def item_created(cls, request, body):
        return cls.create(request, body, status=cls.STATUS_CREATED)
    
    @classmethod
    def item_updated(cls, request, body):
        return cls.create(request, body, status=cls.STATUS_UPDATED)
    
    # @classmethod
    # def success(cls, request, body, qb_id=None):
    #     return cls.create(request, body, qb_id, status=cls.STATUS_SUCCESS)
    #
    # @classmethod
    # def error(cls, request, body, qb_id=None):
    #     return cls.create(request, body, qb_id, status=cls.STATUS_ERROR)
    
    @classmethod
    def exception(cls, request, body):
        return cls.create(request, body, status=cls.STATUS_EXCEPTION)

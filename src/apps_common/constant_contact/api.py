from __future__ import absolute_import

import base64
import json

import requests
from django.conf import settings
from future.moves.urllib.parse import urlencode
from oauthlib.common import generate_token
from requests import Session
from django.shortcuts import redirect
from .models import Log

from constant_contact.models import ConstantContactConfig


def scopes_to_string(scopes):

    return ' '.join(scope for scope in scopes).strip()


def set_attributes(obj, response_json):
    """
    Sets attribute to an object from a dict
    :param obj: Object to set the attributes to
    :param response_json: dict with key names same as object attributes
    """
    for key in response_json:
        try:
            setattr(obj, key, response_json[key])
        except TypeError:
            Log.exception(None, f"Can't parse response \"{response_json}\"")
            return


def send_request(method, url, header, obj, body=None, session=None, oauth1_header=None):
    data = {
        'method': method,
        'url': url,
        'headers': header,
        'data': body,
        'auth': oauth1_header
    }

    response = session.request(
        method=data['method'],
        url=data['url'],
        headers=data['headers'],
        data=data['data'],
        auth=data['auth']
    ) if session is not None and isinstance(session, Session) else requests.request(
        method=data['method'],
        url=data['url'],
        headers=data['headers'],
        data=data['data'],
        auth=data['auth']
    )

    if response.content:
        set_attributes(obj, response.json())

    return response


class AuthClient(requests.Session):
    """Handles OAuth 2.0 and OpenID Connect flows to get access to User Info API, Accounting APIs and Payments APIs
    """

    def __init__(self, client_id, redirect_uri, state_token=None, access_token=None,
                 refresh_token=None, id_token=None, realm_id=None):
        """Constructor for AuthClient
        """

        super(AuthClient, self).__init__()

        self.client_id = client_id
        self.redirect_uri = redirect_uri
        self.state_token = state_token

        # response values
        self.realm_id = realm_id
        self.access_token = access_token
        self.expires_in = None
        self.refresh_token = refresh_token
        self.x_refresh_token_expires_in = None
        self.id_token = id_token

    def get_authorization_url(self, scopes, state_token=None):
        """
        Generates authorization url using scopes specified where user is redirected to
        :param scopes: Scopes for OAuth/OpenId flow
        :param state_token: CSRF token, defaults to None
        :return: Authorization url
        """

        state = state_token or self.state_token
        if state is None:
            state = generate_token()
        self.state_token = state

        url_params = {
            'client_id': self.client_id,
            'response_type': 'code',
            'scope': scopes_to_string(scopes),
            'redirect_uri': self.redirect_uri,
            'state': self.state_token
        }

        return '?'.join([settings.CC_AUTH_URL, urlencode(url_params)])

    def get_bearer_token(self, auth_code):
        """ Gets access_token and refresh_token using authorization code """
        config = ConstantContactConfig.get_solo()
        credentials = base64.b64encode(str.encode(f'{config.client_id}:{config.client_secret}')).decode()
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': f'Basic {credentials}'
        }
        body = {
            'grant_type': 'authorization_code',
            'code': auth_code,
            'redirect_uri': self.redirect_uri
        }

        send_request('POST', 'https://authz.constantcontact.com/oauth2/default/v1/token', headers, self,
                     body=urlencode(body), session=self)

    def refresh(self, refresh_token=None):
        """Gets fresh access_token and refresh_token

        :param refresh_token: Refresh Token
        :raises ValueError: if Refresh Token value not specified
        :raises `intuitlib.exceptions.AuthClientError`: if response status != 200
        """

        token = refresh_token or self.refresh_token

        if token is None:
            error_msg = "Refresh token not specified"
            Log.exception(None, error_msg)
            # return redirect('constant_contact:oauth')
            raise ValueError(error_msg)

        config = ConstantContactConfig.get_solo()
        credentials = base64.b64encode(str.encode(f'{config.client_id}:{config.client_secret}')).decode()
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': f'Basic {credentials}'
        }
        body = {
            'grant_type': 'refresh_token',
            'refresh_token': config.refresh_token,
        }

        send_request('POST', 'https://authz.constantcontact.com/oauth2/default/v1/token', headers, self,
                     body=urlencode(body),
                     session=self)

    def create_list(self, name=None, favorite=False, description=""):
        if not name:
            raise ValueError('Please set name of list')

        config = ConstantContactConfig.get_solo()

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {config.access_token}'
        }
        body = {
            'name': name,
            'favorite': favorite,
            'description': description,
        }

        result = send_request('POST', 'https://api.cc.email/v3/contact_lists/', headers, self,
                              body=json.dumps(body), session=self)

        if result.status_code == 409:   # If duplicates
            self.delete_list()

            result = send_request('POST', 'https://api.cc.email/v3/contact_lists/', headers, self,
                                  body=json.dumps(body), session=self)

        return dict(result.json()).get('list_id')

    def delete_list(self):
        config = ConstantContactConfig.get_solo()
        list_id = config.get_solo()

        if not list_id:
            return None

        headers = {
            'Accept': 'application/x-www-form-urlencoded',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {config.access_token}'
        }

        result = send_request('DELETE', f'https://api.cc.email/v3/contact_lists/{config.list_id}/',
                              headers, self, body={}, session=self)

        return result

    def get_all_contacts_lists(self):
        config = ConstantContactConfig.get_solo()

        headers = {
            'Accept': 'application/x-www-form-urlencoded',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {config.access_token}'
        }

        result = send_request('GET', 'https://api.cc.email/v3/contact_lists?include_count=true&include_membership_count=all',
                              headers, self, body=None, session=self)

        return result

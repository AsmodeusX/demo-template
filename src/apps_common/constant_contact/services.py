import json
import requests

from datetime import datetime
from django.conf import settings
from django.http.response import HttpResponse, HttpResponseForbidden
from django.urls import reverse
from django.utils.timezone import now
from django.forms import forms
from constant_contact.models import ConstantContactConfig, Log
from .api import AuthClient

REDIRECT_URI_CACHE = None
BASE_CC_URL = "https://api.cc.email/v3/"


def get_redirect_uri(request):
    """
        Возвращает REDIRECT_URI, обрабатывающую callback.
        :param request - объект запроса
        :return redirect_uri - ссылка для редиректа после успешной авторизации через OAuth
    """
    global REDIRECT_URI_CACHE
    
    if REDIRECT_URI_CACHE is not None:
        return REDIRECT_URI_CACHE
    else:
        REDIRECT_URI_CACHE = settings.CC_REDIRECT_URI if settings.DEBUG else \
            request.build_absolute_uri(reverse('constant_contact:callback'))
        return REDIRECT_URI_CACHE


def get_expires_by_timestamp(timestamp_value):
    dt = datetime.fromtimestamp(datetime.timestamp(now()) + timestamp_value)
    return dt


def refresh_tokens(callback_uri):
    """
        Обновляет токены
        :param callback_uri - uri для callback при успешной обработке.
    """
    config = ConstantContactConfig.get_solo()
    auth_client = AuthClient(
        config.client_id,
        callback_uri,
        state_token=config.state_token,
    )
    
    try:
        auth_client.refresh(config.refresh_token)
    except Exception as ex:
        Log.exception(request=None, body=str(ex))
        raise HttpResponseForbidden
    
    config.refresh_token = auth_client.refresh_token
    config.access_token = auth_client.access_token
    
    # Expires
    if auth_client.expires_in:
        config.access_token_expires_in = get_expires_by_timestamp(auth_client.expires_in)
    
    if auth_client.x_refresh_token_expires_in:
        config.refresh_token_expires_in = get_expires_by_timestamp(auth_client.x_refresh_token_expires_in)
    
    config.save()


def create_list(callback_uri):
    """
        Создает новый список контактов и записывает его id в конфиг
        :param callback_uri - uri для callback при успешной обработке.
    """
    config = ConstantContactConfig.get_solo()
    auth_client = AuthClient(
        config.client_id,
        callback_uri,
        state_token=config.state_token,
    )

    try:
        try:
            contacts_list = dict(auth_client.get_all_contacts_lists().json())['lists_count']
        except KeyError as ex:
            Log.exception(None, f"Can't get count lists for generate name for new list. Key error on \"{ex}\"")
            raise ValueError(f"Can't get count lists for generate name for new list. Key error on \"{ex}\"")

        contacts_lists_count = int(contacts_list.get('lists_count'))
        cc_list_id = auth_client.create_list(f"List contacts from {settings.PROJECT_NAME} #{contacts_lists_count + 1}")
    except Exception as ex:
        Log.exception(request=None, body=str(ex))
        raise ValueError(ex)

    config.list_id = cc_list_id
    config.save()


def set_bearer_tokens(request):
    """
        Устанавливает токены из request
        :param request - объект запроса
        :return None
    """
    config = ConstantContactConfig.get_solo()
    auth_client = AuthClient(
        config.client_id,
        get_redirect_uri(request),
        state_token=config.state_token,
    )
    error = request.GET.get('error', None)
    auth_code = request.GET.get('code', None)

    use_raise = error or config.state_token is None or auth_code is None or config.state_token != auth_client.state_token

    if use_raise:
        msg = error if error else None
        error_object = None

        if config.state_token is None or config.state_token != auth_client.state_token:
            error_object = 'state_token'

        if auth_code is None:
            error_object = 'auth_code'

        msg = f"Incorrect {error_object}. Please try again." if not msg and error_object else "Unknown error."

        Log.exception(request=None, body=msg)
        raise HttpResponseForbidden

    config.realm_id = request.GET.get('realmId', None)

    auth_client.get_bearer_token(auth_code)
    config.access_token = auth_client.access_token
    config.refresh_token = auth_client.refresh_token

    if auth_client.expires_in:
        config.access_token_expires_in = get_expires_by_timestamp(auth_client.expires_in)
    if auth_client.x_refresh_token_expires_in:
        config.refresh_token_expires_in = get_expires_by_timestamp(auth_client.x_refresh_token_expires_in)
    
    config.save()


def get_cc_oauth_url(callback_uri, scopes=['contact_data', 'campaign_data', 'offline_access']):
    """
        Возвращает OAuth url для авторизации в Constant Contact
        :param callback_uri - uri для callback при успешной обработке.
        :param scopes - области к которым можно будет дотянуться с помощью созданых токенов.
        :return url - ссылка для OAuth авторизации
    """
    config = ConstantContactConfig.get_solo()
    auth_client = AuthClient(
        config.client_id,
        callback_uri
    )
    url = auth_client.get_authorization_url([_ for _ in scopes])
    config.state_token = auth_client.state_token
    config.save()
    
    return url


def send_request(method, url, header, body=None, oauth1_header=None):
    response = requests.request(method, url, headers=header, json=body, auth=oauth1_header)
    
    if response.status_code != 200:
        Log.exception(None, response.text)
        # raise AuthClientError(response)
    
    return response


def _headers():
    cc_config = ConstantContactConfig.get_solo()

    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {cc_config.access_token}'
    }


def create_contact(first_name, last_name, email):
    """ Gets access_token and refresh_token using authorization code """
    body = {
        "email_address": {
            "address": email,
            "permission_to_send": "implicit"
        },
        "first_name": first_name,
        "last_name": last_name,
        "create_source": "Contact",
    }

    response = send_request('POST', f'{BASE_CC_URL}contacts', _headers(), body=body)
    content_decode = json.loads(response.content.decode())
    
    if response.status_code != 200:
        return HttpResponse(content_decode)
    
    return HttpResponse()


def create_cc_contact(request, email, name='', sub_list=None):
    body = {
        "email_address": {
            "address": email,
            "permission_to_send": "implicit"
        },
        "first_name": name,
        "create_source": "Contact",
        "list_memberships": [sub_list],
    }
    
    response = send_request('POST', f'{BASE_CC_URL}contacts', _headers(), body=body)
    content_decode = json.loads(response.content.decode())
    
    if response.status_code != 201:
        Log.exception(request, f'{content_decode} {response.status_code}')


def get_contacts_lists(*args, **kwargs):
    """ Gets contacts lists using access token """
    response = send_request('GET', f'{BASE_CC_URL}contact_lists', _headers())
    data_dict = json.loads(response.content.decode())
    if data_dict.get('error_key'):
        msg = data_dict['error_message'] if data_dict.get('error_message') else "Unknown error"
        Log.exception(request=None, body=f'Try to get list contacts. Error: {msg}')
        raise HttpResponseForbidden
    return HttpResponse('( ID: {} Name: {} ) '.format(i['list_id'], i['name']) for i in data_dict['lists'])

from django.conf.urls import url
from django.views.generic.base import RedirectView

from . import views
from .services import create_contact, get_contacts_lists, create_list

app_name = 'constant_contact'

urlpatterns = [
    url(r'^oauth/$', views.OAuthView.as_view(), name='oauth'),
    url(r'^callback/$', views.CallbackView.as_view(), name='callback'),
    url(r'^refresh/$', views.RefreshTokenView.as_view(), name='refresh'),
    url(r'^list/create/$', views.CreateListView.as_view(), name='create_list'),
    url(r'^create/$', create_contact, name='create'),
    url(r'^get-lists/$', get_contacts_lists, name='get_lists'),
]

from __future__ import absolute_import

import asyncio

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, resolve_url
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import View
from .models import ConstantContactConfig
from .services import get_redirect_uri, set_bearer_tokens, refresh_tokens, get_cc_oauth_url, create_list


class ConstantContactApiView(View):

    http_method_names = ['get', ]
    
    @method_decorator(login_required(login_url='admin:login'))
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return self.http_method_not_allowed(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)
    
    @staticmethod
    def error_response(request):
        referer = request.META.get('HTTP_REFERER', None)
        return redirect(referer if referer else resolve_url('admin:index'))
    
    @staticmethod
    def success_response():
        config = ConstantContactConfig.get_solo()
        opts = config._meta
        return redirect(reverse('admin:%s_%s_change' % (opts.app_label, opts.object_name.lower())))


class OAuthView(ConstantContactApiView):
    def get(self, request, *args, **kwargs):
        callback_uri = get_redirect_uri(request)
        url = get_cc_oauth_url(callback_uri)
        
        return redirect(url)


class CallbackView(ConstantContactApiView):
    
    def dispatch(self, request, *args, **kwargs):
        return super(CallbackView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        try:
            set_bearer_tokens(request)
            messages.add_message(request, messages.SUCCESS, "API call was success")
        
        except Exception as ex:
            messages.add_message(request, messages.ERROR, str(ex))
            return self.error_response(request)
        
        return self.success_response()


class RefreshTokenView(ConstantContactApiView):
    
    def get(self, request, *args, **kwargs):
        try:
            callback_uri = get_redirect_uri(request)
            refresh_tokens(callback_uri)
        except Exception as ex:
            messages.add_message(request, messages.ERROR, str(ex))
            return self.error_response(request)
        return self.success_response()


class CreateListView(ConstantContactApiView):

    def get(self, request, *args, **kwargs):
        try:
            callback_uri = get_redirect_uri(request)
            create_list(callback_uri)
        except Exception as ex:
            messages.add_message(request, messages.ERROR, str(ex))
            return self.error_response(request)
        return self.success_response()


class CreateContactView(ConstantContactApiView):
    
    def get(self, request, *args, **kwargs):
        try:
            callback_uri = get_redirect_uri(request)
            refresh_tokens(callback_uri)
        except Exception as ex:
            messages.add_message(request, messages.ERROR, str(ex))
            return self.error_response(request)
        return self.success_response()

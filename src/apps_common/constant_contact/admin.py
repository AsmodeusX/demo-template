from django.contrib import admin
from django.template.defaultfilters import truncatechars
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin

from constant_contact.models import ConstantContactConfig, Log
from project.admin import ModelAdminMixin


@admin.register(ConstantContactConfig)
class ConstantContactConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    add_form_template = 'constant_contact/admin/change_form.html'
    change_form_template = 'constant_contact/admin/change_form.html'
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'use_module',
            ),
        }),
        (_("Application"), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'client_id', 'client_secret',
            ),
        }),
        (_("Tokens"), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'refresh_token_repr', 'access_token_repr',
            ),
        }),
        (_("Info"), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'list_id', 'access_token_expires_in', 'is_active', 'updated',
            ),
        }),
    )
    readonly_fields = ('access_token_expires_in', 'refresh_token_repr',
                       'access_token_repr', 'is_active', 'updated', )
    suit_form_tabs = (
        ('general', _('General')),
    )
    suit_form_includes = (
        ('constant_contact/admin/description.html', 'top', 'rules'),
    )


@admin.register(Log)
class LogAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'status', 'msg_body', 'created',
            ),
        }),
        (_('Request'), {
            'fields': (
                'fmt_request_post', 'request_ip',
            ),
        }),
    )
    list_filter = ('status',)
    search_fields = ('request_ip',)
    list_display = ('status', 'short_msg_body', 'request_ip', 'created')
    readonly_fields = (
        'status', 'msg_body',
        'fmt_request_post', 'request_ip',
        'created',
    )
    list_display_links = ('status', 'short_msg_body')
    date_hierarchy = 'created'
    
    def short_msg_body(self, obj):
        return truncatechars(obj.msg_body, 200)
    
    short_msg_body.short_description = _('Message')
    
    def fmt_request_post(self, obj):
        return obj.request_post.replace('&', '\n')
    
    fmt_request_post.short_description = _('Payload')
    
    def has_add_permission(self, request):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

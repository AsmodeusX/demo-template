from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    name = 'constant_contact'
    verbose_name = _('Constant Contact')


from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel


class StripeConfig(SingletonModel):
    publishable_key = models.CharField(_('publishable key'), max_length=128)
    secret_key = models.CharField(_('secret key'), max_length=128)

    class Meta:
        verbose_name = ugettext('Stripe config')

    def __str__(self):
        return ugettext('Stripe config')

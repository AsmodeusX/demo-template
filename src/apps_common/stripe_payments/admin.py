from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import StripeConfig
from project.admin import ModelAdminMixin


@admin.register(StripeConfig)
class StripeConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        ('Keys', {
            'fields': (
                'publishable_key', 'secret_key',
            ),
        }),
    )

from django.utils.translation import ugettext_lazy as _
from .models import StripeConfig
import stripe


class StripeAPI:
    CONFIG = None
    keys = None

    def __init__(self):
        self.CONFIG = StripeConfig.get_solo()
        self.keys = {
            'publishable': self.CONFIG.publishable_key,
            'secret': self.CONFIG.secret_key,
        }

        self.auth()

    def auth(self):

        stripe.api_key = self.keys.get('secret')

    @staticmethod
    def payment(card, amount, description):
        p = card.get('date_expired').split('/')
        date_expired_month = p[0]
        date_expired_year = p[1]
        amount = str(amount).replace('$', '').replace(',', '').replace('.', '')

        if None in card:
            return {
                'error': _('Data is incorrect')
            }
        try:
            token = stripe.Token.create(
                card={
                    "number": card.get('card_number'),
                    "exp_month": date_expired_month,
                    "exp_year": date_expired_year,
                    "cvc": card.get('cvv')
                },
            )
        except stripe.error.CardError as e:
            return {
                'error': e.json_body['error']['message']
            }

        try:
            send_order = stripe.Charge.create(
                amount=amount,  # amount in cents,
                currency="usd",
                source=token,
                description=description,
            )
        except (stripe.error.CardError, stripe.error.InvalidRequestError) as e:
            return {
                'error': e.json_body['error']['message']
            }

        return send_order
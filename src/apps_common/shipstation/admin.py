from django import forms
from django.contrib import admin
from django.template.defaultfilters import truncatechars
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableModelAdmin

from project.admin import ModelAdminMixin
from .models import ShipStationConfig, Log, Weight, Dimension


class ShipStationForm(forms.ModelForm):
    class Meta:
        model = ShipStationConfig
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ShipStationForm, self).__init__(*args, **kwargs)


@admin.register(ShipStationConfig)
class ShipStationConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    form = ShipStationForm
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'api_key', 'api_secret'
            ),
        }),
        (_('Shipping cost'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'flat_rate', 'free_shipping_from', 'flat_rate_options', 'weight_unit', 'postal_code',
            ),
        }),
    )

    suit_form_tabs = (
        ('general', _('General')),
    )

    def get_form(self, request, obj=None, **kwargs):
        model_form = super(ShipStationConfigAdmin, self).get_form(request, obj, **kwargs)

        class ModelFormWithRequest(model_form):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return model_form(*args, **kwargs)

        return ModelFormWithRequest


@admin.register(Weight)
class WeightAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('value', 'units'),
        }),
    )
    sortable = 'order'
    list_display = ('__str__',)
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Dimension)
class DimensionAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('length', 'width', 'height', 'units'),
        }),
    )
    sortable = 'order'
    list_display = ('__str__',)
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Log)
class LogAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'status', 'msg_body', 'inv_id', 'created',
            ),
        }),
        (_('Request'), {
            'fields': (
                'fmt_request_get', 'fmt_request_post', 'request_ip',
            ),
        }),
    )
    list_filter = ('status',)
    search_fields = ('inv_id', 'request_ip')
    list_display = ('status', 'short_msg_body', 'inv_id', 'request_ip', 'created')
    readonly_fields = (
        'inv_id', 'status', 'msg_body',
        'fmt_request_get', 'fmt_request_post', 'request_ip',
        'created',
    )
    list_display_links = ('status', 'short_msg_body')
    date_hierarchy = 'created'

    def short_msg_body(self, obj):
        return truncatechars(obj.msg_body, 48)
    short_msg_body.short_description = _('Message')

    def fmt_request_get(self, obj):
        return obj.request_get.replace('&', '\n')
    fmt_request_get.short_description = _('GET')

    def fmt_request_post(self, obj):
        return obj.request_post.replace('&', '\n')
    fmt_request_post.short_description = _('POST')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

import datetime
from decimal import Decimal
import json
import pprint
import requests
from requests.exceptions import Timeout

from libs.cache.cached import cached
from .models import ShipStationConfig
from libs.valute_field.valute import Valute


class ShipStationBase(object):
    @classmethod
    def to_camel_case(cls, name):
        tokens = name.lower().split("_")
        first_word = tokens.pop(0)
        return first_word + "".join(x.title() for x in tokens)

    def as_dict(self):
        d = dict()

        for key, value in self.__dict__.items():
            key = self.to_camel_case(key)
            if value is None:
                d[key] = None
            else:
                d[key] = str(value)

        return d


class ShipStationCustomsItem(ShipStationBase):
    def __init__(
            self,
            description=None,
            quantity=1,
            value=Decimal("0"),
            harmonized_tariff_code=None,
            country_of_origin=None,
    ):
        self.description = description
        self.quantity = quantity
        self.value = value
        self.harmonized_tariff_code = harmonized_tariff_code
        self.country_of_origin = country_of_origin

        if not self.description:
            raise AttributeError("description may not be empty")
        if not self.harmonized_tariff_code:
            raise AttributeError("harmonized_tariff_code may not be empty")
        if not self.country_of_origin:
            raise AttributeError("country_of_origin may not be empty")
        if len(self.country_of_origin) is not 2:
            raise AttributeError("country_of_origin must be two characters")
        if not isinstance(value, Decimal):
            raise AttributeError("value must be decimal")


class ShipStationInternationalOptions(ShipStationBase):
    CONTENTS_VALUES = ("merchandise", "documents",
                       "gift", "returned_goods", "sample")

    NON_DELIVERY_OPTIONS = ("return_to_sender", "treat_as_abandoned")

    def __init__(self, contents=None, non_delivery=None):
        self.customs_items = []
        self.contents = None
        self.non_delivery = None
        self.set_contents(contents)
        self.set_non_delivery(non_delivery)

    def set_contents(self, contents):
        if contents:
            if contents not in self.CONTENTS_VALUES:
                raise AttributeError("contents value not valid")
            self.contents = contents

    def add_customs_item(self, customs_item):
        if customs_item:
            if not isinstance(customs_item, ShipStationCustomsItem):
                raise AttributeError("must be of type ShipStationCustomsItem")
            self.customs_items.append(customs_item)

    def get_items(self):
        return self.customs_items

    def get_items_as_dicts(self):
        return [x.as_dict() for x in self.customs_items]

    def set_non_delivery(self, non_delivery):
        if non_delivery:
            if non_delivery not in self.NON_DELIVERY_OPTIONS:
                raise AttributeError("non_delivery value is not valid")
            self.non_delivery = non_delivery

    def as_dict(self):
        d = super(ShipStationInternationalOptions, self).as_dict()

        d["customsItems"] = self.get_items_as_dicts()

        return d


class ShipStationWeight(ShipStationBase):
    def __init__(self):
        self.units = None
        self.value = None

    def set_units(self, units):
        if units in dict(ShipStationConfig.WEIGHT_CHOICES).keys():
            self.units = dict(ShipStationConfig.WEIGHT_CHOICES)[units]
        else:
            raise AttributeError("Invalid weight units")

    def set_value(self, value):
        if isinstance(value, float) or isinstance(value, int):
            self.value = value
        else:
            raise AttributeError("Invalid weight value")


class ShipStationOption(ShipStationBase):
    def __init__(self, name=None, value=None):
        self.name = name
        self.value = value

    def as_dict(self):
        d = super(ShipStationOption, self).as_dict()
        return d


class ShipStationContainer(ShipStationBase):
    def __init__(self, units=None, length=None, width=None, height=None):
        self.units = units
        self.length = length
        self.width = width
        self.height = height
        self.weight = None

    def set_weight(self, weight):
        if type(weight) is not ShipStationWeight:
            raise AttributeError("Should be type ShipStationWeight")

        self.weight = weight

    def as_dict(self):
        d = super(ShipStationContainer, self).as_dict()

        if self.weight:
            d["weight"] = self.weight.as_dict()

        return d


class ShipStationItem(ShipStationBase):
    def __init__(
            self,
            key=None,
            sku=None,
            name=None,
            image_url='',
            quantity=None,
            unit_price=None,
            warehouse_location=None,
    ):
        self.key = key
        self.sku = sku
        self.name = name
        self.image_url = image_url
        self.weight = None
        self.quantity = quantity
        self.unit_price = unit_price
        self.warehouse_location = warehouse_location
        self.options = []

    def add_option(self, option):
        """
        Adds a new option to the order with all of the required keys.
        """
        self.options.append(option)

    def get_options(self):
        return self.options

    def get_options_as_dicts(self):
        return [x.as_dict() for x in self.options]

    def set_weight(self, weight):
        if type(weight) is not ShipStationWeight:
            raise AttributeError("Should be type ShipStationWeight")

        self.weight = weight

    def as_dict(self):
        d = super(ShipStationItem, self).as_dict()

        d["options"] = self.get_options_as_dicts()
        if self.weight:
            d["weight"] = self.weight.as_dict()

        return d


class ShipStationAddress(ShipStationBase):
    def __init__(
            self,
            name=None,
            company=None,
            street1=None,
            street2=None,
            street3=None,
            city=None,
            state=None,
            postal_code=None,
            country=None,
            phone=None,
            residential=None,
    ):
        self.country = country
        self.name = name
        self.company = company
        self.street1 = street1
        self.street2 = street2
        self.street3 = street3
        self.city = city
        self.state = state
        self.postal_code = postal_code
        self.phone = phone
        self.residential = residential


class ShipStationOrder(ShipStationBase):
    """
    Accepts the data needed for an individual ShipStation order and
    contains the tools for submitting the order to ShipStation.
    """

    ORDER_STATUS_VALUES = (
        "awaiting_payment",
        "awaiting_shipment",
        "shipped",
        "on_hold",
        "cancelled",
    )

    #add method for adding confirmation which respects these values.
    CONFIRMATION_VALUES = (
        "none",
        "delivery",
        "signature",
        "adult_signature",
        "direct_signature",
    )

    def __init__(self, order_key=None, order_number=None):

        # Required attributes
        self.order_number = order_number
        self.order_date = datetime.datetime.now().isoformat()
        self.order_status = None
        self.bill_to = None
        self.ship_to = None

        # Optional attributes
        self.order_key = order_key
        self.payment_date = None
        self.customer_username = None
        self.customer_email = None
        self.items = []
        self.amount_paid = Decimal("0")
        self.tax_amount = Decimal("0")
        self.shipping_amount = Decimal("0")
        self.customer_notes = None
        self.internal_notes = None
        self.gift = None
        self.payment_method = None
        self.carrier_code = None
        self.service_code = None
        self.package_code = None
        self.confirmation = None
        self.ship_date = None
        self.dimensions = None
        self.insurance_options = None
        self.international_options = None
        self.advanced_options = None

    def set_status(self, status=None):
        if not status:
            self.order_status = None
        elif status not in self.ORDER_STATUS_VALUES:
            raise AttributeError("Invalid status value")
        else:
            self.order_status = status

    def set_customer_details(self, username=None, email=None):
        self.customer_username = username
        self.customer_email = email

    def set_shipping_address(self, shipping_address=None):
        if type(shipping_address) is not ShipStationAddress:
            raise AttributeError("Should be type ShipStationAddress")

        self.ship_to = shipping_address

    def get_shipping_address_as_dict(self):
        if self.ship_to:
            return self.ship_to.as_dict()
        else:
            return None

    def set_billing_address(self, billing_address):
        if type(billing_address) is not ShipStationAddress:
            raise AttributeError("Should be type ShipStationAddress")

        self.bill_to = billing_address

    def get_billing_address_as_dict(self):
        if self.bill_to:
            return self.bill_to.as_dict()
        else:
            return None

    def set_dimensions(self, dimensions):
        if type(dimensions) is not ShipStationContainer:
            raise AttributeError("Should be type ShipStationContainer")

        self.dimensions = dimensions

    def get_dimensions_as_dict(self):
        if self.dimensions:
            return self.dimensions.as_dict()
        else:
            return None

    def set_order_date(self, date):
        self.order_date = date

    def get_order_date(self):
        return self.order_date

    def set_payment_date(self, date):
        self.payment_date = date

    def get_payment_date(self):
        return self.payment_date

    def set_ship_date(self, date):
        self.ship_date = date

    def get_ship_date(self):
        return self.ship_date

    def get_weight(self):
        weight = 0
        items = self.get_items()
        for item in items:
            if item.weight and item.quantity:
                weight += item.weight.value * item.quantity
        if self.dimensions and self.dimensions.weight:
            weight += self.dimensions.weight.value

        return dict(units="ounces", value=round(weight, 2))

    def add_item(self, item):
        """
        Adds a new item to the order with all of the required keys.
        """
        self.items.append(item)

    def get_items(self):
        return self.items

    def get_items_as_dicts(self):
        return [x.as_dict() for x in self.items]

    def set_international_options(self, options):
        if not isinstance(options, ShipStationInternationalOptions):
            raise AttributeError(
                "options should be an instance of " + "ShipStationInternationalOptions"
            )
        self.international_options = options

    def get_international_options_as_dict(self):
        if self.international_options:
            return self.international_options.as_dict()
        else:
            return None

    def as_dict(self):
        d = super(ShipStationOrder, self).as_dict()

        d["items"] = self.get_items_as_dicts()
        d["dimensions"] = self.get_dimensions_as_dict()
        d["billTo"] = self.get_billing_address_as_dict()
        d["shipTo"] = self.get_shipping_address_as_dict()
        d["weight"] = self.get_weight()
        d["internationalOptions"] = self.get_international_options_as_dict()

        return d


class ShipStation(ShipStationBase):
    """
    Handles the details of connecting to and querying a ShipStation account.
    """

    ORDER_LIST_PARAMETERS = (
        "customer_name",
        "item_keyword",
        "create_date_start",
        "create_date_end",
        "modify_date_start",
        "modify_date_end",
        "order_date_start",
        "order_date_end",
        "order_number",
        "order_status",
        "payment_date_start",
        "payment_date_end",
        "store_id",
        "sort_by",
        "sort_dir",
        "page",
        "page_size",
    )

    def __init__(self):
        """
        Connecting to ShipStation required an account and a
        :return:
        """

        debug = False
        config = ShipStationConfig.get_solo()
        key = config.api_key
        secret = config.api_secret

        if not key:
            raise AttributeError("Key must be supplied.")
        if not secret:
            raise AttributeError("Secret must be supplied.")

        # mock server
        # self.url = "https://private-anon-6b500e6034-shipstation.apiary-mock.com"

        # debugging proxy
        # self.url = "https://private-anon-48d74acde5-shipstation.apiary-proxy.com"

        # production
        self.url = "https://ssapi.shipstation.com"

        self.key = key
        self.secret = secret
        self.orders = []

        self.debug = debug

    # @shipstation_logger
    def add_order(self, order):
        """
        добавление заказа к списку
        :param order: заказ для добавления
        """
        # Log.message(request, "Webhook: get request. {}".format(data))
        # print("order has been added")
        if type(order) is not ShipStationOrder:
            raise AttributeError("Should be type ShipStationOrder")
        self.orders.append(order)

    def get_orders(self):
        """
        :return: возвращает заказы
        """
        return self.orders

    def submit_orders(self):
        """
        подтверждаем заказы на shipStation
        :return:
        """
        # print("all added orders has been submited!")
        for order in self.orders:
            r = self.post(endpoint="/orders/createorder",
                          data=json.dumps(order.as_dict()))

            # print(r.text)
            try:
                return json.loads(r.text)
            except Exception:
                raise Exception(str(r))

    @cached('self', 'endpoint', 'payload', time=2 * 60 * 60)
    def get(self, endpoint="", payload=None):
        # print('shipstation get')
        url = f"{self.url}{endpoint}"
        try:
            r = requests.get(url, auth=(self.key, self.secret), params=payload, timeout=(2, 7))
        except Timeout as e:
            raise e
        if r.status_code in range(401, 505):
            # print('raise for status')
            r.raise_for_status()
        if self.debug:
            pprint.PrettyPrinter(indent=4).pprint(r.json())
        try:
            return json.loads(r.text)
        except Exception:
            return r.text

    # @cached('self', 'endpoint', 'data', time=2 * 60 * 60)
    def post(self, endpoint="", data=None):
        url = "{}{}".format(self.url, endpoint)
        headers = {"content-type": "application/json"}
        # print(endpoint, data)
        r = requests.post(url, auth=(self.key, self.secret),
                              data=data, headers=headers, timeout=(2, 5))

        # print("post: ", r.json())
        if self.debug:
            pprint.PrettyPrinter(indent=4).pprint(r.json())

        return r

    def delete(self, endpoint="", data=None):
        url = f"{self.url}{endpoint}"
        headers = {"content-type": "application/json"}
        r = requests.delete(url, auth=(self.key, self.secret),
                            data=data, headers=headers)

        # print("post: ", r.json())
        if self.debug:
            pprint.PrettyPrinter(indent=4).pprint(r.json())

        return r

    def get_carriers(self):
        """Получаем список курьерских служб, привязанных к аккаунту ShipStation клиента"""

        # print('shipstation get_carriers')
        carrier_codes = self.fetch_orders(endpoint='/carriers')
        # print(carrier_codes)
        if isinstance(carrier_codes, list):
            # print([(code['code'], code['name']) for code in carrier_codes])
            return [(code['code'], code['name']) for code in carrier_codes]
        else:
            # print(carrier_codes)
            return carrier_codes

    def get_services(self, carrier_code):
        """ Получаем сервисы курьерской службы.
                carrier_code == stamps_com."""

        # print('shipstation get_services')
        services = self.fetch_orders(endpoint='/carriers/listservices', parameters={
            'carrierCode': carrier_code
        })
        # print(services)
        if isinstance(services, list):
            # print([(code['code'], code['name']) for code in services])
            return [(code['code'], code['name']) for code in services]
        else:
            # print(services)
            return services

    def get_packages(self, carrier_code):
        """ Получаем упаковки курьерской службы."""

        # print('shipstation get_services')
        packages = self.fetch_orders(endpoint='/carriers/listpackages', parameters={
            'carrierCode': carrier_code
        })
        # print(packages)
        if isinstance(packages, list):
            # print([(code['code'], code['name']) for code in packages])
            return [(code['code'], code['name']) for code in packages]
        else:
            # print(packages)
            return packages

    def get_subservices(self, carrier_code, from_postal_code, to_postal_code, to_city, to_state, to_country, weight):
        """ Получаем подсервисы сервисов и стоимость доставки.
        Проверялось для carrier_code == stamps_com.
        У остальных курьерских служб может не быть подсервисов."""

        shipstation_weight = ShipStationWeight()
        shipstation_weight.set_units(units=weight[1])
        shipstation_weight.set_value(value=weight[0])

        # print('api get_subservices')
        parameters = {
            'carrierCode': carrier_code,
            'fromPostalCode': from_postal_code,
            'weight': shipstation_weight.as_dict(),
            'toCountry': to_country,
            'toPostalCode': to_postal_code,
            'toState': to_state,
            'toCity': to_city,
        }
        r = self.post(endpoint='/shipments/getrates', data=json.dumps(parameters))
        subservices = json.loads(r.text)
        if isinstance(subservices, list):
            return [(carrier_code+'__'+code['serviceCode']+'__'+code['serviceName']+'__'+str(code['shipmentCost']), code['serviceName'], Valute(str(code['shipmentCost'])), None) for code in subservices]
        else:
            return subservices

    def get_shipping_cost(self, carrier_code, flat_rate, service_code=None, from_postal_code=None, to_postal_code=None, state=None, city=None, country=None, weight=None, service_name=None):
        """
        Получаем стоимость доставки
        :return:
        """
        # print(carrier_code, service_code, flat_rate, postal_code, state, city, weight, service_name)
        try:
            shipstation_weight = ShipStationWeight()
            shipstation_weight.set_units(units=weight[1])
            shipstation_weight.set_value(value=weight[0])
            if carrier_code != 'auto_choice':
                if to_postal_code and state and city:
                    parameters = {
                        'carrierCode': carrier_code,
                        'serviceCode': service_code,
                        'fromPostalCode': from_postal_code,
                        'weight': shipstation_weight.as_dict(),
                        'toCountry': "US",
                        'toPostalCode': to_postal_code,
                        'toState': state,
                        'toCity': city,
                    }
                else:
                    parameters = {
                        'carrierCode': carrier_code,
                        # 'serviceCode': service_code,
                        'fromPostalCode': from_postal_code,
                        'weight': weight.as_dict(),
                        'toCountry': "US",
                        'toPostalCode': "803041",
                        'toState': "CO",
                        'toCity': "Boulder",
                    }
                shippings = json.loads(self.post(endpoint='/shipments/getrates', data=json.dumps(parameters)).text)
                shipping_cost = None
                for shipping in shippings:
                    if shipping['serviceCode'] == service_code and shipping['serviceName'] == service_name:
                        shipping_cost = float(shipping['shipmentCost']) + float(shipping['otherCost'])
            elif carrier_code == 'auto_choice':
                # get all carriers
                carriers = self.get_carriers()
                # get rates
                minimal_rate = (str(flat_rate).replace('$', ''), 'flat_rate', 'flat_rate', 'flat_rate')
                for carrier in carriers:
                    parameters = {
                        'carrierCode': carrier[0],
                        'fromPostalCode': from_postal_code,
                        'weight': shipstation_weight.as_dict(),
                        'toCountry': "US",
                        'toPostalCode': to_postal_code,
                        'toState': state,
                        'toCity': city,
                    }
                    shippings = json.loads(self.post(endpoint='/shipments/getrates', data=json.dumps(parameters)).text)
                    # print('shippings: ', shippings)
                    try:
                        # print(min(shippings, key=lambda x: x['shipmentCost']))
                        for shipping in shippings:
                            shipping_cost = float(shipping['shipmentCost']) + float(shipping['otherCost'])
                            if shipping_cost < (float(minimal_rate[0])):
                                minimal_rate = (shipping_cost, carrier[0], shipping['serviceCode'], shipping['serviceName'])
                        # calculated_min_rate = min(shippings, key=lambda x: x['shipmentCost'])
                        # minimal_rate = (calculated_min_rate['shipmentCost'], carrier[0], calculated_min_rate['serviceCode'], calculated_min_rate['serviceName'])
                    except:
                        pass
                return minimal_rate
            else:
                raise Exception('Exception while getting rates. No acceptable choices.')
        except ValueError as err:
            # print('ERROR: ', err)
            # print('except')
            if flat_rate:
                shipping_cost = flat_rate
            else:
                shipping_cost = "6.99"
        if not shipping_cost:
            # print('NOT SHIPPING!')
            shipping_cost = "6.99"
        # print('shipping cost: ', shipping_cost)
        return shipping_cost

    def create_label_for_order(self, order_id, carrier_code, service_code, ship_date, confirmation=None,
                               weight=None, dimensions=None, insurance_options=None, international_options=None,
                               advanced_options=None, test_label=False):
        """ Создаёт отгрузочный талон(чек, этикетку) для заказа.
            Возвращает:
                shipmentId
                shipmentCost
                insuranceCost
                trackingNumber - номер трека
                labelData - pdf-файл в кодировке base64
                formData
        """

        parameters = {
            'orderId': order_id,
            'carrierCode': carrier_code,
            'serviceCode': service_code,
            'confirmation': confirmation,
            'shipDate': ship_date.isoformat(),
            'weight': weight,
            'dimensions': dimensions,
            'insuranceOption': insurance_options,
            'internationalOptions': international_options,
            'advancedOptions': advanced_options,
            'testLabel': test_label,
        }

        shipping = json.loads(self.post(endpoint='/orders/createlabelfororder', data=json.dumps(parameters)).text)
        return shipping

    def mark_order_shipped(self, order_id, carrier_code, ship_date=None, tracking_number=None, notify_customer=True,
                           notify_sales_channel=False):
        """ Помечает заказ как 'отправленный/Shipped'.
        При этом не создаётся отгрузочный талон!
        :returns new orderId, orderNumber"""

        parameters = {
            'orderId': order_id,
            'carrierCode': carrier_code,
            'shipDate': ship_date.isoformat(),
            'trackingNumber': tracking_number,
            'notifyCustomer': notify_customer,
            'notifySalesChannel': notify_sales_channel,
        }

        shipping = json.loads(self.post(endpoint='/orders/markasshipped', data=json.dumps(parameters)).text)
        return shipping

    def get_order(self, order_id):
        """ Возвращает заказ c ShipStation. """
        endpoint='/orders/'+str(order_id)
        shipping = self.fetch_orders(endpoint=endpoint, )
        return shipping

    def delete_order(self, order_id):
        """ Удаляет заказ из пользовательского интерфейса ShipStation.
        Это «мягкое» удаление, поэтому заказ в базе данных ShipStation все еще будет существовать,
        но будет помечен как неактивный. """
        endpoint = '/orders/' + str(order_id)
        shipping = json.loads(self.delete(endpoint=endpoint))
        return shipping

    def fetch_orders(self, endpoint, parameters={}):
        # print('api fetch_orders')
        """
            Query, fetch, and return existing orders from ShipStation
            Args:
                endpoint (str): url Path
                parameters (dict): Dict of filters to filter by.
            Raises:
                AttributeError: parameters not of type dict
                AttributeError: invalid key in parameters dict.
            Returns:
                A <Response [code]> object.
            Examples:
                # >>> ss.fetch_orders(parameters={'order_status': 'shipped', 'page': '2'})
        """

        if not isinstance(endpoint, str):
            raise AttributeError("`endpoint` must be of type str")

        if not isinstance(parameters, dict):
            raise AttributeError("`parameters` must be of type dict")

        # invalid_keys = set(parameters.keys()).difference(
        #     self.ORDER_LIST_PARAMETERS)

        # if invalid_keys:
        #     raise AttributeError(
        #         "Invalid order list parameters: {}".format(
        #             ", ".join(invalid_keys))
        #     )

        valid_parameters = {
            self.to_camel_case(key): value for key, value in parameters.items()
        }

        return self.get(
            endpoint=endpoint,
            payload=valid_parameters
        )

    def get_shipments(self):
        """ Возвращает заказ c ShipStation. """
        # print('api get_shipments')
        shipments = {}
        page = 1
        pages = 2
        try:
            while pages >= page:
                # print(pages)
                # print(page)
                shipping = self.fetch_orders(endpoint='/shipments/', parameters={'page': page})
                # shipping = self.fetch_orders(endpoint='/shipments/')
                # print('shipping', shipping)

                for shipment in shipping['shipments']:
                    # print(shipment['orderNumber'], shipment['orderId'], shipment['trackingNumber'])
                    if shipment['trackingNumber']:
                        shipments.update({shipment['orderNumber']: shipment['trackingNumber']})
                pages = shipping['pages']
                page += 1
        except Exception:
            shipments = {}

        return shipments

    def get_changed_shipments(self, url):
        """ Возвращает отправления c ShipStation. """
        shipments = {}
        page = 1
        pages = 2
        try:
            while pages >= page:
                r = requests.get(url, auth=(self.key, self.secret), params={'page': page}, timeout=(2, 5))
                if r.status_code > 199 and r.status_code < 300:
                    r = json.loads(r.text)
                    for shipment in r.get('shipments', []):
                        if shipment.get('trackingNumber') and shipment.get('orderNumber'):
                            shipments.update({shipment.get('orderNumber'): shipment.get('trackingNumber')})
                else:
                    return shipments
                pages = r.get('pages', 0)
                page += 1
        except Exception:
            return shipments

        return shipments

    def get_changed_orders(self, url):
        """ Возвращает заказы c ShipStation. """
        orders = {}
        page = 1
        pages = 2
        try:
            while pages >= page:
                r = requests.get(url, auth=(self.key, self.secret), params={'page': page}, timeout=(2, 5))
                if 199 < r.status_code < 300:
                    r = json.loads(r.text)
                    for order in r.get('orders', []):
                        # print(order)
                        if order.get('orderStatus') and order.get('orderNumber'):
                            orders.update({order.get('orderNumber'): order.get('orderStatus')})
                else:
                    return orders
                pages = r.get('pages', 0)
                page += 1
        except Exception:
            return orders

        return order
from django.utils.timezone import now
from .api import (
    ShipStation, ShipStationOrder, ShipStationAddress,
    ShipStationItem, ShipStationOption, ShipStationWeight
)
from .models import ShipStationConfig, ShipStationOrderDetails
from libs.cache.cached import cached
from libs.get_city import get_city

RESOURCE_TYPES = ('SHIP_NOTIFY', 'ITEM_SHIP_NOTIFY', 'ORDER_NOTIFY', 'ITEM_ORDER_NOTIFY')


def _log_errors(errors):
    return '\n'.join(
        f"{key}: {', '.join(errors_list)}"
        for key, errors_list in errors.items()
    )


@cached('shipping')
def get_carrier_choices(shipping):
    carriers = shipping.get_carriers()

    return carriers if isinstance(carriers, list) else []


@cached('cart', 'shipping', 'carrier_code', 'from_postal_code', 'to_postal_code', 'to_city', 'to_country')
def get_subservice_choices(cart, shipping, carrier_code, from_postal_code, to_postal_code, to_city, to_country):
    city = get_city(to_city, to_country)
    if not city:
        return None
    subservices = shipping.get_subservices(
        carrier_code=carrier_code,
        from_postal_code=from_postal_code,
        to_postal_code=to_postal_code,
        to_city=to_city,
        to_state=city.get('state_code'),
        to_country=city.get('country_code'),
        weight=cart.get_total_weight,
    )
    if isinstance(subservices, list):
        return subservices
    else:
        raise Exception(subservices['ExceptionMessage'])

@cached('cart', 'country', 'city', 'postal_code')
def get_services_from_ShipStation(cart, country, city, postal_code):
    try:
        shipping = ShipStation()
    except AttributeError:
        shipping = []

    choices = []
    config = ShipStationConfig.get_solo()

    if config.flat_rate_options == config.FLAT_RATE_CHOICES[1][0]:
        if config.flat_rate == 0:
            choices.append(('stamps_com__usps_first_class_mail__Free shipping__0', 'Free shipping', config.flat_rate, None))
        else:
            shipping_cost = config.flat_rate
            choices.append((f'stamps_com__usps_first_class_mail__Free shipping__{shipping_cost.as_decimal()}', 'Special offer', shipping_cost, None))
        return choices

    # add flat rate
    if config.flat_rate_options == config.FLAT_RATE_CHOICES[2][0]:
        if config.flat_rate == 0:
            choices.append(('stamps_com__usps_first_class_mail__Free shipping__0', 'Free shipping', config.flat_rate, None))
        else:
            shipping_cost = config.flat_rate
            choices.append((f'stamps_com__usps_first_class_mail__Free shipping__{shipping_cost.as_decimal()}',
                            'Special offer', shipping_cost, None))

    # add shipstation services
    if config.flat_rate_options == config.FLAT_RATE_CHOICES[0][0] or config.flat_rate_options == config.FLAT_RATE_CHOICES[2][0]:
        from_postal_code = config.postal_code
        if not from_postal_code:
            try:
                from_postal_code = shipping.fetch_orders(endpoint='/warehouses')[0]['originAddress']['postalCode']

            except IndexError as e:
                raise e

        carriers = get_carrier_choices(shipping)

        for carrier in carriers:
            try:

                carrier_choices = get_subservice_choices(
                    cart,
                    shipping=shipping,
                    carrier_code=carrier[0],
                    from_postal_code=from_postal_code,
                    to_postal_code=postal_code,
                    to_country=country,
                    to_city=city
                )
                for choice in carrier_choices:
                    choices.append(choice)
            except Exception as e:
                if choices:
                    pass
                else:
                    raise e
    return choices

@cached('cart', time=0.5 * 60 * 60)
def use_free_shipping(cart):
    config = ShipStationConfig.get_solo()

    return 0 < config.free_shipping_from <= cart.products_cost


def send_to_shipstation(status='on_hold', order=None):
    try:
        shipstation = ShipStation()
        new_order = ShipStationOrder(order_number=str(order.pk))
        new_order.set_order_date(order.created)
        if order.pay_date:
            new_order.set_payment_date(order.date)
        elif order.is_paid:
            new_order.set_payment_date(now())
        new_order.set_ship_date(now())
        new_order.set_status(status=status)

        order_address_obj = ShipStationAddress(
            name=order.full_name,
            street1=order.address_line_1,
            street2=order.address_line_2,
            city=order.city,
            state=order.address_object.get('state_code'),
            postal_code=order.zip_code,
            country=order.address_object.get('country_code'),
            phone=order.phone
        )

        # получение данных о покупателе
        bill_to = order_address_obj
        ship_to = order_address_obj

        new_order.set_billing_address(billing_address=bill_to)
        new_order.set_shipping_address(shipping_address=ship_to)

        new_order.set_customer_details(
            email=order.email,
            username=order.full_name,
        )
        new_order.customer_notes = getattr(order, 'order_notes', None)
        # получение данных о продуктах, весах, количествах.
        for record in order.records.all():
            item = ShipStationItem(
                name=record.item.title,
                quantity=record.count,
                unit_price=record.order_price.as_decimal(),
                sku=record.item.sku or '',
            )
            if record.item.dimension:
                dimension = record.item.dimension
                dimension = f"{dimension.length} x {dimension.width} x {dimension.height} {dimension.units}"
                new_option = ShipStationOption(
                    name='dimensions', value=dimension,
                )
                item.add_option(new_option)

            if record.item.weight:
                weight = ShipStationWeight()
                weight.set_value(record.item.weight.value)
                weight.set_units(record.item.weight.units)
                item.set_weight(weight)
                weight_option = f"{record.item.weight.value} {record.item.weight.units}"
                new_option = ShipStationOption(
                    name='weight', value=weight_option,
                )
                item.add_option(new_option)
            new_order.add_item(item=item)

        new_order.amount_paid = order.total_cost.as_decimal()
        new_order.tax_amount = order.tax.as_decimal()


        shipstation.add_order(new_order)
        submitted_order = shipstation.submit_orders()
        ShipStationOrderDetails.objects.filter(order_id=order.id).delete()
        ShipStationOrderDetails(
            order=order,
            carrier_code='flat_rate',
            service_code='flat_rate',
            sub_service_code='No data',
            shipping_cost=ShipStationConfig.get_solo().flat_rate,
            shipstation_order_id=submitted_order.get('orderId'),
            shipstation_order_number=submitted_order.get('orderNumber')
        ).save()

    except Exception as e:
        raise e

from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.timezone import now
from django.core.validators import MinValueValidator, MaxValueValidator
from ipware.ip import get_ip
from urllib.parse import unquote_plus
from solo.models import SingletonModel
from libs.valute_field.fields import ValuteField


class ShipStationConfig(SingletonModel):
    WEIGHT_POUNDS = 'lb'
    WEIGHT_OUNCES = 'oz'
    WEIGHT_GRAMS = 'gr'
    WEIGHT_CHOICES = (
        (WEIGHT_POUNDS, _('pounds')),
        (WEIGHT_OUNCES, _('ounces')),
        (WEIGHT_GRAMS, _('grams')),
    )
    FLAT_RATE_CHOICES = (
        ('error', _("Use only data from ShipStation")),
        ('flat_rate_only', _('Use only flat rate for shipping prices')),
        ('both', _('Use flat rate and ShipStation rates both, to get customer choice')),
    )

    api_key = models.CharField(_('Public Key'), max_length=150)
    api_secret = models.CharField(_('Secret Key'), max_length=150)

    flat_rate = ValuteField(_('flat rate'), validators=[MinValueValidator(0.00), MaxValueValidator(999)],
                            help_text=_("Lets you charge a fixed rate for shipping, if can't get data from ShipStation"),
                            default=0)
    flat_rate_options = models.CharField(_('flat rate options'), max_length=15, choices=FLAT_RATE_CHOICES, default=FLAT_RATE_CHOICES[0][0])
    weight_unit = models.CharField(_('weight unit'), max_length=6, choices=WEIGHT_CHOICES, default=WEIGHT_CHOICES[1][0], help_text="weight unit to use in ShipStation")

    postal_code = models.CharField(_('postal code'), max_length=32, blank=True, help_text='postal code of departure point')

    free_shipping_from = ValuteField(_('free shipping from'),
                                     validators=[MinValueValidator(0.00), MaxValueValidator(99999)])

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Config')

    def __str__(self):
        return ugettext("ShipStation Settings")


# model for storing shipping data of order
class ShipStationOrderDetails(models.Model):
    order = models.OneToOneField(
        'shop.ShopOrder', on_delete=models.CASCADE,
        related_name='shipstation_details',
    )
    shipstation_order_number = models.CharField(_('order number'), max_length=64, editable=False, null=True)
    shipstation_order_id = models.CharField(_('order Id'), max_length=64, editable=False, null=True)
    shipstation_tracking_number = models.CharField(_('track number'), max_length=64, null=True)
    shipping_cost = ValuteField(_('shipping cost'))
    carrier_code = models.CharField(_('carrier code'), max_length=64, null=True, blank=True)
    service_code = models.CharField(_('service code'), max_length=64, null=True, blank=True)
    sub_service_code = models.CharField(_('subservice code'), max_length=64, null=True, blank=True)

    def __str__(self):
        return str(self.shipping_cost)


class Log(models.Model):
    STATUS_INFO = 1
    STATUS_SUCCESS = 2
    STATUS_ERROR = 3
    STATUS_EXCEPTION = 4
    STATUSES = (
        (STATUS_INFO, _('Info')),
        (STATUS_SUCCESS, _('Success')),
        (STATUS_ERROR, _('Error')),
        (STATUS_EXCEPTION, _('Exception')),
    )

    inv_id = models.CharField(_('invoice'), max_length=255, blank=True, null=True)
    status = models.PositiveSmallIntegerField(_('status'), choices=STATUSES)
    msg_body = models.TextField(_('message'))
    request_get = models.TextField(_('GET'))
    request_post = models.TextField(_('POST'))
    request_ip = models.GenericIPAddressField(_('IP'))
    created = models.DateTimeField(_('create date'), default=now, editable=False)

    class Meta:
        default_permissions = ('delete', )
        verbose_name = _('log message')
        verbose_name_plural = _('log messages')
        ordering = ('-created', )

    def __str__(self):
        status = dict(self.STATUSES).get(self.status)
        return f'[{status}] {self.msg_body}'

    @classmethod
    def create(cls, request, body, inv_id=None, status=None):
        return cls.objects.create(
            inv_id=inv_id,
            status=status,
            msg_body=body,
            request_get=unquote_plus(request.GET.urlencode()),
            request_post=unquote_plus(request.POST.urlencode()),
            request_ip=get_ip(request),
        )

    @classmethod
    def message(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_INFO)

    @classmethod
    def success(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_SUCCESS)

    @classmethod
    def error(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_ERROR)

    @classmethod
    def exception(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_EXCEPTION)


class Dimension(models.Model):
    DIMENSIONS_INCHES = 'in'
    DIMENSIONS_CENTIMETERS = 'cm'
    DIMENSIONS_CHOICES = (
        (DIMENSIONS_INCHES, _('inches')),
        (DIMENSIONS_CENTIMETERS, _('centimeters')),
    )
    length = models.FloatField(_('length'), validators=[MinValueValidator(0.001), ], default=1)
    width = models.FloatField(_('width'), validators=[MinValueValidator(0.001), ], default=1)
    height = models.FloatField(_('height'), validators=[MinValueValidator(0.001), ], default=1)
    units = models.CharField(_('unit'), max_length=25, choices=DIMENSIONS_CHOICES, default=DIMENSIONS_CHOICES[0][0])
    order = models.PositiveIntegerField(default=1)

    @staticmethod
    def format_dimension(value):
        return str(value).rstrip('0').rstrip('.')

    def __str__(self):
        return f"{self.format_dimension(self.length)} x " \
               f"{self.format_dimension(self.width)} x " \
               f"{self.format_dimension(self.height)} " \
               f"{self.units}"

    class Meta:
        verbose_name = _("Dimension")
        verbose_name_plural = _("Dimensions")
        ordering = ('order',)


class Weight(models.Model):
    WEIGHT_POUNDS = 'lb'
    WEIGHT_OUNCES = 'oz'
    WEIGHT_GRAMS = 'gr'
    WEIGHT_CHOICES = (
        (WEIGHT_POUNDS, _('pounds')),
        (WEIGHT_OUNCES, _('ounces')),
        (WEIGHT_GRAMS, _('grams')),
    )
    value = models.FloatField(_('value'), validators=[MinValueValidator(0.001), ], default=1)
    units = models.CharField(_('unit'), max_length=25, choices=WEIGHT_CHOICES, default=WEIGHT_CHOICES[1][0])

    order = models.PositiveIntegerField(default=1)

    def to_pounds(self):
        # conversion to pounds
        if self.units == self.WEIGHT_CHOICES[0][0]:
            return self.value
        elif self.units == self.WEIGHT_CHOICES[1][0]:
            return self.value / 16
        elif self.units == self.WEIGHT_CHOICES[2][0]:
            return self.value / 453.59237
        else:
            raise Exception(f"Can't convert {self.value} {self.units} to pounds")

    def to_ounces(self):
        # conversion to ounces
        if self.units == self.WEIGHT_CHOICES[1][0]:
            return self.value
        elif self.units == self.WEIGHT_CHOICES[0][0]:
            return self.value * 16
        elif self.units == self.WEIGHT_CHOICES[2][0]:
            return self.value / 28.34952
        else:
            raise Exception(f"Can't convert {self.value} {self.units} to ounces")

    def to_grams(self):
        # conversion to grams
        if self.units == self.WEIGHT_CHOICES[2][0]:
            return self.value
        elif self.units == self.WEIGHT_CHOICES[0][0]:
            return self.value * 453.59237
        elif self.units == self.WEIGHT_CHOICES[1][0]:
            return self.value * 28.34952
        else:
            raise Exception(f"Can't convert {self.value} {self.units} to ounces")

    @staticmethod
    def format_weight(value):
        return str(value).rstrip('0').rstrip('.')

    def __str__(self):
        return f"{self.format_weight(self.value)} {self.units}"

    class Meta:
        verbose_name = _("Weight")
        verbose_name_plural = _("Weights")
        unique_together = ('value', 'units')
        ordering = ('order', )

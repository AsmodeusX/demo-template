from django.conf.urls import url
from . import webhooks


app_name = 'shipstation'
urlpatterns = [
    url(r'^webhook/$', webhooks.webhook, name='webhook'),
]

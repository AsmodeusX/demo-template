from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .api import ShipStation
from .models import Log
from .signals import shipstation_status_changed, shipstation_track_add
import json

RESOURCE_TYPES = ('SHIP_NOTIFY', 'ITEM_SHIP_NOTIFY', 'ORDER_NOTIFY', 'ITEM_ORDER_NOTIFY')


def _log_errors(errors):
    return '\n'.join(
        f"{key}: {', '.join(errors_list)}"
        for key, errors_list in errors.items()
    )


def _parse_webhook(request):
    """
        Валидация хука
    """
    try:
        data = json.loads(request.body.decode())
        Log.message(request, f"Webhook: get request. {data}")
    except Exception as e:
        Log.error(request, f"Webhook: can't parse data of request. {request}")
        return

    resource_type = data.get('resource_type')
    if resource_type:
        if resource_type in RESOURCE_TYPES:
            resource_url = data.get('resource_url')
            if resource_url:
                return data
            else:
                Log.error(request, 'Webhook: no resource url')
                return
        else:
            Log.error(request, f'Webhook: unsupported resource type {resource_type}')
            return
    else:
        Log.error(request, 'Webhook: no resource type')
        return

@csrf_exempt
def webhook(request):
    data = _parse_webhook(request)

    if data is None:
        return HttpResponse()

    api = ShipStation()
    if data.get('resource_type') in RESOURCE_TYPES[:2]:
        shipments = api.get_changed_shipments(data.get('resource_url'))
        for transaction, tracking_num in shipments.items():
            Log.message(request, f'Webhook: tracking number {tracking_num} created for order {transaction}')
            callback = shipstation_track_add.send(
                sender=Log,
                request=request,
                transaction=transaction,
                tracking_number=tracking_num,
            )
            if isinstance(callback, Exception):
                Log.exception(request, f"Transaction: {callback.__class__.__name__}", inv_id=transaction)
    else:
        orders = api.get_changed_orders(data.get('resource_url'))
        for transaction, status in orders.items():
            Log.message(request, f'Webhook: status changed to {status} for order {transaction}')
            callback = shipstation_status_changed.send(
                sender=Log,
                request=request,
                transaction=transaction,
                status=status,
            )

            if isinstance(callback, Exception):
                Log.exception(request, f"Transaction: {callback.__class__.__name__}({', '.join(callback.args)})",
                              inv_id=transaction)

    return HttpResponse()


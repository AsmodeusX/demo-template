from django.dispatch import Signal


shipstation_track_add = Signal(providing_args=[
    'request', 'transaction', 'tracking_number'
])

shipstation_status_changed = Signal(providing_args=[
    'request', 'transaction', 'status'
])
from libs import jinja2
from ..api import get_static_map_url, get_external_map_url
from ..models import geocode_cached


@jinja2.extension
class GoogleMapsExtension(jinja2.Extension):
    tags = {'google_map_static', 'google_map_external'}

    def _google_map_static(self, address, zoom=None, width=None, height=None):
        """
            Возвращает URL картинки с картой.
            Можно применять к объекту класса MapAndAddress, к строке с адресом
            или к экземпляру GoogleCoords

            Параметры:
                уровень детализации, ширина, высота - через запятую.

            Пример:
                <img src="{% google_map_static address zoom=15 width=300 height=200 %}">
        """
        if not address:
            return ''

        lng, lat = geocode_cached(address)
        return get_static_map_url(lng, lat, zoom=zoom, width=width, height=height)

    def _google_map_external(self, address, zoom=14):
        """
            Возвращает URL карты в сервисе Google.
            Можно применять к объекту класса MapAndAddress, к строке с адресом
            или к экземпляру GoogleCoords.

            Принимает необязательный параметр: уровень детализации.

            Пример:
                <a href="{% google_map_external address zoom=15 %}" target="_blank">Show Map</a>
        """
        if not address:
            return ''

        lng, lat = geocode_cached(address)
        return get_external_map_url(lng, lat, zoom)

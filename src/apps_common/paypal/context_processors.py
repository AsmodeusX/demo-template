from .models import PaypalConfig


def config_paypal(request):
    config = PaypalConfig.get_solo()

    return {
        'PAYPAL_CLIENT_ID': config.client_id,
    }

from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import PaypalConfig
from project.admin import ModelAdminMixin


@admin.register(PaypalConfig)
class PaypalConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        ('Keys', {
            'fields': (
                'client_id', 'client_secret',
            ),
        }),
    )

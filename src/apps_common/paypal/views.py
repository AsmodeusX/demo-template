from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from shop.models import ShopOrder
import json


@csrf_exempt
def check_capture(request):
    response = json.loads(request.body)
    status = response['resource']['status']
    order_id = response['resource']['supplementary_data']['related_ids']['order_id']
    order = ShopOrder.objects.filter(tmp_id=order_id)

    if status == 'COMPLETED':
        order.update(
            is_paid = True,
        )
    elif status == 'DENIED' or status == 'REFUNDED' or status == 'REVERSED':
        order.update(
            is_paid=False,
            is_cancelled = True,
        )

    return HttpResponse(content='', status=200)
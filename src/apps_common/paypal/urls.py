from django.conf.urls import url
from . import views


app_name = 'paypal'
urlpatterns = [
    url(r'^check/capture/$', views.check_capture, name='result'),
]

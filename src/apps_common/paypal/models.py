from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel


class PaypalConfig(SingletonModel):
    client_id = models.CharField(_('client id'), max_length=128)
    client_secret = models.CharField(_('client secret key'), max_length=128)

    class Meta:
        verbose_name = ugettext('Paypal config')

    def __str__(self):
        return ugettext('Paypal config')


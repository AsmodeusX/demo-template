from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import TaxJarConfig
from project.admin import ModelAdminMixin


@admin.register(TaxJarConfig)
class TaxJarConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'token',
            ),
        }),
    )

from shop.models import ShopConfig
from .models import TaxJarConfig
import taxjar
from libs.get_city import get_city


def get_order_tax(shipping_cost=None, order_cost=None, products=None, from_addr=None, to_addr=None,):
    shop_config = ShopConfig.get_solo()
    config = TaxJarConfig.get_solo()
    client = taxjar.Client(api_key=config.token)

    if from_addr is None:
        from_addr_data = get_city(shop_config.city, shop_config.country)
        from_addr = {
            'country': from_addr_data.get('country_code'),
            'zip': shop_config.zip,
            'state': from_addr_data.get('state_code'),
            'city': shop_config.city,
        }

    tax_order = {
        'amount': order_cost,
        'shipping': shipping_cost,
        'line_items': products,
    }
    tax_order.update({'from_%s' % key: val for key, val in from_addr.items()})
    tax_order.update({'to_%s' % key: val for key, val in to_addr.items()})
    try:
        # При заведомо неверном индексе (11111) возвращает 0, а не ошибку)
        return client.tax_for_order(tax_order)['amount_to_collect']
    except (
            taxjar.exceptions.TaxJarResponseError,
            taxjar.exceptions.TaxJarTypeError,
            taxjar.exceptions.TaxJarConnectionError,
            taxjar.exceptions.TaxJarError) as e:
        return 0

        # raise TaxJarResponseError(e.full_response['response']['detail'])

from django.conf import settings
from .models import TaxJarConfig

import taxjar


class TaxJarAPI:
    CONFIG = None
    token = None

    def __init__(self):
        self.CONFIG = TaxJarConfig.get_solo()
        self.token = self.CONFIG.token

    @staticmethod
    def get_error_message(e):
        return e.full_response['error']['detail'] if isinstance(e, dict) else str(e)

    def format_error_message(self, e):
        return {
            'error': self.get_error_message(e)
        }

    def send(self, data):
        client = taxjar.Client(
            api_key=self.token,
            api_url=taxjar.DEFAULT_API_URL if not settings.DEBUG else taxjar.SANDBOX_API_URL
        )

        try:
            transaction = client.create_order(data)
            # print(transaction)
            return transaction

        except (taxjar.exceptions.TaxJarResponseError,
                taxjar.exceptions.TaxJarConnectionError,
                taxjar.exceptions.TaxJarTypeError) as _:
            # print(e.full_response)
            return False

        except Exception as _:
            # print(_)
            return False

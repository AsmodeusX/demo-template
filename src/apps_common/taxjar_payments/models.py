from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel


class TaxJarConfig(SingletonModel):
    token = models.CharField(_('token'), max_length=128, )

    class Meta:
        verbose_name = ugettext('TaxJar config')

    def __str__(self):
        return ugettext('TaxJar config')

from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel


class AuthorizeConfig(SingletonModel):
    login_id = models.CharField(_('api login id'), max_length=128)
    transaction_key = models.CharField(_('transaction key'), max_length=128)
    key = models.CharField(_('key'), max_length=128)

    class Meta:
        verbose_name = ugettext('Authorize.Net config')

    def __str__(self):
        return ugettext('Authorize.Net config')

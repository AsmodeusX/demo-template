from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import AuthorizeConfig
from project.admin import ModelAdminMixin


@admin.register(AuthorizeConfig)
class AuthorizeConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        ('Credentials', {
            'fields': (
                'login_id', 'transaction_key', 'key',
            ),
        }),
    )

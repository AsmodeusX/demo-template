from .models import AuthorizeConfig
from authorizenet import apicontractsv1
from authorizenet.apicontrollers import createTransactionController
from shop.cart import Cart
from shop.models import ShopOrder
from libs.get_city import get_city
from pyxb.exceptions_ import *
from datetime import date


class AuthorizeAPI:
    CONFIG = None
    keys = None
    MERCHANT_AUTH = None

    def __init__(self):
        self.CONFIG = AuthorizeConfig.get_solo()
        self.keys = {
            'login_id': self.CONFIG.login_id,
            'transaction_key': self.CONFIG.transaction_key,
            'key': self.CONFIG.key,
        }

        self._auth()

    @staticmethod
    def get_error_message(e):
        if isinstance(e, dict):
            return e.full_response['error']['detail']
        elif isinstance(e, str):
            return e
        else:
            return str(e)

    def format_error_message(self, e):
        err = self.get_error_message(e)

        return {
            'error': err,
        }

    @staticmethod
    def _get_uuid(request):
        cart = Cart(request)
        return cart.get_order

    def _auth(self):
        merchant_auth = apicontractsv1.merchantAuthenticationType()
        merchant_auth.name = self.keys['login_id']
        merchant_auth.transactionKey = self.keys['transaction_key']
        self.MERCHANT_AUTH = merchant_auth

    def _create_customer(self, first_name, last_name, email, order_id):
        create_customer_profile = apicontractsv1.createCustomerProfileRequest()
        create_customer_profile.merchantAuthentication = self.MERCHANT_AUTH
        create_customer_profile.profile = apicontractsv1.customerProfileType(
            f'{first_name}_{last_name}_order_#{order_id}',
            f'{first_name} {last_name}',
            email)

        controller = apicontractsv1.createCustomerProfileController(create_customer_profile)
        controller.execute()

        response = controller.getresponse()

        # if response.messages.resultCode == "Ok":
        #     print("Successfully created a customer profile with id: %s" % response.customerProfileId)
        # else:
        #     print("Failed to create customer payment profile %s" % response.messages.message[0]['text'].text)

        return response

    def _create_credit_card(self, card_number, exp_date, cvv):
        card_number = card_number.replace(' ', '')
        date_parts = exp_date.split('/')
        date_month = date_parts[0]
        date_year = date_parts[1]

        try:
            credit_card = apicontractsv1.creditCardType()
            credit_card.cardCode = cvv
            credit_card.cardNumber = card_number
            credit_card.expirationDate = f"{str(date.today().year)[:2]}{date_year}-{date_month}"
        except SimpleFacetValueError as e:
            return self.format_error_message(e)

        return credit_card

    def _create_payment(self, credit_card):
        # Add the payment data to a paymentType object
        payment = apicontractsv1.paymentType()
        try:
            payment.creditCard = credit_card
        except SimpleTypeValueError as e:
            return self.format_error_message(e)

        return payment

    @staticmethod
    def _create_order(order_id):
        # Create order information
        order = apicontractsv1.orderType()
        order.invoiceNumber = str(order_id)
        order.description = f"Order #{order_id}"

        return order

    @staticmethod
    def _create_customer_address(first_name, last_name, address, city, state, country, zip_code, company_name=None):
        # Set the customer's Bill To address
        customer_address = apicontractsv1.customerAddressType()
        customer_address.firstName = first_name
        customer_address.lastName = last_name
        customer_address.address = address[:50]
        customer_address.city = city
        customer_address.state = state
        customer_address.country = country
        customer_address.zip = zip_code

        if company_name:
            customer_address.company = company_name

        return customer_address

    @staticmethod
    def _create_customer_data(customer_id, email, customer_type="individual"):
        # Set the customer's identifying information
        customer_data = apicontractsv1.customerDataType()
        customer_data.type = customer_type
        customer_data.id = str(customer_id)
        customer_data.email = email

        return customer_data

    @staticmethod
    def _create_array_line_items(line_items):
        # build the array of line items
        array_line_items = apicontractsv1.ArrayOfLineItem()

        for line_item in line_items:
            array_line_items.append(line_item)

        return array_line_items

    def _create_line_items(self, request):
        order_uuid=self._get_uuid(request)

        order = ShopOrder.objects.get(uuid=order_uuid)
        order_records = order.records.all()
        line_items = []

        for order_record in order_records:
            # setup individual line items
            line_item = apicontractsv1.lineItemType()
            line_item.itemId = str(order_record.product.id)
            line_item.name = order_record.title[:30]
            line_item.quantity = str(order_record.count)
            line_item.unitPrice = str(order_record.order_price.as_decimal())

            line_items.append(line_item)

        return self._create_array_line_items(line_items)

    def _create_transaction_request(self, request, transaction_request):
        order_uuid = self._get_uuid(request)
        order = ShopOrder.objects.get(uuid=order_uuid)

        # Assemble the complete transaction request
        call_transaction_request = apicontractsv1.createTransactionRequest()
        call_transaction_request.merchantAuthentication = self.MERCHANT_AUTH
        call_transaction_request.refId = f"Order #{order.id}"
        call_transaction_request.transactionRequest = transaction_request

        return call_transaction_request

    def _transaction_request(self, request, amount, payment, order, customer_address, customer_data, settings, line_items):
        # Create a transactionRequestType object and add the previous objects to it.
        transaction_request = apicontractsv1.transactionRequestType()
        transaction_request.transactionType = "authCaptureTransaction"

        try:
            transaction_request.amount = amount.as_decimal()
            transaction_request.payment = payment
            transaction_request.order = order
            transaction_request.billTo = customer_address
            transaction_request.customer = customer_data
            transaction_request.transactionSettings = settings
            transaction_request.lineItems = line_items
        except SimpleTypeValueError as e:
            return self.format_error_message(e)

        return self._create_transaction_request(request, transaction_request)


    def payment(self, request, card, amount):
        order_uuid = self._get_uuid(request)
        pay_order = ShopOrder.objects.get(uuid=order_uuid)

        # Create the payment data for a credit card
        credit_card = self._create_credit_card(
            card_number=card['card_number'],
            exp_date=card['date_expired'],
            cvv=card['cvv'],
        )

        payment = self._create_payment(credit_card)
        order = self._create_order(pay_order.id)

        customer_address = self._create_customer_address(
            first_name=pay_order.first_name,
            last_name=pay_order.last_name,
            address=pay_order.full_address,
            city=pay_order.city.__str__(),
            state=get_city(pay_order.city.__str__(), pay_order.city.country.__str__()).get('state'),
            country=pay_order.city.country.__str__(),
            zip_code=pay_order.zip_code,
        )

        # Set the customer's identifying information
        customer_data = self._create_customer_data(pay_order.id, pay_order.email)
        line_items = self._create_line_items(request)

        settings = apicontractsv1.ArrayOfSetting()

        transaction_request = self._transaction_request(
            request,
            amount,
            payment,
            order,
            customer_address,
            customer_data,
            settings,
            line_items,
        )

        # Create the controller
        transaction_controller = createTransactionController(transaction_request)
        try:
            transaction_controller.execute()
        except AttributeError as e:
            return self.format_error_message(e)

        response = transaction_controller.getresponse()

        if response is not None:
            # print("isMSG:", getattr(response.transactionResponse, 'messages'))
            if response.messages.resultCode == "Ok":
                if hasattr(response.transactionResponse, 'messages') is True:
                    return response
                else:
                    if hasattr(response.transactionResponse, 'errors') is True:
                        e = response.transactionResponse.errors.error[0].errorText
                        return self.format_error_message(e)
            else:
                if hasattr(response, 'transactionResponse') is True and hasattr(
                        response.transactionResponse, 'errors') is True:
                    e = response.transactionResponse.errors.error[0].errorText
                else:
                    e = response.messages.message[0]['text'].text
                return self.format_error_message(e)

        return None
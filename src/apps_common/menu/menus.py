from django.utils.translation import ugettext_lazy as _
from .base import Menu, MenuItem
from about.models import AboutConfig
from album.models import AlbumConfig
from blog.models import BlogConfig, Post
from contacts.models import ContactsConfig
from faq.models import FAQConfig
from services.models import Service, ServicesConfig
from shop.models import ShopConfig, ShopCategory


def _get_enabled_modules():
    album_config = AlbumConfig.get_solo()
    faq = FAQConfig.get_solo()

    use_about = AboutConfig.get_solo().enable
    use_album = album_config.enable and album_config.gallery
    use_blog = BlogConfig.get_solo().enable and Post.objects.visible().exists()
    use_contacts = ContactsConfig.get_solo().enable
    use_faq = faq.enable and faq.questions.exists()
    use_services = ServicesConfig.get_solo().enable and Service.objects.visible()
    use_shop = ShopConfig.get_solo().enable and ShopCategory.objects.filter(is_visible=True, products__is_visible=True).exists()

    return {
        'ABOUT': use_about,
        'ALBUM': use_album,
        'BLOG': use_blog,
        'CONTACTS': use_contacts,
        'FAQ': use_faq,
        'SERVICES': use_services,
        'SHOP': use_shop,
    }


def menu_mobile(request):
    menu = Menu()
    prefix = 'mobile'

    used_modules = _get_enabled_modules()

    if used_modules['SERVICES']:
        services = Service.objects.root_services()
        list_items = []

        for service in services:
            subservices = service.subservices

            if not subservices.exists():
                item = MenuItem(
                    title=service.title,
                    url=service.get_absolute_url(),
                    item_id='service_%s_%s' % (prefix, service.id),
                )
            else:
                item = MenuItem(
                    title=service.title,
                    url=None,
                    item_id='service_%s_%s' % (prefix, service.id),
                    attrs={
                        'class': 'submenu-parent' if subservices.exists() else ''
                    }
                )

                item_link = MenuItem(
                    title=_("Overview"),
                    url=service.get_absolute_url(),
                    # item_id='service_%s_%s_link' % (prefix, service.id),
                )

                item.append(item_link)

                for subservice in subservices:
                    subservice_item = MenuItem(
                        title=subservice.title,
                        url=subservice.get_absolute_url(),
                        item_id='service_%s_%s' % (prefix, subservice.id)
                    )
                    item.append(subservice_item)
            list_items.append(item)

            menu.append(item)

    if used_modules['ABOUT']:
        menu.append(
            MenuItem(
                title=_('About Us'),
                url='about:index',
            ),
        )
    if used_modules['ALBUM']:
        menu.append(
            MenuItem(
                title=_('Gallery'),
                url='album:index',
            ),
        )
    if used_modules['BLOG']:
        menu.append(
            MenuItem(
                title=_('Blog'),
                url='blog:index',
            ),
        )
    if used_modules.get('SHOP'):
        menu.append(
            MenuItem(
                title=_('Catalog'),
                url='shop:index',
            ),
        )
    if used_modules['FAQ']:
        menu.append(
            MenuItem(
                title=_('FAQ'),
                url='faq:index',
            ),
        )
    if used_modules['CONTACTS']:
        menu.append(
            MenuItem(
                title=_('Contact Us'),
                url='contacts:index',
                item_id='contacts_%s' % prefix
            ),
        )

    return menu


def menu_header(request):

    menu = Menu()
    prefix = 'header'
    list_items = []
    used_modules = _get_enabled_modules()

    if used_modules['SERVICES']:

        services = Service.objects.root_services()

        for service in services:
            subservices = service.subservices.all()
            item = MenuItem(
                title=service.title,
                url=service.get_absolute_url(),
                item_id='service_%s_%s' % (prefix, service.id),
                attrs={
                    'class': 'submenu-parent' if subservices.exists() else ''
                }
            )

            if subservices.exists():
                for subservice in subservices:
                    subservice_item = MenuItem(
                        title=subservice.title,
                        url=subservice.get_absolute_url(),
                        item_id='service_%s_%s' % (prefix, subservice.id)
                    )
                    item.append(subservice_item)
            list_items.append(item)

        services_item = MenuItem(
            title=_('Services'),
            item_id='services',
            url=None,
            attrs={
                'class': 'submenu-parent'
            }
        )
        for item in list_items:
            services_item.append(item)

        menu.append(services_item)

    if used_modules['ABOUT']:
        menu.append(
            MenuItem(
                title=_('About Us'),
                url='about:index',
            ),
        )
    if used_modules['ALBUM']:
        menu.append(
            MenuItem(
                title=_('Gallery'),
                url='album:index',
            ),
        )
    if used_modules['BLOG']:
        menu.append(
            MenuItem(
                title=_('Blog'),
                url='blog:index',
            ),
        )
    if used_modules.get('SHOP'):
        menu.append(
            MenuItem(
                title=_('Catalog'),
                url='shop:index',
            ),
        )
    if used_modules['FAQ']:
        menu.append(
            MenuItem(
                title=_('FAQ'),
                url='faq:index',
            ),
        )
    if used_modules['CONTACTS']:
        menu.append(
            MenuItem(
                title=_('Contact Us'),
                url='contacts:index',
                item_id='contacts_%s' % prefix
            ),
        )

    return menu


def menu_footer(request):
    menu = Menu()
    prefix = 'footer'

    used_modules = _get_enabled_modules()

    if used_modules['SERVICES']:
        services = Service.objects.root_services()

        for service in services:
            menu.append(
                MenuItem(
                    title=service.title,
                    url=service.get_absolute_url(),
                ),
            )

    if used_modules['ABOUT']:
        menu.append(
            MenuItem(
                title=_('About Us'),
                url='about:index',
            ),
        )
    if used_modules['ALBUM']:
        menu.append(
            MenuItem(
                title=_('Gallery'),
                url='album:index',
            ),
        )
    if used_modules['BLOG']:
        menu.append(
            MenuItem(
                title=_('Blog'),
                url='blog:index',
            ),
        )
    if used_modules.get('SHOP'):
        menu.append(
            MenuItem(
                title=_('Catalog'),
                url='shop:index',
            ),
        )
    if used_modules['FAQ']:
        menu.append(
            MenuItem(
                title=_('FAQ'),
                url='faq:index',
            ),
        )
    if used_modules['CONTACTS']:
        menu.append(
            MenuItem(
                title=_('Contact Us'),
                url='contacts:index',
                item_id='contacts_%s' % prefix
            ),
        )

    return menu


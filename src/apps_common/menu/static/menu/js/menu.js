(function($) {
    var doit;

    $(window).on('load', function () {

        var $innerMenus = $('.menu--header .menu__item > .menu-inner .menu'),
            $nextInnerMenus = $innerMenus.find('.menu-inner');

        var mh = $innerMenus.eq(0).innerHeight(),
            mt = $innerMenus.eq(0).offset().top;

        $nextInnerMenus.each(function () {
            var $this = $(this);
            $this.css({
                minHeight: mh + 'px',
                top: -($this.offset().top - mt + 20) + 'px'
            });

            document.documentElement.style.setProperty('--mh', mh + 'px');
        });

        setMob();
    });

    function setMob() {
        var $mobileMenu = $('.menu--mobile'),
            $toggleButtons = $mobileMenu.find('.submenu-parent');

        $mobileMenu.find('.menu-inner').slideToggle();

        $toggleButtons.on('click', function (e) {
            e.preventDefault();
            var $toggleButton = $(e.currentTarget),
                $item = $toggleButton.closest('.general-item'),
                $innerMenu = $item.find('.menu-inner');
            $innerMenu.slideToggle();
            $toggleButton.find('.menu__toggle').toggleClass('opened');
            setTimeout(function () {
                window.CheckMenu();
            }, 700);
        });

        clearTimeout(doit);
    }
})(jQuery);

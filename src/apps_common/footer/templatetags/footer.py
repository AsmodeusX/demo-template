from django.template import loader
from django.utils.timezone import datetime
from django.conf import settings
from libs import jinja2
from policy.models import PolicyConfig
from disclaimer.models import DisclaimerConfig


@jinja2.extension
class FooterExtension(jinja2.Extension):
    tags = {'footer'}
    takes_context = True

    @staticmethod
    def _footer(context, template='footer/footer.html'):
        str_copyright = "{year} {project}".format(year=datetime.now().year, project=settings.PROJECT_NAME)

        return loader.render_to_string(template, {
            'disclaimer': DisclaimerConfig.get_solo().enable,
            'policy': PolicyConfig.get_solo().enable,
            'str_copyright': str_copyright,
        }, request=context.get('request'))

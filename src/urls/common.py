from django.contrib import admin
from django.conf.urls import include, url
from django.shortcuts import Http404
from django.views.i18n import JavaScriptCatalog
from django.contrib.sitemaps.views import sitemap
from project.sitemaps import site_sitemaps
from libs.autoslug import ALIAS_REGEXP


def root_router(request, slug=None):
    from services.models import Service
    from services.views import ServiceView

    services = Service.objects.filter(slug=slug)

    if services.exists():
        return ServiceView.as_view()(request, slug=slug)

    else:
        raise Http404


urlpatterns = [
    url(r'^admin/', include('admin_honeypot.urls')),
    url(r'^dladmin/social/', include('social_networks.admin_urls')),
    url(r'^dladmin/autocomplete/', include('libs.autocomplete.urls')),
    url(r'^dladmin/ckeditor/', include('ckeditor.admin_urls')),
    url(r'^dladmin/gallery/', include('gallery.admin_urls')),
    url(r'^dladmin/users/', include('users.admin_urls')),
    url(r'^dladmin/ctr/', include('admin_ctr.urls')),
    url(r'^dladmin/', include(admin.site.urls)),

    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='jsi18n'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': site_sitemaps}),

    url(r'^ajax/', include('ajax_views.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),

    url(r'^sentry/', include('libs.sentry.urls')),
    url(r'^paypal/', include('paypal.urls')),
    url(r'^shipstation/', include('shipstation.urls')),
    url(r'^constant-contact/', include('constant_contact.urls')),

    url(r'', include('main.urls', namespace='main')),
    url(r'^about/', include('about.urls', namespace='about')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^disclaimer/', include('disclaimer.urls', namespace='disclaimer')),
    url(r'^faq/', include('faq.urls', namespace='faq')),
    url(r'^gallery/', include('album.urls', namespace='album')),
    url(r'^contact/', include('contacts.urls')),
    url(r'^privacy-policy/', include('policy.urls', namespace='policy')),
    url(r'^shop/', include('shop.urls', namespace='shop')),
    url(r'^', include('services.urls', namespace='services')),
    url(r'^(?P<slug>{})/$'.format(ALIAS_REGEXP), root_router, name='root_router'),
]
